/**
 * Created by yichongmiao on 14/12/2017.
 */
$(document).ready(function () {
    $("#sell").click(function (e) {
        e.preventDefault();
        document.getElementById('load').style.visibility = "visible";
        $.ajax({
            type: "POST",
            url: "/api/a2bliving/ProductsStock/sellOrNotFeedback",
            data: {
                id_product: $(this).val(),
                ifSell: true
            },
            success: function (result) {
                handlerData(result)
            },
            error: function (result) {
                document.getElementById('load').style.visibility = "hidden";
                alert('error');
            }
        });
    });
    $("#delete").click(function (e) {
        document.getElementById('load').style.visibility = "visible";
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/api/a2bliving/ProductsStock/sellOrNotFeedback",
            data: {
                id_product: $(this).val(),
                ifSell: false
            },
            success: function (result) {
                handlerData(result)

            },
            error: function (result) {
                document.getElementById('load').style.visibility = "hidden";
                alert('error');
            }
        });
    });
})


function handlerData(result) {
    if (result === "null") {
        $('.container').html(" <h1>No More Product</h1>");
        document.getElementById('load').style.visibility = "hidden";
        return
    }
    var resulData = JSON.parse(result);
    console.log(result);
    $('#product_name').text(resulData.name);
    $('#product_ean').text(resulData.ean13);
    if (resulData.zone != null && +resulData.part != null && resulData.position_number != null) {
        $('#product_location').text(resulData.zone + "-" + resulData.part + resulData.position_number);
    }
    $('#sell').val(resulData.id_product);
    $('#delete').val(resulData.id_product);
    $("#product_image").attr('src', 'http://www.a2bliving.ie/cork/' + resulData.id_image + '-tm_thickbox_default/' + resulData.link_rewrite + '.jpg');
    document.getElementById('load').style.visibility = "hidden";

}