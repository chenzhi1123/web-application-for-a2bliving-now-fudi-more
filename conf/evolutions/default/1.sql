#--- Created by Ebean DDL
#To stop Ebean DDL generation, remove this comment and start using Evolutions

#--- !Ups

create table ps_customer (
  id                        integer not null,
  id_shop                   integer,
  id_shop_group             integer,
  secure_key                varchar(255),
  note                      varchar(255),
  id_gender                 integer,
  id_default_group          integer,
  id_lang                   integer,
  lastname                  varchar(255),
  firstname                 varchar(255),
  birthday                  varchar(255),
  email                     varchar(255),
  newsletter                boolean,
  ip_registration_newsletter varchar(255),
  newsletter_date_add       varchar(255),
  optin                     boolean,
  website                   varchar(255),
  company                   varchar(255),
  siret                     varchar(255),
  ape                       varchar(255),
  outstanding_allow_amount  float,
  show_public_prices        integer,
  id_risk                   integer,
  max_payment_days          integer,
  passwd                    integer,
  last_passwd_gen           varchar(255),
  active                    boolean,
  is_guest                  boolean,
  deleted                   boolean,
  date_add                  varchar(255),
  date_upd                  varchar(255),
  years                     varchar(255),
  days                      varchar(255),
  months                    varchar(255),
  geoloc_id_country         integer,
  geoloc_id_state           integer,
  geoloc_postcode           varchar(255),
  logged                    boolean,
  id_guest                  integer,
  group_box                 integer,
  constraint pk_ps_customer primary key (id))
;

create table product (
  product_id                bigint not null,
  name                      varchar(255),
  information               varchar(255),
  constraint pk_product primary key (product_id))
;

create table my_user (
  id                        bigint not null,
  firstname                 varchar(255),
  lastname                  varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  nationality               varchar(255),
  constraint pk_my_user primary key (id))
;

create sequence ps_customer_seq;

create sequence product_seq;

create sequence my_user_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists ps_customer;

drop table if exists product;

drop table if exists my_user;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists ps_customer_seq;

drop sequence if exists product_seq;

drop sequence if exists my_user_seq;

