/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import play.db.ebean.Model;

import java.math.BigDecimal;

/**
 * Created by Zhi Chen on 2016/10/17.
 */
public class OrderProduct extends Model {

    public int product_id;
    public int product_attribute_id;
    public String product_name;
    public int product_quantity = 0;
    public java.math.BigDecimal product_price = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal reduction_percent = BigDecimal.valueOf(0.00);
    public java.math.BigDecimal reduction_amount = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal reduction_amount_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal reduction_amount_tax_excl = BigDecimal.valueOf(0.000000);
    public String product_upc;
    public String product_reference;
    public int id_tax_rules_group = 0;
    public java.math.BigDecimal total_price_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_price_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal unit_price_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal unit_price_tax_excl = BigDecimal.valueOf(0.000000);
    public int id_tax;
    public java.math.BigDecimal tax_unit_amount = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal tax_total_amount = BigDecimal.valueOf(0.000000);

    public OrderProduct()
    {

    }

    public OrderProduct(int product_id, int product_attribute_id, String product_name, int product_quantity, BigDecimal product_price, BigDecimal reduction_percent, BigDecimal reduction_amount, BigDecimal reduction_amount_tax_incl, BigDecimal reduction_amount_tax_excl, String product_upc, String product_reference, int id_tax_rules_group, BigDecimal total_price_tax_incl, BigDecimal total_price_tax_excl, BigDecimal unit_price_tax_incl, BigDecimal unit_price_tax_excl, int id_tax, BigDecimal tax_unit_amount, BigDecimal tax_total_amount) {
        this.product_id = product_id;
        this.product_attribute_id = product_attribute_id;
        this.product_name = product_name;
        this.product_quantity = product_quantity;
        this.product_price = product_price;
        this.reduction_percent = reduction_percent;
        this.reduction_amount = reduction_amount;
        this.reduction_amount_tax_incl = reduction_amount_tax_incl;
        this.reduction_amount_tax_excl = reduction_amount_tax_excl;
        this.product_upc = product_upc;
        this.product_reference = product_reference;
        this.id_tax_rules_group = id_tax_rules_group;
        this.total_price_tax_incl = total_price_tax_incl;
        this.total_price_tax_excl = total_price_tax_excl;
        this.unit_price_tax_incl = unit_price_tax_incl;
        this.unit_price_tax_excl = unit_price_tax_excl;
        this.id_tax = id_tax;
        this.tax_unit_amount = tax_unit_amount;
        this.tax_total_amount = tax_total_amount;
    }

    @Override
    public String toString() {
        return "OrderProduct{" +
                "product_id=" + product_id +
                ", product_attribute_id=" + product_attribute_id +
                ", product_name='" + product_name + '\'' +
                ", product_quantity=" + product_quantity +
                ", product_price=" + product_price +
                ", reduction_percent=" + reduction_percent +
                ", reduction_amount=" + reduction_amount +
                ", reduction_amount_tax_incl=" + reduction_amount_tax_incl +
                ", reduction_amount_tax_excl=" + reduction_amount_tax_excl +
                ", product_upc='" + product_upc + '\'' +
                ", product_reference='" + product_reference + '\'' +
                ", id_tax_rules_group=" + id_tax_rules_group +
                ", total_price_tax_incl=" + total_price_tax_incl +
                ", total_price_tax_excl=" + total_price_tax_excl +
                ", unit_price_tax_incl=" + unit_price_tax_incl +
                ", unit_price_tax_excl=" + unit_price_tax_excl +
                ", id_tax=" + id_tax +
                ", tax_unit_amount=" + tax_unit_amount +
                ", tax_total_amount=" + tax_total_amount +
                '}';
    }
}
