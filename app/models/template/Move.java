package models.template;

/**
 * A move on the map.
 * @author ndeverge
 *
 */
public class Move {
	/**
	 * The id of the vehicle.
	 */
	public int driver_id;  //key 名称很重要
	/**
	 * A timestamp.
	 */
	public long timestamp;
	public float longitude;
	public float latitude;
	public Move(int driver_id, long timestamp, float longitude, float latitude) {
		this.driver_id = driver_id;
		this.timestamp = timestamp;
		this.longitude = longitude;
		this.latitude = latitude;
	}
	public Move(){}
	@Override
	public String toString() {
		return "Move{" +
				"driver_id=" + driver_id +
				", timestamp=" + timestamp +
				", longitude=" + longitude +
				", latitude=" + latitude +
				'}';
	}
}
