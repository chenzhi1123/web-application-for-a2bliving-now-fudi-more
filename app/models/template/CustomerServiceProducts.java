/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

/**
 * This is a class for encapsulate information related to address, and create an object within ReturnOrder class attributes
 * @see com.avaje.ebean.bean.EntityBean
 * @see ReturnOrder
 * */
public class CustomerServiceProducts{
    public String product_name;
    public Integer quantity;

    public CustomerServiceProducts()
    {}

    public CustomerServiceProducts(String product_name, Integer quantity) {
        this.product_name = product_name;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "CustomerServiceProducts{" +
                "product_name='" + product_name + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
