/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import models.sqlContainer.ReturnDriverOrders;

import java.util.List;

/**
 * Created by 20060 on 2017/5/17.
 */
public class TotalPrintWithDriverOrders {
    public List<TotalPrintOrder> totalPrintOrders;
    public List<ReturnDriverOrders> returnDriverOrderses;

    public TotalPrintWithDriverOrders(List<TotalPrintOrder> totalPrintOrders, List<ReturnDriverOrders> returnDriverOrderses) {
        this.totalPrintOrders = totalPrintOrders;
        this.returnDriverOrderses = returnDriverOrderses;
    }

    public TotalPrintWithDriverOrders() {
    }

    @Override
    public String toString() {
        return "TotalPrintWithDriverOrders{" +
                "totalPrintOrders=" + totalPrintOrders +
                ", returnDriverOrderses=" + returnDriverOrderses +
                '}';
    }
}
