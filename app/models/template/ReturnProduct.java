/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import java.math.BigDecimal;

public class ReturnProduct {
    public int id_order_detail;
    public  String product_upc;
    public  String product_name;
    public  String product_reference;
    public int product_quantity;
    public java.math.BigDecimal total_price_tax_incl = BigDecimal.valueOf(0.000000);

    public ReturnProduct(int id_order_detail, String product_upc, String product_name, String product_reference, int product_quantity, BigDecimal total_price_tax_incl) {
        this.id_order_detail = id_order_detail;
        this.product_upc = product_upc;
        this.product_name = product_name;
        this.product_reference = product_reference;
        this.product_quantity = product_quantity;
        this.total_price_tax_incl = total_price_tax_incl;
    }

    public ReturnProduct() {

    }


    @Override
    public String toString() {
        return "ReturnProduct{" +
                "id_order_detail=" + id_order_detail +
                ", product_upc='" + product_upc + '\'' +
                ", product_name='" + product_name + '\'' +
                ", product_reference='" + product_reference + '\'' +
                ", product_quantity=" + product_quantity +
                ", total_price_tax_incl=" + total_price_tax_incl +
                '}';
    }
}
