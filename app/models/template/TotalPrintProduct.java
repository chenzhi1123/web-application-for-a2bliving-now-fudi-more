/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

import java.math.BigDecimal;

public class TotalPrintProduct {
    public int id_product;
    public int id_order_detail;
    public String product_upc;
    public String product_name;
    public String product_reference;     //new
    public String product_location;     // more new
    public int product_quantity;
    public java.math.BigDecimal total_price_tax_incl = BigDecimal.valueOf(0.000000);

    public TotalPrintProduct() {

    }

    public TotalPrintProduct(int id_product, int id_order_detail, String product_upc, String product_name, String product_reference, String product_location, int product_quantity, BigDecimal total_price_tax_incl) {
        this.id_product = id_product;
        this.id_order_detail = id_order_detail;
        this.product_upc = product_upc;
        this.product_name = product_name;
        this.product_reference = product_reference;
        this.product_location = product_location;
        this.product_quantity = product_quantity;
        this.total_price_tax_incl = total_price_tax_incl;
    }

    @Override
    public String toString() {
        return "TotalPrintProduct{" +
                "id_product=" + id_product +
                ", id_order_detail=" + id_order_detail +
                ", product_upc='" + product_upc + '\'' +
                ", product_name='" + product_name + '\'' +
                ", product_reference='" + product_reference + '\'' +
                ", product_location='" + product_location + '\'' +
                ", product_quantity=" + product_quantity +
                ", total_price_tax_incl=" + total_price_tax_incl +
                '}';
    }
}
