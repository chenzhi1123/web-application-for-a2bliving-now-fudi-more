/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.template;

/**
 * Created by 20060 on 2017/4/10.
 */
public class DriverOrderWithInformation {
    public int id_order;
    public String order_reference;
    public int order_number;
    public String information = "";


    public DriverOrderWithInformation(){

    }
    public DriverOrderWithInformation(int id_order, String order_reference, int order_number, String information) {
        this.id_order = id_order;
        this.order_reference = order_reference;
        this.order_number = order_number;
        this.information = information;
    }

    @Override
    public String toString() {
        return "DriverOrderWithInformation{" +
                "id_order=" + id_order +
                ", order_reference='" + order_reference + '\'' +
                ", order_number=" + order_number +
                ", information='" + information + '\'' +
                '}';
    }
}
