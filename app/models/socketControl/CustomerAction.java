package models.socketControl;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.sqlContainer.SqlHelper;
import play.Logger;
import play.libs.Akka;
import play.libs.F;
import play.libs.Json;
import play.mvc.WebSocket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The moves on the map.
 *
 * @author Zhi Chen
 *
 */
public class CustomerAction extends UntypedActor {
	// Default map.
//	public static ActorRef actor = Akka.system().actorOf(new Props(MapAnime.class));
	public static ActorRef customer_action = Akka.system().actorOf(Props.create(CustomerAction.class)); //这是一个客户地图页面线程池

	/**
	 * The connected clients. 与服务器进行连接的浏览器客户端, 输出信道
	 */
	static Map<String, WebSocket.Out<JsonNode>> connected_customers = new HashMap<String, WebSocket.Out<JsonNode>>(); //顾客的传输信道 OUT
    static Map<Integer, String> customer_websocket = new HashMap<Integer, String>(); //customer_id --> Websocket_key
	public static Map<Integer, Integer> driver_customer = new HashMap<Integer, Integer>(); //key:driver_id --> value: customer_id， 一个司机一次只能让一个顾客看见
	/**
	 *
	 * @param customer_id
	 * @param order_id
	 * @param in
	 * @param out
	 * @throws Exception
	 */
	public static void customerConnect(final String unique_key,final int customer_id, final int order_id, final WebSocket.In<JsonNode> in, final WebSocket.Out<JsonNode> out) throws Exception {
        System.out.println("A new Customer Connected to Socket Successfully");
	    customer_action.tell(new CustomerJoin(unique_key,customer_id, order_id, out),null);
       	// For each event received on the socket,
		in.onMessage(new F.Callback<JsonNode>() {
			@Override
			public void invoke(JsonNode event) {
			 //顾客端口与这个通道通信，来自顾客发送的信息，包括订单和自身位置
                ObjectMapper mapper = new ObjectMapper();
                CustomerMove move = mapper.convertValue(event, CustomerMove.class);
                if (move != null) {
                    customer_action.tell(move, null);//传递顾客自己位置信息，并且通过广播返回, 暂时不会用到
                }
			}
		});
		// When the socket is closed.
		in.onClose(new F.Callback0() { //当socket 被关闭的时候 发送一个内部信息
			@Override
			public void invoke() {
                customer_action.tell(new CustomerQuit(customer_id,unique_key),null);
			}
		});
        ObjectNode event = Json.newObject();
        event.put("marker_type", "Driver");// marker 默认位置在A2bliving
        event.put("timestamp", 20160324);
        event.put("longitude", -8.449145);
        event.put("latitude", 51.899635);
        out.write(event);
	}

	public static void messageFromDriver(DriverMessage driverMessage) { //司机在自己端口发送信息
        customer_action.tell(driverMessage,null);
	}

	@Override
	public void onReceive(Object message) throws Exception { //Akka 的自带方法
		if (message instanceof CustomerJoin) { //顾客第一次登陆的时候，存入hashmap
			// Received a Join message
            CustomerJoin new_customer = (CustomerJoin) message;
            int order_id = new_customer.order_id;
            int id_customer = new_customer.customer_id;
            String channel_key = new_customer.unique_key;//以key 为准
            if(customer_websocket.containsKey(id_customer) && connected_customers.containsKey(channel_key) && customer_websocket.get(id_customer).equals(channel_key)){//同一个顾客，同一个端口
                System.out.println("the same customer is communicating");
            }else if(customer_websocket.containsKey(id_customer) && !connected_customers.containsKey(channel_key) && !customer_websocket.get(id_customer).equals(channel_key)){ //同一个顾客，但是不同一个端口
               //准备替换端口，但是要先关闭前一个端口
               String last_socket_key = customer_websocket.get(id_customer);
               connected_customers.get(last_socket_key).close(); //关闭端口
               connected_customers.remove(last_socket_key);//删除上一个端口
               //添加新的用户--> 端口信息
                customer_websocket.put(id_customer,channel_key);
                connected_customers.put(channel_key,new_customer.channel);
            }else {  //全新用户
                customer_websocket.put(id_customer,channel_key);
                connected_customers.put(channel_key,new_customer.channel);
            }
            //开始验证 是否有司机在配送这单 order_id --> driver_id
            if(!driver_customer.containsValue(id_customer)){ //在没有这个用户记录的情况下才进行后续操作
                String sql =
                        "SELECT id_driver\n" +
                                "from ps_drivers_orders\n" +
                                "where id_order = " + order_id +"\n" +
                                "and if_delivered = 0\n" +
                                "and start_time > '1970-01-01'";
                RawSql rawSql =
                        RawSqlBuilder
                                .parse(sql)
                                .columnMapping("id_driver", "int_value")
                                .create();
                com.avaje.ebean.Query<SqlHelper> query = Ebean.find(SqlHelper.class);
                query.setRawSql(rawSql);
                List<SqlHelper> list = query.findList();
                if(list.size()>0){  //存在记录，司机已经点击开始配送
                    int driver_id = list.get(0).int_value;// driver_id who is delivering this order
                    driver_customer.put(driver_id,id_customer); //添加司机--> 顾客记录
                }
            }
			Logger.info("New customer:" + new_customer.customer_id+ " has opened the map");
            System.out.println(connected_customers.size() + " customers are watching the map");
		} else if (message instanceof CustomerMove) { //顾客自己不断报点，更新自己位置
            System.out.println(connected_customers.size() + " customers are watching the map");
            // Received a Move message
            CustomerMove move = (CustomerMove) message;
            //开始验证 是否有司机在配送这单 order_id --> driver_id
            if(!driver_customer.containsValue(move.customer_id)){ //在没有这个用户记录的情况下才进行后续操作
                String sql =
                        "SELECT id_driver\n" +
                                "from ps_drivers_orders\n" +
                                "where id_order = " + move.customer_id +"\n" +
                                "and if_delivered = 0\n" +
                                "and start_time > '1970-01-01'";
                RawSql rawSql =
                        RawSqlBuilder
                                .parse(sql)
                                .columnMapping("id_driver", "int_value")
                                .create();
                com.avaje.ebean.Query<SqlHelper> query = Ebean.find(SqlHelper.class);
                query.setRawSql(rawSql);
                List<SqlHelper> list = query.findList();
                if(list.size()>0){  //存在记录，司机已经点击开始配送
                    int driver_id = list.get(0).int_value;// driver_id who is delivering this order
                    driver_customer.put(driver_id,move.customer_id); //添加司机--> 顾客记录
                }
            }
            //用多线程同时发送消息
            ObjectNode event = Json.newObject();
            event.put("marker_type", "Customer");//marker)id = 1, marker 是顾客
            event.put("timestamp", move.timestamp);
            event.put("longitude", move.longitude);
            event.put("latitude", move.latitude);
            WebSocket.Out<JsonNode> channel = connected_customers.get(customer_websocket.get(move.customer_id));
            channel.write(event);
            System.out.println("Customers: "+ move.customer_id + "is moving"); //查看有多少个端口在连接地图界面
		} else if (message instanceof DriverMessage) { //司机查找到自己有送单之后，给这个用户通道发送位置信息，更新自己位置
            System.out.println(connected_customers.size() + " customers are watching the map");
            // Received a Move message
            DriverMessage move = (DriverMessage) message;
            Integer customer_id = move.id_customer; //顾客 id 作为索引， 来找到相对的频道
            String web_socket = customer_websocket.get(customer_id);
            if(web_socket !=null && connected_customers.containsKey(web_socket)){
            //用多线程同时发送消息
            ObjectNode event = Json.newObject();
            event.put("marker_type","Driver");  //在顾客地图上，只有两种marker ID, Customer/Driver, marker 是司机
            event.put("timestamp", move.timestamp);
            event.put("longitude", move.longitude);
            event.put("latitude", move.latitude);
            WebSocket.Out<JsonNode> channel = connected_customers.get(web_socket);
            channel.write(event); //找到这个司机配送的顾客通道， 并发送信息
            System.out.println("Driver " + move.driver_id + " is moving");
            }
        }else if (message instanceof CustomerQuit) {
			// Received a Unregistration message
            CustomerQuit quit = (CustomerQuit) message;
			Logger.info("Customer: " + quit.customer_id + " has disconnected");
			//删除一切有关这个顾客的hashMap 条目, 如果还存在的话
            int customer_id = quit.customer_id;
            String socket_key = quit.socket_key;
            if(connected_customers.containsKey(socket_key) && customer_websocket.containsKey(customer_id) && customer_websocket.get(customer_id).equals(socket_key)){ //是同一个顾客主动关闭
                connected_customers.remove(customer_websocket.get(customer_id));//删除顾客 socket记录
                customer_websocket.remove(customer_id); //删除顾客--> socket_key 记录
                System.out.println("Customer closed the page by himself");
            }else{
                System.out.println("Someone Robbed the page");
            }
            System.out.println(connected_customers.size() + " customers are watching the map");
		} else {
			unhandled(message);
		}
	}

	public static class CustomerJoin {  //连接服务器
        public String unique_key;
		public int customer_id;
		public int order_id;
		public WebSocket.Out<JsonNode> channel;

		public CustomerJoin(String unique_key, int customer_id, int order_id,WebSocket.Out<JsonNode> channel) {
			super();
			this.unique_key = unique_key;
			this.customer_id = customer_id;
			this.order_id = order_id;
			this.channel = channel;
		}
	}

	public static class CustomerQuit {  //断开连接服务器
		public int customer_id;
		public String socket_key;

		public CustomerQuit(int customer_id, String socket_key) {
			super();
			this.customer_id = customer_id;
			this.socket_key = socket_key;
		}
	}

    public static class CustomerMove{  //顾客更改自己位置
        public int customer_id;

        public long timestamp;

        public float longitude;

        public float latitude;

        public CustomerMove(int customer_id, long timestamp, float longitude, float latitude){
            super();
            this.customer_id = customer_id;
            this.timestamp = timestamp;
            this.longitude = longitude;
            this.latitude = latitude;
        }
    }

    public static class DriverMessage{  //司机传递过来的信息，在顾客通道里传递给顾客的司机位置信息
        public int driver_id;

        public long timestamp;

        public float longitude;

        public float latitude;

        public int id_customer; //顾客id, 然后打开顾客频道

        public DriverMessage(int driver_id, long timestamp, float longitude, float latitude,int id_customer){
            super();
            this.driver_id = driver_id;
            this.timestamp = timestamp;
            this.longitude = longitude;
            this.latitude = latitude;
            this.id_customer = id_customer;
        }
    }

}
