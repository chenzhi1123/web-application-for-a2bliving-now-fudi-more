package models.socketControl;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.template.Move;
import play.Logger;
import play.libs.Akka;
import play.libs.F;
import play.libs.Json;
import play.mvc.WebSocket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static models.socketControl.CustomerAction.driver_customer;

/**
 * The moves on the map.
 *
 * @author ndeverge
 *
 */
public class DriverAction extends UntypedActor {
    // .
//	public static ActorRef actor = Akka.system().actorOf(new Props(MapAnime.class));
    public static ActorRef drivers_room = Akka.system().actorOf(Props.create(DriverAction.class)); //这是一个司机端口连接池
    /**
     * The connected clients. 与服务器进行连接的司机端,socket 输出信道
     */
    Map<String,Integer> connected_drivers = new HashMap<String,Integer>();  //key 对应 相应司机id
    Map<String, WebSocket.Out<JsonNode>> connected_driver_channels = new HashMap<String, WebSocket.Out<JsonNode>>(); //key 对应socket
    /**
     *
     *
     * @param in
     * @param out
     * @throws Exception
     */
    public static void driverRegister(final String unique_key,final WebSocket.In<JsonNode> in, final WebSocket.Out<JsonNode> out) throws Exception {
        // For each event received on the socket,
        drivers_room.tell(new DriverSocket(unique_key, out),null);//添加新司机输出端口
        in.onMessage(new F.Callback<JsonNode>() {
            public void invoke(JsonNode event) {
                // Log events to the console
                System.out.println(event);
                out.write(event);   ///(返回值)
                //在这里向地图发送更改坐标信息
                ObjectMapper mapper = new ObjectMapper();
                Move move = mapper.convertValue(event, Move.class);
                if (move != null) {
                    drivers_room.tell(new DriverJoin(unique_key, move.driver_id), null);//添加新工作司机id
//                        if (move.id != null) {
                    MapAnime.moveTo(move.driver_id, move.timestamp, move.longitude, move.latitude);//分配给另外一个线程来发送广播位置信息
                    //给顾客频道发送信息
                    if(driver_customer.containsKey(move.driver_id)){ //如果这个司机在送单
                        int customer_id = driver_customer.get(move.driver_id); //得到顾客ID
                        CustomerAction.messageFromDriver(new CustomerAction.DriverMessage(move.driver_id, move.timestamp, move.longitude, move.latitude,customer_id));
                    }
                }
//                        }
//
//                        MapAnime.moveTo(move.id, move.timestamp, move.longitude, move.latitude);
            }
        });
        // When the socket is closed.
        in.onClose(new F.Callback0() {
            public void invoke() {
                drivers_room.tell(new DriverQuit(unique_key), null);
            }
        });
        // Send a single 'Hello!' message
        ObjectNode event = Json.newObject();
        event.put("status", "success");
        out.write(event);  //成功连接端口的时候执行
        System.out.println("Connected to Socket Successfully");  //打开端口的时候显示信息
    }

//	//Nowhere to execute
//	public static void driverMove(int driver_id, long timestamp, float longitude, float latitude) {
//		drivers_room.tell(new MoveMessage(driver_id, timestamp, longitude, latitude),null);
//	}

    @Override
    public void onReceive(Object message) throws Exception { //Akka 的自带方法
        if (message instanceof DriverSocket) {  //添加 socket
            // Received a Join message
            DriverSocket registration = (DriverSocket) message;
            Logger.info("new driver connected with socket:--" + registration.unique_key);
            connected_driver_channels.put(registration.unique_key,registration.channel);
            System.out.println(connected_driver_channels.size()+" driver(s) connected to socket"); //查看有多少个司机端口在连接
        }
        else if (message instanceof DriverJoin) { //司机给socket 发送信息, 添加司机列表
            // Received a DriveJoin (含司机信息)
            DriverJoin driverJoin = (DriverJoin) message;
            String current_socket_key = driverJoin.unique_key; //当前司机的socket_out
            int current_driver = driverJoin.id_driver;
            Logger.info("Driver " + current_driver + " is sending message to socket");
            connected_drivers.put(current_socket_key,current_driver); //添加当前司机进入司机列表
            //根据逻辑判断，只为每个司机保留一个socket_out
            //根据司机ID，先找到上一个socket_out_key, 然后关闭，之后删除
            List<String> all_socket_key = getKeyList(connected_drivers,current_driver);
            if(all_socket_key.size() >1){  //存在有不止一个记录（有重复）
                String last_socket_key = "";
                for(String s : all_socket_key){
                    if(!s.equals(current_socket_key)){
                        last_socket_key = s;   //得到上一个socket_key
                    }
                }
                //准备删除记录
                connected_drivers.remove(last_socket_key);
                //关闭上一个socket_out 通道
                connected_driver_channels.get(last_socket_key).close();
                connected_driver_channels.remove(last_socket_key);
            }
            System.out.println(connected_drivers.size() + " drivers on the list"); //查看有多少个端口在连接地图界面
            System.out.println(connected_driver_channels.size() + " driver(s) are sending messages"); //查看有多少个司机端口在连接
        }
        else if (message instanceof DriverQuit){
            // Received a Unregistration message
            DriverQuit quit = (DriverQuit) message;
            String driver_key = quit.unique_key;
            //如果是这个司机自己关闭
            if(connected_driver_channels.containsKey(driver_key)){
                connected_driver_channels.remove(driver_key);
                System.out.println("Driver disconnected by himself");
                if(connected_drivers.get(driver_key) !=null){
                    int driver_id = connected_drivers.remove(driver_key);
                    connected_drivers.remove(driver_key);
                    System.out.println("Driver ID is:----" +driver_id );
                }
            }else { //端口被抢占，已经被删除过
                System.out.println("Driver Socket has been robbed");
            }
            Logger.info("Driver Socket was closed " + driver_key);
        } else {
            unhandled(message);
        }
    }



    public static class DriverSocket {  //司机连接服务器socket
        public String unique_key;
        public WebSocket.Out<JsonNode> channel;

        public DriverSocket(String unique_key,WebSocket.Out<JsonNode> channel) {
            super();
            this.unique_key = unique_key;
            this.channel = channel;
        }
    }

    public static class DriverJoin {  //司机开始传输信息
        public String unique_key;
        public int id_driver;

        public DriverJoin(String unique_key, int id_driver) {
            super();
            this.unique_key = unique_key;
            this.id_driver = id_driver;
        }
    }


    public static class DriverQuit {  //司机断开连接服务器socket
        public String unique_key;

        public DriverQuit(String unique_key) {
            super();
            this.unique_key = unique_key;
        }
    }

//	public static class Talk {  //连接服务器的管理员互相聊天
//
//		final String username;
//		final String text;
//
//		public Talk(String username, String text) {
//			this.username = username;
//			this.text = text;
//		}
//
//	}


    public  List<String> getKeyList(Map<String, Integer> map, Integer value){
        List<String> keyList = new ArrayList();
        for(String getKey: map.keySet()){
            if(map.get(getKey).equals(value)){
                keyList.add(getKey);
            }
        }
        return keyList;
    }

}
