package models.socketControl;

import akka.actor.ActorRef;
import play.libs.Akka;
import scala.concurrent.duration.Duration;

import static java.util.concurrent.TimeUnit.HOURS;

public class ServerMaintainer {

    public ServerMaintainer(ActorRef mapAnime) {  //参数是ActorRef, 可以看作为一个线程

//        // Create a Fake socket out for the robot that log events to the console.
//        WebSocket.Out<JsonNode> robotChannel = new WebSocket.Out<JsonNode>() {
//
//            public void write(JsonNode frame) {
//                Logger.of("robot").info(Json.stringify(frame));
//            }
//
//            public void close() {}
//
//        };

//        // Join the room
//        mapAnime.tell(new MapAnime.Join("Robot", robotChannel), null);
        // Make the robot talk every 30 seconds
        Akka.system().scheduler().schedule(  //隔一段时间发送请求
            Duration.create(12, HOURS),
            Duration.create(12, HOURS),
//            Duration.create(10, SECONDS),
//            Duration.create(10, SECONDS),
            mapAnime,
            new MapAnime.Clean("Time to Clean",1),
            Akka.system().dispatcher(),
            /** sender **/ null
        );

        Akka.system().scheduler().schedule(  //隔一段时间删除一次顾客的地图端口
                Duration.create(10, HOURS),
                Duration.create(10, HOURS),
//            Duration.create(10, SECONDS),
//            Duration.create(10, SECONDS),
                mapAnime,
                new MapAnime.CleanCustomer("A2bliving",1),
                Akka.system().dispatcher(),
                /** sender **/ null
        );

    }



}
