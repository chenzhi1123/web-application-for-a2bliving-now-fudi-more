package models.socketControl;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.SqlUpdate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.database.Driver;
import models.sqlContainer.DriverLocation;
import models.template.Move;
import models.sqlContainer.ReturnDriverOrders;
import play.Logger;
import play.libs.Akka;
import play.libs.F;
import play.libs.Json;
import play.mvc.WebSocket;

import java.util.*;

import static controllers.MapMoveAPI.getDriverOrders;
import static models.socketControl.CustomerAction.connected_customers;
import static models.socketControl.CustomerAction.customer_websocket;

/**
 * The moves on the map.
 *
 * @author ndeverge
 *
 */
public class MapAnime extends UntypedActor {
    // Default map.
//	public static ActorRef actor = Akka.system().actorOf(new Props(MapAnime.class));
    public static ActorRef actor = Akka.system().actorOf(Props.create(MapAnime.class)); //这是一个地图页面连接池

    /**
     * The connected clients. 与服务器进行连接的浏览器客户端, 输出信道
     */
    static Map<String, WebSocket.Out<JsonNode>> connected_clients = new HashMap<String, WebSocket.Out<JsonNode>>();
    static Map<Integer, String> driver_names;
    static
    {
        System.out.println("Used onece Maybe");
        driver_names = new HashMap<Integer, String>();
        driver_names.put(11, "Kenny Ren");
        driver_names.put(12, "Desong Ye");
        driver_names.put(13, "Eric");
        driver_names.put(14, "Erica");
        driver_names.put(17, "James");
        driver_names.put(18, "Alex");
        driver_names.put(19, "Sabbir");
        driver_names.put(22, "Mario");
        driver_names.put(24, "Liam");
        driver_names.put(26, "Mags");
        driver_names.put(27, "Khan");
        driver_names.put(29, "Kadir");
        driver_names.put(32, "Sam");
        driver_names.put(34, "Krystian");
        driver_names.put(35, "Kazi");
        driver_names.put(36, "Tricia");
        driver_names.put(37, "Mike");
        driver_names.put(38, "Froysal");
        driver_names.put(39, "Li Qi");
        driver_names.put(40, "Mark");
        driver_names.put(41, "Patrick");
        driver_names.put(42, "aldo");
        driver_names.put(43, "lee");
        driver_names.put(44, "LEEe");
        driver_names.put(45, "George");
    }
//	// Create a Robot, just for fun.
//	static {
//		new Robot(actor);
//	}

    static {
        new ServerMaintainer(actor); //持续维护服务器输出端口
    }
    /**
     *
     * @param id
     * @param in
     * @param out
     * @throws Exception
     */
    public static void register(final String id,	final WebSocket.In<JsonNode> in, final WebSocket.Out<JsonNode> out) throws Exception {
        // Send the Join message to the room
//        String result = (String) Await.result(ask(actor,new Join(username, out), 1000), Duration.create(1, SECONDS));
        actor.tell(new Join(id, out),null);
        System.out.println("New Browser Connected to Socket Successfully");
        List<DriverLocation> current_drivers = showAllDrivers();
        if(current_drivers.size()>0){  //有工作司机
            for(DriverLocation this_one: current_drivers){
                String driver_name = driver_names.get(this_one.driver_id);
                if(driver_name == null){
                    driver_name = Driver.getDriverName(this_one.driver_id);  //从数据库找
                    driver_names.put(this_one.driver_id,driver_name);
                }
                ReturnDriverOrders this_driver = getDriverOrders(this_one.driver_id);
                ObjectNode event = Json.newObject();
                event.put("driver_id", this_one.driver_id);
                event.put("timestamp", this_one.timestamp);
                event.put("longitude", this_one.longitude);
                event.put("latitude", this_one.latitude);
                event.put("driver_name", driver_name); //New
                event.put("todo_orders", String.valueOf(this_driver.todo_orders)); //New data from Database
                event.put("doing_orders", String.valueOf(this_driver.doing_orders)); //New data from Database
                event.put("Status", this_driver.working_status); //New data from Database
                out.write(event); //单向广播
            }
        }
        // For each event received on the socket,
        in.onMessage(new F.Callback<JsonNode>() {
            @Override
            public void invoke(JsonNode event) {
                // nothing to do
                System.out.println(id);  //这个端口与浏览器通信，一般不会有消息通过信道传入
            }
        });
        // When the socket is closed.
        in.onClose(new F.Callback0() {
            @Override
            public void invoke() {
                actor.tell(new Quit(id),null);
            }
        });

    }

    public static void moveTo(int driver_id, long timestamp, float longitude,float latitude) {
        actor.tell(new MoveMessage(driver_id, timestamp, longitude, latitude),null);
    }

    @Override
    public void onReceive(Object message) throws Exception { //Akka 的自带方法
        if (message instanceof Join) { //todo：给刷新后的界面发送在线司机的位置信息
            // Received a Join message
            Join registration = (Join) message;
            Logger.info("New browser opened with key: " + registration.id);
            connected_clients.put(registration.id, registration.channel);
            System.out.println(connected_clients.size() + " browser(s) are connected");
        } else if (message instanceof MoveMessage) { //给每个端口发送位置信息
            System.out.println("current connected browser:-----"+connected_clients.size()); //查看有多少个端口在连接地图界面
            // Received a Move message
            MoveMessage move = (MoveMessage) message;
            Move driver_move = new Move(move.driver_id,move.timestamp,move.longitude,move.latitude);
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    saveToDatabase(driver_move); //用一个线程专门存数据库
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();  //for testing later
            ReturnDriverOrders this_driver = getDriverOrders(move.driver_id);
            String driver_name = driver_names.get(move.driver_id);
            if(driver_name == null){
                driver_name = Driver.getDriverName(move.driver_id);  //从数据库找
                driver_names.put(move.driver_id,driver_name);
            }
            //用多线程同时发送消息
            ObjectNode event = Json.newObject();
            event.put("driver_id", move.driver_id);
            event.put("timestamp", move.timestamp);
            event.put("longitude", move.longitude);
            event.put("latitude", move.latitude);
            event.put("driver_name", driver_name); //New
            event.put("todo_orders", String.valueOf(this_driver.todo_orders)); //New data from Database
            event.put("doing_orders", String.valueOf(this_driver.doing_orders)); //New data from Database
            event.put("Status", this_driver.working_status); //New data from Database
            //todo: 利用多线程进行广播
            for (WebSocket.Out<JsonNode> channel : connected_clients.values()) {  //给每一个端口发信息
                channel.write(event);
            }
        } else if (message instanceof Quit) {
            // Received a Unregistration message
            Quit quit = (Quit) message;
            Logger.info("Browser: " + quit.id + "has disconnected");
            if(connected_clients.containsKey(quit.id)) {
                connected_clients.remove(quit.id);//移除浏览器socket 端口记录
            }
            System.out.println(connected_clients.size() + " browser(s) are connected");
        } else if (message instanceof Clean) {
            // Received a Clean message
            Logger.info("Start to clean and close all sockets");
            System.out.println(((Clean) message).command);
            Iterator it = connected_clients.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry<String, WebSocket.Out<JsonNode>> m = (Map.Entry<String, WebSocket.Out<JsonNode>>) it.next();
                m.getValue().close(); //关闭每一个端口
            }
            connected_clients.clear();//清空整个map
            System.out.println(connected_clients.size() + " browser(s) are connected");
        }else if (message instanceof CleanCustomer) {
            // Received a Clean message
            Logger.info("Start to clean and clsoe Customer sockets");
            System.out.println(((CleanCustomer) message).customer_name);
            Iterator it = connected_customers.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry<String, WebSocket.Out<JsonNode>> m = (Map.Entry<String, WebSocket.Out<JsonNode>>) it.next();
                m.getValue().close(); //关闭每一个端口
            }
            connected_customers.clear();//清空整个map
            customer_websocket.clear();
            System.out.println(connected_clients.size() + " browser(s) are connected");
        }else if (message instanceof Refresh) {
            // Received a Clean message
            System.out.println("Refresh Page");
            List<DriverLocation> current_drivers = showAllDrivers();
            if(current_drivers.size()>0){  //有工作司机
                for(DriverLocation this_one: current_drivers){
                    String driver_name = driver_names.get(this_one.driver_id);
                    if(driver_name == null){
                        driver_name = Driver.getDriverName(this_one.driver_id);  //从数据库找
                        driver_names.put(this_one.driver_id,driver_name);
                    }
                    ReturnDriverOrders this_driver = getDriverOrders(this_one.driver_id);
                    ObjectNode event = Json.newObject();
                    event.put("driver_id", this_one.driver_id);
                    event.put("timestamp", this_one.timestamp);
                    event.put("longitude", this_one.longitude);
                    event.put("latitude", this_one.latitude);
                    event.put("driver_name", driver_name); //New
                    event.put("todo_orders", String.valueOf(this_driver.todo_orders)); //New data from Database
                    event.put("doing_orders", String.valueOf(this_driver.doing_orders)); //New data from Database
                    event.put("Status", this_driver.working_status); //New data from Database
                    for (WebSocket.Out<JsonNode> channel : connected_clients.values()) {  //给每一个端口发信息, 更新司机最后一次访问时间
                        channel.write(event);
                    }
                }
            }
        }else {
            unhandled(message);
        }
    }

    public static class Join {  //连接服务器
        public String id;
        public WebSocket.Out<JsonNode> channel;

        public Join(String id, WebSocket.Out<JsonNode> channel) {
            super();
            this.id = id;
            this.channel = channel;
        }
    }

    public static class Quit {  //断开连接服务器
        public String id;

        public Quit(String id) {
            super();
            this.id = id;
        }
    }

    public static class Clean {  //清理服务器端口
        public String command;
        public int command_code;
        public Clean(String command, int command_code) {
            super();
            this.command = command;
            this.command_code = command_code;
        }
    }

    public static class CleanCustomer {  //清理服务器端口
        public String customer_name;
        public int customer_id;
        public CleanCustomer(String customer_name, int customer_id) {
            super();
            this.customer_name = customer_name;
            this.customer_id = customer_id;
        }
    }

    public static class Refresh {  //刷新服务器最新司机状态
        public int if_refresh;
        public int if_success;
        public Refresh(int if_refresh, int if_success) {
            super();
            this.if_refresh = if_refresh;
            this.if_refresh = if_refresh;
        }
    }

    public static void saveToDatabase(Move move){ //向数据库  todo: 修改存储方式，支持大批量存储
        Date current_time = new Date();
        //With Update rawSQL clause
//        String dml = "UPDATE ps_drivers\n" +
//                "SET app_last_login = :app_last_login\n" +
//                "WHERE id_driver = :id_driver";
//        SqlUpdate update = Ebean.createSqlUpdate(dml)
//                .setParameter("app_last_login", login_time)
//                .setParameter("id_driver", id_driver);
//        update.execute();
        String sql = "INSERT INTO ps_driver_locations (id_driver, sent_timestamp, longitude, latitude, date_add) " +
                "VALUES (:id_driver, :sent_timestamp, :longitude, :latitude, :date_add )";
        SqlUpdate insert = Ebean.createSqlUpdate(sql);
        insert.setParameter("id_driver",move.driver_id);
        insert.setParameter("sent_timestamp",move.timestamp);
        insert.setParameter("longitude",move.longitude);
        insert.setParameter("latitude",move.latitude);
        insert.setParameter("date_add",current_time);
        insert.execute();
    }

    /**
     * 在第一次打开浏览器建立socket 连接， 还有刷新浏览器的时候
     * 显示出所有在线司机的位置
     * RawSQL 实现所有司机位置查询
     * */
    public static List<DriverLocation> showAllDrivers(){
        String sql =
                "select table1.id_driver, table1.name, table1.sent_timestamp, table1.longitude, table1.latitude\n" +
                        "from(select ps_drivers.id_driver, ps_drivers.name, ps_driver_locations.sent_timestamp, ps_driver_locations.longitude, ps_driver_locations.latitude, ps_driver_locations.id_driver_locations\n" +
                        "from ps_drivers, ps_driver_locations\n" +
                        "where ps_drivers.id_driver = ps_driver_locations.id_driver\n" +
                        "and ps_drivers.if_work = 1\n" +
                        "and ps_drivers.id_driver !=8\n" +
                        "and ps_driver_locations.date_add > DATE_SUB(NOW(), INTERVAL 1 HOUR)) as table1\n" +
                        "LEFT JOIN \n" +
                        "(select ps_drivers.id_driver, ps_drivers.name, ps_driver_locations.sent_timestamp, ps_driver_locations.longitude, ps_driver_locations.latitude, ps_driver_locations.id_driver_locations\n" +
                        "from ps_drivers, ps_driver_locations\n" +
                        "where ps_drivers.id_driver = ps_driver_locations.id_driver\n" +
                        "and ps_drivers.if_work = 1\n" +
                        "and ps_drivers.id_driver !=8\n" +
                        "and ps_driver_locations.date_add > DATE_SUB(NOW(), INTERVAL 1 HOUR)) as table2\n" +
                        "ON (table1.id_driver = table2.id_driver AND table1.id_driver_locations < table2.id_driver_locations)\n" +
                        "where table2.id_driver_locations IS NULL";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("table1.id_driver", "driver_id")
                        .columnMapping("table1.name", "driver_name")
                        .columnMapping("table1.sent_timestamp", "timestamp")
                        .columnMapping("table1.longitude", "longitude")
                        .columnMapping("table1.latitude", "latitude")
                        .create();
        com.avaje.ebean.Query<DriverLocation> query = Ebean.find(DriverLocation.class);
        query.setRawSql(rawSql);
        List<DriverLocation> list = query.findList();
        return list;
    }



//	public static class Talk {  //连接服务器的管理员互相聊天
//
//		final String username;
//		final String text;
//
//		public Talk(String username, String text) {
//			this.username = username;
//			this.text = text;
//		}
//
//	}

    public static class MoveMessage {  //与服务器之间进行对话，服务器不断传新的坐标信息给浏览器

        public int driver_id;

        public long timestamp;

        public float longitude;

        public float latitude;

        public MoveMessage(int driver_id, long timestamp, float longitude,
                           float latitude) {
            this.driver_id = driver_id;
            this.timestamp = timestamp;
            this.longitude = longitude;
            this.latitude = latitude;
        }
    }

}
