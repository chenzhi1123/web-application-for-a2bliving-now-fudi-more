package models.socketControl;

import akka.actor.ActorRef;
import com.fasterxml.jackson.databind.JsonNode;
import play.Logger;
import play.libs.Akka;
import play.libs.Json;
import play.mvc.WebSocket;
import scala.concurrent.duration.Duration;

import static java.util.concurrent.TimeUnit.HOURS;

public class Robot {

    public Robot(ActorRef mapAnime) {  //参数是ActorRef, 可以看作为一个线程

        // Create a Fake socket out for the robot that log events to the console.
        WebSocket.Out<JsonNode> robotChannel = new WebSocket.Out<JsonNode>() {

            public void write(JsonNode frame) {
                Logger.of("robot").info(Json.stringify(frame));
            }

            public void close() {}

        };

        // Join the room
        mapAnime.tell(new MapAnime.Join("Robot", robotChannel), null);

        // Make the robot talk every 30 seconds
        Akka.system().scheduler().schedule(  //隔一段时间发送请求
            Duration.create(12, HOURS),
            Duration.create(12, HOURS),
            mapAnime,
            new MapAnime.Quit("Robot"),
            Akka.system().dispatcher(),
            /** sender **/ null
        );

    }

}
