/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;

/**
 * Created by 20060 on 2017/1/3.
 */

@Entity
@Sql
public class OrderSequence {
    public int order_sequence;


    public OrderSequence(int order_sequence) {
        this.order_sequence = order_sequence;
    }

    @Override
    public String toString() {
        return "OrderSequence{" +
                "order_sequence=" + order_sequence +
                '}';
    }
}
