/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.io.Serializable;
import java.math.BigDecimal;
@Entity
@Sql
public class ProductWholesale implements Serializable {
    public java.math.BigDecimal whole_sale = BigDecimal.valueOf(0.000000);
}
