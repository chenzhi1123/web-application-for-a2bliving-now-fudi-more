/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/28.
 */
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class StockManagementProduct {

    public int id_product;
    public int id_tax_rules_group;
    public int if_valid;
    public java.math.BigDecimal tax_rate = BigDecimal.valueOf(0.000000);
    public String ean = "";
    public java.math.BigDecimal price = BigDecimal.valueOf(0.000000);
    public String name = "";
    public java.math.BigDecimal reduction = BigDecimal.valueOf(0.000000);
    public String reduction_type = "";
    public Integer id_image = 0;
    public Integer quantity = 0;
    public String category_name = "";
    public Date expiry_date = new Date(0);
    public String expiryDate = "";
    public int if_expiring = 0;


    public StockManagementProduct(int id_product, int id_tax_rules_group, int if_valid, BigDecimal tax_rate, String ean, BigDecimal price, String name, BigDecimal reduction, String reduction_type, Integer id_image, Integer quantity, String category_name, Date expiry_date, String expiryDate, int if_expiring) {
        this.id_product = id_product;
        this.id_tax_rules_group = id_tax_rules_group;
        this.if_valid = if_valid;
        this.tax_rate = tax_rate;
        this.ean = ean;
        this.price = price;
        this.name = name;
        this.reduction = reduction;
        this.reduction_type = reduction_type;
        this.id_image = id_image;
        this.quantity = quantity;
        this.category_name = category_name;
        this.expiry_date = expiry_date;
        this.expiryDate = expiryDate;
        this.if_expiring = if_expiring;
    }

    public StockManagementProduct(){

    }

    @Override
    public String toString() {
        return "StockManagementProduct{" +
                "id_product=" + id_product +
                ", id_tax_rules_group=" + id_tax_rules_group +
                ", if_valid=" + if_valid +
                ", tax_rate=" + tax_rate +
                ", ean='" + ean + '\'' +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", reduction=" + reduction +
                ", reduction_type='" + reduction_type + '\'' +
                ", id_image=" + id_image +
                ", quantity=" + quantity +
                ", category_name='" + category_name + '\'' +
                ", expiry_date=" + expiry_date +
                ", expiryDate='" + expiryDate + '\'' +
                ", if_expiring=" + if_expiring +
                '}';
    }
}

