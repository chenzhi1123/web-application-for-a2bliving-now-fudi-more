/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;
/**
 * Created by Zhi Chen on 14/11/2017.
 */
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class BuyOneGetOneProduct {
    public int id_product;
    public int if_active;
    public int id_image;
    public int quantity;
    public int id_onsale_product;
    public int id_target_img;
    public int id_bogo;
    public int basic_unit;
    public int bundle_number;
    public int if_expiring = 0;
    public java.math.BigDecimal tax_rate = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal price = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal reduction = BigDecimal.valueOf(0.000000);
    public String reduction_type = "";
    public String name = "";
    public String startDate = "";
    public String endDate = "";
    public String expiryDate = "";
    public String admin_name = "";
    public Date expiry_date = new Date(0);
    public Date start_date = new Date(0);
    public Date end_date = new Date(0);

    public BuyOneGetOneProduct(int id_product, int if_active, int id_image, int quantity, int id_onsale_product, int id_target_img, int id_bogo, int basic_unit, int bundle_number, int if_expiring, BigDecimal tax_rate, BigDecimal price, BigDecimal reduction, String reduction_type, String name, String startDate, String endDate, String expiryDate, String admin_name, Date expiry_date, Date start_date, Date end_date) {
        this.id_product = id_product;
        this.if_active = if_active;
        this.id_image = id_image;
        this.quantity = quantity;
        this.id_onsale_product = id_onsale_product;
        this.id_target_img = id_target_img;
        this.id_bogo = id_bogo;
        this.basic_unit = basic_unit;
        this.bundle_number = bundle_number;
        this.if_expiring = if_expiring;
        this.tax_rate = tax_rate;
        this.price = price;
        this.reduction = reduction;
        this.reduction_type = reduction_type;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.expiryDate = expiryDate;
        this.admin_name = admin_name;
        this.expiry_date = expiry_date;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public BuyOneGetOneProduct(){

    }

    @Override
    public String toString() {
        return "BuyOneGetOneProduct{" +
                "id_product=" + id_product +
                ", if_active=" + if_active +
                ", id_image=" + id_image +
                ", quantity=" + quantity +
                ", id_onsale_product=" + id_onsale_product +
                ", id_target_img=" + id_target_img +
                ", id_bogo=" + id_bogo +
                ", basic_unit=" + basic_unit +
                ", bundle_number=" + bundle_number +
                ", if_expiring=" + if_expiring +
                ", tax_rate=" + tax_rate +
                ", price=" + price +
                ", reduction=" + reduction +
                ", reduction_type='" + reduction_type + '\'' +
                ", name='" + name + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                ", admin_name='" + admin_name + '\'' +
                ", expiry_date=" + expiry_date +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                '}';
    }
}

