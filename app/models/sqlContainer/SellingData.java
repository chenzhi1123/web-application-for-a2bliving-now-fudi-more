package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by yichongmiao on 14/12/2017.
 */
@Entity
@Sql
public class SellingData implements Serializable{
    public int id_product;
    public int id_image;
    public String name;
    public String link_rewrite;
    public String ean13;
    public String zone;
    public String part;
    public String position_number;

    public SellingData(int id_product, int id_image, String name, String link_rewrite, String ean13, String zone, String part, String position_number) {
        this.id_product = id_product;
        this.id_image = id_image;
        this.name = name;
        this.link_rewrite = link_rewrite;
        this.ean13 = ean13;
        this.zone = zone;
        this.part = part;
        this.position_number = position_number;
    }

    public SellingData() {
    }




    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink_rewrite() {
        return link_rewrite;
    }

    public void setLink_rewrite(String link_rewrite) {
        this.link_rewrite = link_rewrite;
    }

    public String getEan13() {
        return ean13;
    }

    public void setEan13(String ean13) {
        this.ean13 = ean13;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getPosition_number() {
        return position_number;
    }

    public void setPosition_number(String position_number) {
        this.position_number = position_number;
    }

    public int getId_image() {
        return id_image;
    }

    public void setId_image(int id_image) {
        this.id_image = id_image;
    }


    public String getLocation(){
       if (zone!=null&&part!=null&&position_number!=null) {

           return zone + "-" + part + position_number;
       }else {
           return "";
       }
    }

}
