/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/28.
 */
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class DriverPayrollSummary {

    public Integer id_driver;
    public String name;
    public BigDecimal working_hours = BigDecimal.valueOf(0.0);
    public BigDecimal hourly_wage = BigDecimal.valueOf(0.0);
    public BigDecimal total_wage = BigDecimal.valueOf(0.00);
    public BigDecimal extra_cost = BigDecimal.valueOf(0.00);
    public String specification = "";
    public Date date_add;
    public BigDecimal marina_delivery = BigDecimal.valueOf(0.00);
    public Integer cash_order = 0;
    public Integer online_order = 0;
    public Integer if_company_car = 0;
    public BigDecimal cash_delivery_fee = BigDecimal.valueOf(0.00);
    public BigDecimal online_delivery_fee = BigDecimal.valueOf(0.00);

    public DriverPayrollSummary(Integer id_driver, String name, BigDecimal working_hours, BigDecimal hourly_wage, BigDecimal total_wage, BigDecimal extra_cost, String specification, Date date_add, BigDecimal marina_delivery, Integer cash_order, Integer online_order, Integer if_company_car, BigDecimal cash_delivery_fee, BigDecimal online_delivery_fee) {
        this.id_driver = id_driver;
        this.name = name;
        this.working_hours = working_hours;
        this.hourly_wage = hourly_wage;
        this.total_wage = total_wage;
        this.extra_cost = extra_cost;
        this.specification = specification;
        this.date_add = date_add;
        this.marina_delivery = marina_delivery;
        this.cash_order = cash_order;
        this.online_order = online_order;
        this.if_company_car = if_company_car;
        this.cash_delivery_fee = cash_delivery_fee;
        this.online_delivery_fee = online_delivery_fee;
    }

    public DriverPayrollSummary(){

    }

    @Override
    public String toString() {
        return "DriverPayrollSummary{" +
                "id_driver=" + id_driver +
                ", name='" + name + '\'' +
                ", working_hours=" + working_hours +
                ", hourly_wage=" + hourly_wage +
                ", total_wage=" + total_wage +
                ", extra_cost=" + extra_cost +
                ", specification='" + specification + '\'' +
                ", date_add=" + date_add +
                ", marina_delivery=" + marina_delivery +
                ", cash_order=" + cash_order +
                ", online_order=" + online_order +
                ", if_company_car=" + if_company_car +
                ", cash_delivery_fee=" + cash_delivery_fee +
                ", online_delivery_fee=" + online_delivery_fee +
                '}';
    }
}



