/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/28.
 */
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Sql
public class TaxSummary {

    public int id_tax_rules_group;
    public String tax_type;
    public java.math.BigDecimal total_sales = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_tax_amount = BigDecimal.valueOf(0.000000);

    public TaxSummary(int id_tax_rules_group, String tax_type, BigDecimal total_sales, BigDecimal total_tax_amount) {
        this.id_tax_rules_group = id_tax_rules_group;
        this.tax_type = tax_type;
        this.total_sales = total_sales;
        this.total_tax_amount = total_tax_amount;
    }

    public TaxSummary(){}

    @Override
    public String toString() {
        return "TaxSummary{" +
                "id_tax_rules_group=" + id_tax_rules_group +
                ", tax_type='" + tax_type + '\'' +
                ", total_sales=" + total_sales +
                ", total_tax_amount=" + total_tax_amount +
                '}';
    }
}


