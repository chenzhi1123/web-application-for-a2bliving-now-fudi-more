/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/28.
 */
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Sql
public class DiscountSummary {

    public int id_tax_rules_group;
    public String tax_type = "";
    public java.math.BigDecimal total_discount = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_product_price = BigDecimal.valueOf(0.000000);
    public int product_quantity;

    public DiscountSummary(int id_tax_rules_group, String tax_type, BigDecimal total_discount, BigDecimal total_product_price,int product_quantity) {
        this.id_tax_rules_group = id_tax_rules_group;
        this.tax_type = tax_type;
        this.total_discount = total_discount;
        this.total_product_price =total_product_price;
        this.product_quantity = product_quantity;
    }

    public DiscountSummary(){

    }

    @Override
    public String toString() {
        return "DiscountSummary{" +
                "id_tax_rules_group=" + id_tax_rules_group +
                ", tax_type='" + tax_type + '\'' +
                ", total_discount=" + total_discount +
                ", total_product_price=" + total_product_price +
                ", product_quantity=" + product_quantity +
                '}';
    }
}



