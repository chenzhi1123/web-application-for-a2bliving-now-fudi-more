package models.sqlContainer;

import javax.persistence.Entity;

/**
 * 司机发送过来的位置信息
 * */
@SuppressWarnings("serial")
@Entity
public class DriverLocation {

	public int driver_id;  //key 名称很重要
    public String driver_name;
	public long timestamp;
	public float longitude;
	public float latitude;

    public DriverLocation(int driver_id, String driver_name, long timestamp, float longitude, float latitude) {
        this.driver_id = driver_id;
        this.driver_name = driver_name;
        this.timestamp = timestamp;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "DriverLocation{" +
                "driver_id=" + driver_id +
                ", driver_name='" + driver_name + '\'' +
                ", timestamp=" + timestamp +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}
