/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by Zhi Chen on 2017/3/28.
 */

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class OrderDrillProducts {


    public java.math.BigDecimal total_discounts = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public Date invoice_date;
    public String product_name = "";
    public String product_reference = "";
    public String tax_type = "";  //税收种类
    public java.math.BigDecimal product_tax = BigDecimal.valueOf(0.000000);  //该产品的税
    public java.math.BigDecimal total_price_tax_incl = BigDecimal.valueOf(0.000000);
    public int product_quantity;
    public int id_tax_rules_group;
    public java.math.BigDecimal original_product_price = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal real_original_price = BigDecimal.valueOf(0.000000); //单价
    public java.math.BigDecimal total_reduction = BigDecimal.valueOf(0.000000); //reduction

    public OrderDrillProducts(BigDecimal total_discounts, BigDecimal total_shipping, Date invoice_date, String product_name, String product_reference, String tax_type, BigDecimal product_tax, BigDecimal total_price_tax_incl, int product_quantity, int id_tax_rules_group, BigDecimal original_product_price, BigDecimal real_original_price, BigDecimal total_reduction) {
        this.total_discounts = total_discounts;
        this.total_shipping = total_shipping;
        this.invoice_date = invoice_date;
        this.product_name = product_name;
        this.product_reference = product_reference;
        this.tax_type = tax_type;
        this.product_tax = product_tax;
        this.total_price_tax_incl = total_price_tax_incl;
        this.product_quantity = product_quantity;
        this.id_tax_rules_group = id_tax_rules_group;
        this.original_product_price = original_product_price;
        this.real_original_price = real_original_price;
        this.total_reduction = total_reduction;
    }

    public OrderDrillProducts(){

    }

    @Override
    public String toString() {
        return "OrderDrillProducts{" +
                "total_discounts=" + total_discounts +
                ", total_shipping=" + total_shipping +
                ", invoice_date=" + invoice_date +
                ", product_name='" + product_name + '\'' +
                ", product_reference='" + product_reference + '\'' +
                ", tax_type='" + tax_type + '\'' +
                ", product_tax=" + product_tax +
                ", total_price_tax_incl=" + total_price_tax_incl +
                ", product_quantity=" + product_quantity +
                ", id_tax_rules_group=" + id_tax_rules_group +
                ", original_product_price=" + original_product_price +
                ", real_original_price=" + real_original_price +
                ", total_reduction=" + total_reduction +
                '}';
    }
}

