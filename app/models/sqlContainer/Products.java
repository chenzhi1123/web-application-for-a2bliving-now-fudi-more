/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.ArrayList;
@Entity
public class Products implements Serializable{


    public ArrayList<Product> products;

    public Products(){

    }


    public Products(ArrayList<Product> products)
    {
        super();
        this.products = products;
    }

    @Override
    public String toString() {
        return "Products{" +
                "products=" + products +
                '}';
    }
}