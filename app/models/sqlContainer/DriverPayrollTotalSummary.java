/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/28.
 */
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.ArrayList;

@Entity
@Sql
public class DriverPayrollTotalSummary {

    public Integer id_driver;
    public String name;
    public Integer total_working_days;
    public BigDecimal total_working_hours = BigDecimal.valueOf(0.0);
    public BigDecimal total_wage = BigDecimal.valueOf(0.00);
    public BigDecimal total_extra_cost = BigDecimal.valueOf(0.00);
    public BigDecimal total_marina_delivery = BigDecimal.valueOf(0.00);
    public Integer total_cash_order = 0;
    public Integer total_online_order = 0;
    public BigDecimal total_cash_delivery_fee = BigDecimal.valueOf(0.00);
    public BigDecimal total_online_delivery_fee = BigDecimal.valueOf(0.00);
    public ArrayList<DriverPayrollSummary> detail_list = new ArrayList<>();

    public DriverPayrollTotalSummary(Integer id_driver, String name, Integer total_working_days, BigDecimal total_working_hours, BigDecimal total_wage, BigDecimal total_extra_cost, BigDecimal total_marina_delivery, Integer total_cash_order, Integer total_online_order, BigDecimal total_cash_delivery_fee, BigDecimal total_online_delivery_fee, ArrayList<DriverPayrollSummary> detail_list) {
        this.id_driver = id_driver;
        this.name = name;
        this.total_working_days = total_working_days;
        this.total_working_hours = total_working_hours;
        this.total_wage = total_wage;
        this.total_extra_cost = total_extra_cost;
        this.total_marina_delivery = total_marina_delivery;
        this.total_cash_order = total_cash_order;
        this.total_online_order = total_online_order;
        this.total_cash_delivery_fee = total_cash_delivery_fee;
        this.total_online_delivery_fee = total_online_delivery_fee;
        this.detail_list = detail_list;
    }

    public DriverPayrollTotalSummary(){

    }

    @Override
    public String toString() {
        return "DriverPayrollTotalSummary{" +
                "id_driver=" + id_driver +
                ", name='" + name + '\'' +
                ", total_working_days=" + total_working_days +
                ", total_working_hours=" + total_working_hours +
                ", total_wage=" + total_wage +
                ", total_extra_cost=" + total_extra_cost +
                ", total_marina_delivery=" + total_marina_delivery +
                ", total_cash_order=" + total_cash_order +
                ", total_online_order=" + total_online_order +
                ", total_cash_delivery_fee=" + total_cash_delivery_fee +
                ", total_online_delivery_fee=" + total_online_delivery_fee +
                ", detail_list=" + detail_list +
                '}';
    }
}



