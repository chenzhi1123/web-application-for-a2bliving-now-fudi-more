/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/29.
 */

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class DeliveryOrders {

    public int id_order;
    public int id_driver;
    public int id_cashier;
    public String driver_name = "";
    public String payment_method = "";
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_discounts = BigDecimal.valueOf(0.000000);
    public Date invoice_date;
    public String delivery_type = "";

    public DeliveryOrders(int id_order, int id_driver, int id_cashier, String driver_name, String payment_method, BigDecimal total_shipping, BigDecimal total_discounts, Date invoice_date, String delivery_type) {
        this.id_order = id_order;
        this.id_driver = id_driver;
        this.id_cashier = id_cashier;
        this.driver_name = driver_name;
        this.payment_method = payment_method;
        this.total_shipping = total_shipping;
        this.total_discounts = total_discounts;
        this.invoice_date = invoice_date;
        this.delivery_type = delivery_type;
    }

    public DeliveryOrders(){}

    @Override
    public String toString() {
        return "DeliveryOrders{" +
                "id_order=" + id_order +
                ", id_driver=" + id_driver +
                ", id_cashier=" + id_cashier +
                ", driver_name='" + driver_name + '\'' +
                ", payment_method='" + payment_method + '\'' +
                ", total_shipping=" + total_shipping +
                ", total_discounts=" + total_discounts +
                ", invoice_date=" + invoice_date +
                ", delivery_type='" + delivery_type + '\'' +
                '}';
    }
}
