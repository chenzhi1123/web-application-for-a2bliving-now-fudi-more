/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

import com.avaje.ebean.annotation.Sql;
import models.template.AddressTemplate;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class ReturnDriverOrder {
    public int id_order;
    public int order_number;
    public String driver_name;
    public String order_reference;
    public int id_address_delivery;
    public String payment;
    public java.math.BigDecimal total_paid_real = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_discounts = BigDecimal.valueOf(0.000000);
    public String delivery_address;
    public int if_paid;
    //添加时间 // TODO: 2017/3/29
    public Date start_time;   //new

    //有关顾客情况
    public AddressTemplate customer_address = new AddressTemplate();


    public ReturnDriverOrder(int id_order, int order_number, String driver_name, String order_reference, int id_address_delivery, String payment, BigDecimal total_paid_real, BigDecimal total_shipping, BigDecimal total_discounts, String delivery_address, int if_paid, Date start_time, AddressTemplate customer_address) {
        this.id_order = id_order;
        this.order_number = order_number;
        this.driver_name = driver_name;
        this.order_reference = order_reference;
        this.id_address_delivery = id_address_delivery;
        this.payment = payment;
        this.total_paid_real = total_paid_real;
        this.total_shipping = total_shipping;
        this.total_discounts = total_discounts;
        this.delivery_address = delivery_address;
        this.if_paid = if_paid;
        this.start_time = start_time;
        this.customer_address = customer_address;
    }

    public ReturnDriverOrder() {

    }

    @Override
    public String toString() {
        return "ReturnDriverOrder{" +
                "id_order=" + id_order +
                ", order_number=" + order_number +
                ", driver_name='" + driver_name + '\'' +
                ", order_reference='" + order_reference + '\'' +
                ", id_address_delivery=" + id_address_delivery +
                ", payment='" + payment + '\'' +
                ", total_paid_real=" + total_paid_real +
                ", total_shipping=" + total_shipping +
                ", total_discounts=" + total_discounts +
                ", delivery_address='" + delivery_address + '\'' +
                ", if_paid=" + if_paid +
                ", start_time=" + start_time +
                ", customer_address=" + customer_address +
                '}';
    }
}
