/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by Zhi Chen on 2017/3/28.
 */

import com.avaje.ebean.annotation.Sql;
import models.template.CustomerServiceProducts;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

@Entity
@Sql
public class CustomerServiceOrderInfo {

    public Integer order_sequence;
    public Integer id_order;
    public String order_reference;
    public Integer id_customer;
    public Date date_add;
    public String payment = "";
    public Integer id_address_delivery;
    public BigDecimal total_paid_real = BigDecimal.valueOf(0.000000);  //该产品的税
    public BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public BigDecimal total_discounts = BigDecimal.valueOf(0.000000);
    public Date successful_time;
    public Integer valid;
    public Integer current_state;
    public Integer id_shop;
    public String product_name;
    public Integer product_quantity;
    public ArrayList<CustomerServiceProducts> bought_products;
    public Date print_time;
    public Date assign_driver_time;
    public String driver_name;
    public Integer id_cashier;
    public Integer if_delivered;
    public Date start_deliver_time;
    public Date end_deliver_time;
    public String order_customer_firstname;
    public String order_customer_lastname;
    public String order_address1;
    public String order_address2;
    public String order_phone;


    public CustomerServiceOrderInfo(Integer order_sequence, Integer id_order, String order_reference, Integer id_customer, Date date_add, String payment, Integer id_address_delivery, BigDecimal total_paid_real, BigDecimal total_shipping, BigDecimal total_discounts, Date successful_time, Integer valid, Integer current_state, Integer id_shop, String product_name, Integer product_quantity, ArrayList<CustomerServiceProducts> bought_products, Date print_time, Date assign_driver_time, String driver_name, Integer id_cashier, Integer if_delivered, Date start_deliver_time, Date end_deliver_time, String order_customer_firstname, String order_customer_lastname, String order_address1, String order_address2, String order_phone) {
        this.order_sequence = order_sequence;
        this.id_order = id_order;
        this.order_reference = order_reference;
        this.id_customer = id_customer;
        this.date_add = date_add;
        this.payment = payment;
        this.id_address_delivery = id_address_delivery;
        this.total_paid_real = total_paid_real;
        this.total_shipping = total_shipping;
        this.total_discounts = total_discounts;
        this.successful_time = successful_time;
        this.valid = valid;
        this.current_state = current_state;
        this.id_shop = id_shop;
        this.product_name = product_name;
        this.product_quantity = product_quantity;
        this.bought_products = bought_products;
        this.print_time = print_time;
        this.assign_driver_time = assign_driver_time;
        this.driver_name = driver_name;
        this.id_cashier = id_cashier;
        this.if_delivered = if_delivered;
        this.start_deliver_time = start_deliver_time;
        this.end_deliver_time = end_deliver_time;
        this.order_customer_firstname = order_customer_firstname;
        this.order_customer_lastname = order_customer_lastname;
        this.order_address1 = order_address1;
        this.order_address2 = order_address2;
        this.order_phone = order_phone;
    }

    public CustomerServiceOrderInfo(){

    }

    @Override
    public String toString() {
        return "CustomerServiceOrderInfo{" +
                "order_sequence=" + order_sequence +
                ", id_order=" + id_order +
                ", order_reference='" + order_reference + '\'' +
                ", id_customer=" + id_customer +
                ", date_add=" + date_add +
                ", payment='" + payment + '\'' +
                ", id_address_delivery=" + id_address_delivery +
                ", total_paid_real=" + total_paid_real +
                ", total_shipping=" + total_shipping +
                ", total_discounts=" + total_discounts +
                ", successful_time=" + successful_time +
                ", valid=" + valid +
                ", current_state=" + current_state +
                ", id_shop=" + id_shop +
                ", product_name='" + product_name + '\'' +
                ", product_quantity=" + product_quantity +
                ", bought_products=" + bought_products +
                ", print_time=" + print_time +
                ", assign_driver_time=" + assign_driver_time +
                ", driver_name='" + driver_name + '\'' +
                ", id_cashier=" + id_cashier +
                ", if_delivered=" + if_delivered +
                ", start_deliver_time=" + start_deliver_time +
                ", end_deliver_time=" + end_deliver_time +
                ", order_customer_firstname='" + order_customer_firstname + '\'' +
                ", order_customer_lastname='" + order_customer_lastname + '\'' +
                ", order_address1='" + order_address1 + '\'' +
                ", order_address2='" + order_address2 + '\'' +
                ", order_phone='" + order_phone + '\'' +
                '}';
    }
}

