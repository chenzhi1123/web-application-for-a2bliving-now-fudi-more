/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.sqlContainer;

/**
 * Created by 20060 on 2017/3/28.
 */
import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Sql
public class TaxOrders {

    public int id_order;
    public int id_order_detail;
    public int product_id;
    public int detail_product_quantity;  //order_detail 里面的产品数量
    public int id_tax_rules_group;
    public int total_product_quantity; //一张单里面的所有产品数量
    public java.math.BigDecimal total_price_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal order_tax_amount = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_discounts = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_real = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public Date invoice_date;

    public TaxOrders(int id_order, int id_order_detail, int product_id, int detail_product_quantity, int id_tax_rules_group, int total_product_quantity, BigDecimal total_price_tax_incl, BigDecimal order_tax_amount, BigDecimal total_discounts, BigDecimal total_paid_real, BigDecimal total_shipping, Date invoice_date) {
        this.id_order = id_order;
        this.id_order_detail = id_order_detail;
        this.product_id = product_id;
        this.detail_product_quantity = detail_product_quantity;
        this.id_tax_rules_group = id_tax_rules_group;
        this.total_product_quantity = total_product_quantity;
        this.total_price_tax_incl = total_price_tax_incl;
        this.order_tax_amount = order_tax_amount;
        this.total_discounts = total_discounts;
        this.total_paid_real = total_paid_real;
        this.total_shipping = total_shipping;
        this.invoice_date = invoice_date;
    }

    public TaxOrders(){}

    @Override
    public String toString() {
        return "TaxOrders{" +
                "id_order=" + id_order +
                ", id_order_detail=" + id_order_detail +
                ", product_id=" + product_id +
                ", detail_product_quantity=" + detail_product_quantity +
                ", id_tax_rules_group=" + id_tax_rules_group +
                ", total_product_quantity=" + total_product_quantity +
                ", total_price_tax_incl=" + total_price_tax_incl +
                ", order_tax_amount=" + order_tax_amount +
                ", total_discounts=" + total_discounts +
                ", total_paid_real=" + total_paid_real +
                ", total_shipping=" + total_shipping +
                ", invoice_date=" + invoice_date +
                '}';
    }
}

