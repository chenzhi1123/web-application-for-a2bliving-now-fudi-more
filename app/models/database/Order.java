/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
/**
 * Order Class matches table ps_orders, attributes corresponds to columns in the table
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_orders")
public class Order extends Model
{
    @Id
    @GeneratedValue
    public int id_order;
    public String reference = "";
    public int id_shop_group = 1 ;
    public int id_shop = 1;
    public int id_carrier;
    public int id_lang;
    public int id_customer;
    public int id_cart;
    public int id_currency;
    public int id_address_delivery;
    public int id_address_invoice;
    public int current_state;
    public String secure_key = "-1";
    public String payment;
    public java.math.BigDecimal conversion_rate = BigDecimal.valueOf(1.000000);
    public String module = null;
    public int recyclable = 0;
    public int gift = 0;
    public String gift_message = null;
    public int mobile_theme = 0;
    public String shipping_number = null;
    public java.math.BigDecimal total_discounts = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_discounts_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_discounts_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_tax_excl =  BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_paid_real =  BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products =  BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_products_wt = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal carrier_tax_rate = BigDecimal.valueOf(0.000);
    public java.math.BigDecimal total_wrapping = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_wrapping_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_wrapping_tax_excl = BigDecimal.valueOf(0.000000);
    public int round_mode = 2;
    public int round_type = 1;
    public int invoice_number = 0;
    public int delivery_number = 0;
    public Date invoice_date;
    public Date delivery_date;
    public int valid = 0;
    public Date date_add;
    public Date date_upd;

    public Order()
    {

    }
    public Order(String reference, int id_shop_group, int id_shop, int id_carrier, int id_lang, int id_customer, int id_cart, int id_currency, int id_address_delivery, int id_address_invoice, int current_state, String secure_key, String payment, BigDecimal conversion_rate, String module, int recyclable, int gift, String gift_message, int mobile_theme, String shipping_number, BigDecimal total_discounts, BigDecimal total_discounts_tax_incl, BigDecimal total_discounts_tax_excl, BigDecimal total_paid, BigDecimal total_paid_tax_incl, BigDecimal total_paid_tax_excl, BigDecimal total_paid_real, BigDecimal total_products, BigDecimal total_products_wt, BigDecimal total_shipping, BigDecimal total_shipping_tax_incl, BigDecimal total_shipping_tax_excl, BigDecimal carrier_tax_rate, BigDecimal total_wrapping, BigDecimal total_wrapping_tax_incl, BigDecimal total_wrapping_tax_excl, int round_mode, int round_type, int invoice_number, int delivery_number, Date invoice_date, Date delivery_date, int valid, Date date_add, Date date_upd) {
        this.reference = reference;
        this.id_shop_group = id_shop_group;
        this.id_shop = id_shop;
        this.id_carrier = id_carrier;
        this.id_lang = id_lang;
        this.id_customer = id_customer;
        this.id_cart = id_cart;
        this.id_currency = id_currency;
        this.id_address_delivery = id_address_delivery;
        this.id_address_invoice = id_address_invoice;
        this.current_state = current_state;
        this.secure_key = secure_key;
        this.payment = payment;
        this.conversion_rate = conversion_rate;
        this.module = module;
        this.recyclable = recyclable;
        this.gift = gift;
        this.gift_message = gift_message;
        this.mobile_theme = mobile_theme;
        this.shipping_number = shipping_number;
        this.total_discounts = total_discounts;
        this.total_discounts_tax_incl = total_discounts_tax_incl;
        this.total_discounts_tax_excl = total_discounts_tax_excl;
        this.total_paid = total_paid;
        this.total_paid_tax_incl = total_paid_tax_incl;
        this.total_paid_tax_excl = total_paid_tax_excl;
        this.total_paid_real = total_paid_real;
        this.total_products = total_products;
        this.total_products_wt = total_products_wt;
        this.total_shipping = total_shipping;
        this.total_shipping_tax_incl = total_shipping_tax_incl;
        this.total_shipping_tax_excl = total_shipping_tax_excl;
        this.carrier_tax_rate = carrier_tax_rate;
        this.total_wrapping = total_wrapping;
        this.total_wrapping_tax_incl = total_wrapping_tax_incl;
        this.total_wrapping_tax_excl = total_wrapping_tax_excl;
        this.round_mode = round_mode;
        this.round_type = round_type;
        this.invoice_number = invoice_number;
        this.delivery_number = delivery_number;
        this.invoice_date = invoice_date;
        this.delivery_date = delivery_date;
        this.valid = valid;
        this.date_add = date_add;
        this.date_upd = date_upd;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof Order)
        {
            final Order other = (Order) obj;
            return Objects.equal(reference, other.reference)
                    && Objects.equal(id_shop_group, other.id_shop_group)
                    && Objects.equal(id_shop, other.id_shop)
                    && Objects.equal(id_carrier, other.id_carrier)
                    && Objects.equal(id_lang, other.id_lang)
                    && Objects.equal(id_customer, other.id_customer)
                    && Objects.equal(id_cart, other.id_cart)
                    && Objects.equal(id_currency, other.id_currency)
                    && Objects.equal(id_address_delivery, other.id_address_delivery)
                    && Objects.equal(id_address_invoice, other.id_address_invoice)
                    && Objects.equal(current_state, other.current_state)
                    && Objects.equal(secure_key, other.secure_key)
                    && Objects.equal(payment, other.payment)
                    && Objects.equal(conversion_rate, other.conversion_rate)
                    && Objects.equal(module, other.module)
                    && Objects.equal(recyclable, other.recyclable)
                    && Objects.equal(gift, other.gift)
                    && Objects.equal(gift_message, other.gift_message)
                    && Objects.equal(mobile_theme, other.mobile_theme)
                    && Objects.equal(shipping_number, other.shipping_number)
                    && Objects.equal(total_discounts, other.total_discounts)
                    && Objects.equal(total_discounts_tax_incl, other.total_discounts_tax_incl)
                    && Objects.equal(total_discounts_tax_excl, other.total_discounts_tax_excl)
                    && Objects.equal(total_paid, other.total_paid)
                    && Objects.equal(total_paid_tax_incl, other.total_paid_tax_incl)
                    && Objects.equal(total_paid_tax_excl, other.total_paid_tax_excl)
                    && Objects.equal(total_paid_real, other.total_paid_real)
                    && Objects.equal(total_products, other.total_products)
                    && Objects.equal(total_products_wt, other.total_products_wt)
                    && Objects.equal(total_shipping, other.total_shipping)
                    && Objects.equal(total_shipping_tax_incl, other.total_shipping_tax_incl)
                    && Objects.equal(total_shipping_tax_excl, other.total_shipping_tax_excl)
                    && Objects.equal(carrier_tax_rate, other.carrier_tax_rate)
                    && Objects.equal(total_wrapping, other.total_wrapping)
                    && Objects.equal(total_wrapping_tax_incl, other.total_wrapping_tax_incl)
                    && Objects.equal(total_wrapping_tax_excl, other.total_wrapping_tax_excl)
                    && Objects.equal(round_mode, other.round_mode)
                    && Objects.equal(round_type, other.round_type)
                    && Objects.equal(invoice_number, other.invoice_number)
                    && Objects.equal(delivery_number, other.delivery_number)
                    && Objects.equal(invoice_date, other.invoice_date)
                    && Objects.equal(delivery_date, other.delivery_date)
                    && Objects.equal(valid, other.valid)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(date_upd, other.date_upd);


        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Order{" +
                "id_order=" + id_order +
                ", reference='" + reference + '\'' +
                ", id_shop_group=" + id_shop_group +
                ", id_shop=" + id_shop +
                ", id_carrier=" + id_carrier +
                ", id_lang=" + id_lang +
                ", id_customer=" + id_customer +
                ", id_cart=" + id_cart +
                ", id_currency=" + id_currency +
                ", id_address_delivery=" + id_address_delivery +
                ", id_address_invoice=" + id_address_invoice +
                ", current_state=" + current_state +
                ", secure_key='" + secure_key + '\'' +
                ", payment='" + payment + '\'' +
                ", conversion_rate=" + conversion_rate +
                ", module='" + module + '\'' +
                ", recyclable=" + recyclable +
                ", gift=" + gift +
                ", gift_message='" + gift_message + '\'' +
                ", mobile_theme=" + mobile_theme +
                ", shipping_number='" + shipping_number + '\'' +
                ", total_discounts=" + total_discounts +
                ", total_discounts_tax_incl=" + total_discounts_tax_incl +
                ", total_discounts_tax_excl=" + total_discounts_tax_excl +
                ", total_paid=" + total_paid +
                ", total_paid_tax_incl=" + total_paid_tax_incl +
                ", total_paid_tax_excl=" + total_paid_tax_excl +
                ", total_paid_real=" + total_paid_real +
                ", total_products=" + total_products +
                ", total_products_wt=" + total_products_wt +
                ", total_shipping=" + total_shipping +
                ", total_shipping_tax_incl=" + total_shipping_tax_incl +
                ", total_shipping_tax_excl=" + total_shipping_tax_excl +
                ", carrier_tax_rate=" + carrier_tax_rate +
                ", total_wrapping=" + total_wrapping +
                ", total_wrapping_tax_incl=" + total_wrapping_tax_incl +
                ", total_wrapping_tax_excl=" + total_wrapping_tax_excl +
                ", round_mode=" + round_mode +
                ", round_type=" + round_type +
                ", invoice_number=" + invoice_number +
                ", delivery_number=" + delivery_number +
                ", invoice_date=" + invoice_date +
                ", delivery_date=" + delivery_date +
                ", valid=" + valid +
                ", date_add=" + date_add +
                ", date_upd=" + date_upd +
                '}';
    }

    public static Order findByEmail(String email)

    {
        return Order.find.where().eq("email", email).findUnique();
    }

    public static Order findById(int id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static Order findByReference(String reference) {return find.where().eq("reference", reference).findUnique();}

    /** Find orders of one customer, within 14 days, valid =1
     * @param id_customer
     * @param secure_key
     * */
    public  static  List<Order> findByCustomer(int id_customer, String secure_key) {
        long nowMinus2Weeks = System.currentTimeMillis() - 14 * 24 * (1000 * 60 * 60);
        Date lastTwoWeeks = new Date(nowMinus2Weeks);
        return find.where().eq("id_customer", id_customer).eq("secure_key", secure_key).eq("valid", 1).gt("invoice_date",lastTwoWeeks).orderBy("id_order desc").setMaxRows(10).findList();
    }


    //get Orders to print
    public  static  List<Order> getPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 30;  //half an hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    //get Orders to print
    public  static  List<Order> getManualPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 60;  //one hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    /**
     * Find all orders within the last 14 hours, valid = 1
     * */
    public  static  List<Order> ordersForDriver(){
        long nowMinus1Day = System.currentTimeMillis() - 1 * 14 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneDay).orderBy("id_order desc").findList();
    }

    public static List<Order> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (Order order : Order.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, Order> find = new Model.Finder<String, Order>(String.class, Order.class);

}
