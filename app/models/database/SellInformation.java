/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name = "ps_sell_information")
public class SellInformation extends Model {
    private int id_product;
    private int selling;
    private int alert_amount;


    public SellInformation(int id_product, int selling) {
        this.id_product = id_product;
        this.selling = selling;
    }

    public SellInformation(int id_product, int selling, int alert_amount) {
        this.id_product = id_product;
        this.selling = selling;
        this.alert_amount = alert_amount;
    }
    public static final Model.Finder<String, SellInformation> find = new Model.Finder<>(String.class,SellInformation.class);
    public static SellInformation findById(int id)
    {
        return find.where().eq("id_product", id).findUnique();
    }
    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public int getSelling() {
        return selling;
    }

    public void setSelling(int selling) {
        this.selling = selling;
    }

    public int getAlert_amount() {
        return alert_amount;
    }

    public void setAlert_amount(int alert_amount) {
        this.alert_amount = alert_amount;
    }
}
