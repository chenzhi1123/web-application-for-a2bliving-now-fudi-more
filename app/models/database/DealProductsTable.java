/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
//Driver class has the corresponding table in the database named "ps_drivers"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_deal_sale_table")
public class DealProductsTable extends Model
{
    @Id
    @GeneratedValue
    public int id_deal_sale_table;
    public int deal_product_id;
    public int id_product_attribute;
    public int id_product_shop;
    public String key_word;


    public DealProductsTable()
    {

    }

    public DealProductsTable(int deal_product_id, int id_product_attribute, int id_product_shop, String key_word) {
        this.deal_product_id = deal_product_id;
        this.id_product_attribute = id_product_attribute;
        this.id_product_shop = id_product_shop;
        this.key_word = key_word;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof DealProductsTable)
        {
            final DealProductsTable other = (DealProductsTable) obj;
            return Objects.equal(deal_product_id, other.deal_product_id)
                    && Objects.equal(id_deal_sale_table, other.id_deal_sale_table)
                    && Objects.equal(id_product_shop, other.id_product_shop)
                    && Objects.equal(key_word, other.key_word);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "DealProductsTable{" +
                "id_deal_sale_table=" + id_deal_sale_table +
                ", deal_product_id=" + deal_product_id +
                ", id_product_attribute=" + id_product_attribute +
                ", id_product_shop=" + id_product_shop +
                ", key_word='" + key_word + '\'' +
                '}';
    }

    public static DealProductsTable findByName(String name)

    {
        return DealProductsTable.find.where().eq("name", name).findUnique();
    }

    public static DealProductsTable findByPhone(String contact_number)
    {
        return DealProductsTable.find.where().eq("contact_number", contact_number).findUnique();
    }

    public static DealProductsTable findById(int id)
    {
        return find.where().eq("id_driver", id).findUnique();
    }

    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<DealProductsTable> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }


    public  static ArrayList<Integer> findByShopProduct(int id_product_shop) {
        ArrayList<Integer> all_id_product_attribute = new ArrayList<>();
        List<DealProductsTable> all_records = find.where().eq("id_product_shop", id_product_shop).findList();
        if(all_records.size()>0){
            for(DealProductsTable this_one:all_records){
                   all_id_product_attribute.add(this_one.id_product_attribute);
            }
        }

        return all_id_product_attribute;
    }
    public static List<DealProductsTable> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (DealProductsTable driver : DealProductsTable.findAll())
        {
            driver.delete();
        }
    }

    public static Model.Finder<String, DealProductsTable> find = new Model.Finder<String, DealProductsTable>(String.class, DealProductsTable.class);
}
