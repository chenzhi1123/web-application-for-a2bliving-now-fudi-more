/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;
/**
 * Order Class matches table ps_orders, attributes corresponds to columns in the table
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_stock_abandon")
public class StockAbandon extends Model
{
    @Id
    @GeneratedValue
    public int id_stock_abandon;
    public int id_product;
    public Date expiry_date;
    public int quantity;
    public Date date_add;
    public String person_in_charge = "";
    public int id_stock_product;


    public StockAbandon()
    {

    }

    public StockAbandon(int id_product, Date expiry_date, int quantity, Date date_add, String person_in_charge, int id_stock_product) {
        this.id_product = id_product;
        this.expiry_date = expiry_date;
        this.quantity = quantity;
        this.date_add = date_add;
        this.person_in_charge = person_in_charge;
        this.id_stock_product = id_stock_product;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof StockAbandon)
        {
            final StockAbandon other = (StockAbandon) obj;
            return Objects.equal(id_product, other.id_product)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(quantity, other.quantity)
                    && Objects.equal(expiry_date, other.expiry_date)
                    && Objects.equal(id_stock_product, other.id_stock_product)
                    && Objects.equal(person_in_charge, other.person_in_charge);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "StockAbandon{" +
                "id_stock_abandon=" + id_stock_abandon +
                ", id_product=" + id_product +
                ", expiry_date=" + expiry_date +
                ", quantity=" + quantity +
                ", date_add=" + date_add +
                ", person_in_charge='" + person_in_charge + '\'' +
                ", id_stock_product=" + id_stock_product +
                '}';
    }

    public static StockAbandon findByEmail(String email)

    {
        return StockAbandon.find.where().eq("email", email).findUnique();
    }

    public static StockAbandon findById(int id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static StockAbandon findByReference(String reference) {return find.where().eq("reference", reference).findUnique();}

    /** Find orders of one customer, within 14 days, valid =1
     * @param id_customer
     * @param secure_key
     * */
    public  static  List<StockAbandon> findByCustomer(int id_customer, String secure_key) {
        long nowMinus2Weeks = System.currentTimeMillis() - 14 * 24 * (1000 * 60 * 60);
        Date lastTwoWeeks = new Date(nowMinus2Weeks);
        return find.where().eq("id_customer", id_customer).eq("secure_key", secure_key).eq("valid", 1).gt("invoice_date",lastTwoWeeks).orderBy("id_order desc").setMaxRows(10).findList();
    }

    /**
     * 根据产品ID 和搜索时间间隔，来查找丢货记录
     * */
    public  static  List<StockAbandon> findByProductDate(int id_product, Date search_start, Date search_end) {
        return find.where().eq("id_product", id_product).gt("date_add",search_start).lt("date_add",search_end).orderBy("id_stock_abandon desc").findList();
    }


    //get Orders to print
    public  static  List<StockAbandon> getPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 30;  //half an hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    //get Orders to print
    public  static  List<StockAbandon> getManualPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 60;  //one hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    public  static  List<StockAbandon> findByProduct(int id_product){
        return find.where().eq("id_product", id_product).orderBy("id_stock_abandon").findList();
    }

    public static List<StockAbandon> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (StockAbandon order : StockAbandon.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, StockAbandon> find = new Model.Finder<String, StockAbandon>(String.class, StockAbandon.class);

}
