/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

//CustomerGroup Class matches the table in the database, attributes names match corresponding column names
@SuppressWarnings("serial")
@Entity
@Table(name="ps_customer_group")
public class CustomerGroup extends Model
{
    @Id
    public int   id_customer;
    @Id
    public int   id_group;
    
    public CustomerGroup()
    {
    }

   

    public CustomerGroup(int id_customer, int id_group)
    {
      super();
      this.id_customer = id_customer;
      this.id_group = id_group;
    }



    public void update (CustomerGroup customerGroup)
    {
        this.id_customer = customerGroup.id_customer;
        this.id_group  = customerGroup.id_group;
   
    }



    @Override
    public void update(Object arg0)
    {
     super.update(arg0);
    }



    public  static int getGroupId(int id_customer) {
        return find.where().eq("id_customer", id_customer).findUnique().id_group;
    }

    public static List<CustomerGroup> findByCustomer(int id_customer) {
        return find.where().eq("id_customer", id_customer).orderBy("id_group").findList();
    }

    public static Model.Finder<String, CustomerGroup> find = new Model.Finder<String, CustomerGroup>(String.class, CustomerGroup.class);

    
}