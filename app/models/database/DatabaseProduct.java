/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
/**
 * Order Class matches table ps_orders, attributes corresponds to columns in the table
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_product")
public class DatabaseProduct extends Model
{
    @Id
    @GeneratedValue
    public int id_product;
    public int id_supplier;
    public int id_manufacturer;
    public int id_category_default;
    public int id_shop_default = 1;
    public int id_tax_rules_group;
    public int on_sale =0;
    public int online_only = 0;
    public String ean13;
    public String upc;
    public java.math.BigDecimal ecotax = BigDecimal.valueOf(0.000000);
    public int quantity = 0;
    public int minimal_quantity = 1;
    public java.math.BigDecimal price = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal wholesale_price = BigDecimal.valueOf(0.000000);
    public String unity;
    public java.math.BigDecimal unit_price_ratio = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal additional_shipping_cost = BigDecimal.valueOf(0.00);
    public String reference;
    public String supplier_reference;
    public String location;
    public java.math.BigDecimal width = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal height = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal depth = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal weight = BigDecimal.valueOf(0.000000);
    public int out_of_stock = 2;
    public int quantity_discount =0;
    public int customizable =0;
    public int uploadable_files= 0;
    public int text_fields = 0;
    public int active =0;
    public String redirect_type = "";
    public int id_product_redirected = 0;
    public int available_for_order = 1;
    public 	Date available_date;
    public String condition = "new";
    public int show_price =1;
    public int indexed = 0;
    public String visibility = "both";
    public int cache_is_pack = 0;
    public int cache_has_attachments = 0;
    public int is_virtual =0;
    public int cache_default_attribute;
    public Date date_add;
    public Date date_upd;
    public int advanced_stock_management = 0;
    public int pack_stock_type = 3;

    public DatabaseProduct()
    {

    }

    public DatabaseProduct(int id_supplier, int id_manufacturer, int id_category_default, int id_shop_default, int id_tax_rules_group, int on_sale, int online_only, String ean13, String upc, BigDecimal ecotax, int quantity, int minimal_quantity, BigDecimal price, BigDecimal wholesale_price, String unity, BigDecimal unit_price_ratio, BigDecimal additional_shipping_cost, String reference, String supplier_reference, String location, BigDecimal width, BigDecimal height, BigDecimal depth, BigDecimal weight, int out_of_stock, int quantity_discount, int customizable, int uploadable_files, int text_fields, int active, String redirect_type, int id_product_redirected, int available_for_order, Date available_date, String condition, int show_price, int indexed, String visibility, int cache_is_pack, int cache_has_attachments, int is_virtual, int cache_default_attribute, Date date_add, Date date_upd, int advanced_stock_management, int pack_stock_type) {
        this.id_supplier = id_supplier;
        this.id_manufacturer = id_manufacturer;
        this.id_category_default = id_category_default;
        this.id_shop_default = id_shop_default;
        this.id_tax_rules_group = id_tax_rules_group;
        this.on_sale = on_sale;
        this.online_only = online_only;
        this.ean13 = ean13;
        this.upc = upc;
        this.ecotax = ecotax;
        this.quantity = quantity;
        this.minimal_quantity = minimal_quantity;
        this.price = price;
        this.wholesale_price = wholesale_price;
        this.unity = unity;
        this.unit_price_ratio = unit_price_ratio;
        this.additional_shipping_cost = additional_shipping_cost;
        this.reference = reference;
        this.supplier_reference = supplier_reference;
        this.location = location;
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.weight = weight;
        this.out_of_stock = out_of_stock;
        this.quantity_discount = quantity_discount;
        this.customizable = customizable;
        this.uploadable_files = uploadable_files;
        this.text_fields = text_fields;
        this.active = active;
        this.redirect_type = redirect_type;
        this.id_product_redirected = id_product_redirected;
        this.available_for_order = available_for_order;
        this.available_date = available_date;
        this.condition = condition;
        this.show_price = show_price;
        this.indexed = indexed;
        this.visibility = visibility;
        this.cache_is_pack = cache_is_pack;
        this.cache_has_attachments = cache_has_attachments;
        this.is_virtual = is_virtual;
        this.cache_default_attribute = cache_default_attribute;
        this.date_add = date_add;
        this.date_upd = date_upd;
        this.advanced_stock_management = advanced_stock_management;
        this.pack_stock_type = pack_stock_type;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof DatabaseProduct)
        {
            final DatabaseProduct other = (DatabaseProduct) obj;
            return Objects.equal(reference, other.reference)
                    && Objects.equal(id_category_default, other.id_category_default)
                    && Objects.equal(id_manufacturer, other.id_manufacturer)
                    && Objects.equal(id_product, other.id_product)
                    && Objects.equal(id_product_redirected, other.id_product_redirected)
                    && Objects.equal(id_shop_default, other.id_shop_default)
                    && Objects.equal(indexed, other.indexed)
                    && Objects.equal(active, other.active)
                    && Objects.equal(additional_shipping_cost, other.additional_shipping_cost)
                    && Objects.equal(advanced_stock_management, other.advanced_stock_management)
                    && Objects.equal(available_date, other.available_date)
                    && Objects.equal(available_for_order, other.available_for_order)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(date_upd, other.date_upd);


        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "DatabaseProduct{" +
                "id_product=" + id_product +
                ", id_supplier=" + id_supplier +
                ", id_manufacturer=" + id_manufacturer +
                ", id_category_default=" + id_category_default +
                ", id_shop_default=" + id_shop_default +
                ", id_tax_rules_group=" + id_tax_rules_group +
                ", on_sale=" + on_sale +
                ", online_only=" + online_only +
                ", ean13='" + ean13 + '\'' +
                ", upc='" + upc + '\'' +
                ", ecotax=" + ecotax +
                ", quantity=" + quantity +
                ", minimal_quantity=" + minimal_quantity +
                ", price=" + price +
                ", wholesale_price=" + wholesale_price +
                ", unity='" + unity + '\'' +
                ", unit_price_ratio=" + unit_price_ratio +
                ", additional_shipping_cost=" + additional_shipping_cost +
                ", reference='" + reference + '\'' +
                ", supplier_reference='" + supplier_reference + '\'' +
                ", location='" + location + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", depth=" + depth +
                ", weight=" + weight +
                ", out_of_stock=" + out_of_stock +
                ", quantity_discount=" + quantity_discount +
                ", customizable=" + customizable +
                ", uploadable_files=" + uploadable_files +
                ", text_fields=" + text_fields +
                ", active=" + active +
                ", redirect_type='" + redirect_type + '\'' +
                ", id_product_redirected=" + id_product_redirected +
                ", available_for_order=" + available_for_order +
                ", available_date=" + available_date +
                ", condition='" + condition + '\'' +
                ", show_price=" + show_price +
                ", indexed=" + indexed +
                ", visibility='" + visibility + '\'' +
                ", cache_is_pack=" + cache_is_pack +
                ", cache_has_attachments=" + cache_has_attachments +
                ", is_virtual=" + is_virtual +
                ", cache_default_attribute=" + cache_default_attribute +
                ", date_add=" + date_add +
                ", date_upd=" + date_upd +
                ", advanced_stock_management=" + advanced_stock_management +
                ", pack_stock_type=" + pack_stock_type +
                '}';
    }

    public static DatabaseProduct findById(int id_product)
    {
        return find.where().eq("id_product", id_product).findUnique();
    }

    public static synchronized void updateOneRecord(int id_product, int active, int indexed){
        String dml = "UPDATE ps_product\n" +
                "SET active = :active, indexed = :indexed\n" +
                "WHERE id_product = :id_product";
        SqlUpdate update = Ebean.createSqlUpdate(dml)
                .setParameter("active", active)
                .setParameter("indexed", indexed)
                .setParameter("id_product", id_product);
        update.execute();

        String dml_2 = "UPDATE ps_product_shop\n" +
                "SET active = :active, indexed = :indexed\n" +
                "WHERE id_product = :id_product";
        SqlUpdate update_2 = Ebean.createSqlUpdate(dml_2)
                .setParameter("active", active)
                .setParameter("indexed", indexed)
                .setParameter("id_product", id_product);
        update_2.execute();
    }

    public static DatabaseProduct findByReference(String reference) {return find.where().eq("reference", reference).findUnique();}

    /** Find orders of one customer, within 14 days, valid =1
     * @param id_customer
     * @param secure_key
     * */
    public  static  List<DatabaseProduct> findByCustomer(int id_customer, String secure_key) {
        long nowMinus2Weeks = System.currentTimeMillis() - 14 * 24 * (1000 * 60 * 60);
        Date lastTwoWeeks = new Date(nowMinus2Weeks);
        return find.where().eq("id_customer", id_customer).eq("secure_key", secure_key).eq("valid", 1).gt("invoice_date",lastTwoWeeks).orderBy("id_order desc").setMaxRows(10).findList();
    }


    //get Orders to print
    public  static  List<DatabaseProduct> getPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 30;  //half an hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    //get Orders to print
    public  static  List<DatabaseProduct> getManualPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 60;  //one hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    /**
     * Find all orders within the last 14 hours, valid = 1
     * */
    public  static  List<DatabaseProduct> ordersForDriver(){
        long nowMinus1Day = System.currentTimeMillis() - 1 * 14 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneDay).orderBy("id_order desc").findList();
    }

    public static List<DatabaseProduct> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (DatabaseProduct order : DatabaseProduct.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, DatabaseProduct> find = new Model.Finder<String, DatabaseProduct>(String.class, DatabaseProduct.class);

}
