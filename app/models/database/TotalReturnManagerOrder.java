/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;
import java.math.BigDecimal;
@Entity
@Sql
public class TotalReturnManagerOrder {

    public int total_onlinepay_number;
    public int total_cashpay_number;
    public java.math.BigDecimal total_onlinepay_delivery = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_cashpay_delivery = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_onlinepay_sales = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_cashpay_sales  = BigDecimal.valueOf(0.000000);
    public String payment_method;

    public TotalReturnManagerOrder(int total_onlinepay_number, int total_cashpay_number, BigDecimal total_onlinepay_delivery, BigDecimal total_cashpay_delivery, BigDecimal total_onlinepay_sales, BigDecimal total_cashpay_sales, String payment_method) {
        this.total_onlinepay_number = total_onlinepay_number;
        this.total_cashpay_number = total_cashpay_number;
        this.total_onlinepay_delivery = total_onlinepay_delivery;
        this.total_cashpay_delivery = total_cashpay_delivery;
        this.total_onlinepay_sales = total_onlinepay_sales;
        this.total_cashpay_sales = total_cashpay_sales;
        this.payment_method = payment_method;
    }

    public TotalReturnManagerOrder() {

    }

    @Override
    public String toString() {
        return "TotalReturnManagerOrder{" +
                "total_onlinepay_number=" + total_onlinepay_number +
                ", total_cashpay_number=" + total_cashpay_number +
                ", total_onlinepay_delivery=" + total_onlinepay_delivery +
                ", total_cashpay_delivery=" + total_cashpay_delivery +
                ", total_onlinepay_sales=" + total_onlinepay_sales +
                ", total_cashpay_sales=" + total_cashpay_sales +
                ", payment_method='" + payment_method + '\'' +
                '}';
    }
}
