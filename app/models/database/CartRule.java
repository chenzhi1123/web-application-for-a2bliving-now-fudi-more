/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
//Driver class has the corresponding table in the database named "ps_drivers"
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_cart_rule")
public class CartRule extends Model
{
    @Id
    @GeneratedValue
    public int id_cart_rule;
    public int id_customer;
    public Date date_from;
    public Date date_to;
    public String description;
    public int quantity;
    public int quantity_per_user;
    public int priority;
    public int partial_use;
    public String code;
    public BigDecimal minimum_amount = BigDecimal.valueOf(0.00);
    public int minimum_amount_tax;
    public int minimum_amount_currency;
    public int minimum_amount_shipping;
    public int country_restriction;
    public int carrier_restriction;
    public int group_restriction;
    public int cart_rule_restriction;
    public int product_restriction;
    public int shop_restriction;
    public int free_shipping;
    public BigDecimal reduction_percent = BigDecimal.valueOf(0.00);
    public BigDecimal reduction_amount = BigDecimal.valueOf(0.00);
    public int reduction_tax;
    public int reduction_currency;
    public int reduction_product;
    public int gift_product;
    public int gift_product_attribute;
    @Column(name = "highlight")
    public int high_light;
    @Column(name = "active")
    public int if_active;
    public Date date_add;
    public Date date_upd;




    public CartRule()
    {

    }

    public CartRule(int id_customer, Date date_from, Date date_to, String description, int quantity, int quantity_per_user, int priority, int partial_use, String code, BigDecimal minimum_amount, int minimum_amount_tax, int minimum_amount_currency, int minimum_amount_shipping, int country_restriction, int carrier_restriction, int group_restriction, int cart_rule_restriction, int product_restriction, int shop_restriction, int free_shipping, BigDecimal reduction_percent, BigDecimal reduction_amount, int reduction_tax, int reduction_currency, int reduction_product, int gift_product, int gift_product_attribute, int high_light, int if_active, Date date_add, Date date_upd) {
        this.id_customer = id_customer;
        this.date_from = date_from;
        this.date_to = date_to;
        this.description = description;
        this.quantity = quantity;
        this.quantity_per_user = quantity_per_user;
        this.priority = priority;
        this.partial_use = partial_use;
        this.code = code;
        this.minimum_amount = minimum_amount;
        this.minimum_amount_tax = minimum_amount_tax;
        this.minimum_amount_currency = minimum_amount_currency;
        this.minimum_amount_shipping = minimum_amount_shipping;
        this.country_restriction = country_restriction;
        this.carrier_restriction = carrier_restriction;
        this.group_restriction = group_restriction;
        this.cart_rule_restriction = cart_rule_restriction;
        this.product_restriction = product_restriction;
        this.shop_restriction = shop_restriction;
        this.free_shipping = free_shipping;
        this.reduction_percent = reduction_percent;
        this.reduction_amount = reduction_amount;
        this.reduction_tax = reduction_tax;
        this.reduction_currency = reduction_currency;
        this.reduction_product = reduction_product;
        this.gift_product = gift_product;
        this.gift_product_attribute = gift_product_attribute;
        this.high_light = high_light;
        this.if_active = if_active;
        this.date_add = date_add;
        this.date_upd = date_upd;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof CartRule)
        {
            final CartRule other = (CartRule) obj;
            return Objects.equal(id_cart_rule, other.id_cart_rule)
                    && Objects.equal(id_customer, other.id_customer)
                    && Objects.equal(if_active, other.if_active)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(high_light, other.high_light)
                    && Objects.equal(minimum_amount, other.minimum_amount)
                    && Objects.equal(reduction_amount, other.reduction_amount);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "CartRule{" +
                "id_cart_rule=" + id_cart_rule +
                ", id_customer=" + id_customer +
                ", date_from=" + date_from +
                ", date_to=" + date_to +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                ", quantity_per_user=" + quantity_per_user +
                ", priority=" + priority +
                ", partial_use=" + partial_use +
                ", code='" + code + '\'' +
                ", minimum_amount=" + minimum_amount +
                ", minimum_amount_tax=" + minimum_amount_tax +
                ", minimum_amount_currency=" + minimum_amount_currency +
                ", minimum_amount_shipping=" + minimum_amount_shipping +
                ", country_restriction=" + country_restriction +
                ", carrier_restriction=" + carrier_restriction +
                ", group_restriction=" + group_restriction +
                ", cart_rule_restriction=" + cart_rule_restriction +
                ", product_restriction=" + product_restriction +
                ", shop_restriction=" + shop_restriction +
                ", free_shipping=" + free_shipping +
                ", reduction_percent=" + reduction_percent +
                ", reduction_amount=" + reduction_amount +
                ", reduction_tax=" + reduction_tax +
                ", reduction_currency=" + reduction_currency +
                ", reduction_product=" + reduction_product +
                ", gift_product=" + gift_product +
                ", gift_product_attribute=" + gift_product_attribute +
                ", high_light=" + high_light +
                ", if_active=" + if_active +
                ", date_add=" + date_add +
                ", date_upd=" + date_upd +
                '}';
    }

    public static CartRule findByName(String name)

    {
        return CartRule.find.where().eq("name", name).findUnique();
    }

    public static CartRule findById(int id)
    {
        return find.where().eq("id_cart_rule", id).findUnique();
    }

    public static int findByCode(String str){
        return find.where().eq("code",str).findUnique().id_cart_rule;
    }

    public static int getConstraintUser(String str){
        return find.where().eq("code",str).findUnique().id_customer;
    }


    /**
     * Check drivers list according to work state.
     * In the ps_drivers table, if_work column indicates working status.
     * @param if_work
     * */
    public  static List<CartRule> findByState(int if_work) {
        return find.where().eq("if_work", if_work).orderBy("id_driver").findList();
    }

    public static List<CartRule> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (CartRule driver : CartRule.findAll())
        {
            driver.delete();
        }
    }

    public static Model.Finder<String, CartRule> find = new Model.Finder<String, CartRule>(String.class, CartRule.class);
}
