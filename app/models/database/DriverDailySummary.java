/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
/**
 * DriverOrders class is with regard to a joint table, which records order-driver relation
 * Index keys are id_driver, id_order.
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_driver_daily_summary_flow")
public class DriverDailySummary extends Model
{
    @Id
    @GeneratedValue
    @Column(name = "id_driver_daily_summary_flow")
    public int id_driver_daily_summary_flow;

    @Column(name = "id_driver")
    public int id_driver;        // driver who deliveries this order

    @Column(name = "name")
    public String name;

    @Column(name = "working_hours")
    public BigDecimal working_hours = BigDecimal.valueOf(0.0);

    @Column(name = "hourly_wage")
    public BigDecimal hourly_wage = BigDecimal.valueOf(0.0);

    @Column(name = "total_wage")
    public BigDecimal total_wage = BigDecimal.valueOf(0.00);

    @Column(name = "money_from_till")
    public BigDecimal money_from_till = BigDecimal.valueOf(0.00);

    @Column(name = "total_sales_cash")
    public BigDecimal total_sales_cash = BigDecimal.valueOf(0.00);

    @Column(name = "extra_cost")
    public BigDecimal extra_cost = BigDecimal.valueOf(0.00);

    @Column(name = "specification")
    public String specification;

    @Column(name = "total_cash_left")
    public BigDecimal total_cash_left = BigDecimal.valueOf(0.00);

    @Column(name = "date_add")
    public Date date_add;

    @Column(name = "voucher_quantity")
    public int voucher_quantity;

    @Column(name = "total_voucher_value")
    public BigDecimal total_voucher_value = BigDecimal.valueOf(0.00);

    @Column(name = "marina_delivery")
    public BigDecimal marina_delivery = BigDecimal.valueOf(0.00);

    @Column(name = "cash_order")
    public Integer cash_order = 0;

    @Column(name = "online_order")
    public Integer online_order = 0;

    @Column(name = "if_company_car")
    public Integer if_company_car = 0;

    @Column(name = "cash_delivery_fee")
    public BigDecimal cash_delivery_fee = BigDecimal.valueOf(0.00);

    @Column(name = "online_delivery_fee")
    public BigDecimal online_delivery_fee = BigDecimal.valueOf(0.00);

    public DriverDailySummary()
    {

    }

    public DriverDailySummary(int id_driver, String name, BigDecimal working_hours, BigDecimal hourly_wage, BigDecimal total_wage, BigDecimal money_from_till, BigDecimal total_sales_cash, BigDecimal extra_cost, String specification, BigDecimal total_cash_left, Date date_add, int voucher_quantity, BigDecimal total_voucher_value, BigDecimal marina_delivery, Integer cash_order, Integer online_order, Integer if_company_car, BigDecimal cash_delivery_fee, BigDecimal online_delivery_fee) {
        this.id_driver = id_driver;
        this.name = name;
        this.working_hours = working_hours;
        this.hourly_wage = hourly_wage;
        this.total_wage = total_wage;
        this.money_from_till = money_from_till;
        this.total_sales_cash = total_sales_cash;
        this.extra_cost = extra_cost;
        this.specification = specification;
        this.total_cash_left = total_cash_left;
        this.date_add = date_add;
        this.voucher_quantity = voucher_quantity;
        this.total_voucher_value = total_voucher_value;
        this.marina_delivery = marina_delivery;
        this.cash_order = cash_order;
        this.online_order = online_order;
        this.if_company_car = if_company_car;
        this.cash_delivery_fee = cash_delivery_fee;
        this.online_delivery_fee = online_delivery_fee;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof DriverDailySummary)
        {
            final DriverDailySummary other = (DriverDailySummary) obj;
            return Objects.equal(id_driver, other.id_driver)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(name, other.name)
                    && Objects.equal(working_hours, other.working_hours)
                    && Objects.equal(hourly_wage, other.hourly_wage)
                    && Objects.equal(total_wage, other.total_wage)
                    && Objects.equal(money_from_till, other.money_from_till)
                    && Objects.equal(total_sales_cash, other.total_sales_cash)
                    && Objects.equal(extra_cost, other.extra_cost)
                    && Objects.equal(total_cash_left, other.total_cash_left)
                    && Objects.equal(total_voucher_value, other.total_voucher_value)
                    && Objects.equal(voucher_quantity, other.voucher_quantity)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(marina_delivery, other.marina_delivery)
                    && Objects.equal(cash_order, other.cash_order)
                    && Objects.equal(online_order, other.online_order)
                    && Objects.equal(cash_delivery_fee, other.cash_delivery_fee)
                    && Objects.equal(online_delivery_fee, other.online_delivery_fee)
                    && Objects.equal(if_company_car, other.if_company_car);
        }
        else
        {
            return false;
        }
    }

    public void update(BigDecimal working_hours, BigDecimal hourly_wage, BigDecimal total_wage, BigDecimal money_from_till, BigDecimal total_sales_cash, BigDecimal extra_cost, String specification, BigDecimal total_cash_left, Date date_add, int voucher_quantity, BigDecimal total_voucher_value, BigDecimal marina_delivery, Integer cash_order, Integer online_order, Integer if_company_car, BigDecimal cash_delivery_fee, BigDecimal online_delivery_fee){
        this.working_hours = working_hours;
        this.hourly_wage = hourly_wage;
        this.total_wage = total_wage;
        this.money_from_till = money_from_till;
        this.total_sales_cash = total_sales_cash;
        this.extra_cost = extra_cost;
        this.specification = specification;
        this.total_cash_left = total_cash_left;
        this.date_add = date_add;
        this.voucher_quantity = voucher_quantity;
        this.total_voucher_value = total_voucher_value;
        this.marina_delivery = marina_delivery;
        this.cash_order = cash_order;
        this.online_order = online_order;
        this.if_company_car = if_company_car;
        this.cash_delivery_fee = cash_delivery_fee;
        this.online_delivery_fee = online_delivery_fee;
    }

    @Override
    public String toString() {
        return "DriverDailySummary{" +
                "id_driver_daily_summary_flow=" + id_driver_daily_summary_flow +
                ", id_driver=" + id_driver +
                ", name='" + name + '\'' +
                ", working_hours=" + working_hours +
                ", hourly_wage=" + hourly_wage +
                ", total_wage=" + total_wage +
                ", money_from_till=" + money_from_till +
                ", total_sales_cash=" + total_sales_cash +
                ", extra_cost=" + extra_cost +
                ", specification='" + specification + '\'' +
                ", total_cash_left=" + total_cash_left +
                ", date_add=" + date_add +
                ", voucher_quantity=" + voucher_quantity +
                ", total_voucher_value=" + total_voucher_value +
                ", marina_delivery=" + marina_delivery +
                ", cash_order=" + cash_order +
                ", online_order=" + online_order +
                ", if_company_car=" + if_company_car +
                ", cash_delivery_fee=" + cash_delivery_fee +
                ", online_delivery_fee=" + online_delivery_fee +
                '}';
    }

    public static DriverDailySummary findByOrderId(int id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static DriverDailySummary findByBoth(int id_driver, int id_order)
    {
        return find.where().eq("id_driver", id_driver).eq("id_order", id_order).findUnique();
    }

    public static DriverDailySummary findByReference(String reference) {
        return find.where().eq("order_reference", reference).findUnique();
    }

    /** Find all the orders of one deliver
     *@param id_driver
     * */
    public  static List<DriverDailySummary> findByDriver(int id_driver) {
        return find.where().eq("id_driver", id_driver).orderBy("date_add desc").findList();
    }

    public  static List<DriverDailySummary> salary_of_day() {
        long nowMinus1Day = System.currentTimeMillis() - 1 * 12 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().gt("date_add",lastOneDay).findList();
    }

    public  static List<DriverDailySummary> findSalaryByDate(String startDate, String endDate) {
        return find.where().gt("date_add",startDate).lt("date_add",endDate).findList();
    }


    public  static DriverDailySummary recentRecord(int id_driver) {
        long nowMinus1Day = System.currentTimeMillis() - 1 * 12 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("id_driver", id_driver).gt("date_add",lastOneDay).findUnique();
    }



    public static List<DriverDailySummary> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (DriverDailySummary order : DriverDailySummary.findAll())
        {
            order.delete();
        }
    }

    public static void delete(int id_order)
    {
        find.where().eq("id_order", id_order).findUnique().delete();
    }

    public static Model.Finder<String, DriverDailySummary> find = new Model.Finder<String, DriverDailySummary>(String.class, DriverDailySummary.class);

}
