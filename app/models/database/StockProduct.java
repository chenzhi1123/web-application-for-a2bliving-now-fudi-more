/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;
/**
 * Order Class matches table ps_orders, attributes corresponds to columns in the table
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_stock_product")
public class StockProduct extends Model
{
    @Id
    @GeneratedValue
    public int id_stock_product;
    public int id_product;
    public Date date_add;
    public int if_active = 0;
    public int if_backup = 1;
    public int quantity;
    public Date expiry_date;
    public int original_quantity;
    public int valid = 1;
    public String person_in_charge = "";
    public Date last_update;
    public String on_sale_person = "";
    public int last_product_group;

    public StockProduct()
    {

    }

    public StockProduct(int id_product, Date date_add, int if_active, int if_backup, int quantity, Date expiry_date, int original_quantity, int valid, String person_in_charge, Date last_update, String on_sale_person, int last_product_group) {
        this.id_product = id_product;
        this.date_add = date_add;
        this.if_active = if_active;
        this.if_backup = if_backup;
        this.quantity = quantity;
        this.expiry_date = expiry_date;
        this.original_quantity = original_quantity;
        this.valid = valid;
        this.person_in_charge = person_in_charge;
        this.last_update = last_update;
        this.on_sale_person = on_sale_person;
        this.last_product_group = last_product_group;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof StockProduct)
        {
            final StockProduct other = (StockProduct) obj;
            return Objects.equal(id_product, other.id_product)
                    && Objects.equal(date_add, other.date_add)
                    && Objects.equal(if_active, other.if_active)
                    && Objects.equal(if_backup, other.if_backup)
                    && Objects.equal(quantity, other.quantity)
                    && Objects.equal(expiry_date, other.expiry_date)
                    && Objects.equal(original_quantity, other.original_quantity)
                    && Objects.equal(valid, other.valid)
                    && Objects.equal(person_in_charge, other.person_in_charge)
                    && Objects.equal(last_update, other.last_update)
                    && Objects.equal(last_product_group, other.last_product_group)
                    && Objects.equal(on_sale_person, other.on_sale_person);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "StockProduct{" +
                "id_stock_product=" + id_stock_product +
                ", id_product=" + id_product +
                ", date_add=" + date_add +
                ", if_active=" + if_active +
                ", if_backup=" + if_backup +
                ", quantity=" + quantity +
                ", expiry_date=" + expiry_date +
                ", original_quantity=" + original_quantity +
                ", valid=" + valid +
                ", person_in_charge='" + person_in_charge + '\'' +
                ", last_update=" + last_update +
                ", on_sale_person='" + on_sale_person + '\'' +
                ", last_product_group=" + last_product_group +
                '}';
    }

    public static StockProduct findById(int id)
    {
        return find.where().eq("id_stock_product", id).eq("valid", 1).findUnique();
    }

    public static StockProduct findOriginalById(int id)
    {
        return find.where().eq("id_stock_product", id).findUnique();
    }

    public static StockProduct findByReference(String reference) {return find.where().eq("reference", reference).findUnique();}

    /**
     * 根据产品ID 和搜索时间间隔，来查找丢货记录
     * */
    public  static  List<StockProduct> findAddRecord(int id_product,Date search_start, Date search_end) {
        return find.where().eq("id_product", id_product).gt("date_add",search_start).lt("date_add",search_end).orderBy("date_add desc").findList();
    }

    public  static  List<StockProduct> findValidStock(int id_product) {
        return find.where().eq("id_product", id_product).eq("if_active", 0).eq("if_backup", 1).eq("valid", 1).orderBy("expiry_date").findList();
    }

    public  static  StockProduct findCurrentStock(int id_product) {
        return find.where().eq("id_product", id_product).eq("if_active", 1).eq("if_backup", 0).eq("valid", 1).findUnique();
    }

    public  static  List<StockProduct> findThatGroup(int id_product, Date expiry_date) {
        return find.where().eq("id_product", id_product).eq("if_backup", 0).eq("expiry_date", expiry_date).eq("valid", 1).orderBy("id_stock_product").findList();
    }


    //get Orders to print
    public  static  List<StockProduct> getPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 30;  //half an hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    //get Orders to print
    public  static  List<StockProduct> getManualPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 60;  //one hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    /**
     * Find all orders within the last 14 hours, valid = 1
     * */
    public  static  List<StockProduct> ordersForDriver(){
        long nowMinus1Day = System.currentTimeMillis() - 1 * 14 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneDay).orderBy("id_order desc").findList();
    }

    public static List<StockProduct> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (StockProduct order : StockProduct.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, StockProduct> find = new Model.Finder<String, StockProduct>(String.class, StockProduct.class);

}
