/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_detail_tax")
public class OrderDetailTax extends Model
{
    @Id
    @GeneratedValue
    public int id_order_detail;
    public int id_tax;
    public java.math.BigDecimal unit_amount = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_amount = BigDecimal.valueOf(0.000000);

    public OrderDetailTax()
    {

    }

    public OrderDetailTax(int id_order_detail, int id_tax, BigDecimal unit_amount, BigDecimal total_amount) {
        this.id_order_detail = id_order_detail;
        this.id_tax = id_tax;
        this.unit_amount = unit_amount;
        this.total_amount = total_amount;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderDetail)
        {
            final OrderDetailTax other = (OrderDetailTax) obj;
            return Objects.equal(id_order_detail, other.id_order_detail)
                    && Objects.equal(id_tax, other.id_tax)
                    && Objects.equal(unit_amount, other.unit_amount)
                    && Objects.equal(total_amount, other.total_amount);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderDetailTax{" +
                "id_order_detail=" + id_order_detail +
                ", id_tax=" + id_tax +
                ", unit_amount=" + unit_amount +
                ", total_amount=" + total_amount +
                '}';
    }

    public static OrderDetailTax findByEmail(String email)

    {
        return OrderDetailTax.find.where().eq("email", email).findUnique();
    }

    public static OrderDetailTax findById(Long id)
    {
        return find.where().eq("id_order_detail", id).findUnique();
    }

    public static List<OrderDetailTax> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (OrderDetailTax orderDetailTax : OrderDetailTax.findAll())
        {
            orderDetailTax.delete();
        }
    }

    public static Model.Finder<String, OrderDetailTax> find = new Model.Finder<String, OrderDetailTax>(String.class, OrderDetailTax.class);


}
