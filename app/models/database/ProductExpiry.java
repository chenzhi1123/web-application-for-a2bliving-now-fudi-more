/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;
/**
 * Order Class matches table ps_orders, attributes corresponds to columns in the table
 * */
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_product_expiry")
public class ProductExpiry extends Model
{
    @Id
    @GeneratedValue
    public int id_product_expiry;
    public int id_product;
    public Date expiry_date;

    public ProductExpiry()
    {

    }

    public ProductExpiry(int id_product, Date expiry_date) {
        this.id_product = id_product;
        this.expiry_date = expiry_date;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof ProductExpiry)
        {
            final ProductExpiry other = (ProductExpiry) obj;
            return Objects.equal(id_product, other.id_product)
                    && Objects.equal(expiry_date, other.expiry_date);
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "ProductExpiry{" +
                "id_product_expiry=" + id_product_expiry +
                ", id_product=" + id_product +
                ", expiry_date=" + expiry_date +
                '}';
    }

    public static ProductExpiry findByEmail(String email)

    {
        return ProductExpiry.find.where().eq("email", email).findUnique();
    }

    public static ProductExpiry findById(int id)
    {
        return find.where().eq("id_order", id).findUnique();
    }

    public static ProductExpiry findByProduct(int id_product)
    {
        return find.where().eq("id_product", id_product).findUnique();
    }

    public static ProductExpiry findByReference(String reference) {return find.where().eq("reference", reference).findUnique();}

    /** Find orders of one customer, within 14 days, valid =1
     * @param id_customer
     * @param secure_key
     * */
    public  static  List<ProductExpiry> findByCustomer(int id_customer, String secure_key) {
        long nowMinus2Weeks = System.currentTimeMillis() - 14 * 24 * (1000 * 60 * 60);
        Date lastTwoWeeks = new Date(nowMinus2Weeks);
        return find.where().eq("id_customer", id_customer).eq("secure_key", secure_key).eq("valid", 1).gt("invoice_date",lastTwoWeeks).orderBy("id_order desc").setMaxRows(10).findList();
    }


    //get Orders to print
    public  static  List<ProductExpiry> getPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 30;  //half an hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    //get Orders to print
    public  static  List<ProductExpiry> getManualPrintOrders() {
        long nowMinus1Hour = System.currentTimeMillis() - 1000 * 60 * 60;  //one hour
        Date lastOneHour = new Date(nowMinus1Hour);
        long nowMinusSecond = System.currentTimeMillis() - 1000 * 10;   //10 seconds
        Date lastTenSeconds = new Date(nowMinusSecond);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneHour).lt("invoice_date",lastTenSeconds).orderBy("id_order desc").findList();
    }

    /**
     * Find all orders within the last 14 hours, valid = 1
     * */
    public  static  List<ProductExpiry> ordersForDriver(){
        long nowMinus1Day = System.currentTimeMillis() - 1 * 14 * (1000 * 60 * 60);
        Date lastOneDay = new Date(nowMinus1Day);
        return find.where().eq("valid", 1).gt("invoice_date",lastOneDay).orderBy("id_order desc").findList();
    }

    public static List<ProductExpiry> findAll()
    {
        return find.all();
    }

    public static void deleteAll()
    {
        for (ProductExpiry order : ProductExpiry.findAll())
        {
            order.delete();
        }
    }

    public static Model.Finder<String, ProductExpiry> find = new Model.Finder<String, ProductExpiry>(String.class, ProductExpiry.class);

}
