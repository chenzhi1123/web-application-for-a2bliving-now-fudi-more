/*
 * Copyright (c) 2017. Zhi Chen.
 */

package models.database;

import com.google.common.base.Objects;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
@SuppressWarnings("serial")
@Entity
@Table(name = "ps_order_detail")
public class OrderDetail extends Model
{
    @Id
    @GeneratedValue
    public int id_order_detail;
    public int id_order;
    public int id_order_invoice;
    public int id_warehouse = 0;
    public int id_shop;
    public int product_id;
    public int product_attribute_id;
    public String product_name;
    public int product_quantity = 0;
    public int product_quantity_in_stock = 0;
    public int product_quantity_refunded = 0;
    public int product_quantity_return = 0;
    public int product_quantity_reinjected = 0;
    public java.math.BigDecimal product_price = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal reduction_percent = BigDecimal.valueOf(0.00);
    public java.math.BigDecimal reduction_amount = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal reduction_amount_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal reduction_amount_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal group_reduction = BigDecimal.valueOf(0.00);
    public java.math.BigDecimal product_quantity_discount = BigDecimal.valueOf(0.000000);
    public String product_ean13 = null;
    public String product_upc = null;
    public String product_reference = null;
    public String product_supplier_reference = null;
    public java.math.BigDecimal product_weight;
    public int id_tax_rules_group = 0 ;
    public int tax_computation_method = 0;
    public String tax_name;
    public java.math.BigDecimal tax_rate = BigDecimal.valueOf(0.000);
    public java.math.BigDecimal ecotax = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal ecotax_tax_rate = BigDecimal.valueOf(0.000);
    public int discount_quantity_applied = 0;
    public String download_hash = null;
    public int download_nb = 0;
    public Date download_deadline = null;
    public java.math.BigDecimal total_price_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_price_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal unit_price_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal unit_price_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping_price_tax_incl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal total_shipping_price_tax_excl = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal purchase_supplier_price = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal original_product_price = BigDecimal.valueOf(0.000000);
    public java.math.BigDecimal original_wholesale_price = BigDecimal.valueOf(0.000000);



    public OrderDetail()
    {

    }

    public OrderDetail(int id_order, int id_order_invoice, int id_warehouse, int id_shop, int product_id, int product_attribute_id, String product_name, int product_quantity, int product_quantity_in_stock, int product_quantity_refunded, int product_quantity_return, int product_quantity_reinjected, BigDecimal product_price, BigDecimal reduction_percent, BigDecimal reduction_amount, BigDecimal reduction_amount_tax_incl, BigDecimal reduction_amount_tax_excl, BigDecimal group_reduction, BigDecimal product_quantity_discount, String product_ean13, String product_upc, String product_reference, String product_supplier_reference, BigDecimal product_weight, int id_tax_rules_group, int tax_computation_method, String tax_name, BigDecimal tax_rate, BigDecimal ecotax, BigDecimal ecotax_tax_rate, int discount_quantity_applied, String download_hash, int download_nb, Date download_deadline, BigDecimal total_price_tax_incl, BigDecimal total_price_tax_excl, BigDecimal unit_price_tax_incl, BigDecimal unit_price_tax_excl, BigDecimal total_shipping_price_tax_incl, BigDecimal total_shipping_price_tax_excl, BigDecimal purchase_supplier_price, BigDecimal original_product_price, BigDecimal original_wholesale_price) {
        this.id_order = id_order;
        this.id_order_invoice = id_order_invoice;
        this.id_warehouse = id_warehouse;
        this.id_shop = id_shop;
        this.product_id = product_id;
        this.product_attribute_id = product_attribute_id;
        this.product_name = product_name;
        this.product_quantity = product_quantity;
        this.product_quantity_in_stock = product_quantity_in_stock;
        this.product_quantity_refunded = product_quantity_refunded;
        this.product_quantity_return = product_quantity_return;
        this.product_quantity_reinjected = product_quantity_reinjected;
        this.product_price = product_price;
        this.reduction_percent = reduction_percent;
        this.reduction_amount = reduction_amount;
        this.reduction_amount_tax_incl = reduction_amount_tax_incl;
        this.reduction_amount_tax_excl = reduction_amount_tax_excl;
        this.group_reduction = group_reduction;
        this.product_quantity_discount = product_quantity_discount;
        this.product_ean13 = product_ean13;
        this.product_upc = product_upc;
        this.product_reference = product_reference;
        this.product_supplier_reference = product_supplier_reference;
        this.product_weight = product_weight;
        this.id_tax_rules_group = id_tax_rules_group;
        this.tax_computation_method = tax_computation_method;
        this.tax_name = tax_name;
        this.tax_rate = tax_rate;
        this.ecotax = ecotax;
        this.ecotax_tax_rate = ecotax_tax_rate;
        this.discount_quantity_applied = discount_quantity_applied;
        this.download_hash = download_hash;
        this.download_nb = download_nb;
        this.download_deadline = download_deadline;
        this.total_price_tax_incl = total_price_tax_incl;
        this.total_price_tax_excl = total_price_tax_excl;
        this.unit_price_tax_incl = unit_price_tax_incl;
        this.unit_price_tax_excl = unit_price_tax_excl;
        this.total_shipping_price_tax_incl = total_shipping_price_tax_incl;
        this.total_shipping_price_tax_excl = total_shipping_price_tax_excl;
        this.purchase_supplier_price = purchase_supplier_price;
        this.original_product_price = original_product_price;
        this.original_wholesale_price = original_wholesale_price;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof OrderDetail)
        {
            final OrderDetail other = (OrderDetail) obj;
            return Objects.equal(id_order, other.id_order)
                    && Objects.equal(id_order_invoice, other.id_order_invoice)
                    && Objects.equal(id_warehouse, other.id_warehouse)
                    && Objects.equal(id_shop, other.id_shop)
                    && Objects.equal(product_id, other.product_id)
                    && Objects.equal(product_attribute_id, other.product_attribute_id)
                    && Objects.equal(product_name, other.product_name)
                    && Objects.equal(product_quantity, other.product_quantity)
                    && Objects.equal(product_quantity_in_stock, other.product_quantity_in_stock)
                    && Objects.equal(product_quantity_refunded, other.product_quantity_refunded)
                    && Objects.equal(product_quantity_return, other.product_quantity_return)
                    && Objects.equal(product_quantity_reinjected, other.product_quantity_reinjected)
                    && Objects.equal(product_price, other.product_price)
                    && Objects.equal(reduction_percent, other.reduction_percent)
                    && Objects.equal(reduction_amount, other.reduction_amount)
                    && Objects.equal(reduction_amount_tax_incl, other.reduction_amount_tax_incl)
                    && Objects.equal(reduction_amount_tax_excl, other.reduction_amount_tax_excl)
                    && Objects.equal(group_reduction, other.group_reduction)
                    && Objects.equal(product_quantity_discount, other.product_quantity_discount)
                    && Objects.equal(product_ean13, other.product_ean13)
                    && Objects.equal(product_upc, other.product_upc)
                    && Objects.equal(product_reference, other.product_reference)
                    && Objects.equal(product_supplier_reference, other.product_supplier_reference)
                    && Objects.equal(product_weight, other.product_weight)
                    && Objects.equal(id_tax_rules_group, other.id_tax_rules_group)
                    && Objects.equal(tax_computation_method, other.tax_computation_method)
                    && Objects.equal(tax_name, other.tax_name)
                    && Objects.equal(tax_rate, other.tax_rate)
                    && Objects.equal(ecotax, other.ecotax)
                    && Objects.equal(ecotax_tax_rate, other.ecotax_tax_rate)
                    && Objects.equal(discount_quantity_applied, other.discount_quantity_applied)
                    && Objects.equal(download_hash, other.download_hash)
                    && Objects.equal(download_nb, other.download_nb)
                    && Objects.equal(download_deadline, other.download_deadline)
                    && Objects.equal(total_price_tax_incl, other.total_price_tax_incl)
                    && Objects.equal(total_price_tax_excl, other.total_price_tax_excl)
                    && Objects.equal(unit_price_tax_incl, other.unit_price_tax_incl)
                    && Objects.equal(unit_price_tax_excl, other.unit_price_tax_excl)
                    && Objects.equal(total_shipping_price_tax_incl, other.total_shipping_price_tax_incl)
                    && Objects.equal(total_shipping_price_tax_excl, other.total_shipping_price_tax_excl)
                    && Objects.equal(purchase_supplier_price, other.purchase_supplier_price)
                    && Objects.equal(original_product_price, other.original_product_price)
                    && Objects.equal(original_wholesale_price, other.original_wholesale_price);


        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "id_order_detail=" + id_order_detail +
                ", id_order=" + id_order +
                ", id_order_invoice=" + id_order_invoice +
                ", id_warehouse=" + id_warehouse +
                ", id_shop=" + id_shop +
                ", product_id=" + product_id +
                ", product_attribute_id=" + product_attribute_id +
                ", product_name='" + product_name + '\'' +
                ", product_quantity=" + product_quantity +
                ", product_quantity_in_stock=" + product_quantity_in_stock +
                ", product_quantity_refunded=" + product_quantity_refunded +
                ", product_quantity_return=" + product_quantity_return +
                ", product_quantity_reinjected=" + product_quantity_reinjected +
                ", product_price=" + product_price +
                ", reduction_percent=" + reduction_percent +
                ", reduction_amount=" + reduction_amount +
                ", reduction_amount_tax_incl=" + reduction_amount_tax_incl +
                ", reduction_amount_tax_excl=" + reduction_amount_tax_excl +
                ", group_reduction=" + group_reduction +
                ", product_quantity_discount=" + product_quantity_discount +
                ", product_ean13='" + product_ean13 + '\'' +
                ", product_upc='" + product_upc + '\'' +
                ", product_reference='" + product_reference + '\'' +
                ", product_supplier_reference='" + product_supplier_reference + '\'' +
                ", product_weight=" + product_weight +
                ", id_tax_rules_group=" + id_tax_rules_group +
                ", tax_computation_method=" + tax_computation_method +
                ", tax_name='" + tax_name + '\'' +
                ", tax_rate=" + tax_rate +
                ", ecotax=" + ecotax +
                ", ecotax_tax_rate=" + ecotax_tax_rate +
                ", discount_quantity_applied=" + discount_quantity_applied +
                ", download_hash='" + download_hash + '\'' +
                ", download_nb=" + download_nb +
                ", download_deadline=" + download_deadline +
                ", total_price_tax_incl=" + total_price_tax_incl +
                ", total_price_tax_excl=" + total_price_tax_excl +
                ", unit_price_tax_incl=" + unit_price_tax_incl +
                ", unit_price_tax_excl=" + unit_price_tax_excl +
                ", total_shipping_price_tax_incl=" + total_shipping_price_tax_incl +
                ", total_shipping_price_tax_excl=" + total_shipping_price_tax_excl +
                ", purchase_supplier_price=" + purchase_supplier_price +
                ", original_product_price=" + original_product_price +
                ", original_wholesale_price=" + original_wholesale_price +
                '}';
    }

    public static OrderDetail findByEmail(String email)

    {
        return OrderDetail.find.where().eq("email", email).findUnique();
    }

    public static OrderDetail findById(int id)
    {
        return find.where().eq("id_order_detail", id).findUnique();
    }
    public static List<OrderDetail> findAll()
    {
        return find.all();
    }


    /**
     * Find all the order details of one order
     * @param id_order each order has a unique order ID
     * */
    public  static  List<OrderDetail> findByOrder(int id_order) {
        return find.where().eq("id_order", id_order).findList();
    }

    public static void deleteAll()
    {
        for (OrderDetail orderDetail : OrderDetail.findAll())
        {
            orderDetail.delete();
        }
    }
    public static Model.Finder<String, OrderDetail> find = new Model.Finder<String, OrderDetail>(String.class, OrderDetail.class);

}
