/*
 * Copyright (c) 2017. Zhi Chen.
 * Json converter utility class
 */

package parsers;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import models.database.Customer;
import models.database.Driver;
import models.socketControl.MapAnime;
import models.sqlContainer.Categories;
import models.sqlContainer.Category;
import models.sqlContainer.Product;
import models.template.*;

import java.util.ArrayList;
import java.util.List;

//import models.database.Customer;
//import models.sqlContainer.Product;

public class JsonParser
{
  //Initiate JSONSerializer object
  private static JSONSerializer  customerSerializer     = new JSONSerializer();
  private static JSONSerializer  productSerializer     = new JSONSerializer();
  private static JSONSerializer  categorySerializer     = new JSONSerializer();
  private static JSONSerializer  categoriesSerializer     = new JSONSerializer();
  private static JSONSerializer  addressSerializer     = new JSONSerializer();
  private static JSONSerializer  driverSerializer     = new JSONSerializer();

 /**
  * Convert an object to Json String
  * @param obj
  * @return Json string
  * */
  public static String renderAddress(Object obj){
        return addressSerializer.serialize(obj);
  }


  /**
   * Convert Json String and parse it to an object
   * of a Certain Class.
   * @param json
   * @return Object
   * */
  public static Product renderProduct(String json)
  {
    return new JSONDeserializer<Product>().deserialize(json, Product.class); 
  }

  public static String renderProduct(Object obj)
  {
    return productSerializer.serialize(obj);
  }
  
  
  public static Customer renderCustomer(String json)
  {
    return new JSONDeserializer<Customer>().deserialize(json, Customer.class);
  }

  public static String renderCustomer(Object obj)
  {
    return customerSerializer.serialize(obj);
  }
  
  public static Category renderCategory(String json)
  {
    return new JSONDeserializer<Category>().deserialize(json, Category.class); 
  }

  public static String renderCategory(Object obj)
  {
    return categorySerializer.serialize(obj);
  }
  
  
  public static Categories renderCategories(String json)
  {
    return new JSONDeserializer<Categories>().deserialize(json, Categories.class);
  }


    /**Deep Json serializer, this is for complex object which may contain more then two hierarchies of data
    * @param obj
    * @return  Json String
    * */
   public static String renderCategories(Object obj)
  {
      return categoriesSerializer.deepSerialize(obj);
  }

  public static AddressTemplate renderAddressTemplate(String json)
  {
    return new JSONDeserializer<AddressTemplate>().deserialize(json, AddressTemplate.class);
  }


    public static TotalOrder renderTotalOrder(String json)
    {
    return new JSONDeserializer<TotalOrder>().deserialize(json, TotalOrder.class);
    }

    public static ReturnInformation renderReturnInformation(String json)
    {
    return new JSONDeserializer<ReturnInformation>().deserialize(json, ReturnInformation.class);
    }

//    public static DriverOrders renderDriverOrder(String json)
//    {
//    return new JSONDeserializer<DriverOrders>().deserialize(json, DriverOrders.class);
//    }
    public static ArrayList<RequestDriverOrders> renderRequestDriverTemplate(String json)
    {
//        ArrayList<RequestDriverOrders> result = (ArrayList<RequestDriverOrders>) new JSONDeserializer<ArrayList<RequestDriverOrders>>().deserialize( json, RequestDriverOrders.class );
        ArrayList<RequestDriverOrders> result = new JSONDeserializer<ArrayList<RequestDriverOrders>>()
                .use("values", RequestDriverOrders.class)
                .deserialize(json);
        return result;
    }

    public static RequestDriverOrders renderRequestDriverOrders(String json)
    {
        return new JSONDeserializer<RequestDriverOrders>().deserialize(json, RequestDriverOrders.class);
    }

    /**
     * Convert an object to Json String
     * @param obj
     * @return Json string
     * */
    public static String renderDriver(Object obj){
        return driverSerializer.serialize(obj);
    }

    /**Deep Json serializer, this is for complex object which may contain more then two hierarchies of data
     * @param obj
     * @return  Json String
     * */
    public static String renderDeepDriver(Object obj)
    {
        return driverSerializer.deepSerialize(obj);
    }

    /**
     * Convert Json String and parse it to an object
     * of a Certain Class.
     * @param json
     * @return Object
     * */
    public static Driver convertToDriver(String json)
    {
        return new JSONDeserializer<Driver>().deserialize(json, Driver.class);
    }

    public static ArrayList<Driver> renderArrayDriver(String json)
    {
        ArrayList<Driver> result = new JSONDeserializer<ArrayList<Driver>>()
                .use("values", Driver.class)
                .deserialize(json);
        return result;
    }

    public static List<Driver> renderListDriver(String json)
    {
        List<Driver> result = new JSONDeserializer<List<Driver>>()
                .use("values", Driver.class)
                .deserialize(json);
        return result;
    }

    public static List<MapAnime.MoveMessage> renderMoves(String json)
    {
        List<MapAnime.MoveMessage> result = new JSONDeserializer<List<MapAnime.MoveMessage>>()
                .use("values", MapAnime.MoveMessage.class)
                .deserialize(json);
        return result;
    }

    public static List<Move> renderDriverMoves(String json)
    {
        List<Move> result = new JSONDeserializer<List<Move>>()
                .use("values", Move.class)
                .deserialize(json);
        return result;
    }

    public static Move convertToMoveMessage(String json)
    {
        return new JSONDeserializer<Move>().deserialize(json, Move.class);
    }



}