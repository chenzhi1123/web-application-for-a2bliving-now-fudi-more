/**
 * This file is located under app/assets, so it is compiled
 * by Google Closure Compiler
 * see https://github.com/playframework/Play20/wiki/AssetsGoogleClosureCompiler
 *
 */
function addKMLLayer(aMap, aKMLPath) {

    var layer = new OpenLayers.Layer.Vector(aKMLPath, {
        strategies: [new OpenLayers.Strategy.Fixed()],
        protocol: new OpenLayers.Protocol.HTTP({
            url: "/assets/kml/" + aKMLPath,
            format: new OpenLayers.Format.KML({
                extractAttributes: true,
                maxDepth: 2
            })
        }),
        transitionEffect: 'resize'
    });

    aMap.addLayer(layer);

    return layer;
}

function moveMarker(aMap, aMarker, aId, aLon, aLat,aStatus) {

    if (aMarker != null) {
        // Google.v3 uses EPSG:900913 as projection, so we have to
        // transform our coordinates
        var newLonLat = new OpenLayers.LonLat(aLon, aLat).transform(
            new OpenLayers.Projection("EPSG:4326"),
            aMap.getProjectionObject());
        var newPx = aMap.getLayerPxFromLonLat(newLonLat);
        aMarker.moveTo(newPx);
        if(aStatus == "In delivery"){
            aMarker.setUrl('/assets/images/map-marker.png');
        }else if(aStatus == "Customer"){
            aMarker.setUrl('/assets/images/you.png');
        }else if(aStatus == "Driver"){
            aMarker.setUrl('/assets/images/map-marker.png');
        }else {
            aMarker.setUrl('/assets/images/green-icon.png');
        }
    }

    return aMarker;

}

function createMarker(aMap, aId, aLon, aLat ,aName, aStatus) {
    // Google.v3 uses EPSG:900913 as projection, so we have to
    // transform our coordinates
    var newLonLat = new OpenLayers.LonLat(aLon, aLat).transform(
        new OpenLayers.Projection("EPSG:4326"),
        aMap.getProjectionObject());

    var size = new OpenLayers.Size(32, 37);
    var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
    var icon_path;
    if (aStatus == "In delivery") {
        icon_path = '/assets/images/map-marker.png';
    }else if(aStatus == "Customer"){
        icon_path = '/assets/images/you.png';
    }else if(aStatus == "Driver"){
        icon_path = '/assets/images/map-marker.png';
    }else {
        icon_path = '/assets/images/green-icon.png';
    }
    var icon = new OpenLayers.Icon(icon_path,size,offset);
    var marker = new OpenLayers.Marker(newLonLat,icon);
    marker.map = aMap;
    marker.id = aId;
    var div = document.createElement('div');
    var invisible_id = document.createElement('div');
    div.style = "text-align: center; margin-top: -10px; font-weight:bold; font-size:13px;z-index:1";
    invisible_id.style = "display:none";
    invisible_id.innerHTML = aId;
    div.innerHTML = aName;
    marker.icon.imageDiv.appendChild(div).appendChild(invisible_id);
    aMap.getLayer("Markers").addMarker(marker);
    return marker;

}

function createRow(aName, aTodo, aDoing ,aStatus,aLastAccess) {
    //make a table reference
    var tableRef = document.getElementById('driver_lists');
    var row = tableRef.insertRow(1);
    row.id = aName;
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    cell5.id = aName + "_timer";
    cell1.innerHTML = aName;
    cell2.innerHTML = aTodo;
    cell3.innerHTML = aDoing;
    cell4.innerHTML = aStatus;
    var totalSeconds = parseInt(aLastAccess/1000);
    hashMap.Set(aName,totalSeconds);  //存入时间，作为初始value
    setInterval(countTimer, 1000); //一秒钟执行一次
    function countTimer() {
        var driverSeconds = hashMap.Get(aName); //get current value
        ++driverSeconds;//increase value
        document.getElementById(aName+"_timer").innerHTML = driverSeconds + " seconds ago...";
        hashMap.Set(aName,driverSeconds);//overwrite the former value
    }

}



function updateRow(aName, aTodo, aDoing ,aStatus,aLastAccess) {
    var older_row = document.getElementById(aName);
    var cell2 = older_row.cells[1];
    var cell3 = older_row.cells[2];
    var cell4 = older_row.cells[3];
    // var cell5 = older_row.cells[4];
    cell2.innerHTML = aTodo;
    cell3.innerHTML = aDoing;
    cell4.innerHTML = aStatus;
    // cell5.innerHTML = parseInt(aLastAccess/1000) + " seconds ago...";
    hashMap.Set(aName,parseInt(aLastAccess/1000));//overwrite the former value
}

function initMap() {

    var map = new OpenLayers.Map("map", {
        numZoomLevels: 19
    });
    var mapnik = new OpenLayers.Layer.OSM();
    map.addLayer(mapnik);
    // addKMLLayer(map, "bdx_buildingshape.kml");
    // addKMLLayer(map, "bdx_emergencyroadshape.kml");
    // addKMLLayer(map, "bdx_periphroadshape.kml");
    // addKMLLayer(map, "bdx_rwyshape.kml");
    // addKMLLayer(map, "bdx_servroadshape.kml");
    // addKMLLayer(map, "bdx_standshape.kml");

    var markers = new OpenLayers.Layer.Markers( "Markers" );
    markers.id = "Markers";
    map.addLayer(markers);

    // Google.v3 uses EPSG:900913 as projection, so we have to
    // transform our coordinates
    map.setCenter(new OpenLayers.LonLat(-8.472075, 51.884535).transform(
        new OpenLayers.Projection("EPSG:4326"),
        map.getProjectionObject()
    ), 13);

    // // list layers
    // $.each(map.layers, function(index, value) {
    // 	$('#layersForm').append('<label class="checkbox"><input type="checkbox" value="' + index + '" checked> ' + value.name + '</label>');
    // });
    //
    // // add a click listener on each layer checkbox
    // $("#layersForm input:checkbox").change(function() {
    // 	  var target = $(this);
    // 	  map.layers[target.prop("value")].setVisibility(target.prop("checked"));
    //
    // });
    return map;
}

function initCustomerMap(customerlon, customerlat) {

    var map = new OpenLayers.Map("map", {
        numZoomLevels: 19
    });
    var mapnik = new OpenLayers.Layer.OSM();
    map.addLayer(mapnik);
    // addKMLLayer(map, "bdx_buildingshape.kml");
    // addKMLLayer(map, "bdx_emergencyroadshape.kml");
    // addKMLLayer(map, "bdx_periphroadshape.kml");
    // addKMLLayer(map, "bdx_rwyshape.kml");
    // addKMLLayer(map, "bdx_servroadshape.kml");
    // addKMLLayer(map, "bdx_standshape.kml");

    var markers = new OpenLayers.Layer.Markers( "Markers" );
    markers.id = "Markers";
    map.addLayer(markers);

    // Google.v3 uses EPSG:900913 as projection, so we have to
    // transform our coordinates
    map.setCenter(new OpenLayers.LonLat(customerlon, customerlat).transform(
        new OpenLayers.Projection("EPSG:4326"),
        map.getProjectionObject()
    ), 14);

    // // list layers
    // $.each(map.layers, function(index, value) {
    // 	$('#layersForm').append('<label class="checkbox"><input type="checkbox" value="' + index + '" checked> ' + value.name + '</label>');
    // });
    //
    // // add a click listener on each layer checkbox
    // $("#layersForm input:checkbox").change(function() {
    // 	  var target = $(this);
    // 	  map.layers[target.prop("value")].setVisibility(target.prop("checked"));
    //
    // });
    return map;
}