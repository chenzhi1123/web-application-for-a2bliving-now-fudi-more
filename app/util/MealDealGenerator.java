/*
 * Copyright (c) 2017. Zhi Chen.
 */

package util;

import models.database.CustomerGroup;
import models.database.DealProductsTable;

import java.io.*;

/**
 * Created by 20060 on 2017/6/2.
 */
public class MealDealGenerator {

    public static void generate_table_value(){
        File file = play.Play.application().getFile("/conf/templates/mealDeal.csv");
    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ",";
    try {
        FileReader fr = new FileReader(file);
        br = new BufferedReader(fr);
        while ((line = br.readLine()) != null) {
            // use comma as separator
            String[] customer = line.split(cvsSplitBy);
            int a = Integer.valueOf(customer[0]);
            int b = Integer.valueOf(customer[1]);
            int c = Integer.valueOf(customer[2]);
            String key_word = customer[3];

            DealProductsTable new_one = new DealProductsTable(a,b,c,key_word);
            new_one.save();
            System.out.println(customer[0] + " "+ customer[1] + " "+ customer[2] + " " + customer[3]);
        }
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        if (br != null) {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    }

    public static void generate_customer_group(){
        File file = play.Play.application().getFile("/conf/templates/customer_id.csv");
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        try {
            FileReader fr = new FileReader(file);
            br = new BufferedReader(fr);
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] customer = line.split(cvsSplitBy);
                int a = Integer.valueOf(customer[0]);
                CustomerGroup new_one = new CustomerGroup(a,4);
                new_one.save();
                System.out.println(customer[0]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}


