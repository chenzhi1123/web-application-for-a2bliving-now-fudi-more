/*
 * Copyright (c) 2017. Zhi Chen.
 */

package util;

public class ThreadController implements Runnable {

    String combine;

    public ThreadController(String combine){
        this.combine = combine;
    }
    @Override
    public void run() {
        try{
            HttpRequest.sendPost(combine);
        } catch (Exception e){
            System.out.println("cannot update user login time");
        }
    }



}
