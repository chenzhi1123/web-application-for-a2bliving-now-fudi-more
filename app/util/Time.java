/*
 * Copyright (c) 2017. Zhi Chen.
 */

package util;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Time
{

  public String getDateTime() {
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    Date date = new Date();
    return dateFormat.format(date);
  
  }
  
  public static Date convertStringToDate(String dateString) throws Exception{
    
      SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
      Date date = formatter.parse(dateString);
      return date;

  }
  
}
