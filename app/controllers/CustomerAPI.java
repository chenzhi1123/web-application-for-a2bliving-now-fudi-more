package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.database.Driver;
import models.socketControl.CustomerAction;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import views.html.customerMap;
import views.html.customerMapInfo;

import java.util.List;

import static models.socketControl.CustomerAction.driver_customer;
import static parsers.JsonParser.renderDriver;
//import flexjson.JSONDeserializer;

public class CustomerAPI extends Controller {

	public static Result customerMap(int customer_id, int order_id, float latitude, float longitude) {
		if(customer_id == 0 || order_id ==0) {
			return badRequest("Invalid customer or order");
		}else{
			//如果这一单正在被司机配送
            if(driver_customer.containsValue(customer_id)){
                return ok(customerMap.render(customer_id,order_id,latitude,longitude));
            }else {
                //显示一个页面，告诉顾客，他的订单已经被送过了
                return ok(customerMapInfo.render());
            }
		}
	}

	/**
	 * Handle the map websocket.
	 */
	public static WebSocket<JsonNode> customerSocket(int customer_id, int order_id) {
		return new WebSocket<JsonNode>() {
			// Called when the Websocket Handshake is done.
			@Override
			public void onReady(final In<JsonNode> in, final Out<JsonNode> out) {
				try {
					CustomerAction.customerConnect(java.util.UUID.randomUUID().toString(),customer_id, order_id,
							in, out);  //地图连接socket 需要注册
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};
	}

	public static Result showDrivers(){
		List<Driver> all_drivers = Driver.findAll();
		return ok(renderDriver(all_drivers));
	}

}