/*
 * Copyright (c) 2017. Zhi Chen.
 * Application Controller is used for user to register and login,
 * through HTTP request. For convenience, Login process uses Play
 * Form feature, while Register process uses DynamicForm utility
 */

package controllers;

import com.avaje.ebean.annotation.Transactional;
import models.database.*;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import util.EmailSender;
import util.MD5;
import util.MealDealGenerator;
import util.TimeStamp;
import views.html.index;

import java.util.Date;
import java.util.List;

import static controllers.ProductAPI.reGenerateCache;
import static parsers.JsonParser.renderCategories;
import static parsers.JsonParser.renderCustomer;

public class Application extends Controller {
    /**
     * API Test page
     * Always valid to check if the api is working smoothly
     * @return customer info/ view_page for index
     * */
    public static Result index() throws InterruptedException {
    //check the existance of email address in the session
    String email = session("email");
    Customer customer = Customer.findByEmail(email);
    if (email != null) {
    return customer==null? notFound() : ok(renderCustomer(customer));
    } else {
    return ok(index.render("please log in"));
    }
    }
    /**
     * Customer Registration class
     * Compulsory information at registration
     *@deprecated
     **/
    public static class Registration{
      public String company;
      @Required
      public String firstname;
      @Required
      public String lastname;
      @Email
      public String email;
      @Required
      public String passwd;
      public String birthday;
      public int newsletter;

}

  /**
   * Mobile users register action
   * This API receives a registration form from HTTP request
   * */
   @Transactional
   public static Result postRegister() throws Exception {
       DynamicForm customerForm = Form.form().bindFromRequest();
//     Form<Registration> customerForm = Form.form(Registration.class).bindFromRequest();
//     String model = customerForm.get("birthday");
       String a = request().remoteAddress();
       String email_address = customerForm.get("email");
       //Check if the email is already taken
       if (Customer.findByEmail(email_address)!= null){

         return status(409,"This email is already taken");
         
       }
       //if it is a new email, allow to register
       else{
           String firstName =   customerForm.get("firstname");
           if(firstName == null){
               firstName = "";
           }
           String lastName  =   customerForm.get("lastname");
           if(lastName == null){
               lastName = "";
           }

           String last_passwd_gen= TimeStamp.getDateTime();
       int num_newsletter;
       int genderNumber;
       String ip_registration_newsletter = customerForm.get("ip_registration_newsletter");

       if (ip_registration_newsletter != null && ip_registration_newsletter.equals("1")){
           ip_registration_newsletter = a;
       }else{
           ip_registration_newsletter = null;
       }

       if (customerForm.get("newsletter").equals("1")){
         num_newsletter = 1;
       }else{
         num_newsletter = 0;
       }
             
       if (customerForm.get("id_gender").equals("")){
          genderNumber=0;
       }else{
         genderNumber = Integer.parseInt(customerForm.get("id_gender"));
       }

       Date  newsletter_start = new Date();
       Date newsletter_date_add;

       if (num_newsletter==0){
         newsletter_date_add = null;
       }else{
         newsletter_date_add = newsletter_start;
       }

       String birthday_date;
       Date myDate;

      if (customerForm.get("birthday").equals("") || customerForm.get("birthday").equals("0")){
            myDate = new Date(0);
      }  //生日
      else{
          birthday_date = customerForm.get("birthday");
          
          try{
               myDate=new Date(Long.valueOf(birthday_date));
              }catch(Exception e){
               myDate=new Date(0);
              }          
       }
       
       String company;

       try{
           company  = customerForm.get("company");
           if(company.contains("A2BLiving")){
               company ="";
           }
       }catch(Exception e){
           company= "";
       }

       String reference_email;
           try{
               reference_email  = customerForm.get("recommendation_email");
           }catch(Exception e){
               reference_email= "";
           }

      Customer customer = new Customer(
           1,
           1, 
           genderNumber, 
           3, 
           1, 
           0,
           company,
           null,
           null,
           firstName,
           lastName,
           email_address, 
           MD5.encrypt(customerForm.get("passwd")), 
           last_passwd_gen, 
           newsletter_date_add,
           myDate, 
           num_newsletter,
           ip_registration_newsletter,
           0, 
           null, 
           0, 
           0, 
           0, 
           MD5.getSecureKey(),
           null, 
           1, 
           0, 
           0, 
           newsletter_start, 
           newsletter_start);
      customer.save();
      System.out.println("successfully registered");
      Customer newCus = Customer.findByEmail(customerForm.get("email"));
      int id_customer = newCus.id_customer;
      CustomerGroup newRecord = new CustomerGroup(id_customer,3);    // id_group TODO later change it to 4
      newRecord.save();
      System.out.println("id_group generated");
           final String finalFirstName = firstName;
           Runnable sendEmail = new Runnable() {
               @Override
               public void run() {
                   EmailSender.sendEmailToCustomer(finalFirstName,email_address);
               }
           };
           new Thread(sendEmail).start();
      
            return status(201,"registered");
       }
   }

    public static Result sendEmailToMe() {
        EmailSender.sendEmailToCustomer("chenzhi","chenzhi1123@gmail.com");
        return ok("email sent successfully");
    }

    public static Result getAllProducts() {
        DatabaseProduct new_one = DatabaseProduct.findById(447);
        return ok(renderCategories(new_one));
    }

    public static Result generateMealDeal() {
        MealDealGenerator.generate_table_value();
        return ok("email sent successfully");
    }
    public static Result generateUserGroup() {
        MealDealGenerator. generate_customer_group();
        return ok("email sent successfully");
    }


    public static Result allMealProduct() {
        List<DealProductsTable> a = DealProductsTable.findAll();
        return ok(renderCategories(a));
    }


     /**
      * Customer Login template information
      * @email
      * @passwd
      * */
    public static class Login {
      @Email
      public String email;
      @Required
      public String passwd;

      public String validate() throws Exception {
//          System.out.println("<---------------------Remote--------------------------------------------------------------------------->");
//          System.out.println(passwd);
//          System.out.println("<------------------------------------------------------------------------------------------------------>");
//          String private_key = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAOPs1NQUysvGFgoc\n" +
//                  "NNT8wxKrHaaAQ4dSWjEJYyI8GvDwO2047YV1D/0yt4uyHRllAn29cZTcuVIWRk1b\n" +
//                  "fXmgAP5eCbVkcHgoPTts+OZ8rpfF2K+CQTqbbL0AXVcwoBfqjYvJ5nR5ZSb4I5RK\n" +
//                  "G8FjKMX9CRfxcrUkBzZi3GHudtMdAgMBAAECgYA6C2w//nOO5YljVb+WpjA/pthM\n" +
//                  "OMn5wl05s7xNUaVCyss3i5HhtOg3gR0BNID4oKK7xu0QDg4b+5Q3yx7vRHI72kTV\n" +
//                  "lOkF+OeAdaDrS1r6jjvsIgrKOYvT+dUnQTtxGBNBDTa+Nv5kn0uTAanKKIfEQHTm\n" +
//                  "p+yY35iD8dUGpPjyAQJBAPYq0g1xrXxU9sFL9GHuTqVs8wOHqh+7WdbQfNC26Im0\n" +
//                  "ZiA5fMM9THGjW2XsAHlCe20na8Jtj1R3Kk+Js1OYoF0CQQDtB3ngRAwDxkHUuyKR\n" +
//                  "7WHdSsiVEwO7UPoJiWaIPZKYZib5h/rYOoLa681RkZXebXE+DFPKWXd4W3FtWGFz\n" +
//                  "wtHBAkEAyZAaWoPAR9PSEEg/HIjiwLVK7pMBC+Z+E1S3PrbmBQs/rBcmPz9L/qiB\n" +
//                  "yBzL7n4vThs/Mv4+pCzG6kWYHlUUyQI/Gtm99KnAsqzCyFZqlB1P+MF/92MwCQgj\n" +
//                  "SvFFrClNcfYgdCPzRotaR3Juz86uK4Q6T80VsNZxsjCpqLlaUMLBAkAFTa6LIsIJ\n" +
//                  "dJZf8ESYkq+KmoHb/hu1G1HmjqMHtC35jr7P5OofbnKGYoxxxQXmCxLCGVIyXrrh\n" +
//                  "muBgMR7eXdDN";
//
//          byte[] a = Base64Utils.decode("YSAQOlcIEbERabftk8GnyOfhe7pXq6MtbkqsfQlcRUEdlwQ+aOZxS6/7I8IMeADuBFZFW7MnLlCiex4Ds8J/S5ELUBZ4IE/wpJcD3DOg9XnzRMy7GwX5RdWjGk7Zv5518O1mdm9PFaAgigDCv+8qnkYEBzUamsolZaNGZ80m0rY=");
//          PrivateKey privateKey = RSAUtils.loadPrivateKey(private_key);
//          byte[] decryptByte = RSAUtils.decryptData(a, privateKey);
//          String decryptStr = new String(decryptByte);

//          PrivateKey privateKey = null;
//          try {
//               privateKey = RSAUtils.loadPrivateKey(private_key);
//          } catch (Exception e) {
//              e.printStackTrace();
//          }
//          String password = "";
//          if (null != privateKey){
//              byte[] a = Base64Utils.decode(passwd);
//            byte[] decryptByte = RSAUtils.decryptData(a , privateKey);
////            password = decryptByte.toString();
//          }
          if (Customer.authenticate(email, passwd) == null) {
              return "Invalid user or password";
          } else{
          return null; }
      }

  }
  
    /**
     * Mobile users login action
     * This API receives a Login Form information
     * */
    public static Result postLogin() throws Exception {
//  encrypt with RSA algorithm, Later Use
//       String PUCLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDj7NTUFMrLxhYKHDTU/MMSqx2m\n" +
//                "gEOHUloxCWMiPBrw8DttOO2FdQ/9MreLsh0ZZQJ9vXGU3LlSFkZNW315oAD+Xgm1\n" +
//                "ZHB4KD07bPjmfK6XxdivgkE6m2y9AF1XMKAX6o2LyeZ0eWUm+COUShvBYyjF/QkX\n" +
//                "8XK1JAc2Ytxh7nbTHQIDAQAB";
//
//        PublicKey publicKey = RSAUtils.loadPublicKey(PUCLIC_KEY);
//        byte[] encryptByte = RSAUtils.encryptData("911220".getBytes(), publicKey);
//
//        String passWordAfterencrypt= Base64Utils.encode(encryptByte);
//
//
//        String p="MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAOPs1NQUysvGFgoc\n"+
//                "NNT8wxKrHaaAQ4dSWjEJYyI8GvDwO2047YV1D/0yt4uyHRllAn29cZTcuVIWRk1b\n" +
//                "fXmgAP5eCbVkcHgoPTts+OZ8rpfF2K+CQTqbbL0AXVcwoBfqjYvJ5nR5ZSb4I5RK\n" +
//                "G8FjKMX9CRfxcrUkBzZi3GHudtMdAgMBAAECgYA6C2w//nOO5YljVb+WpjA/pthM\n" +
//                "OMn5wl05s7xNUaVCyss3i5HhtOg3gR0BNID4oKK7xu0QDg4b+5Q3yx7vRHI72kTV\n" +
//                "lOkF+OeAdaDrS1r6jjvsIgrKOYvT+dUnQTtxGBNBDTa+Nv5kn0uTAanKKIfEQHTm\n" +
//                "p+yY35iD8dUGpPjyAQJBAPYq0g1xrXxU9sFL9GHuTqVs8wOHqh+7WdbQfNC26Im0\n" +
//                "ZiA5fMM9THGjW2XsAHlCe20na8Jtj1R3Kk+Js1OYoF0CQQDtB3ngRAwDxkHUuyKR\n" +
//                "7WHdSsiVEwO7UPoJiWaIPZKYZib5h/rYOoLa681RkZXebXE+DFPKWXd4W3FtWGFz\n" +
//                "wtHBAkEAyZAaWoPAR9PSEEg/HIjiwLVK7pMBC+Z+E1S3PrbmBQs/rBcmPz9L/qiB\n" +
//                "yBzL7n4vThs/Mv4+pCzG6kWYHlUUyQI/Gtm99KnAsqzCyFZqlB1P+MF/92MwCQgj\n" +
//                "SvFFrClNcfYgdCPzRotaR3Juz86uK4Q6T80VsNZxsjCpqLlaUMLBAkAFTa6LIsIJ\n" +
//                "dJZf8ESYkq+KmoHb/hu1G1HmjqMHtC35jr7P5OofbnKGYoxxxQXmCxLCGVIyXrrh\n" +
//                "muBgMR7eXdDN";
//
//        byte[] a = Base64Utils.decode(passWordAfterencrypt);
//        PrivateKey privateKey = RSAUtils.loadPrivateKey(p);
//        byte[] decryptByte = RSAUtils.decryptData(Base64Utils.decode(passWordAfterencrypt), privateKey);
//        String decryptStr = new String(decryptByte);
//
//        System.out.println("Hello World!");
//        System.out.println("<---------------------------------------LOCAL--------------------------------------------------------------->");
//        System.out.println(passWordAfterencrypt);
//        System.out.println("<------------------------------------------------------------------------------------------------------>");
//        System.out.println("<---------------------------------------LOCAL--------------------------------------------------------------->");
//        System.out.println(decryptStr);
//        System.out.println("<----------------------------------------------------------------------------------------------------------->");

        Form<Login> customerForm = Form.form(Login.class).bindFromRequest();
        if (customerForm.hasErrors()) {
            ///Play framework Form features, hasError() method will make the form valid
            System.out.println("failed login");
            return badRequest("Wrong user/password");
        } else {
            String email = customerForm.get().email;
            session().clear();
            session("email",email);
            Customer customer = Customer.findByEmail(email);
//            int customer_id = customer.id_customer;
//            String secure_key = customer.secure_key;
//            String combine =  "secure_key=" + secure_key + "&"+"id_customer=" + customer_id;
            // Better with Try Catch
            //update the last login time of a customer
//            new Thread(new ThreadController(combine)).start();
            //create a new object via Class method setNewCustomer, filter unnecessary information
            if (customer != null) {
                Customer newOne = Customer.setNewCustomer(customer);
                System.out.println("Login successfully " + "User: "+ email);
                return  customer==null? notFound() : ok(renderCustomer(newOne));

            }else {
                return notFound("Invalid user");
            }

        }
    }
    /**
     * Update the last visit time of a customer
     * @param email
     * @deprecated
     * */
    public static void updateDate(String email) {
        Customer new_one = Customer.findByEmail(email);
        if (new_one != null){
            new_one.date_upd = new Date();
            new_one.save();
        }
    }


     /**
     * Login with the param of email
     * @param email
     * @deprecated
     * */
     @Transactional
     public static Result newLogin(String email){
        DynamicForm user_info = Form.form().bindFromRequest();
        String customer_email = user_info.get("email");
        String password = user_info.get("passwd");
        if(email.equals(customer_email)){
            if (Customer.authenticate(customer_email, password) == null){
                    return badRequest("Wrong user/password");
             }else{

            session().clear();
            session("email",customer_email);
            int customer_id = Customer.getID(email);
            Customer customer = Customer.findById(customer_id);
//            customer.date_upd = new_date;
//            customer.save();
//            String secure_key = customer.secure_key;
//            String combine =  "?secure_key =" + secure_key + "&"+"id_customer =" + customer_id;
//            HttpRequest.sendPost(combine);
//            Customer newOne = Customer.setNewCustomer(customer);
            customer.date_upd =new Date();
            customer.save();
            Customer newOne = Customer.setNewCustomer(customer);
                    return  customer==null? notFound() : ok(renderCustomer(newOne));
            }
        }
        else{
            return badRequest("Wrong network request");
        }
     }

    public static Result clearCache() {
        //clear and regenerate
        for(int i = 1; i <550; i++){
        reGenerateCache(i);
            System.out.println("Cache cleared------------------"+ i);
        }

        return ok("cache cleared and regenerated");
    }

    public static Result singleClearCache(int category_id) {

        reGenerateCache(category_id);
//        System.out.println("Cache generated!!!!!!");
        return ok("cache cleared and regenerated");
    }

    public static Result saveDeviceToken() {
        DynamicForm user_info = Form.form().bindFromRequest();
        String id_customer = user_info.get("id_customer");
        Integer customer_id;
        try{
            customer_id = Integer.valueOf(id_customer);
        }catch (Exception e){
            return ok("this cannot be added");
        }
        String device_token = user_info.get("device_token");
        if(device_token ==null){
            device_token = "";
        }
        String device_type = user_info.get("device_type");
        if(device_type == null){
            device_type = "";
        }
        Date last_access = new Date();
        //是否已经存在
        CustomerDeviceToken customerDeviceToken =CustomerDeviceToken.findByToken(customer_id,device_token);
       if(customerDeviceToken!=null){  //已经存在
           customerDeviceToken.last_access_time = last_access;
           customerDeviceToken.save();
       }else {
           CustomerDeviceToken new_one = new CustomerDeviceToken(
                   customer_id,
                   device_token,
                   device_type,
                   last_access
           );
           new_one.save();
       }
        return ok("Device token added");
    }

    public static Result addNewUnsubscribe(String phone) {
        String phone_number = "";
        if (phone != null){
            phone_number = phone;
        }
        CustomerUnsubscribeMobile new_record = new CustomerUnsubscribeMobile(phone_number);
        new_record.save();
        System.out.println("Added a new mobile number");
        return ok("new record added");
    }

    //MapTracker Application.java Start
    public static Result mapTrackerIndex() {  //打开主页就直接跳转到/map 路径
        // TODO: 2017/8/9
        String a = null;
        return redirect(routes.MapMoveAPI.showMap(a));//登陆界面
    }


    public static WebSocket<String> test() {
        return new WebSocket<String>() {
            // Called when the Websocket Handshake is done.
            public void onReady(WebSocket.In<String> in, WebSocket.Out<String> out) {
                // For each event received on the socket,
                in.onMessage(new F.Callback<String>() {
                    public void invoke(String event) {
                        // Log events to the console
                        System.out.println(event);
                    }
                });
                // When the socket is closed.
                in.onClose(new F.Callback0() {
                    public void invoke() {
                        System.out.println("Disconnected");
                    }
                });
                // Send a single 'Hello!' message
                out.write("Hello!");
                System.out.println("Connected to Socket Successfully");  //打开端口的时候显示信息
            }
        };
    }
//	public static Result showMap() {
//        System.out.println("this is--showMap-------" + Thread.currentThread().getId());
//		return ok(views.html.map.render());
//	}

    public static Result seeSample() {
        return ok(views.html.sample.render());
    }

    //Maptracker End

}
