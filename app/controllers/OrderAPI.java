/*
 * Copyright (c) 2017. Zhi Chen.
 * OrderAPI is for the process of customers making orders.
 * Actions like adding records in a series of tables
 * in the database,as well as updating the certain attribute
 * numbers, generating new reference,etc.
 */

package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import flexjson.JSONSerializer;
import models.database.*;
import models.sqlContainer.CodeValidation;
import models.sqlContainer.ProductQuantity;
import models.sqlContainer.ProductWholesale;
import models.sqlContainer.ReturnDriverOrders;
import models.template.*;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static controllers.DriverAPI.getSequenceNumber;
import static parsers.JsonParser.*;

public class OrderAPI extends Controller {


    /**
     * Test Methods, to see if Java class has successfully connected database tables via Ebean Model
     **/
    public static Result getOneOrder(int id) {

        Order order = Order.findById(id);

        return ok(renderCategories(order));
    }
    public static Result getOneOrderCarrier(long id) {

        OrderCarrier orderCarrier = OrderCarrier.findById(id);

        return ok(renderCategories(orderCarrier));
    }
    public static Result getOneOrderDetail(int id) {

        OrderDetail orderDetail = OrderDetail.findById(id);

        return ok(renderCategories(orderDetail));
    }
    public static Result getOneOrderDetailTax(long id) {

        OrderDetailTax orderDetailTax = OrderDetailTax.findById(id);

        return ok(renderCategories(orderDetailTax));
    }
    public static Result getOneOrderHistory(int id) {

        OrderHistory orderHistory = OrderHistory.findById(id);

        return ok(renderCategories(orderHistory));
    }
    public static Result getOneOrderInvoice(long id) {

        OrderInvoice orderInvoice = OrderInvoice.findById(id);

        return ok(renderCategories(orderInvoice));
    }
    public static Result getOneOrderInvoicePayment(long id) {

        OrderInvoicePayment orderInvoicePayment = OrderInvoicePayment.findById(id);

        return ok(renderCategories(orderInvoicePayment));
    }
    /**
     * Test Method, to see if Java class has successfully connected database tables via Ebean Model
     * @deprecated
     **/
    public static Result getOneOrderInvoiceTax(long id) {

    OrderInvoiceTax orderInvoiceTax = OrderInvoiceTax.findById(id);

    return ok(renderCategories(orderInvoiceTax));
    }
    public static Result getOneOrderPayment(long id) {

        OrderPayment orderPayment = OrderPayment.findById(id);

        return ok(renderCategories(orderPayment));
    }
    public static Result getOneOrderMessage(int id) {

        OrderMessage orderMessage = OrderMessage.findById(id);

        return ok(renderCategories(orderMessage));
    }


    /**
     * Mobile side checks out and adds one order
     * This method contains a series of actions to the database,
     * which involves many tables. at least 9 tables will be affected,
     * meanwhile 9 records will be added in a sequential order.
     * @param customer_id
     * @see Order
     * @see OrderHistory
     * @see OrderPayment
     * @see OrderInvoice
     * @see OrderCarrier
     * @see OrderDetail
     * @see OrderDetailTax
     * @see StockAvailable
     * @see OrderMessage
     * @see ReturnInformation  this is a template for order information sent
     * back to the mobile client in case the order is in online payment process
     * */
    @Transactional
    public static Result addOneOrder(int customer_id) throws Exception {
        TotalOrder totalOrder = renderTotalOrder(request().body().asJson().toString());
        // promotion
        int id_cart_rule = totalOrder.id_cart_rule;
        String name = totalOrder.name;
        BigDecimal value = totalOrder.value;
        BigDecimal value_tax_excl = totalOrder.value_tax_excl;
        int free_shipping = totalOrder.free_shipping;

        String order_message = totalOrder.message;
        ArrayList<OrderProduct> new_products = totalOrder.products;
        int number_of_product = new_products.size();
        List<String> payment_method = new ArrayList<>();  //fixed String list for two payment methods
        payment_method.add("Online Payment");
        payment_method.add("Cash on delivery (COD)");
        List<String> payment_module = new ArrayList<>();  //fixed String list for two payment modules
        payment_module.add("ogone");
        payment_module.add("cashondelivery");
        String shop_address = "A2B Living<br />Marina Commercial Park<br />Cork,<br />+353 (0)21 4947888";
        Customer a = Customer.findById(totalOrder.id_customer);
        Customer b = Customer.findBySecure(totalOrder.secure_key);
        if (customer_id == totalOrder.id_customer && a != null && b != null && a.equals(b)) {
            int payment_type = totalOrder.payment_method;
            String order_reference;
            int order_status = 0;
            do{
                order_reference = getRandomString();
            }while (Order.findByReference(order_reference) != null);
            /* if the order is paid by card, the payment method code should be 0, and the order status code should be 11 */
            if (payment_type == 0) {
                order_status = 11;
            }
            /* if it is cash-on-delivery order, the payment method code should be 1, and the order status code should be 3 */
            else if (payment_type == 1) {
                order_status = 3;
            }
            Date nullDaTe = new Date(0);
            Date date_add = new Date();
            /* First table affected, ps_orders, a new record will be generated */
            Order order = new Order(
                    order_reference,
                    1,
                    1,
                    5,
                    1,
                    customer_id,
                    0,
                    1,
                    totalOrder.id_address_delivery,
                    totalOrder.id_address_invoice,
                    order_status, //order status code, 11 or 3
                    totalOrder.secure_key,
                    payment_method.get(payment_type),
                    BigDecimal.valueOf(1.000000),
                    payment_module.get(payment_type),
                    0,
                    0,
                    "",
                    0,
                    "",
                    value,
                    value,
                    value_tax_excl,
                    totalOrder.total_paid,
                    totalOrder.total_paid_tax_incl,
                    totalOrder.total_paid_tax_excl,
                    totalOrder.total_paid_real,
                    totalOrder.total_products,
                    totalOrder.total_products_wt,
                    totalOrder.total_shipping,
                    totalOrder.total_shipping_tax_incl,
                    totalOrder.total_shipping_tax_excl,
                    BigDecimal.valueOf(0.000),
                    BigDecimal.valueOf(0.000000),
                    BigDecimal.valueOf(0.000000),
                    BigDecimal.valueOf(0.000000),
                    2,
                    2,
                    0,
                    0,
                    nullDaTe,
                    nullDaTe,
                    1,
                    date_add,
                    date_add
            );
            order.save();
            Order just_added = Order.findByReference(order_reference);
            int id_order = just_added.id_order;
            /*
            The second period, add new records in table
            ps_order_history, ps_order_invoice, ps_order_payment, and ps_message
            */

            OrderHistory orderHistory = new OrderHistory(         //ps_order_history
                    0,
                    id_order,
                    order_status,
                    date_add
            );
            orderHistory.save();
            System.out.println("orderHistory.save(); " + System.currentTimeMillis());

            OrderPayment orderPayment = new OrderPayment(       //ps_order_payment
                    order_reference,
                    1,
                    totalOrder.total_paid,
                    payment_method.get(payment_type),
                    BigDecimal.valueOf(1.000000),
                    "",
                    "",
                    "",
                    "",
                    "",
                    date_add
            );
            orderPayment.save();
            System.out.println("orderPayment.save(); " + System.currentTimeMillis());
            OrderPayment new_one = OrderPayment.findByReference(order_reference);
            int id_order_payment = new_one.id_order_payment;
            OrderInvoice orderInvoice = new OrderInvoice(    //ps_order_invoice
                    id_order,
                    0,
                    0,
                    date_add,
                    value_tax_excl,
                    value,
                    totalOrder.total_paid_tax_excl,
                    totalOrder.total_paid_tax_incl,
                    totalOrder.total_products,
                    totalOrder.total_products_wt,
                    totalOrder.total_shipping_tax_excl,
                    totalOrder.total_shipping_tax_incl,
                    0,
                    BigDecimal.valueOf(0.000000),
                    BigDecimal.valueOf(0.000000),
                    shop_address,
                    totalOrder.invoice_address,
                    totalOrder.delivery_address,
                    "",
                    date_add
            );
            /* create a new record in the table of ps_order_invoice only when the order status is 3 */
            if (order_status == 3) {
                orderInvoice.save();
                System.out.println("orderInvoice.save(); " + System.currentTimeMillis());
                OrderInvoice new_added_invoice = OrderInvoice.findByOrder(id_order);
                int id_order_invoice = new_added_invoice.id_order_invoice;
                new_added_invoice.number = id_order_invoice;
                new_added_invoice.delivery_number = id_order_invoice;
                new_added_invoice.save();
                System.out.println("new_added_invoice.save(); " + System.currentTimeMillis());
                just_added.invoice_number = id_order_invoice;
                just_added.delivery_number = id_order_invoice;
                just_added.invoice_date = date_add;
                just_added.delivery_date = date_add;
                just_added.save();
                System.out.println("just_added.save(); " + System.currentTimeMillis());
                /*
                The third period, generate records in the following tables in the database,
                meanwhile reduce the quantity of each product bought
                */
                OrderCarrier orderCarrier = new OrderCarrier(                                     // ps_order_carrier
                        id_order,
                        5,
                        id_order_invoice,
                        BigDecimal.valueOf(0.000000),
                        totalOrder.total_shipping_tax_excl,
                        totalOrder.total_shipping_tax_incl,
                        "",
                        date_add
                );
                orderCarrier.save();

                OrderInvoicePayment orderInvoicePayment = new OrderInvoicePayment(              //ps_order_invoice_payment
                        id_order_invoice,
                        id_order_payment,
                        id_order

                );
                orderInvoicePayment.save();

                OrderInvoiceTax orderInvoiceTax = new OrderInvoiceTax(                          //ps_order_invoice_tax
                        id_order_invoice,
                        "shipping",
                        1,
                        BigDecimal.valueOf(0.000000)
                );
                orderInvoiceTax.save();
               /* create a thread to process order_details adding action */
                Runnable forloop1 = new Runnable() {
                    @Override
                    public synchronized void run() {
                        for (int i = 0; i < number_of_product; i++) {
                            OrderProduct orderProduct = new_products.get(i);
                            String product_reference = orderProduct.product_reference;
                            if (product_reference == null){
                                product_reference = "";
                            }
                            BigDecimal price = getWholeSale(orderProduct.product_id);
                            OrderDetail orderDetail = new OrderDetail(                       //ps_order_detail
                                    id_order,
                                    id_order_invoice,
                                    0,
                                    1,
                                    orderProduct.product_id,
                                    orderProduct.product_attribute_id,
                                    orderProduct.product_name,
                                    orderProduct.product_quantity,
                                    orderProduct.product_quantity,
                                    0,
                                    0,
                                    0,
                                    orderProduct.product_price,
                                    orderProduct.reduction_percent,
                                    orderProduct.reduction_amount,
                                    orderProduct.reduction_amount_tax_incl,
                                    orderProduct.reduction_amount_tax_excl,
                                    BigDecimal.valueOf(0.00),
                                    BigDecimal.valueOf(0.000000),
                                    "",
                                    orderProduct.product_upc,
                                    product_reference,
                                    "",
                                    BigDecimal.valueOf(0.000000),
                                    orderProduct.id_tax_rules_group,
                                    0,
                                    "",
                                    BigDecimal.valueOf(0.000),
                                    BigDecimal.valueOf(0.000000),
                                    BigDecimal.valueOf(0.000),
                                    0,
                                    "",
                                    0,
                                    new Date(0),
                                    orderProduct.total_price_tax_incl,
                                    orderProduct.total_price_tax_excl,
                                    orderProduct.unit_price_tax_incl,
                                    orderProduct.unit_price_tax_excl,
                                    BigDecimal.valueOf(0.000000),
                                    BigDecimal.valueOf(0.000000),
                                    BigDecimal.valueOf(0.000000),
                                    orderProduct.product_price,
                                    price
                            );
                            orderDetail.save();
                            int id_order_detail = orderDetail.id_order_detail;

                            OrderDetailTax orderDetailTax = new OrderDetailTax(                   //ps_order_detail_tax
                                    id_order_detail,
                                    orderProduct.id_tax,
                                    orderProduct.tax_unit_amount,
                                    orderProduct.tax_total_amount
                            );
                            orderDetailTax.save();
                            //get the current quantity of the product and then reduce the quantity by the number bought in the order
                            StockAvailable stockAvailable = StockAvailable.findByAttribute(orderProduct.product_id,0);
                            if(stockAvailable != null) {
                                stockAvailable.quantity -= orderProduct.product_quantity;
                                stockAvailable.save();
                                System.out.println("Quantity changed");
                            }
                        }
                        // promotion code actions
                        if (id_cart_rule !=0){
                            OrderCartRule new_record = new OrderCartRule(
                                    id_order,
                                    id_cart_rule,
                                    id_order_invoice,
                                    name,
                                    value,
                                    value_tax_excl,
                                    free_shipping
                            );
                            new_record.save();

                            CartRule cartRule = CartRule.findById(id_cart_rule);
                            cartRule.quantity -=1;
                            cartRule.save();
                        }
                    }
                };
                new Thread(forloop1).start();
            } else if (order_status == 11) {  //pay online
                /*
                if the status code is 11, the record in table order_invoice will not be created
                currently the id_order_invoice will be 0 in the database until the record is generated
                */
                OrderCarrier orderCarrier = new OrderCarrier(                               //ps_order_carrier
                        id_order,
                        5,
                        0, //id_order_invoice
                        BigDecimal.valueOf(0.000000),
                        totalOrder.total_shipping_tax_excl,
                        totalOrder.total_shipping_tax_incl,
                        "",
                        date_add
                );
                orderCarrier.save();
                /* Create a new thread for order_details, and order_detail_tax */
                Runnable forloop2 = new Runnable() {
                    @Override
                    public synchronized void run() {
                        for (int i = 0; i < number_of_product; i++) {
                            OrderProduct orderProduct = new_products.get(i);
                            String product_reference = orderProduct.product_reference;
                            if (product_reference == null){
                                product_reference = "";
                            }
                            BigDecimal price = getWholeSale(orderProduct.product_id);
                            OrderDetail orderDetail = new OrderDetail(                       //ps_order_details
                                    id_order,
                                    0, //id_order_invoice
                                    0,
                                    1,
                                    orderProduct.product_id,
                                    orderProduct.product_attribute_id,
                                    orderProduct.product_name,
                                    orderProduct.product_quantity,
                                    orderProduct.product_quantity,
                                    0,
                                    0,
                                    0,
                                    orderProduct.product_price,
                                    orderProduct.reduction_percent,
                                    orderProduct.reduction_amount,
                                    orderProduct.reduction_amount_tax_incl,
                                    orderProduct.reduction_amount_tax_excl,
                                    BigDecimal.valueOf(0.00),
                                    BigDecimal.valueOf(0.000000),
                                    "",
                                    orderProduct.product_upc,
                                    product_reference,
                                    "",
                                    BigDecimal.valueOf(0.000000),
                                    orderProduct.id_tax_rules_group,
                                    0,
                                    "",
                                    BigDecimal.valueOf(0.000),
                                    BigDecimal.valueOf(0.000000),
                                    BigDecimal.valueOf(0.000),
                                    0,
                                    "",
                                    0,
                                    new Date(0),
                                    orderProduct.total_price_tax_incl,
                                    orderProduct.total_price_tax_excl,
                                    orderProduct.unit_price_tax_incl,
                                    orderProduct.unit_price_tax_excl,
                                    BigDecimal.valueOf(0.000000),
                                    BigDecimal.valueOf(0.000000),
                                    BigDecimal.valueOf(0.000000),
                                    orderProduct.product_price,
                                    price
                            );
                            orderDetail.save();
                            int id_order_detail = orderDetail.id_order_detail;

                            OrderDetailTax orderDetailTax = new OrderDetailTax(                    //ps_order_detail_tax
                                    id_order_detail,
                                    orderProduct.id_tax,
                                    orderProduct.tax_unit_amount,
                                    orderProduct.tax_total_amount
                            );
                            orderDetailTax.save();
                            // 在线付款不减去数量 第一次请求的时候

                        }
                    }
                };
                new Thread(forloop2).start();
            }
            /*
            Return some of the order information to Client side for further use
            For online-payment methods, it requires authentication from a third
            party, and some records will be generated after the authentication
            */
            ReturnInformation return_info = new ReturnInformation(
                    id_order,
                    customer_id,
                    totalOrder.id_address_delivery,
                    totalOrder.id_address_invoice,
                    0,
                    id_order_payment,
                    order_reference,
                    "",
                    totalOrder.secure_key,
                    totalOrder.invoice_address,
                    totalOrder.delivery_address,
                    totalOrder.total_paid,
                    totalOrder.total_paid_tax_incl,
                    totalOrder.total_paid_tax_excl,
                    totalOrder.total_paid_real,
                    totalOrder.total_products,
                    totalOrder.total_products_wt,
                    totalOrder.total_shipping,
                    totalOrder.total_shipping_tax_excl,
                    totalOrder.total_shipping_tax_incl,
                    id_cart_rule,
                    name,
                    value,
                    value_tax_excl,
                    free_shipping
            );
            /* Either conditional process will lead to the return of general information of this order(if the order contains a message, add one record in ps_message) */
            if (order_message != null) {
                if (!order_message.equals("")) {
                    OrderMessage message = new OrderMessage(
                            0,
                            customer_id,
                            0,
                            id_order,
                            order_message,
                            1,
                            date_add
                    );
                    message.save();
                }

                return status(201, renderCategories(return_info));
            } else {

                return status(201, renderCategories(return_info));
            }
        } else {
            return status(409, "Invalid user, you cannot order");
        }
    }
    /**
     * This method is for online-payment transaction, after authentication, some records will be generated
     * in corresponding tables. It will receive the order info from client end through HTTP request
     * @param order_id
     * @see ReturnInformation
     * @see OrderInvoice   create a new object of this class, accordingly add one new record in this table
     * @see OrderInvoicePayment
     * @see OrderInvoiceTax
     * @see OrderDetail
     * @see Order
     * @see OrderHistory
     * @see OrderCarrier
     * */
    @Transactional
    public static Result updateOneOrder(int order_id) throws Exception {
        ReturnInformation returnInformation = renderReturnInformation(request().body().asJson().toString());
        System.out.println("receive time ----" + String.valueOf(System.currentTimeMillis()) + "----------------");
        int id_cart_rule = returnInformation.id_cart_rule;
        String name = returnInformation.name;
        BigDecimal value = returnInformation.value;
        BigDecimal value_tax_excl = returnInformation.value_tax_excl;
        int free_shipping = returnInformation.free_shipping;
        int id_order = returnInformation.id_order;
        String shop_address = "A2B Living<br />Marina Commercial Park<br />Cork,<br />+353 (0)21 4947888";
        int id_order_payment = returnInformation.id_order_payment;
        Order order = Order.findById(order_id);
        int order_status = order.current_state;
        if (order == null) {
            return status(404, "No such order");
        } else if (order_status == 3) {
            return status(409, "You have already paid");
        } else{
            Date date_add = new Date();
            /* parse the information from http request, then use them to generate records in each table */
            OrderInvoice orderInvoice = new OrderInvoice(               // ps_order_invoice
                    id_order,
                    0,
                    0,
                    date_add,
                    value_tax_excl,
                    value,
                    returnInformation.total_paid_tax_excl,
                    returnInformation.total_paid_tax_incl,
                    returnInformation.total_products,
                    returnInformation.total_products_wt,
                    returnInformation.total_shipping_tax_excl,
                    returnInformation.total_shipping_tax_incl,
                    0,
                    BigDecimal.valueOf(0.000000),
                    BigDecimal.valueOf(0.000000),
                    shop_address,
                    returnInformation.invoice_address,
                    returnInformation.delivery_address,
                    "",
                    date_add
            );
            orderInvoice.save();
            OrderInvoice new_added_invoice = OrderInvoice.findByOrder(id_order);
            int id_order_invoice = new_added_invoice.id_order_invoice;
            new_added_invoice.number = id_order_invoice;
            new_added_invoice.delivery_number = id_order_invoice;
            new_added_invoice.save();
            OrderInvoicePayment orderInvoicePayment = new OrderInvoicePayment( //ps_order_invoice_payment
                    id_order_invoice,
                    id_order_payment,
                    id_order

            );
            orderInvoicePayment.save();
            OrderInvoiceTax orderInvoiceTax = new OrderInvoiceTax(            //ps_order_invoice_tax
                    id_order_invoice,
                    "shipping",
                    1,
                    BigDecimal.valueOf(0.000000)
            );
            orderInvoiceTax.save();
            /* Update the id_order_invoice for each record in the ps_order_details table */
            List<OrderDetail> orderDetails = OrderDetail.findByOrder(id_order);
            int size = orderDetails.size();

            /* Create a new thread for order_details, and order_detail_tax */
            Runnable forloop_3 = new Runnable() {
                @Override
                public synchronized void run() {
                    for (int i = 0; i < size; i++) {
                        orderDetails.get(i).id_order_invoice = id_order_invoice;
                        orderDetails.get(i).save();
                        //change product quantity in the database
                        int product_id = orderDetails.get(i).product_id;
                        int product_quantity = orderDetails.get(i).product_quantity;
                        //change quantity
                        StockAvailable stockAvailable = StockAvailable.findByAttribute(product_id,0);// ps_stock_available
                        if(stockAvailable != null) {
                            stockAvailable.quantity -= product_quantity;
                            stockAvailable.save();
                            System.out.println("Quantity changed");
                        }
                    }
                    System.out.println("loop products finished-----" + String.valueOf(System.currentTimeMillis()) + "----------------");
                }
            };
            new Thread(forloop_3).start();
            System.out.println("start to loop products-----" + String.valueOf(System.currentTimeMillis()) + "----------------");
            /* Update the order status after this payment is authenticated by payment organisation */
            order.current_state = 3;                                         // ps_orders
            order.invoice_number = id_order_invoice;
            order.delivery_number = id_order_invoice;
            order.delivery_date = date_add;
            order.invoice_date = date_add;
            order.save();
            OrderHistory orderHistory = OrderHistory.findById(order_id);     //ps_order_history
            orderHistory.id_order_state = 3;
            orderHistory.save();
            OrderCarrier orderCarrier = OrderCarrier.findByOrder(id_order);  //ps_order_cashier
            orderCarrier.id_order_invoice = id_order_invoice;
            orderCarrier.save();
            // promotion code actions
            if (id_cart_rule !=0){
                OrderCartRule new_record = new OrderCartRule(
                        id_order,
                        id_cart_rule,
                        id_order_invoice,
                        name,
                        value,
                        value_tax_excl,
                        free_shipping
                );
                new_record.save();

                CartRule cartRule = CartRule.findById(id_cart_rule);
                cartRule.quantity -=1;
                cartRule.save();
            }
            System.out.println("updated");
            System.out.println("Payment Finished-----" + String.valueOf(System.currentTimeMillis()) + "----------------");
            return ok("order paid successfully");
        }
    }
    /**
     * Allocate random and unique string to each order, as "order_reference"
     * @return random_String
     * */
    private static String getRandomString() {
        StringBuffer buffer = new StringBuffer("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        StringBuffer reference = new StringBuffer();
        Random random = new Random();
        int range = buffer.length();
        for (int i = 0; i < 8; i++) {
            reference.append(buffer.charAt(random.nextInt(range)));
        }
        return reference.toString();
    }

    /**
     * Return orders within 2 weeks time of a customer, including the details
     * @param id
     * @param secure_key  each user has one unique key for verification
     * @return  ArrayList<returnOrders>
     * @see ReturnOrder
     * @see ReturnProduct
     * @see AddressTemplate
     * */
    public static Result showOrders(int id, String secure_key) {
        System.out.println("start to return");
        List<Order> all_orders = Order.findByCustomer(id, secure_key);  //find all orders of a customer within 14 days
        if (all_orders != null) {
            List<ReturnOrder> returnOrders = new ArrayList<>();         //create a new list of orders using a pre-defined template
            for (Order this_order : all_orders) {
                int order_id = this_order.id_order;
                List<ReturnProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
                List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);  // get all the products detail from table ps_order_detail
                for (OrderDetail this_product : detail_products) {
                    ReturnProduct product = new ReturnProduct(
                            this_product.id_order_detail,
                            this_product.product_upc,
                            this_product.product_name,
                            this_product.product_reference,
                            this_product.product_quantity,
                            this_product.total_price_tax_incl
                    );
                    bought_products.add(product);

                }
                int address_id = this_order.id_address_delivery;         //Address information of each order
                AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
                if (address_id != 0) {                                      //If there is an address
                    Address address = Address.findById(address_id);         //Find the address in the table of Address
                    String alias = address.alias;
                    String company = address.company;
                    String firstname = address.firstname;
                    String lastname = address.lastname;
                    String address1 = address.address1;
                    String address2 = address.address2;
                    String city = address.city;
                    String phone = address.phone;
                    delivery_address = new AddressTemplate(
                            alias,
                            company,
                            firstname,
                            lastname,
                            address1,
                            address2,
                            city,
                            phone
                    );
                } else {
                    delivery_address = null;
                }
                int id_state = this_order.current_state;                   //find the order status
                String state = OrderState.findById(id_state);
                String payment = this_order.payment;
                String reference = this_order.reference;
                ReturnOrder final_order = new ReturnOrder(
                        order_id,
                        reference,
                        delivery_address,
                        state,
                        payment,
                        this_order.total_paid_real,
                        this_order.total_shipping,
                        this_order.date_add,
                        bought_products
                );
               returnOrders.add(final_order);                              //Keep adding orders into the ReturnOrder list
            }
            System.out.println("orders returned");
            return ok(new JSONSerializer().deepSerialize(returnOrders));
        } else {
            System.out.println("no such user");
            return notFound("Not invalid user");
        }
    }
    /**
     * Get the wholesale price of each product.
     * This method will be called when creating a new order.
     * @param productID
     * @return wholesale_price
     * @see ProductWholesale
     * */
    public static BigDecimal getWholeSale(int productID) {
        String sql =
                "select ps_product.wholesale_price\n" +
                        "\n" +
                        "from ps_product\n" +
                        "\n" +
                        " where ps_product.id_product = \n" + productID;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product.wholesale_price", "whole_sale")
                        .create();
        com.avaje.ebean.Query<ProductWholesale> query = Ebean.find(ProductWholesale.class);
        query.setRawSql(rawSql);
        List<ProductWholesale> list = query.findList();
        ArrayList<ProductWholesale> whole_prices = new ArrayList<ProductWholesale>();
        whole_prices.addAll(list);
        BigDecimal wholesale_price = whole_prices.get(0).whole_sale;
        return wholesale_price;
    }


    public static Result showAllCart() {
        List<CartRule> all_record = CartRule.findAll();
        return ok(renderCategories(all_record));
    }

    public static Result showAllGroup() {
        List<CartRuleGroup> all_record = CartRuleGroup.findAll();
        return ok(renderCategories(all_record));
    }

    public static int getUsedTimes(int id_cart_rule, int id_customer){
        String sql =
                "select count(*) \n" +
                        "\n" +
                        "from ps_orders, ps_order_cart_rule\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_order_cart_rule.id_order\n" +
                        "\n" +
                        "and ps_order_cart_rule.id_cart_rule =" + id_cart_rule + "\n" +
                        "\n" +
                        "and ps_orders.id_customer =" + id_customer +  "\n" +
                        "and ps_orders.valid = 1";  //只记录有效订单

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(*)", "quantity")
                        .create();

        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        return query.findUnique().quantity;



    }

    public static Result codeValidate() {
        DynamicForm order_driver = Form.form().bindFromRequest();
        System.out.println("Start to validate");
        int id_customer;
        String promotion_code;
        try {
            id_customer = Integer.parseInt(order_driver.get("id_customer"));
            promotion_code = order_driver.get("promotion_code");
        } catch (Exception e) {
            return badRequest("Wrong request format");
        }
        //第一层条件过滤， 用户是否登录之后再验证
        if (id_customer == 0){
            return  badRequest("Please log in");
        }else {
            int constraint_customer_id;
            try {
                constraint_customer_id = CartRule.getConstraintUser(promotion_code);
            } catch (Exception e) {
                constraint_customer_id = 0;
            }
            //第二层条件过滤， Voucher是否绑定特定用户才能使用
            if(constraint_customer_id !=0 && id_customer != constraint_customer_id){
                return  badRequest("Invalid user for this voucher");
            }else {
                //get the promotion code ID from database
                int id_cart_rule;
                try {
                    id_cart_rule = CartRule.findByCode(promotion_code);
                } catch (Exception e) {
                    id_cart_rule = 0;
                }
                //第三层条件过滤， 是否存在这个vouher
                if (id_cart_rule == 0) {
                    return badRequest("No such promotion code");
                } else {
                    //see if this code is already used up by this user
                    int used_time = getUsedTimes(id_cart_rule, id_customer);
//                if (used_time != 0) {
//                    return badRequest("promotion code already used");
//                } else {
                    ArrayList<Integer> products_list = new ArrayList<Integer>();
                    ArrayList<CodeValidation> total_record = new ArrayList<CodeValidation>();
                    try {
                        String sql =
                                "select ps_cart_rule_lang.name, ps_cart_rule.id_cart_rule, ps_cart_rule.description, ps_cart_rule.quantity_per_user, ps_cart_rule.minimum_amount, ps_cart_rule.minimum_amount_shipping, ps_cart_rule.free_shipping, ps_cart_rule.reduction_percent, ps_cart_rule.reduction_amount,ps_cart_rule.reduction_product, ps_cart_rule.group_restriction,ps_cart_rule_product_rule_group.quantity, ps_cart_rule_product_rule_value.id_item\n" +
                                        "\n" +
                                        "from ps_cart_rule_lang\n" +
                                        "\n" +
                                        "left join ps_cart_rule\n" +
                                        "on ps_cart_rule_lang.id_cart_rule = ps_cart_rule.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_group\n" +
                                        "on ps_cart_rule.id_cart_rule = ps_cart_rule_product_rule_group.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule\n" +
                                        "on ps_cart_rule_product_rule_group.id_product_rule_group = ps_cart_rule_product_rule.id_product_rule_group \n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_value\n" +
                                        "on ps_cart_rule_product_rule.id_product_rule = ps_cart_rule_product_rule_value.id_product_rule\n" +
                                        "\n" +
                                        "where ps_cart_rule.id_customer = " + constraint_customer_id+ "\n" +   //加上限制用户
                                        "\n" +
                                        "and ps_cart_rule.date_from < now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.date_to > now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.quantity > 0\n" +
                                        "\n" +
                                        "and ps_cart_rule.active = 1\n" +
                                        "\n" +
                                        "and ps_cart_rule.id_cart_rule =" + id_cart_rule;
                        RawSql rawSql =
                                RawSqlBuilder
                                        .parse(sql)
                                        .columnMapping("ps_cart_rule_lang.name", "promotion_name")
                                        .columnMapping("ps_cart_rule.id_cart_rule", "id_cart_rule")
                                        .columnMapping("ps_cart_rule.description", "description")
                                        .columnMapping("ps_cart_rule.quantity_per_user", "quantity_per_user")
                                        .columnMapping("ps_cart_rule.minimum_amount", "minimum_amount")
                                        .columnMapping("ps_cart_rule.minimum_amount_shipping", "minimum_amount_if_shipping")
                                        .columnMapping("ps_cart_rule.free_shipping", "if_free_shipping")
                                        .columnMapping("ps_cart_rule.reduction_percent", "reduction_percent")
                                        .columnMapping("ps_cart_rule.reduction_amount", "reduction_amount")
                                        .columnMapping("ps_cart_rule.reduction_product", "reduction_product")
                                        .columnMapping("ps_cart_rule.group_restriction", "group_restriction")
                                        .columnMapping("ps_cart_rule_product_rule_group.quantity", "minimum_product_quantity_required")
                                        .columnMapping("ps_cart_rule_product_rule_value.id_item", "if_valid")  //存在产品要求
                                        .create();
                        com.avaje.ebean.Query<CodeValidation> query = Ebean.find(CodeValidation.class);
                        query.setRawSql(rawSql);
                        List<CodeValidation> list = query.findList();
                        total_record.addAll(list);
                    } catch (Exception e) {
                        String sql =
                                "select ps_cart_rule_lang.name, ps_cart_rule.id_cart_rule, ps_cart_rule.description, ps_cart_rule.quantity_per_user, ps_cart_rule.minimum_amount, ps_cart_rule.minimum_amount_shipping, ps_cart_rule.free_shipping, ps_cart_rule.reduction_percent, ps_cart_rule.reduction_amount,ps_cart_rule.reduction_product, ps_cart_rule.group_restriction\n" +
                                        "\n" +
                                        "from ps_cart_rule_lang\n" +
                                        "\n" +
                                        "left join ps_cart_rule\n" +
                                        "on ps_cart_rule_lang.id_cart_rule = ps_cart_rule.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_group\n" +
                                        "on ps_cart_rule.id_cart_rule = ps_cart_rule_product_rule_group.id_cart_rule\n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule\n" +
                                        "on ps_cart_rule_product_rule_group.id_product_rule_group = ps_cart_rule_product_rule.id_product_rule_group \n" +
                                        "\n" +
                                        "left join ps_cart_rule_product_rule_value\n" +
                                        "on ps_cart_rule_product_rule.id_product_rule = ps_cart_rule_product_rule_value.id_product_rule\n" +
                                        "\n" +
                                        "where ps_cart_rule.id_customer = " + constraint_customer_id+ "\n" +   //加上限制用户
                                        "\n" +
                                        "and ps_cart_rule.date_from < now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.date_to > now()\n" +
                                        "\n" +
                                        "and ps_cart_rule.quantity > 0\n" +
                                        "\n" +
                                        "and ps_cart_rule.active = 1\n" +
                                        "\n" +
                                        "and ps_cart_rule.id_cart_rule =" + id_cart_rule;
                        RawSql rawSql =
                                RawSqlBuilder
                                        .parse(sql)
                                        .columnMapping("ps_cart_rule_lang.name", "promotion_name")
                                        .columnMapping("ps_cart_rule.id_cart_rule", "id_cart_rule")
                                        .columnMapping("ps_cart_rule.description", "description")
                                        .columnMapping("ps_cart_rule.quantity_per_user", "quantity_per_user")
                                        .columnMapping("ps_cart_rule.minimum_amount", "minimum_amount")
                                        .columnMapping("ps_cart_rule.minimum_amount_shipping", "minimum_amount_if_shipping")
                                        .columnMapping("ps_cart_rule.free_shipping", "if_free_shipping")
                                        .columnMapping("ps_cart_rule.reduction_percent", "reduction_percent")
                                        .columnMapping("ps_cart_rule.reduction_amount", "reduction_amount")
                                        .columnMapping("ps_cart_rule.reduction_product", "reduction_product")
                                        .columnMapping("ps_cart_rule.group_restriction", "group_restriction")
                                        .create();
                        com.avaje.ebean.Query<CodeValidation> query = Ebean.find(CodeValidation.class);
                        query.setRawSql(rawSql);
                        List<CodeValidation> list = query.findList();
                        total_record.addAll(list);
                    }
                    int number_of_products = total_record.size();
                    //第四层条件过滤，voucher在有效时间以内
                    if (number_of_products != 0) { //SQL 返回值
                        CodeValidation sample = total_record.get(0); //拿到第一条记录作为样本Voucher
                        int code_used_time = sample.quantity_per_user;  //times of usage of each person
                        //第五层条件过滤， 没有超过使用上限
                        if (used_time < code_used_time) { //没有超过使用次数上限
                            if (sample.if_valid != 0) {  //存在限定产品
                                for (CodeValidation aList : total_record) {
                                    int product_id = aList.if_valid;
                                    products_list.add(product_id);
                                }
                            }
                            // get customer group id
                            ArrayList<Integer> customer_group_id = new ArrayList<>();
                            //            int id_group;
                            List<CustomerGroup> all_group = CustomerGroup.findByCustomer(id_customer);
                            if (all_group != null) {
                                for (CustomerGroup this_one : all_group) {
                                    customer_group_id.add(this_one.id_group);
                                }
                            }
                            int if_group_restriction = sample.group_restriction;
                            if (if_group_restriction == 1) {   //user group constraint， 用户群组限定
                                List<CartRuleGroup> all_record = CartRuleGroup.findByCartRule(id_cart_rule);
                                List<Integer> group_list = new ArrayList<>();
                                int length = all_record.size();
                                for (int a = 0; a < length; a++) {
                                    group_list.add(all_record.get(a).id_group);
                                }
                                sample.if_valid = 0;
                                for (Integer this_one : group_list) {
                                    for (Integer that_one : customer_group_id) {
                                        if (this_one == that_one) {
                                            sample.if_valid = 1;
                                        }

                                    }
                                }
                            } else {
                                sample.if_valid = 1;
                            }
                            //第六层条件过滤，存在限定用户群组的情况下，用户不在特定群组里面
                            if (sample.if_valid == 0) {
                                return badRequest("Invalid User");
                            } else {
                                sample.promotion_products = products_list;
                                return ok(renderCategories(sample));
                            }
                        } else {
                            return badRequest("Sorry,this voucher has expired");
                            //                            Achieved maximum usage already
                            //                            Sorry, free dishes are sold out!
                        }
                    } else {
                        return badRequest("Invalid Voucher");
//                        Code expired already
                    }
//                }
                }
            }
        }
    }

    public static Result showAllOrderCart() {
        List<OrderCartRule> all_record = OrderCartRule.findAll();
        return ok(renderCategories(all_record));
    }

    //自动打印订单，返回所有订单 10 秒被访问一次
    @Transactional
    public static Result printOrders() {
        System.out.println("start to return +++++++");
        ArrayList<ReturnDriverOrders> driver_orders = getDriverOrders();
        List<Order> print_orders = Order.getPrintOrders();
        List<TotalPrintOrder> returnOrders = new ArrayList<>();
//        List orderID = OrderPrintHistory.getLastHour();
        List<Integer> orderID = new ArrayList<>();
        List<OrderPrintHistory> past_record = OrderPrintHistory.getLastHour();
        int size = past_record.size();
        for (int i = 0; i<size;i++){
            orderID.add(past_record.get(i).id_order);
        }
        //create a new list of orders using a pre-defined template
        for (Order order : print_orders) {
            int order_id = order.id_order;
            List<TotalPrintProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
            int address_id = order.id_address_delivery;              //Address information of each order
            AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
            if (address_id != 0) {                                      //If there is an address
                Address address = Address.findById(address_id);         //Find the address in the table of Address
                String alias = address.alias;
                String company = address.company;
                String firstname = address.firstname;
                String lastname = address.lastname;
                String address1 = address.address1;
                String address2 = address.address2;
                String city = address.city;
                String phone = address.phone;
                delivery_address = new AddressTemplate(
                        alias,
                        company,
                        firstname,
                        lastname,
                        address1,
                        address2,
                        city,
                        phone
                );
            } else {
                delivery_address = null;
            }
            String payment = order.payment;
            String reference = order.reference;
            String order_message;
            OrderMessage message = OrderMessage.findByID(order_id);
            if(message != null){
                order_message = message.message;
            }else {
                order_message = "";
            }
            int order_sequence = getSequenceNumber(order_id);
            List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);
            // get all the products detail from table ps_order_detail
            for (OrderDetail this_product : detail_products) {
                String product_location = "";
                if(this_product.product_upc.equals("")) {  //如果是shop产品
                    ProductLocation this_location = ProductLocation.findByProduct(this_product.product_id);
                    if (this_location != null) {
                        product_location = this_location.zone + "-" + this_location.layer + "-" + this_location.part + this_location.position_number;
                    }
                }
                TotalPrintProduct printProduct = new TotalPrintProduct(
                        this_product.product_id,
                        this_product.id_order_detail,
                        this_product.product_upc,
                        this_product.product_name,
                        this_product.product_reference,
                        product_location, //new
                        this_product.product_quantity,
                        this_product.total_price_tax_incl
                );
                bought_products.add(printProduct);
            }
            TotalPrintOrder final_order = new TotalPrintOrder(
            order_id,
            order_sequence,
            reference,
            payment,
            order_message,
            order.total_discounts_tax_incl,
            order.total_discounts_tax_excl,
            order.total_paid_real,
            order.total_paid_tax_excl,
            order.total_products,
            order.total_products_wt,
            order.total_shipping,
            order.date_add,
            delivery_address,
            bought_products,
            ""
            );
            if (!orderID.contains(order_id)){
            returnOrders.add(final_order);
            OrderPrintHistory new_record = new OrderPrintHistory(order_id,order_sequence,new Date());
            new_record.save();
            }                                      //Keep adding orders into the ReturnOrder list
        }
        TotalPrintWithDriverOrders super_total = new TotalPrintWithDriverOrders(
                returnOrders,
                driver_orders
        );

        System.out.println("orders returned"+ "----------"+ returnOrders.size());
        return ok(new JSONSerializer().deepSerialize(super_total));
    }

    public static Result allPrintOrders() {
        List<OrderPrintHistory> print_orders = OrderPrintHistory.findAll();
        return ok(renderCategories(print_orders));
    }
    public static Result getOneHour(){
        List<Integer> orderID = new ArrayList<>();
        List<OrderPrintHistory> past_record = OrderPrintHistory.getLastHour();
        int size = past_record.size();
        for (int i = 0; i<size;i++){
            orderID.add(past_record.get(i).id_order);
        }
        return ok(renderCategories(orderID));
    }

    //手动重新打印一张订单
    public static Result reprint(int orderID){
        System.out.println("start to return +++++++");
        Order new_print = Order.findById(orderID);
        //create a new list of orders using a pre-defined template
            int order_id = new_print.id_order;
            List<TotalPrintProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
            List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);
            // get all the products detail from table ps_order_detail
            for (OrderDetail this_product : detail_products) {
                String product_location = "";
                if(this_product.product_upc.equals("")) {
                    ProductLocation this_location = ProductLocation.findByProduct(this_product.product_id);
                    if (this_location != null) {
                        product_location = this_location.zone + "-" + this_location.layer + "-" + this_location.part + this_location.position_number;
                    }
                }

                TotalPrintProduct printProduct = new TotalPrintProduct(
                        this_product.product_id,
                        this_product.id_order_detail,
                        this_product.product_upc,
                        this_product.product_name,
                        this_product.product_reference,
                        product_location, //new
                        this_product.product_quantity,
                        this_product.total_price_tax_incl
                );
                bought_products.add(printProduct);
            }
            int address_id = new_print.id_address_delivery;              //Address information of each order
            AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
            if (address_id != 0) {                                      //If there is an address
                Address address = Address.findById(address_id);         //Find the address in the table of Address
                String alias = address.alias;
                String company = address.company;
                String firstname = address.firstname;
                String lastname = address.lastname;
                String address1 = address.address1;
                String address2 = address.address2;
                String city = address.city;
                String phone = address.phone;
                delivery_address = new AddressTemplate(
                        alias,
                        company,
                        firstname,
                        lastname,
                        address1,
                        address2,
                        city,
                        phone
                );
            } else {
                delivery_address = null;
            }
            String payment = new_print.payment;
            String reference = new_print.reference;
            String order_message;
            OrderMessage message = OrderMessage.findByID(order_id);
            if(message != null){
                order_message = message.message;
            }else {
                order_message = "";
            }
            int order_sequence = getSequenceNumber(order_id);
            int order_state = new_print.current_state;
            String current_state;
            switch (order_state) {
                case 1:  current_state = "Awaiting check payment";
                    break;
                case 2:  current_state = "Payment accepted";
                    break;
                case 3:  current_state = "Processing in Progress";
                    break;
                case 4:  current_state = "shipped";
                    break;
                case 5:  current_state = "Delivered";
                    break;
                case 6:  current_state = "Cancelled";
                    break;
                case 11:  current_state = "Refunded";
                    break;
                default: current_state = "Processing in Progress";
                    break;
            }
            TotalPrintOrder final_order = new TotalPrintOrder(
                    order_id,
                    order_sequence,
                    reference,
                    payment,
                    order_message,
                    new_print.total_discounts_tax_incl,
                    new_print.total_discounts_tax_excl,
                    new_print.total_paid_real,
                    new_print.total_paid_tax_excl,
                    new_print.total_products,
                    new_print.total_products_wt,
                    new_print.total_shipping,
                    new_print.date_add,
                    delivery_address,
                    bought_products,
                    current_state
            );
        System.out.println("orders returned"+ "----------");
        return ok(new JSONSerializer().deepSerialize(final_order));
    }

    /**
     * All the orders of the day for drivers to deliver
     * Cashier will allocate those orders to at-work drivers
     * */
    public static Result reprintAll() throws Exception {
        List<Order> orders_of_the_day = Order.ordersForDriver();
        ArrayList<OrderWithSequence> orders_with_sequence = new ArrayList<>();
        for (Order current_order : orders_of_the_day) {
            OrderWithSequence new_sequence_order = new OrderWithSequence();
            int id_order = current_order.id_order;
            OrderInvoice current_invoice = OrderInvoice.findByOrder(id_order);
                if (current_invoice != null){          //only successfully paid orders can be counted
                    new_sequence_order.delivery_address = current_invoice.delivery_address;
                    new_sequence_order.id_order = id_order;
                    new_sequence_order.order_reference = current_order.reference;
                    new_sequence_order.order_sequence = getSequenceNumber(id_order);
                    new_sequence_order.id_customer = current_order.id_customer;
                    new_sequence_order.payment_method = current_order.payment;
                    int state = current_order.current_state;
                    new_sequence_order.order_state = OrderState.findById(state);
                    new_sequence_order.total_discounts = current_order.total_discounts;
                    new_sequence_order.total_paid = current_order.total_paid;
                    new_sequence_order.total_shipping = current_order.total_shipping;
                    new_sequence_order.date_add = current_order.date_add;
                    new_sequence_order.delivery_date = current_order.delivery_date;
                    orders_with_sequence.add(new_sequence_order);
                }
        }
        return ok(renderCategories(orders_with_sequence));
    }
    //手动打单，返回所有订单
    public static Result manualPrint() {
        System.out.println("start to return +++++++");
        ArrayList<ReturnDriverOrders> driver_orders = getDriverOrders();
        List<Order> print_orders = Order.getManualPrintOrders();
        List<TotalPrintOrder> returnOrders = new ArrayList<>();
//        List orderID = OrderPrintHistory.getLastHour();
        List<Integer> orderID = new ArrayList<>();
        List<OrderPrintHistory> past_record = OrderPrintHistory.getLastHour();
        int size = past_record.size();
        for (int i = 0; i<size;i++){
            orderID.add(past_record.get(i).id_order);
        }
        //create a new list of orders using a pre-defined template
        for (Order order : print_orders) {
            int order_id = order.id_order;
            List<TotalPrintProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
            List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);
            // get all the products detail from table ps_order_detail
            for (OrderDetail this_product : detail_products) {
                String product_location = "";
                if(this_product.product_upc.equals("")) {
                    ProductLocation this_location = ProductLocation.findByProduct(this_product.product_id);
                    if (this_location != null) {
                    product_location = this_location.zone + "-" + this_location.layer + "-" + this_location.part + this_location.position_number;
                    }
                }

                TotalPrintProduct printProduct = new TotalPrintProduct(
                        this_product.product_id,
                        this_product.id_order_detail,
                        this_product.product_upc,
                        this_product.product_name,
                        this_product.product_reference,
                        product_location,  //new
                        this_product.product_quantity,
                        this_product.total_price_tax_incl
                );
                bought_products.add(printProduct);
            }
            int address_id = order.id_address_delivery;              //Address information of each order
            AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
            if (address_id != 0) {                                      //If there is an address
                Address address = Address.findById(address_id);         //Find the address in the table of Address
                String alias = address.alias;
                String company = address.company;
                String firstname = address.firstname;
                String lastname = address.lastname;
                String address1 = address.address1;
                String address2 = address.address2;
                String city = address.city;
                String phone = address.phone;
                delivery_address = new AddressTemplate(
                        alias,
                        company,
                        firstname,
                        lastname,
                        address1,
                        address2,
                        city,
                        phone
                );
            } else {
                delivery_address = null;
            }
            String payment = order.payment;
            String reference = order.reference;
            String order_message;
            OrderMessage message = OrderMessage.findByID(order_id);
            if(message != null){
                order_message = message.message;
            }else {
                order_message = "";
            }
            int order_sequence = getSequenceNumber(order_id);
            int order_state = order.current_state;
            String current_state;
            switch (order_state) {
                case 1:  current_state = "Awaiting check payment";
                    break;
                case 2:  current_state = "Payment accepted";
                    break;
                case 3:  current_state = "Processing in Progress";
                    break;
                case 4:  current_state = "shipped";
                    break;
                case 5:  current_state = "Delivered";
                    break;
                case 6:  current_state = "Cancelled";
                    break;
                case 11:  current_state = "Refunded";
                    break;
                default: current_state = "Processing in Progress";
                    break;
            }
            TotalPrintOrder final_order = new TotalPrintOrder(
                    order_id,
                    order_sequence,
                    reference,
                    payment,
                    order_message,
                    order.total_discounts_tax_incl,
                    order.total_discounts_tax_excl,
                    order.total_paid_real,
                    order.total_paid_tax_excl,
                    order.total_products,
                    order.total_products_wt,
                    order.total_shipping,
                    order.date_add,
                    delivery_address,
                    bought_products,
                    current_state
            );
            if (!orderID.contains(order_id)){
                returnOrders.add(final_order);
//                OrderPrintHistory new_record = new OrderPrintHistory(order_id,order_sequence,new Date());
//                new_record.save();
            }                                      //Keep adding orders into the ReturnOrder list
        }
        System.out.println("orders returned"+ "----------"+ returnOrders.size());
        TotalPrintWithDriverOrders all_in_one = new TotalPrintWithDriverOrders(
                returnOrders,driver_orders
        );
        return ok(new JSONSerializer().deepSerialize(all_in_one));
    }

    //手动添加打印记录
    public static Result manualAddHistory() throws Exception {
        DynamicForm printForm = Form.form().bindFromRequest();
        int order_id = Integer.valueOf(printForm.get("order_id"));
        int order_sequence = Integer.valueOf(printForm.get("order_sequence"));

        if(OrderPrintHistory.findById(order_id) == null){
        OrderPrintHistory new_record = new OrderPrintHistory(order_id,order_sequence,new Date());
        new_record.save();
        System.out.println("new record ");
        return ok("new record added");}
        else {
            System.out.println("already printed ");
            return  ok("already printed before");
        }
    }

    /** Get a group of order numbers
     */
    public static ArrayList<ProductQuantity> getOrderIDs(String phoneNumber) {
        String sql =
                "select distinct ps_orders.id_order\n" +
                        "\n" +
                        "from ps_orders, ps_address \n" +
                        "\n" +
                        "where ps_orders.id_address_delivery = ps_address.id_address\n" +
                        "\n" +
                        "and ps_address.phone like" + "'%" + phoneNumber + "%'\n" +
                        "\n" +
                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                        "\n" +
                        "order by ps_orders.id_order";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_orders.id_order", "quantity")
                        .create();

        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        List<ProductQuantity> list = query.findList();
        ArrayList<ProductQuantity> orderIDs = new ArrayList<ProductQuantity>();
        orderIDs.addAll(list);                  //list to Array
        return orderIDs;
    }

    public static Result quickSearchOrders(String phoneNumber) {
        System.out.println("start to return +++++++");
        ArrayList<ProductQuantity> orderIDs = getOrderIDs(phoneNumber);
        List<TotalPrintOrder> returnOrders = new ArrayList<>();
        for(ProductQuantity id: orderIDs){
            int order_id = id.quantity;
            Order order = Order.findById(order_id);
            List<TotalPrintProduct> bought_products = new ArrayList<>();                // create a template list of products info, which will be returned
            List<OrderDetail> detail_products = OrderDetail.findByOrder(order_id);
            // get all the products detail from table ps_order_detail
            for (OrderDetail this_product : detail_products) {
                TotalPrintProduct printProduct = new TotalPrintProduct(
                        this_product.product_id,
                        this_product.id_order_detail,
                        this_product.product_upc,
                        this_product.product_name,
                        this_product.product_reference,
                        "",
                        this_product.product_quantity,
                        this_product.total_price_tax_incl
                );
                bought_products.add(printProduct);
            }
            int address_id = order.id_address_delivery;              //Address information of each order
            AddressTemplate delivery_address;                           //Define an object using AddressTemplate for later return
            if (address_id != 0) {                                      //If there is an address
                Address address = Address.findById(address_id);         //Find the address in the table of Address
                String alias = address.alias;
                String company = address.company;
                String firstname = address.firstname;
                String lastname = address.lastname;
                String address1 = address.address1;
                String address2 = address.address2;
                String city = address.city;
                String phone = address.phone;
                delivery_address = new AddressTemplate(
                        alias,
                        company,
                        firstname,
                        lastname,
                        address1,
                        address2,
                        city,
                        phone
                );
            } else {
                delivery_address = null;
            }
            String payment = order.payment;
            String reference = order.reference;
            String order_message;
            OrderMessage message = OrderMessage.findByID(order_id);
            if(message != null){
                order_message = message.message;
            }else {
                order_message = "";
            }
            int order_sequence = getSequenceNumber(order_id);
            int order_state = order.current_state;
            String current_state;
            switch (order_state) {
                case 1:  current_state = "Awaiting check payment";
                    break;
                case 2:  current_state = "Payment accepted";
                    break;
                case 3:  current_state = "Processing in Progress";
                    break;
                case 4:  current_state = "shipped";
                    break;
                case 5:  current_state = "Delivered";
                    break;
                case 6:  current_state = "Cancelled";
                    break;
                case 11:  current_state = "Refunded";
                    break;
                default: current_state = "Processing in Progress";
                    break;
            }
            TotalPrintOrder final_order = new TotalPrintOrder(
                    order_id,
                    order_sequence,
                    reference,
                    payment,
                    order_message,
                    order.total_discounts_tax_incl,
                    order.total_discounts_tax_excl,
                    order.total_paid_real,
                    order.total_paid_tax_excl,
                    order.total_products,
                    order.total_products_wt,
                    order.total_shipping,
                    order.date_add,
                    delivery_address,
                    bought_products,
                    current_state
            );
            returnOrders.add(final_order);
            //Keep adding orders into the ReturnOrder list
        }
        System.out.println("orders returned"+ "----------"+ returnOrders.size());
        return ok(new JSONSerializer().deepSerialize(returnOrders));
    }

    public static ArrayList<ReturnDriverOrders> getDriverOrders(){
        String sql =
                "select ps_drivers.name, ps_drivers.contact_number, ps_drivers_orders.order_number, ps_drivers_orders.start_time, ps_drivers_orders.delivered_time\n" +
                        "from ps_drivers\n" +
                        "LEFT JOIN ps_drivers_orders\n" +
                        "ON ps_drivers_orders.id_driver = ps_drivers.id_driver\n" +
                        "AND ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                        "and ps_drivers_orders.if_delivered = 0\n" +
                        "where ps_drivers.id_driver != 8 \n" +
                        "and ps_drivers.if_work = 1\n" +
                        "order by ps_drivers.id_driver";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_drivers.name", "driver_name")
                        .columnMapping("ps_drivers.contact_number", "contact_number")
                        .columnMapping("ps_drivers_orders.order_number", "order_number")
                        .columnMapping("ps_drivers_orders.start_time", "start_time")
                        .columnMapping("ps_drivers_orders.delivered_time", "delivered_time")
                        .create();
        com.avaje.ebean.Query<ReturnDriverOrders> query = Ebean.find(ReturnDriverOrders.class);
        query.setRawSql(rawSql);
        List<ReturnDriverOrders> list = query.findList();
        ArrayList<ReturnDriverOrders> all_driver_order = new ArrayList<>();
        String index_driver_name;  //当前指针
        String contact_number;    //当前指针的电话
        ArrayList<Integer> doing_orders = new ArrayList<>();  //当前指针的数组
        ArrayList<Integer> todo_orders = new ArrayList<>();   //当前指针的数组
        Date original_time = new Date(0);
        int number_of_items = list.size();
        if(number_of_items>0){
            index_driver_name = list.get(0).driver_name;
            contact_number = list.get(0).contact_number;
        }else {
            index_driver_name = "";
            contact_number = "";
        }
        for(int i = 0; i < number_of_items; i++){
            ReturnDriverOrders this_one = list.get(i);
            if(!this_one.driver_name.equals(index_driver_name)){  //跳进下一个司机的送单情况， 处理上一个司机的所有信息
              ReturnDriverOrders last_one = new ReturnDriverOrders();
                last_one.driver_name = index_driver_name;
                last_one.contact_number = contact_number;
                last_one.doing_orders.addAll(doing_orders);
                last_one.todo_orders.addAll(todo_orders);
                if(todo_orders.size() ==0 && doing_orders.size() ==0){
                    last_one.working_status = "Available";
                }else {
                    last_one.working_status = "In delivery";
                }
                all_driver_order.add(last_one);  //把上一个司机的所有情况加入 返回数组
                //更新指针
                index_driver_name = this_one.driver_name;
                contact_number = this_one.contact_number;
                doing_orders.clear();
                todo_orders.clear();
            }
            //处理这个司机的每个订单的情况
          if(this_one.start_time!=null && this_one.delivered_time !=null){
           if(this_one.start_time.after(original_time) && this_one.delivered_time.equals(original_time)){
               doing_orders.add(this_one.order_number);
           }else if(this_one.start_time.equals(original_time) && this_one.delivered_time.equals(original_time)){
               todo_orders.add(this_one.order_number);
           }
          }
           // 最后一个司机的送单情况，加入总数组
            if(i == number_of_items -1){
            ReturnDriverOrders very_last = new ReturnDriverOrders();
            very_last.driver_name = index_driver_name;
            very_last.contact_number = contact_number;
            very_last.doing_orders.addAll(doing_orders);
            very_last.todo_orders.addAll(todo_orders);
            if(todo_orders.size() ==0 && doing_orders.size() ==0){
                very_last.working_status = "Available";
            }else {
                very_last.working_status = "In delivery";
            }
            all_driver_order.add(very_last);
            }

        }
        return all_driver_order;
    }

}