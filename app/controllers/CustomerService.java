package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.database.Address;
import models.database.Customer;
import models.sqlContainer.CustomerServiceOrderInfo;
import models.sqlContainer.ProductQuantity;
import models.template.CustomerServiceProducts;
import models.template.DeliverySummary;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.customerService.*;
import views.html.errorMessageTemplate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
//import flexjson.JSONDeserializer;

public class CustomerService extends Controller {

    public static class SearchCustomerOrder{
        public Integer order_number;
        public Integer phone_number;
        public String address;
        public String customer_name;
        public String email;
        public BigDecimal order_price;
        public Date start_time ;
        public Date end_time ;

        public SearchCustomerOrder(){

        }

        public SearchCustomerOrder(int order_number, int phone_number, String address, String customer_name, String email, BigDecimal order_price, Date start_time, Date end_time) {
            this.order_number = order_number;
            this.phone_number = phone_number;
            this.address = address;
            this.customer_name = customer_name;
            this.email = email;
            this.order_price = order_price;
            this.start_time = start_time;
            this.end_time = end_time;
        }

        public int getOrder_number() {
            return order_number;
        }

        public void setOrder_number(int order_number) {
            this.order_number = order_number;
        }

        public int getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(int phone_number) {
            this.phone_number = phone_number;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public BigDecimal getOrder_price() {
            return order_price;
        }

        public void setOrder_price(BigDecimal order_price) {
            this.order_price = order_price;
        }

        public Date getStart_time() {
            return start_time;
        }

        public void setStart_time(Date start_time) {
            this.start_time = start_time;
        }

        public Date getEnd_time() {
            return end_time;
        }

        public void setEnd_time(Date end_time) {
            this.end_time = end_time;
        }
    }

    public static Result showCsPage(){
        Form<SearchCustomerOrder> searchCustomerInfo = Form.form(SearchCustomerOrder.class);
        return ok(customerServiceSearchPage.render(searchCustomerInfo));
    }

    public static Result searchCustomerOrder(){
        DynamicForm searchCustomerOrderForm = Form.form().bindFromRequest();  //receive
        try {
                ArrayList<ProductQuantity> order_list = new ArrayList<>(); //用来存放搜索出来的实际订单号码
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                //解析出8个参数, 根据不同的情况进行不同的数据库筛选。 拼接SQL
                if( searchCustomerOrderForm.get("order_number")!=null && searchCustomerOrderForm.get("order_number")!= ""){  //如果顾客有办法提供order_number, 说明这一单已经被司机送出去了，理论上说，司机-订单 那张表格里面一定有记录
                    // 根据 order_number， 在 ps_drivers_orders  里面找到这张单， 找到单号
                    StringBuilder query_start = new StringBuilder("SELECT id_order \n FROM ps_drivers_orders\n");
                    StringBuilder conditions = new StringBuilder("WHERE order_number= ");
                    conditions.append(searchCustomerOrderForm.get("order_number")).append("\n");
                    if (searchCustomerOrderForm.get("start_time") !=null && searchCustomerOrderForm.get("end_time")!=null
                            &&searchCustomerOrderForm.get("start_time") !=""&& searchCustomerOrderForm.get("end_time")!=""){ //检索条件里面含有开始时间和结束时间
                        if (sdf.parse(searchCustomerOrderForm.get("start_time")).after(sdf.parse(searchCustomerOrderForm.get("end_time")))) {
                            return badRequest(errorMessageTemplate.render("Start Time cannot be later than End time!"));
                        }else {
                            conditions.append("AND date_add > ")
                                    .append("'")
                                    .append(searchCustomerOrderForm.get("start_time"))
                                    .append("'")
                                    .append("\n")
                                    .append("AND date_end < ")
                                    .append("'")
                                    .append(searchCustomerOrderForm.get("end_time"))
                                    .append("'")
                                    .append("\n");
                        }
                    }else if(searchCustomerOrderForm.get("start_time") !=null
                            && searchCustomerOrderForm.get("start_time") !=""
                            && (searchCustomerOrderForm.get("end_time") == null || searchCustomerOrderForm.get("end_time") =="")){ //检索条件里面只有开始时间，没有结束时间，默认 start_time --> now
                        conditions.append("AND date_add > ")
                                .append("'")
                                .append(searchCustomerOrderForm.get("start_time"))
                                .append("'")
                                .append("\n")
                                .append("AND date_add < now()\n");

                    }else if((searchCustomerOrderForm.get("start_time")==null ||searchCustomerOrderForm.get("start_time")=="")
                            && searchCustomerOrderForm.get("end_time")!=null
                            && searchCustomerOrderForm.get("end_time")!=""){ //检索条件里面没有开始时间，只有结束时间，默认 past --> 结束时间
                        conditions.append("AND date_add < ")
                                .append("'")
                                .append(searchCustomerOrderForm.get("end_time"))
                                .append("'")
                                .append("\n");
                    }else { //没有说明开始或者结束时间，所以就只查询24 小时之内的订单,应该查询当天的订单，得到当天的日子，并且得到日期
                           conditions.append("AND date_add > ")
                                .append("'")
                                .append(sdf.format(new Date())) //查询当天的订单
                                .append("'")
                                .append("\n");
                        }
                    conditions.append("Order BY id_order desc\n");
                    conditions.append("LIMIT 30");
                    RawSql rawSql =
                            RawSqlBuilder
                                    .parse(query_start.append(conditions).toString())
                                    .columnMapping("id_order", "quantity")
                                    .create();
                    com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                    query.setRawSql(rawSql);
                    List<ProductQuantity> list = query.findList();
                    if(list.size() ==0){
                        return notFound(errorMessageTemplate.render("Sorry,no such record"));
                    }else{
                        order_list.addAll(list);  //得到所有单号
                    }
                }else{ //用户没有提供order_number, 所以只有从其他的条件里筛选出内容来进行SQL 判断, SQL拼接
                    StringBuilder query_start = new StringBuilder("Select distinct ps_orders.id_order\n");
                    StringBuilder table_joint = new StringBuilder();
                    StringBuilder conditions = new StringBuilder();
                    if(searchCustomerOrderForm.get("email") !=null && searchCustomerOrderForm.get("email") != ""){ //搜索条件里面添加了邮箱，需要多合并一张表
                        query_start.append("FROM ps_orders, ps_order_detail, ps_address, ps_customer\n");
                        table_joint.append("WHERE ps_customer.id_customer = ps_orders.id_customer \n")
                                .append("AND ps_orders.id_order = ps_order_detail.id_order\n")
                                .append("AND ps_orders.id_address_delivery = ps_address.id_address\n");
                        conditions.append("AND ps_customer.email like '%").append(searchCustomerOrderForm.get("email").replaceAll(" ","")).append("%'\n"); //去掉空格
                    }else { //搜索条件中没有加上邮箱，所以只用三张表格
                        query_start.append("FROM ps_orders, ps_order_detail, ps_address\n");
                        table_joint.append("WHERE ps_orders.id_order = ps_order_detail.id_order\n")
                                .append("AND ps_orders.id_address_delivery = ps_address.id_address\n");
                    }
                    if(searchCustomerOrderForm.get("phone_number")!=null && searchCustomerOrderForm.get("phone_number") !=""){ //Phone Number
                        conditions.append("AND ps_address.phone like '%")
                                  .append(searchCustomerOrderForm.get("phone_number").replace(" ","")).append("%'\n");
                    }
                    if(searchCustomerOrderForm.get("address")!=null && searchCustomerOrderForm.get("address")!=""){ //Address
                        conditions.append("AND CONCAT(ps_address.address1,ps_address.address2) like '%")
                                  .append(searchCustomerOrderForm.get("address")
                                          .replaceAll("[^A-Za-z0-9 ]", " ") //保留字符，去除符号，留空
                                          .replace(" ", "%")) //在字符间隔中加上模糊判断符号
                                  .append("%'\n");
                    }
                    if(searchCustomerOrderForm.get("customer_name")!=null && searchCustomerOrderForm.get("customer_name") != ""){ //Customer Name
                        conditions.append("AND replace(CONCAT(ps_address.firstname,ps_address.lastname),' ','') like '%")
                                   .append(searchCustomerOrderForm.get("customer_name").replace(" ","")) //去除空格
                                   .append("%'\n");
                    }
                    if(searchCustomerOrderForm.get("order_price")!=null && searchCustomerOrderForm.get("order_price") !=""){ //Order price
                        conditions.append("AND ps_orders.total_paid_real = ")
                        .append(searchCustomerOrderForm.get("order_price").replace(" ","")) //价格去空
                        .append("\n");
                    }
                    if (searchCustomerOrderForm.get("start_time") !=null && searchCustomerOrderForm.get("end_time")!=null
                            &&searchCustomerOrderForm.get("start_time") !=""&& searchCustomerOrderForm.get("end_time")!=""){ //检索条件里面含有开始时间和结束时间
                        if (sdf.parse(searchCustomerOrderForm.get("start_time")).after(sdf.parse(searchCustomerOrderForm.get("end_time")))) {
                            return badRequest(errorMessageTemplate.render("Start Time cannot be later than End time!"));
                        }else {
                            conditions.append("AND ps_orders.date_add > ")
                                    .append("'")
                                    .append(searchCustomerOrderForm.get("start_time"))
                                    .append("'")
                                    .append("\n")
                                    .append("AND ps_orders.date_add < ")
                                    .append("'")
                                    .append(searchCustomerOrderForm.get("end_time"))
                                    .append("'")
                                    .append("\n");
                        }
                    }else if(searchCustomerOrderForm.get("start_time") !=null
                            && searchCustomerOrderForm.get("start_time") != ""
                            && (searchCustomerOrderForm.get("end_time") == null || searchCustomerOrderForm.get("end_time") =="")){ //检索条件里面只有开始时间，没有结束时间，默认 start_time --> now
                        conditions.append("AND ps_orders.date_add > ")
                                .append("'")
                                .append(searchCustomerOrderForm.get("start_time"))
                                .append("'")
                                .append("\n")
                                .append("AND ps_orders.date_add < now()\n");

                    }else if((searchCustomerOrderForm.get("start_time")==null ||searchCustomerOrderForm.get("start_time")=="")
                            && searchCustomerOrderForm.get("end_time")!=null
                            && searchCustomerOrderForm.get("end_time")!=""){ //检索条件里面没有开始时间，只有结束时间，默认 past --> 结束时间
                        conditions.append("AND ps_orders.date_add < ")
                                .append("'")
                                .append(searchCustomerOrderForm.get("end_time"))
                                .append("'")
                                .append("\n");
                    }else { //没有说明开始或者结束时间，所以就只查询24 小时之内的订单
                        conditions.append("AND ps_orders.date_add > ")
                                .append("'")
                                .append(sdf.format(new Date())) //查询当天的订单
                                .append("'")
                                .append("\n");
                    }
                    conditions.append("ORDER BY ps_orders.id_order desc\n");
                    conditions.append("LIMIT 30");
                    //Final SQL: query_start + base_tables + table_joint + conditions
                    RawSql rawSql =
                            RawSqlBuilder
                                    .parse(query_start.append(table_joint).append(conditions).toString())
                                    .columnMapping("ps_orders.id_order", "quantity")
                                    .create();
                    com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                    query.setRawSql(rawSql);
                    List<ProductQuantity> list = query.findList();
                    if(list.size() ==0){
                        return notFound(errorMessageTemplate.render("No Related Records"));
                    }else {
                        order_list.addAll(list);  //得到所有单号
                    }
                }
                //所有订单相关的信息start
                String customer_name = "";
                String customer_email = "";
                int id_customer = 0;
                int id_address = 0; //看最后选择哪个作为标准来查询顾客信息
                CustomerServiceProducts data_container; //用来记录 id_customer/id_address,  address or customer Label
                if(order_list.size()>0){ //if there is any record, it means the past order(s) really exists
                    ArrayList<CustomerServiceOrderInfo> return_all_orders = new ArrayList<>();
                    for(ProductQuantity each_order: order_list){
                        //根据订单的实际单号 id_order, 筛选出每个订单的情况， 订单详情，是否被打印，司机配送情况， 用户点单历史记录
                        StringBuilder query_sql = new StringBuilder();
                        query_sql.append("select ps_order_print_history.order_sequence,ps_orders.id_order,ps_orders.reference, ps_orders.id_customer,ps_orders.date_add, ps_orders.payment,ps_orders.id_address_delivery, ps_orders.total_paid_real,ps_orders.total_shipping, ps_orders.total_discounts, ps_orders.invoice_date, ps_orders.valid, ps_orders.current_state, ps_orders.id_shop, ps_order_detail.product_name, ps_order_detail.product_quantity, ps_order_print_history.date_add, ps_drivers_orders.date_add, ps_drivers_orders.driver_name, ps_drivers_orders.id_cashier, ps_drivers_orders.if_delivered, ps_drivers_orders.start_time, ps_drivers_orders.delivered_time,ps_address.lastname, ps_address.firstname, ps_address.address1, ps_address.address2, ps_address.phone \n")
                                        .append("from ps_orders\n")
                                        .append("left join ps_order_detail\n")
                                        .append("on ps_order_detail.id_order = ps_orders.id_order\n")
                                        .append("left join ps_order_print_history\n")
                                        .append("on ps_order_print_history.id_order = ps_orders.id_order\n")
                                        .append("left join ps_drivers_orders\n")
                                        .append("on ps_drivers_orders.id_order = ps_orders.id_order\n")
                                        .append("left join ps_address\n")
                                        .append("on ps_address.id_address = ps_orders.id_address_delivery\n")
                                        .append("where ps_orders.id_order = ")
                                        .append(each_order.quantity);
                        RawSql rawSql =
                                RawSqlBuilder
                                .parse(query_sql.toString())
                                .columnMapping("ps_order_print_history.order_sequence", "order_sequence")
                                .columnMapping("ps_orders.id_order", "id_order")
                                .columnMapping("ps_orders.reference", "order_reference")
                                .columnMapping("ps_orders.id_customer", "id_customer")
                                .columnMapping("ps_orders.date_add", "date_add")
                                .columnMapping("ps_orders.payment", "payment")
                                .columnMapping("ps_orders.id_address_delivery", "id_address_delivery")
                                .columnMapping("ps_orders.total_paid_real", "total_paid_real")
                                .columnMapping("ps_orders.total_shipping", "total_shipping")
                                .columnMapping("ps_orders.total_discounts", "total_discounts")
                                .columnMapping("ps_orders.invoice_date", "successful_time")
                                .columnMapping("ps_orders.valid", "valid")
                                .columnMapping("ps_orders.current_state", "current_state")
                                .columnMapping("ps_orders.id_shop", "id_shop")
                                .columnMapping("ps_order_detail.product_name", "product_name")
                                .columnMapping("ps_order_detail.product_quantity", "product_quantity")
                                .columnMapping("ps_order_print_history.date_add", "print_time")
                                .columnMapping("ps_drivers_orders.date_add", "assign_driver_time")
                                .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                                .columnMapping("ps_drivers_orders.id_cashier", "id_cashier")
                                .columnMapping("ps_drivers_orders.if_delivered", "if_delivered")
                                .columnMapping("ps_drivers_orders.start_time", "start_deliver_time")
                                .columnMapping("ps_drivers_orders.delivered_time", "end_deliver_time")
                                .columnMapping("ps_address.firstname", "order_customer_firstname")
                                .columnMapping("ps_address.lastname", "order_customer_lastname")
                                .columnMapping("ps_address.address1", "order_address1")
                                .columnMapping("ps_address.address2", "order_address2")
                                .columnMapping("ps_address.phone", "order_phone")
                                .create();
                        com.avaje.ebean.Query<CustomerServiceOrderInfo> query = Ebean.find(CustomerServiceOrderInfo.class);
                        query.setRawSql(rawSql);
                        List<CustomerServiceOrderInfo> list_record = query.findList();
                        //理论情况下，应该存在至少一个记录
                        if(list_record.size()==0){
                            return notFound(errorMessageTemplate.render("Sorry,some information of this order was incomplete, please contact IT staff for help."));
                        }else{
                           //进入每一张订单的 产品查询
                            ArrayList<CustomerServiceProducts> products_list = new ArrayList<>(); //产品列表
                            for(CustomerServiceOrderInfo each_one: list_record){
                                if(each_one.product_name!=null && each_one.product_quantity !=null){
                                    products_list.add(new CustomerServiceProducts(each_one.product_name.replace("<b>","")
                                            .replace("</b>"," ")
                                            .replace("<br>"," ")
                                            .replace("<br/>"," ")
                                            .replace("<br />"," "),each_one.product_quantity));
                                }
                            }
                            list_record.get(0).bought_products = products_list;
                            if(list_record.get(0).id_customer ==149 || list_record.get(0).id_customer ==102 ){ //如果这个是电话点单，就在上面的标签值上面添加 id_address
                                id_address = list_record.get(0).id_address_delivery;
                            }else {
                                id_customer = list_record.get(0).id_customer;
                            }
                            if(customer_name.equals("")){
                                 customer_name = list_record.get(0).order_customer_firstname + " " +  list_record.get(0).order_customer_lastname;
                            }
                            return_all_orders.add(list_record.get(0));
                        }
                    }
                    //所有订单相关的信息end
//                    String address_or_customer;
                    ProductQuantity total_order_times;
                    if(id_address !=0 && id_customer ==0){ //用 id_address 作为索引，来查询这个用户之前的下单记录
                        StringBuilder sql = new StringBuilder();
                                sql.append("select count(*)\n")
                                        .append("from ps_orders\n")
                                        .append("where valid = 1\n")
                                        .append("and invoice_date > '1970-01-01'\n")
                                        .append("and id_address_delivery = ")
                                        .append(id_address);
                        RawSql rawSql =
                                RawSqlBuilder
                                        .parse(sql.toString())
                                        .columnMapping("count(*)", "quantity")
                                        .create();

                        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                        query.setRawSql(rawSql);
                        total_order_times = query.findUnique();
                        data_container = new CustomerServiceProducts("address",id_address);
//                        address_or_customer = "address";
                    }else{ //用 id_customer 作为索引，查询记录
                        StringBuilder sql = new StringBuilder();
                                sql.append("select count(*) \n")
                                        .append("from ps_orders\n")
                                        .append("where valid = 1\n")
                                        .append("and invoice_date > '1970-01-01'\n")
                                        .append("and id_customer = ")
                                        .append(id_customer);
                        RawSql rawSql =
                                RawSqlBuilder
                                        .parse(sql.toString())
                                        .columnMapping("count(*)", "quantity")
                                        .create();
                        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                        query.setRawSql(rawSql);
                        total_order_times = query.findUnique();
                        data_container = new CustomerServiceProducts("customer",id_customer);
                        Customer if_exist = Customer.findById(id_customer);
                        if(if_exist !=null){
                            customer_email = if_exist.email;
                        }else {
                            return notFound(errorMessageTemplate.render("No Such Customer."));
                        }
                    }
                    //循环结束之后，拿到所有的值，收集所有相关信息，整理好了之后传递到前端
                    return ok(customerServiceOrderInfo.render(return_all_orders,total_order_times,data_container,customer_name,customer_email));
                }else{ //如果没有搜索到记录，就提示客服人员，没有相关记录
                    return notFound(errorMessageTemplate.render("Sorry, No Related Record was found, please provide more information!"));
                }
            }catch (Exception e){
             return badRequest(errorMessageTemplate.render("Invalid Data requested"));
        }
    }


    public static Result checkOrderHistory(String customer_address,Integer id_customer_address){
        if(customer_address ==null || id_customer_address ==null){
            return badRequest(errorMessageTemplate.render("Invalid Request"));
        }else{
            List<Address> possible_address = new ArrayList<>();   //用来储存用户地址
            List<ProductQuantity> total_orders = new ArrayList<>(); //用来存储所有订单号码
            String customer_email = "";
            if(customer_address.equals("customer")){
                Customer if_exist = Customer.findById(id_customer_address);
                if(if_exist !=null){
                     customer_email = if_exist.email;
                }else {
                    return notFound(errorMessageTemplate.render("Not a valid user"));
                }
              //这个索引是 id_customer, customer 用自己的账号点单
                StringBuilder sql = new StringBuilder();
                sql.append("select id_order\n")
                        .append("from ps_orders\n")
                        .append("where valid = 1\n")
                        .append("and invoice_date > '1970-01-01'\n")
                        .append("and id_customer = ")
                        .append(id_customer_address)
                        .append("\n")
                        .append("Order by id_order desc");
                RawSql rawSql =
                        RawSqlBuilder
                                .parse(sql.toString())
                                .columnMapping("id_order", "quantity")
                                .create();
                com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                query.setRawSql(rawSql);
                List<ProductQuantity> total_order_id = query.findList();  //找到所有这个地址点过的订单ID
                total_orders.addAll(total_order_id);
                possible_address.addAll(Address.findCustomerAddress(id_customer_address)); //找到这个用户的所有地址
            }else if(customer_address.equals("address")){
              //这个索引是 id_address， customer 通过前台点单
                //就不用搜索这个用户所有的地址了，因为是前台，所有地址不具有代表性
                StringBuilder sql = new StringBuilder();
                sql.append("select id_order\n")
                        .append("from ps_orders\n")
                        .append("where valid = 1\n")
                        .append("and invoice_date > '1970-01-01'\n")
                        .append("and id_address_delivery = ")
                        .append(id_customer_address)
                        .append("\n")
                        .append("Order by id_order desc");
                RawSql rawSql =
                        RawSqlBuilder
                                .parse(sql.toString())
                                .columnMapping("id_order", "quantity")
                                .create();
                com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
                query.setRawSql(rawSql);
                List<ProductQuantity> total_order_id = query.findList();  //找到所有这个地址点过的订单ID
                total_orders.addAll(total_order_id);
                possible_address.add(Address.findById(id_customer_address)); //找到这个地址, 并且添加到所有地址栏去
            }
            ArrayList<CustomerServiceOrderInfo> return_all_orders = new ArrayList<>();
            String customer_name = "";
            BigDecimal total_spent_history = BigDecimal.valueOf(0);
            //准备做循环，来搜索每张订单里面的产品，价格， 是否使用voucher
            for(ProductQuantity each_order: total_orders){
                StringBuilder query_sql = new StringBuilder();
                query_sql.append("select ps_orders.id_order,ps_orders.reference, ps_orders.payment, ps_orders.total_paid_real,ps_orders.total_shipping, ps_orders.invoice_date, ps_orders.id_shop, ps_order_detail.product_name, ps_order_detail.product_quantity, ps_address.firstname, ps_address.lastname,ps_order_cart_rule.name, ps_order_cart_rule.value \n")
                        .append("from ps_orders\n")
                        .append("left join ps_order_detail\n")
                        .append("on ps_order_detail.id_order = ps_orders.id_order\n")
                        .append("left join ps_address\n")
                        .append("on ps_address.id_address = ps_orders.id_address_delivery\n")
                        .append("left join ps_order_cart_rule\n")
                        .append("on ps_order_cart_rule.id_order = ps_orders.id_order\n")
                        .append("where ps_orders.id_order = ")
                        .append(each_order.quantity);
                RawSql rawSql =
                        RawSqlBuilder
                                .parse(query_sql.toString())
                                .columnMapping("ps_orders.id_order", "id_order")
                                .columnMapping("ps_orders.reference", "order_reference")
                                .columnMapping("ps_orders.payment", "payment")
                                .columnMapping("ps_orders.total_paid_real", "total_paid_real")
                                .columnMapping("ps_orders.total_shipping", "total_shipping")
                                .columnMapping("ps_orders.invoice_date", "successful_time")
                                .columnMapping("ps_orders.id_shop", "id_shop")
                                .columnMapping("ps_order_detail.product_name", "product_name")
                                .columnMapping("ps_order_detail.product_quantity", "product_quantity")
                                .columnMapping("ps_address.firstname", "order_customer_firstname")
                                .columnMapping("ps_address.lastname", "order_customer_lastname")
                                .columnMapping("ps_order_cart_rule.name", "driver_name") //替代 voucher 的名字
                                .columnMapping("ps_order_cart_rule.value", "total_discounts") //voucher 的折扣
                                .create();
                com.avaje.ebean.Query<CustomerServiceOrderInfo> query = Ebean.find(CustomerServiceOrderInfo.class);
                query.setRawSql(rawSql);
                List<CustomerServiceOrderInfo> list_record = query.findList(); //每一张订单的所有产品列表
                //理论情况下，应该存在至少一个记录
                if(list_record.size()==0){
                    return notFound(errorMessageTemplate.render("Sorry,some information of this order was incomplete, please contact IT staff for help."));
                }else{
                    //进入每一张订单的 产品查询
                    ArrayList<CustomerServiceProducts> products_list = new ArrayList<>(); //产品列表
                    for(CustomerServiceOrderInfo each_one: list_record){
                        if(each_one.product_name!=null && each_one.product_quantity !=null){
                            products_list.add(new CustomerServiceProducts(each_one.product_name.replace("<b>","")
                                    .replace("</b>"," ")
                                    .replace("<br>"," ")
                                    .replace("<br/>"," ")
                                    .replace("<br />"," "),each_one.product_quantity));
                        }
                    }
                    list_record.get(0).bought_products = products_list;
                    if(customer_name.equals("")) {//如果有值了就不继续修改用户名
                        customer_name = list_record.get(0).order_customer_firstname + " " + list_record.get(0).order_customer_lastname; //循环获取，一直到有值为止
                    }
                    total_spent_history = total_spent_history.add(list_record.get(0).total_paid_real); //历史总消费
                    return_all_orders.add(list_record.get(0));
                }
            }
            DeliverySummary container_two = new DeliverySummary(0,total_spent_history,customer_email);
            return ok(customerServicePersonalPage.render(possible_address,return_all_orders,customer_name,container_two));
        }
    }
}