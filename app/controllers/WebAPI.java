/*
 * Copyright (c) 2017. Zhi Chen
 * WebAPI is for later use, showing the template method to connect Ebean class
 * with Database tables, as well as making use of Ebean Finder and call class methods
 * using controller methods..
 * @see JsonParser
 */

package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.database.*;
import models.sqlContainer.*;
import models.template.ManagerOrdersMarinaNumber;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
import views.html.driverPayroll.chooseDatePage;
import views.html.driverPayroll.driverSalarySummary;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static controllers.routes.StockAPI;
import static controllers.routes.WebAPI;
import static parsers.JsonParser.renderCustomer;
import static parsers.JsonParser.renderProduct;

public class WebAPI extends Controller
{
    public static class ManagerLogin {
        @Constraints.Required
        public String email = "a2b.manager@a2bliving.ie";
        public String password;

        public String validate() throws Exception {

            if (Customer.authenticate(email, password) == null) {
                return "Invalid user or password";
            }
             return null;

        }
    }

    public static class CounterLogin {
        @Constraints.Required
        public String email = "chenzhi1111@gmail.com";
        public String password;

        public String validate() throws Exception {

            if (Customer.authenticate(email, password) == null) {
                return "Invalid user or password";
            }
            return null;

        }
    }

    public static class DriverLogin {
        @Constraints.Required
        public String email = "chenzhi1111@gmail.com";
        public String password;

        public String validate() throws Exception {

            if (Customer.authenticate(email, password) == null) {
                return "Invalid user or password";
            }
            return null;

        }
    }

    public static class ShopAssistantLogin {
        @Constraints.Required
        public String email = "chenzhi1111@gmail.com";
        public String password;

        public String validate() throws Exception {

            if (Customer.authenticate(email, password) == null) {
                return "Invalid user or password";
            }
            return null;

        }
    }

    //Counter Log in
    public static Result shopAssistantLogin() {
        Form<ShopAssistantLogin> shopAssistantForm = Form.form(ShopAssistantLogin.class);
        return ok(shopAssistantlogin.render(shopAssistantForm));
    }

    public static class CashierID{
        @Constraints.Required
        public int cashier_id;
    }

    //Driver Log in
    public static Result driverLogin() {
        Form<DriverLogin> driverForm = Form.form(DriverLogin.class);
        return ok(driverslogin.render(driverForm));
    }

    //Counter Log in
    public static Result counterLogin() {
        Form<CounterLogin> counterForm = Form.form(CounterLogin.class);
        return ok(counterslogin.render(counterForm));
    }

    //Manager Log in
    public static Result login() {
        Form<ManagerLogin> managerForm = Form.form(ManagerLogin.class);
        return ok(login.render(managerForm));
    }

    //Drivers Main Page, Choose one working driver to check out
    public static Result driverDashboard() throws Exception {
        String email = session("driverEmail");
        // if in session
        if (email != null && email.equals("chenzhi1111@gmail.com")) {
            Date current_time = new Date();
            SimpleDateFormat matter1 = new SimpleDateFormat("yyyy-MM-dd");
            String date = matter1.format(current_time);
            //得到当天工作的所有司机信息, 除了free
            List<Driver> workingDrivers = Driver.findWorkingDrivers();
            return ok(driverDashboardPage.render(workingDrivers,date));
        }else{
            flash("error","please log in first");
            return redirect(WebAPI.driverLogin());
        }
    }

   //Counter Main Page, Choose which Tablet to check
    public static Result counterDashboard() throws Exception {
        String email = session("counterEmail");
        // if in session
        if (email != null && email.equals("chenzhi1111@gmail.com")) {
            Form<CashierID> cashierForm = Form.form(CashierID.class);
            return ok(counterDashboardPage.render(cashierForm));

        }else{
            flash("error","please log in first");
            return redirect(WebAPI.counterLogin());
        }
    }

    //Manager 系统查看当天营业情况汇总
    public static Result dashboard() throws Exception {
        String email = session("email");
        // if in session
        if (email != null && email.equals("a2b.manager@a2bliving.ie")) {
            ArrayList<TotalReturnManagerOrder> totalReturnManagerOrders;
            totalReturnManagerOrders = getOrdersForManager();
            /**create two empty object for later use. One will be assigned the value as "Cash payment summary"
             *the other will be assigned the value of "Online payment summary"
             * */
            TotalReturnManagerOrder a;
            TotalReturnManagerOrder b;
            TotalReturnManagerOrder c;
            TotalReturnManagerOrder exception_a = new TotalReturnManagerOrder();  //cash
            TotalReturnManagerOrder exception_b = new TotalReturnManagerOrder();   //online
            TotalReturnManagerOrder exception_c = new TotalReturnManagerOrder();   //online
            TotalReturnManagerOrder total = new TotalReturnManagerOrder();
            if (totalReturnManagerOrders != null){
                if (totalReturnManagerOrders.size() == 1 ){
                    a = totalReturnManagerOrders.get(0);
                    if(a.payment_method.contains("Cash")){
                        total.total_cashpay_number = a.total_onlinepay_number;
                        total.total_cashpay_sales = a.total_onlinepay_sales;
                        total.total_cashpay_delivery = a.total_onlinepay_delivery;
                    }else{
                        total.total_onlinepay_number= a.total_onlinepay_number;
                        total.total_onlinepay_sales= a.total_onlinepay_sales;
                        total.total_onlinepay_delivery = a.total_onlinepay_delivery;
                    }
                }else if (totalReturnManagerOrders.size() == 2){
                    if (!totalReturnManagerOrders.get(0).payment_method.contains("Cash")){
                        a = totalReturnManagerOrders.get(0);
                        b = totalReturnManagerOrders.get(1);
                    }
                    else{
                        a = totalReturnManagerOrders.get(1);
                        b = totalReturnManagerOrders.get(0);
                    }
                    total.total_onlinepay_number = a.total_onlinepay_number;
                    total.total_onlinepay_sales = a.total_onlinepay_sales;
                    total.total_onlinepay_delivery = a.total_onlinepay_delivery;
                    total.total_cashpay_number = b.total_onlinepay_number;
                    total.total_cashpay_sales = b.total_onlinepay_sales;
                    total.total_cashpay_delivery = b.total_onlinepay_delivery;
                }else if (totalReturnManagerOrders.size() == 3){
                    a = totalReturnManagerOrders.get(0);
                    b = totalReturnManagerOrders.get(1);
                    c = totalReturnManagerOrders.get(2);

                    total.total_cashpay_number = a.total_onlinepay_number;
                    total.total_cashpay_sales = a.total_onlinepay_sales;
                    total.total_cashpay_delivery = a.total_onlinepay_delivery;

                    total.total_onlinepay_number = b.total_onlinepay_number + c.total_onlinepay_number;
                    total.total_onlinepay_sales = b.total_onlinepay_sales.add(c.total_onlinepay_sales);
                    total.total_onlinepay_delivery = b.total_onlinepay_delivery.add(c.total_onlinepay_delivery);
                }
                ArrayList<TotalReturnManagerOrder> totalExceptionOrders;
                totalExceptionOrders = getExceptionManagerOrders();
                if(totalExceptionOrders.size() > 0 ){
                    if (totalExceptionOrders.size() ==1){
                        if(totalExceptionOrders.get(0).payment_method.contains("Cash")){
                            exception_a = totalExceptionOrders.get(0);//cash
                        }else {
                            exception_b = totalExceptionOrders.get(0);//online
                        }
                    }else if (totalExceptionOrders.size() ==2){
                        if(totalExceptionOrders.get(0).payment_method.contains("Cash")){
                            exception_a = totalExceptionOrders.get(0);
                            exception_b = totalExceptionOrders.get(1);
                        }else {
                            exception_a = totalExceptionOrders.get(1);
                            exception_b = totalExceptionOrders.get(0);
                        }
                    }else if (totalExceptionOrders.size() ==3){
                        exception_a = totalExceptionOrders.get(0);
                        exception_b = totalExceptionOrders.get(1);
                        exception_c = totalExceptionOrders.get(2);
                    }
                    total.total_cashpay_sales = total.total_cashpay_sales.add(exception_a.total_onlinepay_delivery);
                    total.total_onlinepay_sales = total.total_onlinepay_sales.add(exception_b.total_onlinepay_delivery).add(exception_c.total_onlinepay_delivery);
                    total.total_cashpay_delivery = total.total_cashpay_delivery.subtract(exception_a.total_onlinepay_delivery);
                    total.total_onlinepay_delivery = total.total_onlinepay_delivery.subtract(exception_b.total_onlinepay_delivery).subtract(exception_c.total_onlinepay_delivery);
                }
                int cashpay_orders = total.total_cashpay_number;
                int onlinepay_orders = total.total_onlinepay_number;
                int total_orders = cashpay_orders + onlinepay_orders;
                double total_sales = total.total_cashpay_sales.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + total.total_onlinepay_sales.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();  // round the number to two digits
                DecimalFormat df = new DecimalFormat("#.##");
                total_sales = Double.valueOf(df.format(total_sales));
                ManagerOrdersMarinaNumber managerOrdersMarinaNumber = managerTotalOrders();
                ArrayList<ReturnManagerOrder> totalOrders = managerOrdersMarinaNumber.returnManagerOrders;
//                int marinaOrders = managerOrdersMarinaNumber.marinaOrders;
//                int marinaChargedOrders = managerOrdersMarinaNumber.marinaChargedOrders;
                ArrayList<BrandSummary> totalBrands = getBrandSummary();
                List<CashierCash> all_cashiers = CashierCash.findCashiers();
                int cashier_number = all_cashiers.size();
                ArrayList<MealDealProduct> cashier_vouchers = getCashierSentVouchers(); //前台发出去的代金券
                List<DriverDailySummary> all_driver_salary = DriverDailySummary.salary_of_day();
//                BigDecimal total_driver_salary = BigDecimal.valueOf(0);
                BigDecimal total_voucher_value = BigDecimal.valueOf(0);
                for (DriverDailySummary this_one : all_driver_salary) {
//                    total_driver_salary = total_driver_salary.add(this_one.total_wage).add(this_one.extra_cost);
                    total_voucher_value = total_voucher_value.add(this_one.total_voucher_value);
                }
//                //总现金结算： 现金付款收入(不含运费) - 司机在线付款运费 - 司机工资（包含extra cost） - Marina 特殊订单 - 总voucher代金券
//                BigDecimal final_cash_left = total.total_cashpay_sales.subtract(total.total_onlinepay_delivery).subtract(total_driver_salary).subtract(BigDecimal.valueOf(marinaChargedOrders)).subtract(total_voucher_value);
                //新现金结算规则： 现金订单总收入 - 司机在线付款运费 - 总Voucher 代金券
                BigDecimal final_cash_left = total.total_cashpay_sales.subtract(total.total_onlinepay_delivery).subtract(total_voucher_value);
                TotalReturnManagerOrder data_container = new TotalReturnManagerOrder(0,0,BigDecimal.valueOf(0),total_voucher_value, final_cash_left, BigDecimal.valueOf(0), "");
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                String today = dateFormat.format(date);
                return ok(managerDashboard.render(total,total_orders,total_sales,totalOrders,all_cashiers,cashier_number,totalBrands,cashier_vouchers,today,data_container));
            } else {
                return notFound("No such driver");
            }
        }else{
            flash("error","please log in first");
            return redirect(WebAPI.login());
        }
    }

    public static ArrayList<MealDealProduct> getCashierSentVouchers() {
        String sql =
                "SELECT ps_message.id_customer, count(ps_message.id_message)\n" +
                        "\n" +
                        "FROM ps_orders, ps_message\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_message.id_order\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +
                        "\n" +
                        "and ps_message.message like '%+CONGRATULATIONS%'\n" +
                        "\n" +
                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 13 HOUR)\n" +
                        "\n" +
                        "group by ps_message.id_customer";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_message.id_customer", "id_product_shop")
                        .columnMapping("count(ps_message.id_message)", "purchased_quantity")
                        .create();

        com.avaje.ebean.Query<MealDealProduct> query = Ebean.find(MealDealProduct.class);
        query.setRawSql(rawSql);
        List<MealDealProduct> list = query.findList();
        ArrayList<MealDealProduct> totalCashierVouchers = new ArrayList<>();
        totalCashierVouchers.addAll(list);
        return totalCashierVouchers;
    }

    //打印manager总单
    public static Result managerPrint() throws Exception {
        String email = session("email");
        // if in session
        if (email != null && email.equals("a2b.manager@a2bliving.ie")) {
            List<DriverOrders> unpaid_orders = DriverOrders.driverUnpaidOrders();
            if(unpaid_orders.size()>0){
                return ok(driverUnpaidOrdersPage.render(unpaid_orders)); //返回没有点击付钱的订单
            }else {
                ArrayList<TotalReturnManagerOrder> totalReturnManagerOrders;
                totalReturnManagerOrders = getOrdersForManager();
                /**create two empty object for later use. One will be assigned the value as "Cash payment summary"
                 *the other will be assigned the value of "Online payment summary"
                 * */
                TotalReturnManagerOrder a;
                TotalReturnManagerOrder b;
                TotalReturnManagerOrder c;
                TotalReturnManagerOrder exception_a = new TotalReturnManagerOrder();  //cash
                TotalReturnManagerOrder exception_b = new TotalReturnManagerOrder();   //online
                TotalReturnManagerOrder exception_c = new TotalReturnManagerOrder();   //online
                TotalReturnManagerOrder total = new TotalReturnManagerOrder();
                if (totalReturnManagerOrders != null) {
                    if (totalReturnManagerOrders.size() == 1) {
                        a = totalReturnManagerOrders.get(0);
                        if (a.payment_method.contains("Cash")) {
                            total.total_cashpay_number = a.total_onlinepay_number;
                            total.total_cashpay_sales = a.total_onlinepay_sales;
                            total.total_cashpay_delivery = a.total_onlinepay_delivery;
                        } else {
                            total.total_onlinepay_number = a.total_onlinepay_number;
                            total.total_onlinepay_sales = a.total_onlinepay_sales;
                            total.total_onlinepay_delivery = a.total_onlinepay_delivery;
                        }
                    } else if (totalReturnManagerOrders.size() == 2) {
                        if (!totalReturnManagerOrders.get(0).payment_method.contains("Cash")) {
                            a = totalReturnManagerOrders.get(0); //online
                            b = totalReturnManagerOrders.get(1); //cash

                        } else {
                            a = totalReturnManagerOrders.get(1); //online
                            b = totalReturnManagerOrders.get(0); //cash
                        }
                        total.total_onlinepay_number = a.total_onlinepay_number;
                        total.total_onlinepay_sales = a.total_onlinepay_sales;
                        total.total_onlinepay_delivery = a.total_onlinepay_delivery;
                        total.total_cashpay_number = b.total_onlinepay_number;
                        total.total_cashpay_sales = b.total_onlinepay_sales;
                        total.total_cashpay_delivery = b.total_onlinepay_delivery;
                    } else if (totalReturnManagerOrders.size() == 3) {
                        a = totalReturnManagerOrders.get(0);
                        b = totalReturnManagerOrders.get(1);
                        c = totalReturnManagerOrders.get(2);

                        total.total_cashpay_number = a.total_onlinepay_number;
                        total.total_cashpay_sales = a.total_onlinepay_sales;
                        total.total_cashpay_delivery = a.total_onlinepay_delivery;

                        total.total_onlinepay_number = b.total_onlinepay_number + c.total_onlinepay_number;
                        total.total_onlinepay_sales = b.total_onlinepay_sales.add(c.total_onlinepay_sales);
                        total.total_onlinepay_delivery = b.total_onlinepay_delivery.add(c.total_onlinepay_delivery);
                    }

                    ArrayList<TotalReturnManagerOrder> totalExceptionOrders;
                    totalExceptionOrders = getExceptionManagerOrders();
                    if (totalExceptionOrders.size() > 0) {
                        if (totalExceptionOrders.size() == 1) {
                            if (totalExceptionOrders.get(0).payment_method.contains("Cash")) {
                                exception_a = totalExceptionOrders.get(0);//cash
                            } else {
                                exception_b = totalExceptionOrders.get(0);//online
                            }
                        } else if (totalExceptionOrders.size() == 2) {
                            if (totalExceptionOrders.get(0).payment_method.contains("Cash")) {
                                exception_a = totalExceptionOrders.get(0);
                                exception_b = totalExceptionOrders.get(1);
                            } else {
                                exception_a = totalExceptionOrders.get(1);
                                exception_b = totalExceptionOrders.get(0);
                            }
                        } else if (totalExceptionOrders.size() == 3) {
                            exception_a = totalExceptionOrders.get(0);
                            exception_b = totalExceptionOrders.get(1);
                            exception_c = totalExceptionOrders.get(2);
                        }
                        total.total_cashpay_sales = total.total_cashpay_sales.add(exception_a.total_onlinepay_delivery);
                        total.total_onlinepay_sales = total.total_onlinepay_sales.add(exception_b.total_onlinepay_delivery).add(exception_c.total_onlinepay_delivery);
                        total.total_cashpay_delivery = total.total_cashpay_delivery.subtract(exception_a.total_onlinepay_delivery);
                        total.total_onlinepay_delivery = total.total_onlinepay_delivery.subtract(exception_b.total_onlinepay_delivery).subtract(exception_c.total_onlinepay_delivery);
                    }
                    int cashpay_orders = total.total_cashpay_number;
                    int onlinepay_orders = total.total_onlinepay_number;
                    int total_orders = cashpay_orders + onlinepay_orders;
                    double total_sales = total.total_cashpay_sales.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + total.total_onlinepay_sales.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();  // round the number to two digits
                    DecimalFormat df = new DecimalFormat("#.##");
                    total_sales = Double.valueOf(df.format(total_sales));
                    ArrayList<BrandSummary> totalBrands = getBrandSummary();
//                    ManagerOrdersMarinaNumber managerOrdersMarinaNumber = managerTotalOrders();
//                ArrayList<ReturnManagerOrder> totalOrders = managerOrdersMarinaNumber.returnManagerOrders;
//                int marinaOrders = managerOrdersMarinaNumber.marinaOrders;
//                    int marinaChargedOrders = managerOrdersMarinaNumber.marinaChargedOrders;
//                    List<CashierCash> all_cashiers = CashierCash.findCashiers();
                    List<DriverDailySummary> all_driver_salary = DriverDailySummary.salary_of_day();
//                    BigDecimal total_driver_salary = BigDecimal.valueOf(0);
                    BigDecimal total_voucher_value = BigDecimal.valueOf(0);
                    for (DriverDailySummary this_one : all_driver_salary) {
//                        total_driver_salary = total_driver_salary.add(this_one.total_wage).add(this_one.extra_cost);
                        total_voucher_value = total_voucher_value.add(this_one.total_voucher_value);
                    }
//                    //总现金结算： 现金付款收入(不含运费) - 司机在线付款运费 - 司机工资（包含extra cost） - Marina 特殊订单 - 总voucher代金券
//                    BigDecimal final_cash_left = total.total_cashpay_sales.subtract(total.total_onlinepay_delivery).subtract(total_driver_salary).subtract(BigDecimal.valueOf(marinaChargedOrders)).subtract(total_voucher_value);
                    //新现金结算规则： 现金订单总收入 - 司机在线付款运费 - 总Voucher 代金券
                    BigDecimal final_cash_left = total.total_cashpay_sales.subtract(total.total_onlinepay_delivery).subtract(total_voucher_value);
//                  TaxSummary data_container = new TaxSummary(1, "", total_driver_salary, final_cash_left);
                    TotalReturnManagerOrder data_container = new TotalReturnManagerOrder(0,0,BigDecimal.valueOf(0),total_voucher_value, final_cash_left, BigDecimal.valueOf(0), "");
                    String today = new Date().toString();
                    //得到今天司机的额外运费收入
                    DriverPayrollSummary extra_delivery_income = getDailyDeliveryIncome();
                    return ok(dailyTotalSummary.render(total, total_orders, total_sales,totalBrands, today, data_container,extra_delivery_income));
                } else {
                    return notFound("No such driver");
                }
            }
        }else{
            flash("error","please log in first");
            return redirect(WebAPI.login());
        }
    }

    public static Result logout(){
        session().clear();
    return redirect(WebAPI.login());
    }




    public static Result postLogin() throws Exception {
        Form<ManagerLogin> loginForm = Form.form(ManagerLogin.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            ///Play framework Form features, hasError() method will make the form valid
            flash("error","wrong password");
            return badRequest(login.render(Form.form(ManagerLogin.class)));
        } else {
            flash("success","successful log in");
            session().clear();
            session("email", loginForm.get().email);
            return redirect(
                    WebAPI.dashboard()
            );

        }
    }

    public static Result counterPostLogin() throws Exception {
        Form<CounterLogin> loginForm = Form.form(CounterLogin.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            ///Play framework Form features, hasError() method will make the form valid
            flash("error","wrong password");
            return badRequest(counterslogin.render(Form.form(CounterLogin.class)));
        } else {
            flash("success","successful log in");
            session().clear();
            session("counterEmail", loginForm.get().email);
            return redirect(
                    WebAPI.counterDashboard() //counters main page
            );

        }
    }

    public static Result driverPostLogin() throws Exception {
        Form<DriverLogin> loginForm = Form.form(DriverLogin.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            ///Play framework Form features, hasError() method will make the form valid
            flash("error","wrong password");
            return badRequest(driverslogin.render(Form.form(DriverLogin.class)));
        } else {
            flash("success","successful log in");
            session().clear();
            session("driverEmail", loginForm.get().email);
            return redirect(
                    WebAPI.driverDashboard() //drivers main page, 显示所有working drivers
            );

        }
    }

    public static Result shopAssistantPostLogin() throws Exception {
        Form<ShopAssistantLogin> loginForm = Form.form(ShopAssistantLogin.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            ///Play framework Form features, hasError() method will make the form valid
            flash("error","wrong password");
            return badRequest(shopAssistantlogin.render(Form.form(ShopAssistantLogin.class)));
        } else {
            flash("success","successful log in");
            session().clear();
            session("shopAssistantEmail", loginForm.get().email);
            return redirect(
                    StockAPI.productsStockPage(0) //Shop Stock Management main page
            );

        }
    }
    /**
     * Manager check total sales of the day
     * @return receipt View page
     * */
    public static ManagerOrdersMarinaNumber managerTotalOrders() throws Exception{
        int marinaOrders = 0;
        int marinaChargedOrders = 0;
        try{
            String sql =
                "SELECT ps_orders.id_order, ps_orders.reference, ps_orders.id_customer, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_orders.invoice_date, ps_order_state_lang.name, ps_drivers_orders.driver_name, ps_drivers_orders.order_number\n" +
                        "FROM ps_orders\n" +
                        "LEFT JOIN ps_drivers_orders\n" +
                        "ON ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "LEFT JOIN ps_order_state_lang\n" +
                        "ON ps_orders.current_state = ps_order_state_lang.id_order_state\n" +
                        "\n" +
                        "WHERE  ps_orders.valid = 1\n" +
                        "AND ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 13 HOUR)\n" +   // 更改时间
//                        "AND ps_orders.invoice_date > '2017-02-06'\n" +
//                        "AND ps_orders.invoice_date <'2017-03-01'\n" +
                        "order by ps_orders.invoice_date";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "id_order")
                            .columnMapping("ps_orders.reference", "order_reference")
                            .columnMapping("ps_orders.id_customer", "id_customer")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.total_paid_real", "total_products_wt")  // calculate only the products prices
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_orders.invoice_date", "date_add")
                            .columnMapping("ps_order_state_lang.name", "order_status")
                            .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                            .columnMapping("ps_drivers_orders.order_number", "order_number")
                            .create();

            com.avaje.ebean.Query<ReturnManagerOrder> query = Ebean.find(ReturnManagerOrder.class);
            query.setRawSql(rawSql);
            List<ReturnManagerOrder> list = query.findList();
            for(ReturnManagerOrder this_one :list){
                if(this_one.driver_name ==null){
                    this_one.driver_name = "Unknown";
                    this_one.order_number = 0;
                }
                BigDecimal discount_amount = this_one.total_discounts;
                BigDecimal shipping_amount = this_one.total_shipping;
                if (shipping_amount.doubleValue() == 0.0) {
                    marinaOrders += 1;
                    if (!this_one.driver_name.contains("free")) {
                        marinaChargedOrders += 1;
                    }
                } else if (discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0) {  //特殊情况1
                    this_one.total_shipping = BigDecimal.valueOf(0.0);
                    marinaOrders += 1;
                    if (!this_one.driver_name.contains("free")) {
                        marinaChargedOrders += 1;
                    }
                } else if (shipping_amount.doubleValue() == 2.0 && discount_amount.doubleValue() == 7.0) {  //特殊情况2
                    this_one.total_shipping = BigDecimal.valueOf(0.0);
                    marinaOrders += 1;
                    if (!this_one.driver_name.contains("free")) {
                        marinaChargedOrders += 1;
                    }
                }
                this_one.total_products_wt = this_one.total_products_wt.subtract(this_one.total_shipping);  //实际产品的价钱 = 实际付款 - 运费
            }
            ArrayList<ReturnManagerOrder> totalOrders = new ArrayList<>();
            totalOrders.addAll(list);
            ManagerOrdersMarinaNumber totalManagerOrders = new ManagerOrdersMarinaNumber(totalOrders,marinaOrders, marinaChargedOrders);
            return totalManagerOrders;
        }catch (Exception e){    //null pointer exception
            String sql =
                    "SELECT ps_orders.id_order, ps_orders.reference, ps_orders.id_customer, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_orders.invoice_date, ps_order_state_lang.name\n" +
                            "FROM ps_orders\n" +
                            "LEFT JOIN ps_drivers_orders\n" +
                            "ON ps_orders.id_order = ps_drivers_orders.id_order\n" +
                            "\n" +
                            "LEFT JOIN ps_order_state_lang\n" +
                            "ON ps_orders.current_state = ps_order_state_lang.id_order_state\n" +
                            "\n" +
                            "WHERE  ps_orders.valid = 1\n" +
                            "AND ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 13 HOUR)\n" +   //更改时间
                            "order by ps_orders.invoice_date";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "id_order")
                            .columnMapping("ps_orders.reference", "order_reference")
                            .columnMapping("ps_orders.id_customer", "id_customer")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.total_paid_real", "total_products_wt")  // calculate only the products prices
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_orders.invoice_date", "date_add")
                            .columnMapping("ps_order_state_lang.name", "order_status")
                            .create();

            com.avaje.ebean.Query<ReturnManagerOrder> query = Ebean.find(ReturnManagerOrder.class);
            query.setRawSql(rawSql);
            List<ReturnManagerOrder> list = query.findList();
            int size = list.size();
            for(int i =0; i<size;i++){
                BigDecimal discount_amount = list.get(i).total_discounts;
                BigDecimal shipping_amount = list.get(i).total_shipping;
                if (shipping_amount.doubleValue() == 0.0){
                    marinaOrders +=1;
                    if(!list.get(i).driver_name.contains("free")){
                        marinaChargedOrders +=1;
                    }
                }else if(discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0 ){
                    list.get(i).total_products_wt = list.get(i).total_products_wt;
                    list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                    marinaOrders +=1;
                    if(!list.get(i).driver_name.contains("free")){
                        marinaChargedOrders +=1;
                    }
                }else if(shipping_amount.doubleValue()==2.0 && discount_amount.doubleValue() == 7.0 ){  //特殊情况2
                    list.get(i).total_products_wt = list.get(i).total_products_wt;
                    list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                    marinaOrders +=1;
                    if(!list.get(i).driver_name.contains("free")){
                        marinaChargedOrders +=1;
                    }
                }else{
                    list.get(i).total_products_wt = list.get(i).total_products_wt.subtract(list.get(i).total_shipping);
                }
            }
            ArrayList<ReturnManagerOrder> totalOrders = new ArrayList<>();
            totalOrders.addAll(list);
            ManagerOrdersMarinaNumber totalManagerOrders = new ManagerOrdersMarinaNumber(totalOrders,marinaOrders, marinaChargedOrders);
            return totalManagerOrders;
        }
    }


    public static DriverPayrollSummary getDailyDeliveryIncome() {
        String sql =
                "SELECT sum(cash_delivery_fee), sum(online_delivery_fee)\n" +
                        "FROM ps_driver_daily_summary_flow\n" +
                        "WHERE if_company_car = 2\n" +
                        "AND date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(cash_delivery_fee)", "cash_delivery_fee")
                        .columnMapping("sum(online_delivery_fee)", "online_delivery_fee")
                        .create();

        com.avaje.ebean.Query<DriverPayrollSummary> query = Ebean.find(DriverPayrollSummary.class);
        query.setRawSql(rawSql);
        DriverPayrollSummary income_delivery = query.findUnique();
        return income_delivery;
    }
    /**
     * Get a total summary of overall orders of a day, including cashpay and online pay orders
     * @see TotalReturnManagerOrder
     * */
    public static ArrayList<TotalReturnManagerOrder> getOrdersForManager() {
        String sql =
                "SELECT count(*), sum(ps_orders.total_paid_real) as total_product_sales, sum(ps_orders.total_shipping) as total_shipping, ps_orders.payment\n" +
                        "FROM ps_orders\n" +
                        "where  ps_orders.valid = 1\n" +
                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 13 HOUR)\n" +  //更改时间
//                        "AND ps_orders.invoice_date > '2017-02-06'\n" +
//                        "AND ps_orders.invoice_date <'2017-03-01'\n" +
                        "group by ps_orders.payment";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(*)", "total_onlinepay_number")
                        .columnMapping("sum(ps_orders.total_paid_real)", "total_onlinepay_sales")
                        .columnMapping("sum(ps_orders.total_shipping)", "total_onlinepay_delivery")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .create();

        com.avaje.ebean.Query<TotalReturnManagerOrder> query = Ebean.find(TotalReturnManagerOrder.class);
        query.setRawSql(rawSql);
        List<TotalReturnManagerOrder> list = query.findList();
        int size = list.size();
        for (int i =0; i<size;i++){
            list.get(i).total_onlinepay_sales = list.get(i).total_onlinepay_sales.subtract(list.get(i).total_onlinepay_delivery);  //total_paid_real - total_shipping
        }
        ArrayList<TotalReturnManagerOrder> totalReturnDriverOrders = new ArrayList<>();
        totalReturnDriverOrders.addAll(list);
        return totalReturnDriverOrders;
    }

    public static ArrayList<TotalReturnManagerOrder> getExceptionManagerOrders() {
        String sql =
                "SELECT count(*), sum(ps_orders.total_shipping) as total_shipping, ps_orders.payment\n" +
                        "FROM ps_orders\n" +
                        "where  ps_orders.valid = 1\n" +
                        "and ((ps_orders.total_discounts = 2.000000 and ps_orders.total_shipping = 2.000000)\n" +
                        "or (ps_orders.total_discounts = 7.000000 and ps_orders.total_shipping = 2.000000))\n" +
                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 13 HOUR)\n" +  //更改时间
                        "group by ps_orders.payment";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(*)", "total_onlinepay_number")
                        .columnMapping("sum(ps_orders.total_shipping)", "total_onlinepay_delivery")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .create();

        com.avaje.ebean.Query<TotalReturnManagerOrder> query = Ebean.find(TotalReturnManagerOrder.class);
        query.setRawSql(rawSql);
        List<TotalReturnManagerOrder> list = query.findList();
        ArrayList<TotalReturnManagerOrder> totalReturnDriverOrders = new ArrayList<>();
        totalReturnDriverOrders.addAll(list);
        return totalReturnDriverOrders;
    }

     /**
     * Get a list of items, showing the sales conditions of a day
     * @see BrandSummary
     * */
    public static ArrayList<BrandSummary> getBrandSummary() {
        BrandSummary shop = new BrandSummary();
        BrandSummary pizza = new BrandSummary();
        BrandSummary asian = new BrandSummary();
        BrandSummary chinese = new BrandSummary();
        BrandSummary starter = new BrandSummary();
        BrandSummary tobacco = new BrandSummary();// TODO: 2017/2/17
        BrandSummary lunch =   new BrandSummary();

        String sql =
                "SELECT distinct ps_order_detail.product_reference, sum(ps_order_detail.product_quantity), sum(ps_order_detail.total_price_tax_incl)\n" +
                        "\n" +
                        "from  ps_orders, ps_order_detail\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_order_detail.id_order\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +
                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 13 HOUR)\n" +  //更改时间
//                        "AND ps_orders.invoice_date > '2017-02-06'\n" +
//                        "AND ps_orders.invoice_date <'2017-03-01'\n" +
                        "\n"+
                        "group by ps_order_detail.product_reference";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_order_detail.product_reference", "brand_name")
                        .columnMapping("sum(ps_order_detail.product_quantity)", "number_of_piece")
                        .columnMapping("sum(ps_order_detail.total_price_tax_incl)", "total_sales")
                        .create();
        com.avaje.ebean.Query<BrandSummary> query = Ebean.find(BrandSummary.class);
        query.setRawSql(rawSql);
        List<BrandSummary> list = query.findList();
        int number = list.size();
        for (int i = 0; i < number;i++){
            if(list.get(i).brand_name.equals("")){
                shop = list.get(i);
            }else if(list.get(i).brand_name.equals("pizza")){
                pizza = list.get(i);
            }else if(list.get(i).brand_name.equals("asian")){
                asian = list.get(i);
            }else if(list.get(i).brand_name.equals("chinese")){
                chinese = list.get(i);
            }else if(list.get(i).brand_name.equals("starter")){
                starter = list.get(i);
            }else if(list.get(i).brand_name.equals("tobacco")){
                tobacco = list.get(i);
            }else if(list.get(i).brand_name.equals("lunch")){
                lunch = list.get(i);
            }
        }
        ArrayList<BrandSummary> totalSummary = new ArrayList<>();
        totalSummary.add(shop);
        totalSummary.add(pizza);
        totalSummary.add(asian);
        totalSummary.add(chinese);
        totalSummary.add(starter);
        totalSummary.add(tobacco);
        totalSummary.add(lunch);
        return totalSummary;
    }

    /**
     * Ebean related method to return Objects from database to Http return
     * @deprecated
     * @see Product
     * */
    public static Result products()
    {
        List<Product> products = Product.findAll();
        return ok(renderProduct(products));
    }

    /**
     * Ebean related method to return one object of Product class from database to Http return
     * @deprecated
     * @param id: product id
     * @see Product
     * */
    public static Result product(Long id)
    {
        Product product = Product.findByProductId(id);
        return product==null? notFound() : ok(renderProduct(product));
    }
    

    public static Result customers(){
        String email = session("email");
        // if in session
        if (email != null) {
        List<Customer> customers = Customer.findAll();
        return ok(renderCustomer(customers));
        }else{
            return ok("not logged in");
        }
    }

    public static Result customer(Long id)
    {
        Customer customer = Customer.findById(id);
        return customer==null? notFound() : ok(renderCustomer(customer));
    }
    
    
    public static Result createCustomer()
    {
      Customer customer = renderCustomer(request().body().asJson().toString());
      customer.save();
        return ok(renderCustomer(customer));
    }

    public static Result deleteCustomer(Long id)
    {
        Result result = notFound();
        Customer customer = Customer.findById(id);
        if (customer != null)
        {
          customer.delete();
            result = ok();
        }
        return result;
    }

    /**
     * Sample method to execute actions to Database records through Ebean class connection
     * */
    public static Result deleteAllCustomers()
    {
      Customer.deleteAll();
        return ok();
    }

    public static Result updateCustomer(Long id)
    {
        Result result = notFound();
        Customer customer = Customer.findById(id);
        if (customer != null)
        {
          Customer updatedCustomer = renderCustomer(request().body().asJson().toString());
          customer.update(updatedCustomer);
          customer.save();
          result = ok(renderCustomer(customer));
        }
        return result;
    }

    public static Result welcome_aboard(){
        return ok(mainPanel.render());
    }

//    //Driver Payroll Page 主页 选择一个司机来查看情况并且发工资， GET
//    public static Result driverPayrollDashboard() throws Exception {
//        String email = session("driverEmail");
//        // if in session
//        if (email != null && email.equals("chenzhi1111@gmail.com")){
////            Form<CashierID> driverIdForm = Form.form(CashierID.class);
//            //显示所有司机的名单
//            //updated:  显示一周之内工作的司机名单， 11天之内登陆App的用户
//            List<Driver> all_drivers = Driver.payRollDrivers();
//            return ok(chooseDriverPage.render(all_drivers));
//        }else{
//            flash("error","please log in first");  //如果没有登陆就返回到登陆页面
//            return redirect(WebAPI.driverLogin());
//        }
//    }

    public static class PayRollDate{
        public String startDate;  //选择工资计算开始时间
        public String endDate;  //选择工资计算结束时间
    }

    //打开日期页面，筛选相应时间之内的所有工作司机的结算情况，并且发工资
    public static Result showDriverTimePicker() throws Exception {
        String email = session("driverEmail");
        // if in session
        if (email != null && email.equals("chenzhi1111@gmail.com")){
         Form<PayRollDate> newForm = Form.form(PayRollDate.class);
            return ok(chooseDatePage.render(newForm)); //ignore
        }else{
            flash("error","please log in first");  //如果没有登陆就返回到登陆页面
            return redirect(WebAPI.driverLogin());
        }
    }

    //POST 显示司机详细工资情况
    public static Result calculateTotalSalary() throws Exception {
        DynamicForm dateForm = Form.form().bindFromRequest();  //receive
        String startDate = dateForm.get("startDate");
        String endDate = dateForm.get("endDate");
        if (startDate ==null || endDate ==null || startDate.equals("") || endDate.equals("") ){
            flash("error", "please enter Start and End Date");
            return redirect(routes.WebAPI.showDriverTimePicker());
        }
        else {
                String[] endString = endDate.split("-");
                String day = endString[2];
                int realDay = Integer.valueOf(day) + 1;
                String real_endDate = endString[0] + "-" + endString[1] + "-" + String.valueOf(realDay);
                System.out.println(startDate);
                System.out.println(endDate);
                List<DriverPayrollTotalSummary> salary_records = getDriverPayroll(startDate,real_endDate); //得到这段时间内的所有司机记录
                if(salary_records.size()==0){
                    return notFound(errorMessageTemplate.render("No Record Found"));
                }
                HashMap<String, String> submittedDate = new HashMap<String, String>();
                submittedDate.put("startDate",startDate);
                submittedDate.put("endDate",endDate);
                Form<PayRollDate> newForm = Form.form(PayRollDate.class).bind(submittedDate);
             return ok(driverSalarySummary.render(newForm,salary_records));
        }
    }

    public static List<DriverPayrollTotalSummary> getDriverPayroll(String startDate, String endDate) {
        String sql_1 =
                "SELECT id_driver, name, count(id_driver_daily_summary_flow), sum(working_hours), sum(total_wage), sum(extra_cost), sum(marina_delivery), sum(cash_order), sum(online_order), sum(cash_delivery_fee), sum(online_delivery_fee)\n" +
                    "from ps_driver_daily_summary_flow\n" +
                    "where date_add >" + "'" + startDate + "'" + "\n" +
                    "and date_add <" + "'" + endDate + "'" + "\n" +
                    "group by id_driver\n" +
                    "order by id_driver";
        RawSql rawSql_1 =
                RawSqlBuilder
                        .parse(sql_1)
                        .columnMapping("id_driver", "id_driver")
                        .columnMapping("name", "name")
                        .columnMapping("count(id_driver_daily_summary_flow)", "total_working_days")
                        .columnMapping("sum(working_hours)", "total_working_hours")
                        .columnMapping("sum(total_wage)", "total_wage")
                        .columnMapping("sum(extra_cost)", "total_extra_cost")
                        .columnMapping("sum(marina_delivery)", "total_marina_delivery")
                        .columnMapping("sum(cash_order)", "total_cash_order")
                        .columnMapping("sum(online_order)", "total_online_order")
                        .columnMapping("sum(cash_delivery_fee)", "total_cash_delivery_fee")
                        .columnMapping("sum(online_delivery_fee)", "total_online_delivery_fee")
                        .create();
        com.avaje.ebean.Query<DriverPayrollTotalSummary> query_1 = Ebean.find(DriverPayrollTotalSummary.class);
        query_1.setRawSql(rawSql_1);
        List<DriverPayrollTotalSummary> list_1 = query_1.findList();  //第一个是总数 列表，代表每个司机的总结情况
        if(list_1.size() == 0){
            return new ArrayList<DriverPayrollTotalSummary>();
        }
        String sql_2 =
                "SELECT id_driver, name, working_hours, hourly_wage, total_wage, extra_cost, specification, marina_delivery, cash_order, online_order, date_add, if_company_car, cash_delivery_fee, online_delivery_fee \n" +
                        "from ps_driver_daily_summary_flow\n" +
                        "where date_add >" + "'" + startDate + "'" + "\n" +
                        "and date_add <" + "'" + endDate + "'" + "\n" +
                        "order by id_driver";
        RawSql rawSql_2 =
                RawSqlBuilder
                        .parse(sql_2)
                        .columnMapping("id_driver", "id_driver")
                        .columnMapping("name", "name")
                        .columnMapping("working_hours", "working_hours")
                        .columnMapping("hourly_wage", "hourly_wage")
                        .columnMapping("total_wage", "total_wage")
                        .columnMapping("extra_cost", "extra_cost")
                        .columnMapping("specification", "specification")
                        .columnMapping("marina_delivery", "marina_delivery")
                        .columnMapping("cash_order", "cash_order")
                        .columnMapping("online_order", "online_order")
                        .columnMapping("date_add", "date_add")
                        .columnMapping("if_company_car", "if_company_car")
                        .columnMapping("cash_delivery_fee", "cash_delivery_fee")
                        .columnMapping("online_delivery_fee", "online_delivery_fee")
                        .create();
        com.avaje.ebean.Query<DriverPayrollSummary> query_2 = Ebean.find(DriverPayrollSummary.class);
        query_2.setRawSql(rawSql_2);
        List<DriverPayrollSummary> list_2 = query_2.findList();  //第二个是所有司机每天送单的情况列表，需要遍历之后加入上面列表个每个记录里面
        for (DriverPayrollTotalSummary each_driver: list_1){
            if(list_2.size()>0){
                //针对每一个司机的总情况
                BigDecimal total_marina_delivery = BigDecimal.valueOf(0);
                for(DriverPayrollSummary each_day_detail: list_2){
                    if(each_day_detail.id_driver == each_driver.id_driver){
                        if(each_day_detail.if_company_car != 2){
                            total_marina_delivery = total_marina_delivery.add(each_day_detail.marina_delivery);
                        }
                        each_driver.detail_list.add(each_day_detail);
                    }
                }
                each_driver.total_marina_delivery = total_marina_delivery;
            }
        }
        return list_1;
    }
}