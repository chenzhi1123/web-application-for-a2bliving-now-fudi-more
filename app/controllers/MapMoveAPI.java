package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.fasterxml.jackson.databind.JsonNode;
import models.database.Driver;
import models.template.Move;
import models.sqlContainer.ReturnDriverOrders;
import models.socketControl.DriverAction;
import models.socketControl.MapAnime;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static models.socketControl.MapAnime.saveToDatabase;
import static parsers.JsonParser.renderDriver;
import static parsers.JsonParser.renderDriverMoves;
//import flexjson.JSONDeserializer;

public class MapMoveAPI extends Controller {

	public static Result showMap(String username) {
		System.out.println("this is--showMap-------" + Thread.currentThread().getId());
		return ok(views.html.map.render(username));
	}

	//转换方法，更改为 socket
	public static Result moveTo() {   //传输Move这个对象
		System.out.println("-------------start to move--------------------");
		play.data.Form<Move> moveForm = play.data.Form.form(Move.class);
//		String a = request().body().asJson().toString();
		Move move = moveForm.bindFromRequest().get();
//		if (move.id == 0) {
//			return badRequest("Missing the 'id' parameter");
//		}
		MapAnime.moveTo(move.driver_id, move.timestamp, move.longitude, move.latitude);
		System.out.println(move.timestamp);
		System.out.println("this is-------thread:  " + Thread.currentThread().getId() + "----------Driver-" + move.driver_id);
		System.out.println("+++++++++++++Move Finished++++++++++++++++");
		return ok(String.valueOf(move.latitude));
	}

	public static WebSocket<JsonNode> changeLocationSocket() {  //与每个司机的单独socket通讯
		//原方法：begin
//        return new WebSocket<JsonNode>() {
//            // Called when the Websocket Handshake is done.
//            public void onReady(In<JsonNode> in, Out<JsonNode> out) {
//                // For each event received on the socket,
//                in.onMessage(new Callback<JsonNode>() {
//                    public void invoke(JsonNode event) {
//                        // Log events to the console
//                        System.out.println(event);
//                        out.write(event);   ///(返回值)
//                        //在这里向地图发送更改坐标信息
//                        ObjectMapper mapper = new ObjectMapper();
//                        Move move = mapper.convertValue(event, Move.class);
////                        if (move.id != null) {
//                            MapAnime.moveTo(move.driver_id, move.timestamp, move.longitude, move.latitude); //分配给另外一个线程
////                        }
////
////                        MapAnime.moveTo(move.id, move.timestamp, move.longitude, move.latitude);
//                    }
//                });
//                // When the socket is closed.
//                in.onClose(new Callback0() {
//                    public void invoke() {
//
//                        System.out.println("Disconnected");
//                    }
//                });
//                // Send a single 'Hello!' message
//                ObjectNode event = Json.newObject();
//                event.put("status", "success");
//                out.write(event);  //成功连接端口的时候执行
//                System.out.println("Connected to Socket Successfully");  //打开端口的时候显示信息
//            }
//        };
		//原方法： end
		return new WebSocket<JsonNode>() {
			// Called when the Websocket Handshake is done.
			@Override
			public void onReady(final In<JsonNode> in,final Out<JsonNode> out) {
				try {
					DriverAction.driverRegister(java.util.UUID.randomUUID().toString(),
							in, out);  //地图连接socket 需要注册
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};

	}
	/**
	 * Handle the map websocket.
	 */
	public static WebSocket<JsonNode> mapsocket() {
		return new WebSocket<JsonNode>() {
			// Called when the Websocket Handshake is done.
			@Override
			public void onReady(final In<JsonNode> in, final Out<JsonNode> out) {
				try {
					MapAnime.register(java.util.UUID.randomUUID().toString(),
							in, out);  //地图连接socket 需要注册
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};
	}


	public static Result showDrivers(){
		List<Driver> all_drivers = Driver.findAll();
		return ok(renderDriver(all_drivers));
	}

	public static Result saveOfflineLocations(){
		String body = request().body().asJson().toString();
		try{
			List<Move> offline_records = renderDriverMoves(body);
			for(Move this_one: offline_records){
				saveToDatabase(this_one);
	     		System.out.println("take a break");
			}
		}catch (Exception e){
			return badRequest("Invalid data format");
		}
		return ok("Offline records have been saved");
	}



	public static ReturnDriverOrders getDriverOrders(int driver_id){  //获得当前这个司机的送单情况
		String sql =
				"select ps_drivers.name, ps_drivers.contact_number, ps_drivers_orders.order_number, ps_drivers_orders.start_time, ps_drivers_orders.delivered_time\n" +
						"from ps_drivers\n" +
						"LEFT JOIN ps_drivers_orders\n" +
						"ON ps_drivers_orders.id_driver = ps_drivers.id_driver\n" +
						"AND ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 12 HOUR)\n" +
						"and ps_drivers_orders.if_delivered = 0\n" +
						"where ps_drivers.id_driver != 8 \n" +
						"and ps_drivers.id_driver = " + driver_id + "\n" +
						"and ps_drivers.if_work = 1";

		RawSql rawSql =
				RawSqlBuilder
						.parse(sql)
						.columnMapping("ps_drivers.name", "driver_name")
						.columnMapping("ps_drivers.contact_number", "contact_number")
						.columnMapping("ps_drivers_orders.order_number", "order_number")
						.columnMapping("ps_drivers_orders.start_time", "start_time")
						.columnMapping("ps_drivers_orders.delivered_time", "delivered_time")
						.create();
		com.avaje.ebean.Query<ReturnDriverOrders> query = Ebean.find(ReturnDriverOrders.class);
		query.setRawSql(rawSql);
		List<ReturnDriverOrders> list = query.findList();
		ReturnDriverOrders this_driver = new ReturnDriverOrders();
		if(list.size()>0){ //存在这个司机的记录
			this_driver = list.get(0); //用第一条记录作为样本信息
			int original_year = 1970;
			ArrayList<Integer> doing_orders = new ArrayList<>();  //这个司机正在送的单
			ArrayList<Integer> todo_orders = new ArrayList<>();   //这个司机还没送的单
			//处理这个司机的每个订单的情况
			for(ReturnDriverOrders this_one: list) {
				if (this_one.start_time != null && this_one.delivered_time != null) {
					Calendar start_calendar = new GregorianCalendar();
					Calendar delivery_calendar = new GregorianCalendar();
					start_calendar.setTime(this_one.start_time);
					delivery_calendar.setTime(this_one.delivered_time);
					int start_year = start_calendar.get(Calendar.YEAR);
					int delivery_year = delivery_calendar.get(Calendar.YEAR);
					if (start_year > original_year && delivery_year == original_year) {
						doing_orders.add(this_one.order_number);
					} else if (start_year == original_year && delivery_year == original_year) {
						todo_orders.add(this_one.order_number);
					}
				}
			}
			//循环结束
			if(todo_orders.size() ==0 && doing_orders.size() ==0){
				this_driver.working_status = "Available";
			}else {
				this_driver.working_status = "In delivery";
			}
			this_driver.todo_orders = todo_orders;
			this_driver.doing_orders = doing_orders;
		}
		return this_driver;
	}


}