/*
 * Copyright (c) 2017. Zhi Chen.
 */

package controllers;
/**
 * 有时间跨度的总结算
 * */

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.database.DriverDailySummary;
import models.database.TotalReturnManagerOrder;
import models.sqlContainer.BrandSummary;
import models.sqlContainer.DriverPayrollSummary;
import models.sqlContainer.MealDealProduct;
import models.sqlContainer.ReturnManagerOrder;
import models.template.ManagerOrdersMarinaNumber;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.totalSummary.totalManagerSummary;
import views.html.totalSummary.totalSummaryPage;
import views.html.totalSummary.totalSummaryPrint;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static controllers.routes.WebAPI;

public class ManagerAPI extends Controller{

    public static Result totalSummaryPage()
    {   //登陆session 验证
        Form<StatisticsAPI.PickDate> dateForm = Form.form(StatisticsAPI.PickDate.class);
        return ok(totalSummaryPage.render(dateForm)); //ignore
    }

    //显示manager界面(含时间跨度)
    public static Result dashboardAPI() throws Exception {
        String email = session("email");
        // if in session
        if (email != null && email.equals("a2b.manager@a2bliving.ie")) {
            DynamicForm dateForm = Form.form().bindFromRequest();  //receive
            String startDate = dateForm.get("startDate");
            String endDate = dateForm.get("endDate");
            if (startDate ==null || endDate ==null || startDate.equals("") || endDate.equals("") ){
                flash("error", "please enter Start and End Date");
                return redirect(routes.ManagerAPI.totalSummaryPage());
            }
            else {
                String[]  endString = endDate.split("-");
                String day = endString[2];
                int realDay = Integer.valueOf(day) + 1;
                String real_endDate = endString[0]+"-"+endString[1]+"-"+ String.valueOf(realDay);
                System.out.println(startDate);
                System.out.println(endDate);

                ArrayList<TotalReturnManagerOrder> totalReturnManagerOrders;
                totalReturnManagerOrders = getOrdersForManagerAPI(startDate, real_endDate);
                /**create two empty object for later use. One will be assigned the value as "Cash payment summary"
                 *the other will be assigned the value of "Online payment summary"
                 * */
                TotalReturnManagerOrder a;
                TotalReturnManagerOrder b;
                TotalReturnManagerOrder c;
                TotalReturnManagerOrder exception_a = new TotalReturnManagerOrder();  //cash
                TotalReturnManagerOrder exception_b = new TotalReturnManagerOrder();   //online
                TotalReturnManagerOrder exception_c = new TotalReturnManagerOrder();   //online
                TotalReturnManagerOrder total = new TotalReturnManagerOrder();
                if (totalReturnManagerOrders != null) {
                    if (totalReturnManagerOrders.size() == 1) {
                        a = totalReturnManagerOrders.get(0);
                        if (a.payment_method.contains("Cash")) {
                            total.total_cashpay_number = a.total_onlinepay_number;
                            total.total_cashpay_sales = a.total_onlinepay_sales;
                            total.total_cashpay_delivery = a.total_onlinepay_delivery;
                        } else {
                            total.total_onlinepay_number = a.total_onlinepay_number;
                            total.total_onlinepay_sales = a.total_onlinepay_sales;
                            total.total_onlinepay_delivery = a.total_onlinepay_delivery;
                        }
                    } else if (totalReturnManagerOrders.size() == 2) {
                        if (!totalReturnManagerOrders.get(0).payment_method.contains("Cash")) {
                            a = totalReturnManagerOrders.get(0);
                            b = totalReturnManagerOrders.get(1);
                        } else {
                            a = totalReturnManagerOrders.get(1);
                            b = totalReturnManagerOrders.get(0);
                        }
                        total.total_onlinepay_number = a.total_onlinepay_number;
                        total.total_onlinepay_sales = a.total_onlinepay_sales;
                        total.total_onlinepay_delivery = a.total_onlinepay_delivery;
                        total.total_cashpay_number = b.total_onlinepay_number;
                        total.total_cashpay_sales = b.total_onlinepay_sales;
                        total.total_cashpay_delivery = b.total_onlinepay_delivery;
                    } else if (totalReturnManagerOrders.size() == 3) {
                        a = totalReturnManagerOrders.get(0);
                        b = totalReturnManagerOrders.get(1);
                        c = totalReturnManagerOrders.get(2);

                        total.total_cashpay_number = a.total_onlinepay_number;
                        total.total_cashpay_sales = a.total_onlinepay_sales;
                        total.total_cashpay_delivery = a.total_onlinepay_delivery;

                        total.total_onlinepay_number = b.total_onlinepay_number + c.total_onlinepay_number;
                        total.total_onlinepay_sales = b.total_onlinepay_sales.add(c.total_onlinepay_sales);
                        total.total_onlinepay_delivery = b.total_onlinepay_delivery.add(c.total_onlinepay_delivery);
                    }
                    ArrayList<TotalReturnManagerOrder> totalExceptionOrders;
                    totalExceptionOrders = getExceptionManagerOrdersAPI(startDate, real_endDate);
                    if (totalExceptionOrders.size() > 0) {
                        if (totalExceptionOrders.size() == 1) {
                            if (totalExceptionOrders.get(0).payment_method.contains("Cash")) {
                                exception_a = totalExceptionOrders.get(0);//cash
                            } else {
                                exception_b = totalExceptionOrders.get(0);//online
                            }
                        } else if (totalExceptionOrders.size() == 2) {
                            if (totalExceptionOrders.get(0).payment_method.contains("Cash")) {
                                exception_a = totalExceptionOrders.get(0);
                                exception_b = totalExceptionOrders.get(1);
                            } else {
                                exception_a = totalExceptionOrders.get(1);
                                exception_b = totalExceptionOrders.get(0);
                            }
                        } else if (totalExceptionOrders.size() == 3) {
                            exception_a = totalExceptionOrders.get(0);
                            exception_b = totalExceptionOrders.get(1);
                            exception_c = totalExceptionOrders.get(2);
                        }
                        total.total_cashpay_sales = total.total_cashpay_sales.add(exception_a.total_onlinepay_delivery);
                        total.total_onlinepay_sales = total.total_onlinepay_sales.add(exception_b.total_onlinepay_delivery).add(exception_c.total_onlinepay_delivery);
                        total.total_cashpay_delivery = total.total_cashpay_delivery.subtract(exception_a.total_onlinepay_delivery);
                        total.total_onlinepay_delivery = total.total_onlinepay_delivery.subtract(exception_b.total_onlinepay_delivery).subtract(exception_c.total_onlinepay_delivery);
                    }
                    int cashpay_orders = total.total_cashpay_number;
                    int onlinepay_orders = total.total_onlinepay_number;
                    int total_orders = cashpay_orders + onlinepay_orders;
                    double total_sales = total.total_cashpay_sales.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + total.total_onlinepay_sales.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();  // round the number to two digits
                    DecimalFormat df = new DecimalFormat("#.##");
                    total_sales = Double.valueOf(df.format(total_sales));
                    ManagerOrdersMarinaNumber managerOrdersMarinaNumber = managerTotalOrdersAPI(startDate, real_endDate);
//                    ArrayList<ReturnManagerOrder> totalOrders = managerOrdersMarinaNumber.returnManagerOrders;
//                int marinaOrders = managerOrdersMarinaNumber.marinaOrders;
//                    int marinaChargedOrders = managerOrdersMarinaNumber.marinaChargedOrders;
                    ArrayList<BrandSummary> totalBrands = getBrandSummaryAPI(startDate, real_endDate);
//                    List<CashierCash> all_cashiers = CashierCash.findCashierByDate(startDate, real_endDate);  //Ebean
//                    int cashier_number = all_cashiers.size();
                    ArrayList<MealDealProduct> cashier_vouchers = getCashierSentVouchersAPI(startDate, real_endDate); //Ebean

                //from print info start
                     List<DriverDailySummary> all_driver_salary = DriverDailySummary.findSalaryByDate(startDate,real_endDate); //Ebean
//                    BigDecimal total_driver_salary = BigDecimal.valueOf(0);
                    BigDecimal total_voucher_value = BigDecimal.valueOf(0);
                    for (DriverDailySummary this_one : all_driver_salary) {
//                        total_driver_salary = total_driver_salary.add(this_one.total_wage).add(this_one.extra_cost);
                        total_voucher_value = total_voucher_value.add(this_one.total_voucher_value);
                    }
//                    //总现金结算： 现金付款收入(不含运费) - 司机在线付款运费 - 司机工资（包含extra cost） - Marina 特殊订单 - 总voucher代金券
//                    BigDecimal final_cash_left = total.total_cashpay_sales.subtract(total.total_onlinepay_delivery).subtract(total_driver_salary).subtract(BigDecimal.valueOf(marinaChargedOrders)).subtract(total_voucher_value);
                    //新现金结算规则： 现金订单总收入 - 司机在线付款运费 - 总Voucher 代金券
                    BigDecimal final_cash_left = total.total_cashpay_sales.subtract(total.total_onlinepay_delivery).subtract(total_voucher_value);
                    TotalReturnManagerOrder data_container = new TotalReturnManagerOrder(0,0,BigDecimal.valueOf(0),total_voucher_value, final_cash_left, BigDecimal.valueOf(0), "");
                    String today = "Start: "+ startDate + " End: "+ endDate;
                //from print info end
                    HashMap<String, String> submittedDate = new HashMap<String, String>();
                    submittedDate.put("startDate",startDate);
                    submittedDate.put("endDate",endDate);
                    Form<StatisticsAPI.PickDate> newForm = Form.form(StatisticsAPI.PickDate.class).bind(submittedDate);
                    return ok(totalManagerSummary.render(newForm,total, total_orders, total_sales,totalBrands, cashier_vouchers,today,data_container));
                }else {
                    return notFound("No Orders");
                }
            }
        }else{
            flash("error","please log in first");
            return redirect(WebAPI.login());
        }

    }
    //打印manager总单(含时间跨度)
    public static Result managerPrintAPI() throws Exception {
        String email = session("email");
        // if in session
        if (email != null && email.equals("a2b.manager@a2bliving.ie")) {
            DynamicForm dateForm = Form.form().bindFromRequest();  //receive
            String startDate = dateForm.get("startDate");
            String endDate = dateForm.get("endDate");
            if (startDate ==null || endDate ==null || startDate.equals("") || endDate.equals("") ){
                flash("error", "please enter Start and End Date");
                return redirect(routes.ManagerAPI.totalSummaryPage());
            }else {
                String[] endString = endDate.split("-");
                String day = endString[2];
                int realDay = Integer.valueOf(day) + 1;
                String real_endDate = endString[0] + "-" + endString[1] + "-" + String.valueOf(realDay);
                System.out.println(startDate);
                System.out.println(endDate);
                ArrayList<TotalReturnManagerOrder> totalReturnManagerOrders;
                totalReturnManagerOrders = getOrdersForManagerAPI(startDate, real_endDate);
                /**create two empty object for later use. One will be assigned the value as "Cash payment summary"
                 *the other will be assigned the value of "Online payment summary"
                 * */
                TotalReturnManagerOrder a;
                TotalReturnManagerOrder b;
                TotalReturnManagerOrder c;
                TotalReturnManagerOrder exception_a = new TotalReturnManagerOrder();  //cash
                TotalReturnManagerOrder exception_b = new TotalReturnManagerOrder();   //online
                TotalReturnManagerOrder exception_c = new TotalReturnManagerOrder();   //online
                TotalReturnManagerOrder total = new TotalReturnManagerOrder();
                if (totalReturnManagerOrders != null) {
                    if (totalReturnManagerOrders.size() == 1) {
                        a = totalReturnManagerOrders.get(0);
                        if (a.payment_method.contains("Cash")) {
                            total.total_cashpay_number = a.total_onlinepay_number;
                            total.total_cashpay_sales = a.total_onlinepay_sales;
                            total.total_cashpay_delivery = a.total_onlinepay_delivery;
                        } else {
                            total.total_onlinepay_number = a.total_onlinepay_number;
                            total.total_onlinepay_sales = a.total_onlinepay_sales;
                            total.total_onlinepay_delivery = a.total_onlinepay_delivery;
                        }
                    } else if (totalReturnManagerOrders.size() == 2) {
                        if (!totalReturnManagerOrders.get(0).payment_method.contains("Cash")) {
                            a = totalReturnManagerOrders.get(0); //online
                            b = totalReturnManagerOrders.get(1); //cash

                        } else {
                            a = totalReturnManagerOrders.get(1); //online
                            b = totalReturnManagerOrders.get(0); //cash
                        }
                        total.total_onlinepay_number = a.total_onlinepay_number;
                        total.total_onlinepay_sales = a.total_onlinepay_sales;
                        total.total_onlinepay_delivery = a.total_onlinepay_delivery;
                        total.total_cashpay_number = b.total_onlinepay_number;
                        total.total_cashpay_sales = b.total_onlinepay_sales;
                        total.total_cashpay_delivery = b.total_onlinepay_delivery;
                    } else if (totalReturnManagerOrders.size() == 3) {
                        a = totalReturnManagerOrders.get(0);
                        b = totalReturnManagerOrders.get(1);
                        c = totalReturnManagerOrders.get(2);

                        total.total_cashpay_number = a.total_onlinepay_number;
                        total.total_cashpay_sales = a.total_onlinepay_sales;
                        total.total_cashpay_delivery = a.total_onlinepay_delivery;

                        total.total_onlinepay_number = b.total_onlinepay_number + c.total_onlinepay_number;
                        total.total_onlinepay_sales = b.total_onlinepay_sales.add(c.total_onlinepay_sales);
                        total.total_onlinepay_delivery = b.total_onlinepay_delivery.add(c.total_onlinepay_delivery);
                    }
                    ArrayList<TotalReturnManagerOrder> totalExceptionOrders;
                    totalExceptionOrders = getExceptionManagerOrdersAPI(startDate, real_endDate);
                    if (totalExceptionOrders.size() > 0) {
                        if (totalExceptionOrders.size() == 1) {
                            if (totalExceptionOrders.get(0).payment_method.contains("Cash")) {
                                exception_a = totalExceptionOrders.get(0);//cash
                            } else {
                                exception_b = totalExceptionOrders.get(0);//online
                            }
                        } else if (totalExceptionOrders.size() == 2) {
                            if (totalExceptionOrders.get(0).payment_method.contains("Cash")) {
                                exception_a = totalExceptionOrders.get(0);
                                exception_b = totalExceptionOrders.get(1);
                            } else {
                                exception_a = totalExceptionOrders.get(1);
                                exception_b = totalExceptionOrders.get(0);
                            }
                        } else if (totalExceptionOrders.size() == 3) {
                            exception_a = totalExceptionOrders.get(0);
                            exception_b = totalExceptionOrders.get(1);
                            exception_c = totalExceptionOrders.get(2);
                        }
                        total.total_cashpay_sales = total.total_cashpay_sales.add(exception_a.total_onlinepay_delivery);
                        total.total_onlinepay_sales = total.total_onlinepay_sales.add(exception_b.total_onlinepay_delivery).add(exception_c.total_onlinepay_delivery);
                        total.total_cashpay_delivery = total.total_cashpay_delivery.subtract(exception_a.total_onlinepay_delivery);
                        total.total_onlinepay_delivery = total.total_onlinepay_delivery.subtract(exception_b.total_onlinepay_delivery).subtract(exception_c.total_onlinepay_delivery);
                    }
                    int cashpay_orders = total.total_cashpay_number;
                    int onlinepay_orders = total.total_onlinepay_number;
                    int total_orders = cashpay_orders + onlinepay_orders;
                    double total_sales = total.total_cashpay_sales.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + total.total_onlinepay_sales.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();  // round the number to two digits
                    DecimalFormat df = new DecimalFormat("#.##");
                    total_sales = Double.valueOf(df.format(total_sales));
                    ArrayList<BrandSummary> totalBrands = getBrandSummaryAPI(startDate, real_endDate);
                    ManagerOrdersMarinaNumber managerOrdersMarinaNumber = managerTotalOrdersAPI(startDate, real_endDate);
//                ArrayList<ReturnManagerOrder> totalOrders = managerOrdersMarinaNumber.returnManagerOrders;
//                int marinaOrders = managerOrdersMarinaNumber.marinaOrders;
//                    int marinaChargedOrders = managerOrdersMarinaNumber.marinaChargedOrders;
//                    List<CashierCash> all_cashiers = CashierCash.findCashierByDate(startDate, real_endDate); 忽略Cashier 收入情况
                    List<DriverDailySummary> all_driver_salary = DriverDailySummary.findSalaryByDate(startDate, real_endDate);
//                    BigDecimal total_driver_salary = BigDecimal.valueOf(0);
                    BigDecimal total_voucher_value = BigDecimal.valueOf(0);
                    for (DriverDailySummary this_one : all_driver_salary) {
//                        total_driver_salary = total_driver_salary.add(this_one.total_wage).add(this_one.extra_cost); //只算入了司机工资（按小时） + 额外补贴， 没加入Marina park 的补贴
                        total_voucher_value = total_voucher_value.add(this_one.total_voucher_value); //代金券
                    }
//                    //总现金结算： 现金付款收入(不含运费) - 司机在线付款运费 - 司机工资（包含extra cost） - Marina 特殊订单 - 总voucher代金券
//                    BigDecimal final_cash_left = total.total_cashpay_sales.subtract(total.total_onlinepay_delivery).subtract(total_driver_salary).subtract(BigDecimal.valueOf(marinaChargedOrders)).subtract(total_voucher_value);
                    //新现金结算规则： 现金订单总收入 - 司机在线付款运费 - 总Voucher 代金券
                    BigDecimal final_cash_left = total.total_cashpay_sales.subtract(total.total_onlinepay_delivery).subtract(total_voucher_value);
                    TotalReturnManagerOrder data_container = new TotalReturnManagerOrder(0, 0, BigDecimal.valueOf(0), total_voucher_value, final_cash_left, BigDecimal.valueOf(0), "");
                    String today = "Start: " + startDate + " End: " + endDate;
                    DriverPayrollSummary total_delivery_summary = getAllDeliveryIncome(startDate, real_endDate);
                    return ok(totalSummaryPrint.render(total, total_orders, total_sales, totalBrands, today, data_container,total_delivery_summary));
                } else {
                    return notFound("No such driver");
                }
            }

        }else{
            flash("error","please log in first");
            return redirect(WebAPI.login());
        }
    }


    public static ArrayList<MealDealProduct> getCashierSentVouchersAPI(String startDate, String endDate) {
        String sql =
                "SELECT ps_message.id_customer, count(ps_message.id_message)\n" +
                        "\n" +
                        "FROM ps_orders, ps_message\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_message.id_order\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +
                        "\n" +
                        "and ps_message.message like '%+CONGRATULATIONS%'\n" +
//                        "\n" +
//                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                        "AND ps_orders.invoice_date >" + "'" + startDate + "'" + "\n" +
                        "AND ps_orders.invoice_date <" + "'" + endDate + "'" + "\n" +
                        "\n" +
                        "group by ps_message.id_customer";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_message.id_customer", "id_product_shop")
                        .columnMapping("count(ps_message.id_message)", "purchased_quantity")
                        .create();

        com.avaje.ebean.Query<MealDealProduct> query = Ebean.find(MealDealProduct.class);
        query.setRawSql(rawSql);
        List<MealDealProduct> list = query.findList();
        ArrayList<MealDealProduct> totalCashierVouchers = new ArrayList<>();
        totalCashierVouchers.addAll(list);
        return totalCashierVouchers;
    }

    /**
     * Manager check total sales of the day
     * @return receipt View page
     * */
    public static ManagerOrdersMarinaNumber managerTotalOrdersAPI(String startDate, String endDate) throws Exception{
        int marinaOrders = 0;
        int marinaChargedOrders = 0;
        try{
            String sql =
                "SELECT ps_orders.id_order, ps_drivers_orders.order_number, ps_orders.reference, ps_orders.id_customer, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_orders.invoice_date, ps_order_state_lang.name, ps_drivers_orders.driver_name\n" +
                        "FROM ps_orders\n" +
                        "LEFT JOIN ps_drivers_orders\n" +
                        "ON ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "LEFT JOIN ps_order_state_lang\n" +
                        "ON ps_orders.current_state = ps_order_state_lang.id_order_state\n" +
                        "\n" +
                        "WHERE  ps_orders.valid = 1\n" +
//                        "AND ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +   // 更改时间
                        "AND ps_orders.invoice_date >" + "'" + startDate + "'" + "\n" +
                        "AND ps_orders.invoice_date <" + "'" + endDate + "'" + "\n" +
                        "order by ps_orders.invoice_date";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "id_order")
                            .columnMapping("ps_drivers_orders.order_number", "order_number")
                            .columnMapping("ps_orders.reference", "order_reference")
                            .columnMapping("ps_orders.id_customer", "id_customer")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.total_paid_real", "total_products_wt")  // calculate only the products prices
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_orders.invoice_date", "date_add")
                            .columnMapping("ps_order_state_lang.name", "order_status")
                            .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                            .create();

            com.avaje.ebean.Query<ReturnManagerOrder> query = Ebean.find(ReturnManagerOrder.class);
            query.setRawSql(rawSql);
            List<ReturnManagerOrder> list = query.findList();
//            int size = list.size();
            for(ReturnManagerOrder this_one :list){
                if(this_one.driver_name ==null){
                    this_one.driver_name = "Unknown";
                    this_one.order_number = 0;
                }
                BigDecimal discount_amount = this_one.total_discounts;
                BigDecimal shipping_amount = this_one.total_shipping;
                if (shipping_amount.doubleValue() == 0.0) {
                    marinaOrders += 1;
                    if (!this_one.driver_name.contains("free")) {
                        marinaChargedOrders += 1;
                    }
                } else if (discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0) {  //特殊情况1
                    this_one.total_shipping = BigDecimal.valueOf(0.0);
                    marinaOrders += 1;
                    if (!this_one.driver_name.contains("free")) {
                        marinaChargedOrders += 1;
                    }
                } else if (shipping_amount.doubleValue() == 2.0 && discount_amount.doubleValue() == 7.0) {  //特殊情况2
                    this_one.total_shipping = BigDecimal.valueOf(0.0);
                    marinaOrders += 1;
                    if (!this_one.driver_name.contains("free")) {
                        marinaChargedOrders += 1;
                    }
                }
                this_one.total_products_wt = this_one.total_products_wt.subtract(this_one.total_shipping);  //实际产品的价钱 = 实际付款 - 运费
            }
//            for(int i =0; i<size;i++){
//                BigDecimal discount_amount = list.get(i).total_discounts;
//                BigDecimal shipping_amount = list.get(i).total_shipping;
//                if (shipping_amount.doubleValue() == 0.0){
//                    marinaOrders +=1;
////                    if(list.get(i).driver_name !=null) {
//                        if (!list.get(i).driver_name.contains("free")) {
//                            marinaChargedOrders += 1;
//                        }
////                    }
//                }else if(discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0 ){  //特殊情况1
//                    list.get(i).total_products_wt = list.get(i).total_products_wt;
//                    list.get(i).total_shipping = BigDecimal.valueOf(0.0);
//                    marinaOrders +=1;
//                    if(!list.get(i).driver_name.contains("free")){
//                        marinaChargedOrders +=1;
//                    }
//                }else if(shipping_amount.doubleValue()==2.0 && discount_amount.doubleValue() == 7.0 ){  //特殊情况2
//                    list.get(i).total_products_wt = list.get(i).total_products_wt;
//                    list.get(i).total_shipping = BigDecimal.valueOf(0.0);
//                    marinaOrders +=1;
//                    if(!list.get(i).driver_name.contains("free")){
//                        marinaChargedOrders +=1;
//                    }
//                }else{
//                    list.get(i).total_products_wt = list.get(i).total_products_wt.subtract(list.get(i).total_shipping);
//                }
//            }
            ArrayList<ReturnManagerOrder> totalOrders = new ArrayList<>();  //有司机名字
            totalOrders.addAll(list);
            ManagerOrdersMarinaNumber totalManagerOrders = new ManagerOrdersMarinaNumber(totalOrders,marinaOrders, marinaChargedOrders);
            return totalManagerOrders;
        }catch (Exception e){    //null pointer exception
            String sql =
                    "SELECT ps_orders.id_order, ps_orders.reference, ps_orders.id_customer, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_orders.invoice_date, ps_order_state_lang.name\n" +
                            "FROM ps_orders\n" +
                            "LEFT JOIN ps_drivers_orders\n" +
                            "ON ps_orders.id_order = ps_drivers_orders.id_order\n" +
                            "\n" +
                            "LEFT JOIN ps_order_state_lang\n" +
                            "ON ps_orders.current_state = ps_order_state_lang.id_order_state\n" +
                            "\n" +
                            "WHERE  ps_orders.valid = 1\n" +
//                            "AND ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +   //更改时间
                            "AND ps_orders.invoice_date >" + "'" + startDate + "'" + "\n" +
                            "AND ps_orders.invoice_date <" + "'" + endDate + "'" + "\n" +
                            "order by ps_orders.invoice_date";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "id_order")
                            .columnMapping("ps_orders.reference", "order_reference")
                            .columnMapping("ps_orders.id_customer", "id_customer")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.total_paid_real", "total_products_wt")  // calculate only the products prices
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_orders.invoice_date", "date_add")
                            .columnMapping("ps_order_state_lang.name", "order_status")
                            .create();

            com.avaje.ebean.Query<ReturnManagerOrder> query = Ebean.find(ReturnManagerOrder.class);
            query.setRawSql(rawSql);
            List<ReturnManagerOrder> list = query.findList();
            int size = list.size();
            for(int i =0; i<size;i++){
                BigDecimal discount_amount = list.get(i).total_discounts;
                BigDecimal shipping_amount = list.get(i).total_shipping;
                if (shipping_amount.doubleValue() == 0.0){
                    marinaOrders +=1;
                    if(!list.get(i).driver_name.contains("free")){
                        marinaChargedOrders +=1;
                    }
                }else if(discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0 ){
                    list.get(i).total_products_wt = list.get(i).total_products_wt;
                    list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                    marinaOrders +=1;
                    if(!list.get(i).driver_name.contains("free")){
                        marinaChargedOrders +=1;
                    }
                }else if(shipping_amount.doubleValue()==2.0 && discount_amount.doubleValue() == 7.0 ){  //特殊情况2
                    list.get(i).total_products_wt = list.get(i).total_products_wt;
                    list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                    marinaOrders +=1;
                    if(!list.get(i).driver_name.contains("free")){
                        marinaChargedOrders +=1;
                    }
                }else{
                    list.get(i).total_products_wt = list.get(i).total_products_wt.subtract(list.get(i).total_shipping);
                }
            }
            ArrayList<ReturnManagerOrder> totalOrders = new ArrayList<>();
            totalOrders.addAll(list);
            ManagerOrdersMarinaNumber totalManagerOrders = new ManagerOrdersMarinaNumber(totalOrders,marinaOrders, marinaChargedOrders);
            return totalManagerOrders;
        }
    }

    /**
     * Get a total summary of overall orders of a day, including cashpay and online pay orders
     * @see TotalReturnManagerOrder
     * */
    public static ArrayList<TotalReturnManagerOrder> getOrdersForManagerAPI(String startDate, String endDate) {
        String sql =
                "SELECT count(*), sum(ps_orders.total_paid_real) as total_product_sales, sum(ps_orders.total_shipping) as total_shipping, ps_orders.payment\n" +
                        "FROM ps_orders\n" +
                        "WHERE  ps_orders.valid = 1\n" +
//                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +  //更改时间
                        "AND ps_orders.invoice_date >" + "'" + startDate + "'" + "\n" +
                        "AND ps_orders.invoice_date <" + "'" + endDate + "'" + "\n" +
                        "group by ps_orders.payment";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(*)", "total_onlinepay_number")
                        .columnMapping("sum(ps_orders.total_paid_real)", "total_onlinepay_sales")
                        .columnMapping("sum(ps_orders.total_shipping)", "total_onlinepay_delivery")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .create();

        com.avaje.ebean.Query<TotalReturnManagerOrder> query = Ebean.find(TotalReturnManagerOrder.class);
        query.setRawSql(rawSql);
        List<TotalReturnManagerOrder> list = query.findList();
        int size = list.size();
        for (int i =0; i<size;i++){
            list.get(i).total_onlinepay_sales = list.get(i).total_onlinepay_sales.subtract(list.get(i).total_onlinepay_delivery);  //total_paid_real - total_shipping
        }
        ArrayList<TotalReturnManagerOrder> totalReturnDriverOrders = new ArrayList<>();
        totalReturnDriverOrders.addAll(list);
        return totalReturnDriverOrders;
    }

    public static ArrayList<TotalReturnManagerOrder> getExceptionManagerOrdersAPI(String startDate, String endDate) {
        String sql =
                "SELECT count(*), sum(ps_orders.total_shipping) as total_shipping, ps_orders.payment\n" +
                        "FROM ps_orders\n" +
                        "where  ps_orders.valid = 1\n" +
                        "and ((ps_orders.total_discounts = 2.000000 and ps_orders.total_shipping = 2.000000)\n" +
                        "or (ps_orders.total_discounts = 7.000000 and ps_orders.total_shipping = 2.000000))\n" +
//                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +  //更改时间
                        "AND ps_orders.invoice_date >" + "'" + startDate + "'" + "\n" +
                        "AND ps_orders.invoice_date <" + "'" + endDate + "'" + "\n" +
                        "group by ps_orders.payment";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(*)", "total_onlinepay_number")
                        .columnMapping("sum(ps_orders.total_shipping)", "total_onlinepay_delivery")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .create();

        com.avaje.ebean.Query<TotalReturnManagerOrder> query = Ebean.find(TotalReturnManagerOrder.class);
        query.setRawSql(rawSql);
        List<TotalReturnManagerOrder> list = query.findList();
        ArrayList<TotalReturnManagerOrder> totalReturnDriverOrders = new ArrayList<>();
        totalReturnDriverOrders.addAll(list);
        return totalReturnDriverOrders;
    }

     /**
     * Get a list of items, showing the sales conditions of a day
     * @see BrandSummary
     * */
    public static ArrayList<BrandSummary> getBrandSummaryAPI(String startDate, String endDate) {
        BrandSummary shop = new BrandSummary();
        BrandSummary pizza = new BrandSummary();
        BrandSummary asian = new BrandSummary();
        BrandSummary chinese = new BrandSummary();
        BrandSummary starter = new BrandSummary();
        BrandSummary tobacco = new BrandSummary();// 2017/2/17
        BrandSummary lunch =   new BrandSummary();// 2017/7/18

        String sql =
                "SELECT distinct ps_order_detail.product_reference, sum(ps_order_detail.product_quantity), sum(ps_order_detail.total_price_tax_incl)\n" +
                        "\n" +
                        "from  ps_orders, ps_order_detail\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_order_detail.id_order\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +
//                        "and ps_orders.invoice_date > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +  //更改时间
                        "AND ps_orders.invoice_date >" + "'" + startDate + "'" + "\n" +
                        "AND ps_orders.invoice_date <" + "'" + endDate + "'" + "\n" +
                        "\n"+
                        "group by ps_order_detail.product_reference";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_order_detail.product_reference", "brand_name")
                        .columnMapping("sum(ps_order_detail.product_quantity)", "number_of_piece")
                        .columnMapping("sum(ps_order_detail.total_price_tax_incl)", "total_sales")
                        .create();
        com.avaje.ebean.Query<BrandSummary> query = Ebean.find(BrandSummary.class);
        query.setRawSql(rawSql);
        List<BrandSummary> list = query.findList();
        int number = list.size();
        for (int i = 0; i < number;i++){
            if(list.get(i).brand_name.equals("")){
                shop = list.get(i);
            }else if(list.get(i).brand_name.equals("pizza")){
                pizza = list.get(i);
            }else if(list.get(i).brand_name.equals("asian")){
                asian = list.get(i);
            }else if(list.get(i).brand_name.equals("chinese")){
                chinese = list.get(i);
            }else if(list.get(i).brand_name.equals("starter")){
                starter = list.get(i);
            }else if(list.get(i).brand_name.equals("tobacco")){
                tobacco = list.get(i);
            }else if(list.get(i).brand_name.equals("lunch")){
                lunch = list.get(i);
            }
        }
        ArrayList<BrandSummary> totalSummary = new ArrayList<>();
        totalSummary.add(shop);
        totalSummary.add(pizza);
        totalSummary.add(asian);
        totalSummary.add(chinese);
        totalSummary.add(starter);
        totalSummary.add(tobacco);
        totalSummary.add(lunch);//增添了lunch 统计
        return totalSummary;
    }

//    //GET 链接，点击进入产品销量排名 （todo）
//    public static Result getShopProductRanking(String startTime, String endTime, int product){
//        //登陆session验证
//        DynamicForm dateForm = Form.form().bindFromRequest();  //receive
//        String form_startDate = dateForm.get("startDate");
//        String form_endDate = dateForm.get("endDate");
//        String startDate = "";
//        String endDate = "";
//        String product_type = "";
//        switch (product){
//            case 1: product_type = "";
//                break;
//            case 2: product_type = "pizza";
//                break;
//            case 3: product_type = "asian";
//                break;
//            case 4: product_type = "chinese";
//                break;
//            case 5: product_type = "starter";
//                break;
//            case 6: product_type = "tobacco";
//                break;
//            case 7: product_type = "lunch";
//                break;
//        }
//        if (form_startDate ==null || form_endDate ==null || form_startDate.equals("") || form_endDate.equals("") ){   //如果不是表格重新传入数据
//            startDate = startTime;
//            endDate = endTime;
//        }
//        else {
//            startDate = form_startDate;
//            endDate = form_endDate;
//        }
//        int sizeOfStart = startDate.length();
//        int sizeOfEnd  = endDate.length();
//        if(sizeOfEnd < 10 || sizeOfStart < 10){
//            return badRequest("Please enter a valid Start or End Date");
//        }else{
//            String[]  endString = endDate.split("-");
//            if(endString.length < 3){
//                return badRequest("Please enter a valid Start or End Date");
//            }else{
//                String day = endString[2];
//                int realDay = Integer.valueOf(day) + 1;
//                String real_endDate = endString[0]+"-"+endString[1]+"-"+ String.valueOf(realDay);
//                //搜索orders brands
//                String sql =
//                        "select ps_order_detail.product_name, ps_order_detail.product_id, sum(ps_order_detail.product_quantity), sum(ps_order_detail.total_price_tax_incl), ps_order_detail.product_reference, ps_order_detail.original_product_price, ps_order_detail.id_tax_rules_group\n" +
//                                "from ps_order_detail, ps_orders\n" +
//                                "where ps_order_detail.id_order = ps_orders.id_order\n" +
//                                "and ps_order_detail.product_reference =" + "'"+ product_type + "'"+"\n" +
//                                "and ps_orders.valid = 1\n" +
//                                "and ps_orders.invoice_date >" + "'" + startDate + "'" + "\n" +
//                                "and ps_orders.invoice_date <" + "'" + real_endDate + "'" + "\n" +
//                                "group by ps_order_detail.product_id\n" +
//                                "order by sum(ps_order_detail.product_quantity) desc";
//                RawSql rawSql =
//                        RawSqlBuilder
//                                .parse(sql)
//                                .columnMapping("ps_order_detail.product_name", "product_name")
//                                .columnMapping("ps_order_detail.product_id", "product_id")
//                                .columnMapping("sum(ps_order_detail.product_quantity)", "product_quantity")
//                                .columnMapping("sum(ps_order_detail.total_price_tax_incl)", "total_price_tax_incl")
//                                .columnMapping("ps_order_detail.product_reference", "product_reference")
//                                .columnMapping("ps_order_detail.original_product_price", "original_product_price")
//                                .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
//                                .create();
//                com.avaje.ebean.Query<DiscountOrders> query = Ebean.find(DiscountOrders.class);
//                query.setRawSql(rawSql);
//                List<DiscountOrders> list = query.findList();
//                int size = list.size();
//                BigDecimal total_sales = BigDecimal.valueOf(0);
//                int total_number = 0;
//                for(int i =0; i<size;i++) {
//                    total_sales = total_sales.add(list.get(i).total_price_tax_incl);
//                    total_number += list.get(i).product_quantity;
//                    if (list.get(i).id_tax_rules_group == 1) {
//                        list.get(i).tax_type = "23%";
//                        list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23)).subtract(list.get(i).total_price_tax_incl); //总折扣
//                        list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23) ); // 总原价（含数量)
//                        list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.23); //原价单价
//                    } else if (list.get(i).id_tax_rules_group == 2) {
//                        list.get(i).tax_type = "13.5%";
//                        list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135)).subtract(list.get(i).total_price_tax_incl);
//                        list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135) ); // 总原价（含数量)
//                        list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.135); //原价单价
//                    } else if (list.get(i).id_tax_rules_group == 3) {
//                        list.get(i).tax_type = "9%";
//                        list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09)).subtract(list.get(i).total_price_tax_incl);
//                        list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09) ); // 总原价（含数量)
//                        list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.09); //原价单价
//                    } else if (list.get(i).id_tax_rules_group == 4) {  //Special tax
//                        list.get(i).tax_type = "4.8%";
//                        list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048)).subtract(list.get(i).total_price_tax_incl);
//                        list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048) ); // 总原价（含数量)
//                        list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.048); //原价单价
//                    } else if (list.get(i).id_tax_rules_group == 6) {
//                        list.get(i).tax_type = "0%";
//                        list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0)).subtract(list.get(i).total_price_tax_incl);
//                        list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0) ); // 总原价（含数量)
//                        list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.0); //原价单价
//                    }
//                    list.get(i).total_product_discount = list.get(i).total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
//                    list.get(i).original_unit_price = list.get(i).original_unit_price.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
//                    list.get(i).original_product_price = list.get(i).original_product_price.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
//                    list.get(i).product_name= list.get(i).product_name.replace("<b>","").replace("</b>","").replace("<br>"," ").replace("<br/>","").replace("<br />","").replace(":","")
//                            .replace("class=awp_mark_38>","")
//                            .replace("<span ","")
//                            .replace("</span ","")
//                            .replace("</span ","")
//                            .replace("</span ","");
//                    if(list.get(i).product_name.length()>100){
//                        list.get(i).product_name = list.get(i).product_name.substring(0,100);
//                    }
//                }
//                total_sales = total_sales.setScale(2, BigDecimal.ROUND_HALF_UP);
//                BrandSummary summary = new BrandSummary(product_type,total_number, total_sales);
//                ArrayList<DiscountOrders> shop_products = new ArrayList<DiscountOrders>();
//                shop_products.addAll(list);
//                //code finish here
//                HashMap<String, String> submittedDate = new HashMap<String, String>();
//                submittedDate.put("startDate",startDate);
//                submittedDate.put("endDate",endDate);
//                submittedDate.put("productType",product_type);
//                Form<StatisticsAPI.PickDate> newForm = Form.form(StatisticsAPI.PickDate.class).bind(submittedDate);
//                return ok(views.html.products.productsRanking.render(newForm,shop_products,summary));
//            }
//        }
//    }
    public static DriverPayrollSummary getAllDeliveryIncome(String startDate, String endDate) {
        String sql =
                "SELECT sum(cash_delivery_fee), sum(online_delivery_fee)\n" +
                        "FROM ps_driver_daily_summary_flow\n" +
                        "WHERE if_company_car = 2\n" +
                        "AND date_add >" + "'" + startDate + "'" + "\n" +
                        "AND date_add <" + "'" + endDate + "'" ;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(cash_delivery_fee)", "cash_delivery_fee")
                        .columnMapping("sum(online_delivery_fee)", "online_delivery_fee")
                        .create();
        com.avaje.ebean.Query<DriverPayrollSummary> query = Ebean.find(DriverPayrollSummary.class);
        query.setRawSql(rawSql);
        DriverPayrollSummary income_delivery = query.findUnique();
        return income_delivery;
    }
}