/*
 * Copyright (c) 2017. Zhi Chen.
 * DriverAPI is used for cashiers to allocate orders to drivers.
 * Then to calculate how much the orders allocated cost, and how
 * much cash drivers should return.Lastly to have a summary of
 * what a driver did in the past whole day(14 hours interval).
 */

package controllers;


import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Transactional;
import models.database.*;
import models.sqlContainer.OrderSequence;
import models.sqlContainer.ReturnDriverOrder;
import models.template.AddressTemplate;
import models.template.DriverOrderWithInformation;
import models.template.OrderWithSequence;
import models.template.RequestDriverOrders;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.Constraints;
import play.data.validation.Constraints.Required;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.driverPersonalPage;
import views.html.driverPrintDailySummary;
import views.html.driverUnpaidOrdersPage;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static parsers.JsonParser.*;

public class DriverAPI extends Controller {

    /**
     * New Drive Registration class
     * */
    public static class NewDriver{
        @Required
        public String name;

        public String contact_number;

        public NewDriver(String name, String contact_number) {
            this.name = name;
            this.contact_number = contact_number;
        }

        public NewDriver() {
        }
    }

    public static class DriverDailyPayroll{
        @Constraints.Required
        public String till_money;  //driver took money from till
        @Constraints.Required
        public String salary;   //driver salary the day
        @Constraints.Required
        public String extra_cost;   //extra money for Driver
        @Constraints.Required
        public String specification;   //extra money for Driver
        @Constraints.Required
        public String working_hours;   //Driver working time
        @Constraints.Required
        public String voucher_quantity;   //Voucher quantity
        @Constraints.Required
        public String voucher_total_value;   //Voucher total value
        @Constraints.Required
        public String if_company_van;   //Voucher total value
    }

    /**
     * Query all the drivers
     * */
    public static Result allDrivers() throws Exception {
        List<Driver> all_drivers = Driver.findAll();
        if(all_drivers ==null){
            return notFound("Invalid request");
        }
        else {
            return ok(renderCategories(all_drivers));
        }
    }

    /**
     * Query drivers according to work status
     * @param if_work
     * @return List<Driver>
     * */
    public static Result checkDrivers(int if_work) throws Exception {
        List<Driver> work_drivers = Driver.findByState(if_work);
        if(work_drivers == null){
            return notFound("Invalid request");
        }
        else {
            return ok(renderCategories(work_drivers));
        }
    }

    /**
     * Driver register action. through HTTP POST request
     * */
    public static Result postRegister() throws Exception {
       Form<NewDriver> driverForm = Form.form(NewDriver.class).bindFromRequest();
        if (driverForm != null){
                String driver_name = driverForm.get().name;
                String contact_number = driverForm.get().contact_number;
                if (driver_name == null){
                    driver_name = "";
                }
                if (contact_number == null){
                    contact_number = "";
                }
                if (Driver.findByName(driver_name)!= null || Driver.findByPhone(contact_number)!= null){

                    return status(409,"The driver already exists");

                }else{
                    Date register_time = new Date();
                    Driver driver = new Driver(
                            driver_name,
                            0,
                            register_time,
                            register_time,
                            new Date(0),
                            contact_number,
                            "a2bliving",
                            new Date(),
                            new Date(),
                            0
                           );
                    driver.save();
                    System.out.println("A new driver successfully registered");
                    return status(201,"registered");
                }
        }else {
            return status(404,"bad request");
        }
    }

    /**
     * Driver Login data template
     * */
    public static class DriverLogin {
        @Required
        public int id_driver;

        public DriverLogin(int id_driver) {
            this.id_driver = id_driver;
        }
        public DriverLogin(){}
    }

    /**
     * All the orders of the day for drivers to deliver
     * Cashier will allocate those orders to at-work drivers
     * */
    public static Result OrdersOfTheDay() throws Exception {
        List<Order> orders_of_the_day = Order.ordersForDriver();
        ArrayList<OrderWithSequence> orders_with_sequence = new ArrayList<>();
        for (Order current_order : orders_of_the_day) {
            OrderWithSequence new_sequence_order = new OrderWithSequence();
            int id_order = current_order.id_order;
            DriverOrders if_exist = DriverOrders.findByOrderId(id_order);
            //check if this order is already allocated to one driver
            if (if_exist == null) {
                OrderInvoice current_invoice = OrderInvoice.findByOrder(id_order);
                if (current_invoice != null){          //only successfully paid orders can be counted
                new_sequence_order.delivery_address = current_invoice.delivery_address;
                new_sequence_order.id_order = id_order;
                new_sequence_order.order_reference = current_order.reference;
                new_sequence_order.order_sequence = getSequenceNumber(id_order);
                new_sequence_order.id_customer = current_order.id_customer;
                new_sequence_order.payment_method = current_order.payment;
                int state = current_order.current_state;
                new_sequence_order.order_state = OrderState.findById(state);
                new_sequence_order.total_discounts = current_order.total_discounts;
                new_sequence_order.total_paid = current_order.total_paid;
                new_sequence_order.total_shipping = current_order.total_shipping;
                new_sequence_order.date_add = current_order.date_add;
                new_sequence_order.delivery_date = current_order.delivery_date;


                orders_with_sequence.add(new_sequence_order);
                }
            }
        }
        return ok(renderCategories(orders_with_sequence));
    }

    /**
     * In the charge of one certain cashier, all the orders of driver delivery will be displayed
     * @param id_driver
     */
    public static Result OrderOfDriver(int id_driver) throws Exception {
        DynamicForm driver_cashier_order = Form.form().bindFromRequest();
        int id_cashier;
        int if_paid;
        try{
        id_cashier = Integer.parseInt(driver_cashier_order.get("id_cashier"));
        if_paid = Integer.parseInt(driver_cashier_order.get("if_paid"));
        }
        catch(Exception e) {
        return status(409,"No valid cashier information");
        }
        List<DriverOrders> driverOrders = DriverOrders.workOfDriver(id_driver,id_cashier,if_paid);
        return ok(renderCategories(driverOrders));
    }

    /**
     * Cashier controls drivers to log in, log in with Driver_ID
     * Change the value of if_work in the Drivers Table from 0 to 1
     * */
    public static Result DriverLogin() throws Exception {
        Form<DriverLogin> driverForm = Form.form(DriverLogin.class).bindFromRequest();
         int id = driverForm.get().id_driver;
         Date login_time = new Date();
         Driver driver = Driver.findById(id);
        if (driver != null) {
               driver.if_work = 1;
               driver.last_login = login_time;
               driver.save();
            return ok("login successfully");
        }else {
            return notFound("Invalid driver");
        }
     }

    /**
     * Cashier controls driver to log out.
     * Change the if_paid value to 0.
     * @param id_driver
     * */
    public static Result DriverLogout(int id_driver) throws Exception {
        Date logout_time = new Date();
        Driver driver = Driver.findById(id_driver);
        if (driver != null) {
            driver.if_work = 0;
            driver.last_logout = logout_time;
            driver.save();
            return ok("log out successfully");
        }else {
            return notFound("Invalid driver");
        }
    }
    /**
     * Cashier allocate orders to drivers.
     * In this process, a joint record combined with cashier_id, order_id and driver_id
     * will be created in the database.
     * @param id_driver
     * @param cashier_id
     * */
//    @Transactional
//    public static Result addOrderToDriver(int id_driver, int cashier_id) throws Exception {
//        DynamicForm order_driver = Form.form().bindFromRequest();
//        int id_order = Integer.parseInt(order_driver.get("id_order"));
//        int order_number = Integer.parseInt(order_driver.get("order_number")); //new
//        String driver_name = order_driver.get("driver_name");
//        if (driver_name == null){
//            driver_name = "";
//        }
//        String order_reference = order_driver.get("order_reference");
//        if (order_reference == null) {
//            order_reference = "";
//        }
//        Date date_add = new Date();
//        DriverOrders a = DriverOrders.findByOrderId(id_order);
//        DriverOrders b = DriverOrders.findByReference(order_reference);
//        if (a != null || b !=  null){
//            return status(409,"This order was already added");
//        }else{
//            //directly assign if_paid = 1 if the order is paid through online payment
//            int if_paid = 0;
//            String payment = order_driver.get("payment_method");
//            if (payment!= null && !payment.contains("Cash")){
//                if_paid = 1;
//            }
//            DriverOrders driverOrders = new DriverOrders(
//                    id_driver,
//                    driver_name,
//                    id_order,
//                    order_reference,
//                    1,
//                    date_add,
//                    order_number,
//                    cashier_id,
//                    if_paid,
//                    0
//            );
//            driverOrders.save();
//            int id_driver_order = driverOrders.id_driver_order;
//            return ok(renderCategory(id_driver_order));
//        }
//    }
    /**
     * Cashier allocate orders to drivers.
     * In this process, a joint record combined with cashier_id, order_id and driver_id
     * will be created in the database.
     * @param id_driver
     * @param cashier_id
     * */
    @Transactional
    public static Result allocateOrderToDriver(int id_driver, int cashier_id) throws Exception {
        ArrayList<RequestDriverOrders> requestDriverOrders = renderRequestDriverTemplate(request().body().asJson().toString());
        int size = requestDriverOrders.size();
        if (size == 0){
        return notFound("Please assign one order first");
        }else {
//            System.out.println(requestDriverOrders);
            ArrayList<DriverOrderWithInformation> error_orders = new ArrayList<>();
            for(RequestDriverOrders driverOrder: requestDriverOrders){
                int id_order = driverOrder.id_order;
                int order_number = driverOrder.order_number;
                String driver_name = driverOrder.driver_name;
                if (driver_name == null) {
                    driver_name = "";
                }
                String order_reference = driverOrder.order_reference;
                Date date_add = new Date();
                DriverOrders a = DriverOrders.findByOrderId(id_order);
                DriverOrders b = DriverOrders.findByReference(order_reference);
                if (a != null || b != null) {
                    error_orders.add(new DriverOrderWithInformation(id_order,order_reference,order_number,"This order was already added"));
                }else {
                    int if_paid = 0;
                    String payment = driverOrder.payment_method;
                    if (payment != null && !payment.contains("Cash")) {
                        if_paid = 1;
                    }
                    Date original_time = new Date(0);
                    DriverOrders driverOrders = new DriverOrders(
                            id_driver,
                            driver_name,
                            id_order,
                            order_reference,
                            1,
                            date_add,
                            order_number,
                            cashier_id,
                            if_paid,
                            0,
                            0,
                            original_time,
                            original_time

                    );
                    driverOrders.save();
                    changeOrderStatus(id_order,4);
                }
            }
            return ok(renderCategory(error_orders));
        }
    }

    public static void changeOrderStatus(int order_id, int status_number){
        Order this_order = Order.findById(order_id);
        this_order.current_state = status_number;
        this_order.save();
    }

    public static Result deleteOrdersFromDriver(int id_order){
        DriverOrders driverOrder = DriverOrders.findByOrderId(id_order);
        if(driverOrder == null){
            return notFound("No such record yet");
        }else{
            DriverOrders.delete(id_order);
            return ok("Record deleted");
        }
    }

    public static Result cancelOneOrder(int id_order, int id_cashier){
        Order old_order  = Order.findById(id_order);
        if(old_order == null){
            return notFound("No such record yet"); //404
        }else if(old_order.current_state ==6){
            return ok("Already Cancelled before"); //200
        }else{
            if(id_cashier == 149){
                id_cashier = 7;
            }else{
                id_cashier = 0;
            }
            old_order.current_state = 6;  //order cancelled state
            old_order.valid = 0;
            old_order.save();
            OrderHistory orderHistory = new OrderHistory(
                    id_cashier,
                    id_order,
                    6,
                    new Date()
            );
            orderHistory.save();

            List<OrderCartRule> if_has_voucher = OrderCartRule.findByOrder(id_order);
            if(if_has_voucher.size()>0){
                for(OrderCartRule this_record: if_has_voucher){
                 int id_cart_rule = this_record.id_cart_rule;
                    CartRule this_rule = CartRule.findById(id_cart_rule);
                    if(null !=this_rule){
                        this_rule.quantity = this_rule.quantity + 1;   //返回数量
                        this_rule.save();
                    }
                    this_record.delete();  //删除使用记录
                }
            }

            //ps_order_detail
            List<OrderDetail> all_products = OrderDetail.findByOrder(id_order);
            if(all_products.size()>0){
                for(OrderDetail this_product: all_products){
                    int id_product = this_product.product_id; //这件产品的id
                    int quantity = this_product.product_quantity; //这件产品的被购买数量
                    //ps_stock_available
                    StockAvailable this_quantity = StockAvailable.findByAttribute(id_product,0);//得到现在这件产品的数量
                    this_quantity.quantity = this_quantity.quantity + quantity;
                    this_quantity.save();  //数量恢复
                }
            }

            return ok("Order Cancelled Successfully"); //200
        }
    }


    /**
     * Cashier creates a new record when the driver delivered an order and paid back the order sales(delivery fee exclusive)
     * The record consists of cashier_id, order_id as well as driver_id
     * @param id_order
     * */
    public static Result driverBackPay(int id_order) throws Exception {
        DynamicForm driverPay = Form.form().bindFromRequest();
//        int order_number = Integer.parseInt(driverPay.get("order_number"));
        int id_cashier = Integer.parseInt(driverPay.get("id_cashier")); //new
        DriverOrders newOrder = DriverOrders.findByOrderId(id_order);
        if (newOrder == null){
            return status(409,"Please allocate a driver first");
        }else{

            if(newOrder.if_paid ==1){
                return status(404,"Already paid to cashier");
            }else {
                newOrder.if_paid = 1;
                newOrder.paid_cashier = id_cashier;
                newOrder.save();
                return ok("successfully paid to cashier");
            }
        }
    }

    /**
     * All delivered orders of a driver
     * @param id_driver
     * */
    public static Result orderHistories(int id_driver) throws Exception {
        Driver driver = Driver.findById(id_driver);
        if (driver == null){
          return status(409,"Not a valid driver");
        }else{
            List<DriverOrders> all_history = DriverOrders.findByDriver(id_driver);
            return ok(renderCategories(all_history));
        }
    }

    /**
     * Generate the order sequence number for each one, for the convenience of
     * calculating order numbers and counting orders of a day.
     * */
    public static int getSequenceNumber(int id_order) {
    //SQL get the sequential number of each order among all the orders of a day
    String sql = "SELECT count(*) FROM ps_orders\n" +
                    " WHERE invoice_date >= cast((SELECT invoice_date FROM ps_orders\n" +
                    " WHERE id_order =" + id_order + ") AS date) \n" +
                    " AND invoice_date < (SELECT invoice_date FROM ps_orders\n" +
                    " WHERE  id_order =" + id_order+ ")";
    RawSql rawSql =
            RawSqlBuilder
                    .parse(sql)
                    .columnMapping("count(*)", "order_sequence")
                    .create();

    com.avaje.ebean.Query<OrderSequence> query = Ebean.find(OrderSequence.class);
    query.setRawSql(rawSql);
    OrderSequence sequence = query.findUnique();
        //order sequential number starts from 101.
    return sequence.order_sequence + 101;
}
    /**
     * Get a total summary of overall orders that a drivers did in one day.
     * The summary includes explicit quantity of online-payment orders and
     * cash-payment orders,as well as the total price amount of both payment
     * methods, and also total cash in the driver's hand after the whole day's work..
     * @param id_driver
     * @see TotalReturnDriverOrder
     * */
    public static ArrayList<TotalReturnDriverOrder> getOrdersOfDriver(int id_driver) {  //司机晚上回来付完钱才知道结果  if_paid= 1
        String sql =
                "select ps_drivers_orders.driver_name, ps_orders.payment, sum(total_paid_real) as total_amount_sale, sum(total_shipping) as total_shipping_fee, count(payment) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 1\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                        "\n" +
                        "group by payment\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .columnMapping("sum(total_paid_real)", "total_sale_online")      //total_paid_real        total_paid_real - total_shipping
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(payment)", "total_numberof_online_order")
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
//        query.setParameter
        List<TotalReturnDriverOrder> list = query.findList();
        int size = list.size();
        for (int i =0; i <size; i++){
            list.get(i).total_sale_online = list.get(i).total_sale_online.subtract(list.get(i).total_shipping_online);
        }
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders = new ArrayList<>();
        totalReturnDriverOrders.addAll(list);
        return totalReturnDriverOrders;
    }

    public static ArrayList<TotalReturnDriverOrder> getAllOrdersOfDriver(int id_driver) {  //在司机付钱之前就算出所有数据  MySQL 筛选条件删除if_paid= 1
        String sql =
                "select ps_drivers_orders.driver_name, ps_orders.payment, sum(total_paid_real) as total_amount_sale, sum(total_shipping) as total_shipping_fee, count(payment) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                        "\n" +
                        "group by payment\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .columnMapping("sum(total_paid_real)", "total_sale_online")      //total_paid_real        total_paid_real - total_shipping
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(payment)", "total_numberof_online_order")
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
//        query.setParameter
        List<TotalReturnDriverOrder> list = query.findList();
        int size = list.size();
        for (int i =0; i <size; i++){
            list.get(i).total_sale_online = list.get(i).total_sale_online.subtract(list.get(i).total_shipping_online);
        }
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders = new ArrayList<>();
        totalReturnDriverOrders.addAll(list);
        return totalReturnDriverOrders;
    }


    public static ArrayList<TotalReturnDriverOrder> getExceptionOrders(int id_driver) {  //算出网上使用 marina voucher 的订单数量
        String sql =
                "select ps_orders.payment, sum(total_shipping) as total_shipping_fee,  count(*) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_orders.total_discounts = 2.000000\n" +  //网站使用voucher，数据库
                        "\n" +
                        "and ps_orders.total_shipping = 2.000000\n" +   //网站付钱
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 1\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                        "\n" +
                        "group by payment\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_orders.payment", "payment_method")
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(*)", "marina_orders")      //获得特殊情况数量
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
//        query.setParameter
        List<TotalReturnDriverOrder> list = query.findList();
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders = new ArrayList<>();
        if(list !=null){
        totalReturnDriverOrders.addAll(list);
        }
        return totalReturnDriverOrders;
    }

    /**
     * After cashier allocates orders to a driver, and even after the driver comes back and pays back some
     * order sales, calculate how much unpaid money the driver needs to pay back. (Only with regard to cash-payment
     * orders)
     * @param id_driver
     * @see TotalReturnDriverOrder
     * */
    private static TotalReturnDriverOrder getUnpaidOrders(int id_driver) {
        String sql =
                "select sum(total_paid_real) as total_amount_sale, sum(total_shipping) as total_shipping_fee, count(*) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 0\n" +
                        "\n" +
                        "and ps_drivers_orders.paid_cashier = 0\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                        "\n" +
                        "group by ps_orders.payment";   // 只有一种可能， 未付款的账单，是cash 支付 //偶尔会出现异常： 在线付款的情况下，状态是unpaid

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(total_paid_real)", "unpaid_amount")
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(*)", "unpaid_number")
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
        TotalReturnDriverOrder unpaid_orders = query.findUnique();

        String sqlException =
                "select sum(total_shipping) as total_shipping_fee\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_orders.total_discounts = 2.000000\n" +
                        "\n" +
                        "and ps_orders.total_shipping = 2.000000\n" +
                        "\n" +
                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 0\n" +
                        "\n" +
                        "and ps_drivers_orders.paid_cashier = 0\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                        "\n" +
                        "group by ps_orders.payment";  //防止数据库返回值为空
        RawSql rawSqlException =
                RawSqlBuilder
                        .parse(sqlException)
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .create();
        com.avaje.ebean.Query<TotalReturnDriverOrder> queryException = Ebean.find(TotalReturnDriverOrder.class);
        queryException.setRawSql(rawSqlException);
        TotalReturnDriverOrder unpaidException = queryException.findUnique();
        if(unpaidException == null){
            unpaidException = new TotalReturnDriverOrder();
            unpaidException.total_shipping_online = BigDecimal.valueOf(0);
        }else {
            if(unpaidException.total_shipping_online ==null){
                unpaidException.total_shipping_online = BigDecimal.valueOf(0);
            }
        }
        if(unpaid_orders != null){
        unpaid_orders.unpaid_amount = unpaid_orders.unpaid_amount.subtract(unpaid_orders.total_shipping_online).add(unpaidException.total_shipping_online);
        }else{
          unpaid_orders = new TotalReturnDriverOrder();
        }
        return unpaid_orders;
    }

    /**
     * Total delivery details and orders of a driver in one day
     * Summary about: online-payment quantity, cash-payment quantity,
     * online-payment total amount, cash-payment total amount,
     * and all the orders he delivered including
     * */
    public static Result totalOrdersOfDriver(int id_driver) {
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders;
        totalReturnDriverOrders = getOrdersOfDriver(id_driver);
        /**create two empty object for later use. One will be assigned the value as "Cash payment summary"
         *the other will be assigned the value of "Online payment summary"
         * */
        TotalReturnDriverOrder a; //cash
        TotalReturnDriverOrder b;  //online
        TotalReturnDriverOrder c;  //online
        TotalReturnDriverOrder exception_a = new TotalReturnDriverOrder();  //cash
        TotalReturnDriverOrder exception_b = new TotalReturnDriverOrder();   //online
        TotalReturnDriverOrder exception_c = new TotalReturnDriverOrder();   //online
        TotalReturnDriverOrder total = new TotalReturnDriverOrder();
        TotalReturnDriverOrder summary = getUnpaidOrders(id_driver);
        if (totalReturnDriverOrders != null){
            if (totalReturnDriverOrders.size() == 1 ){
                    a = totalReturnDriverOrders.get(0);
                if(a.payment_method.contains("Cash")){
                    total.total_numberof_cash_order = a.total_numberof_online_order;
                    total.total_sale_cash = a.total_sale_online;
                    total.total_shipping_cash = a.total_shipping_online;
                    total.driver_name = a.driver_name;
                }else{
                    total.total_numberof_online_order= a.total_numberof_online_order;
                    total.total_sale_online= a.total_sale_online;
                    total.total_shipping_online = a.total_shipping_online;
                    total.driver_name = a.driver_name;
                }
            }else if (totalReturnDriverOrders.size() == 2){
                if (totalReturnDriverOrders.get(0).payment_method.contains("Ingenico") && totalReturnDriverOrders.get(1).payment_method.contains("Online")){
                    b = totalReturnDriverOrders.get(0); //online
                    c = totalReturnDriverOrders.get(1); //online
                    total.total_numberof_online_order= b.total_numberof_online_order + c.total_numberof_online_order;
                    total.total_sale_online= b.total_sale_online.add(c.total_sale_online);
                    total.total_shipping_online = b.total_shipping_online.add(c.total_shipping_online);
                    total.driver_name = b.driver_name;
                }
                else{
                    a = totalReturnDriverOrders.get(1); //online
                    b = totalReturnDriverOrders.get(0); //cash
                    total.total_numberof_online_order = a.total_numberof_online_order;
                    total.total_sale_online = a.total_sale_online;
                    total.total_shipping_online = a.total_shipping_online;
                    total.total_numberof_cash_order = b.total_numberof_online_order;
                    total.total_sale_cash = b.total_sale_online;
                    total.total_shipping_cash = b.total_shipping_online;
                    total.driver_name = a.driver_name;
                }

            }else if (totalReturnDriverOrders.size() == 3){
                a = totalReturnDriverOrders.get(0);  //代表 cash
                b = totalReturnDriverOrders.get(1);  //代表 online
                c = totalReturnDriverOrders.get(2);  //代表 online

                total.total_numberof_cash_order = a.total_numberof_online_order;
                total.total_sale_cash = a.total_sale_online;
                total.total_shipping_cash = a.total_shipping_online;

                total.total_numberof_online_order = b.total_numberof_online_order + c.total_numberof_online_order;
                total.total_sale_online = b.total_sale_online.add(c.total_sale_online);
                total.total_shipping_online = b.total_shipping_online.add(c.total_shipping_online);
                total.driver_name = a.driver_name;
            }
            if (summary != null){
                total.unpaid_amount = summary.unpaid_amount;
                total.unpaid_number = summary.unpaid_number;
            }
            else {
                total.unpaid_amount = BigDecimal.valueOf(0);
                total.unpaid_number = 0;
            }
            int marinaExceptionOrders = 0;
            int marinaNormalOrders = 0;
            ArrayList<TotalReturnDriverOrder> totalExceptionOrders;
            totalExceptionOrders = getExceptionOrders(id_driver);
            if(totalExceptionOrders.size() > 0 ){
                if (totalExceptionOrders.size() ==1){
                    if(totalExceptionOrders.get(0).payment_method.contains("Cash")){
                        exception_a = totalExceptionOrders.get(0);
                    }else {
                        exception_b = totalExceptionOrders.get(0);
                    }
                }else if (totalExceptionOrders.size() ==2){
                    if(totalExceptionOrders.get(0).payment_method.contains("Cash")){
                        exception_a = totalExceptionOrders.get(0);
                        exception_b = totalExceptionOrders.get(1);
                    }else if(totalExceptionOrders.get(0).payment_method.contains("Ingenico") && totalExceptionOrders.get(1).payment_method.contains("Online")){
                        exception_b = totalExceptionOrders.get(0);
                        exception_c = totalExceptionOrders.get(1);
                    }else{
                        exception_a = totalExceptionOrders.get(1);
                        exception_b = totalExceptionOrders.get(0);
                    }
                }else if (totalExceptionOrders.size() ==3){
                        exception_a = totalExceptionOrders.get(0);
                        exception_b = totalExceptionOrders.get(1);
                        exception_c = totalExceptionOrders.get(2);
                }

                total.total_sale_cash = total.total_sale_cash.add(exception_a.total_shipping_online);
                total.total_sale_online = total.total_sale_online.add(exception_b.total_shipping_online).add(exception_c.total_shipping_online);
                total.total_shipping_cash = total.total_shipping_cash.subtract(exception_a.total_shipping_online);
                total.total_shipping_online = total.total_shipping_online.subtract(exception_b.total_shipping_online).subtract(exception_c.total_shipping_online);
                marinaExceptionOrders = exception_a.marina_orders + exception_b.marina_orders + exception_c.marina_orders;  //网站 使用 marina voucher
            }

            /**
             * Query all the orders delivered by this certain driver, return a list of orders
             * @see DriverOrders
             * */
            String sql =
                    "select ps_drivers_orders.id_order, ps_drivers_orders.driver_name, ps_drivers_orders.order_reference, ps_drivers_orders.order_number, ps_drivers_orders.if_paid, ps_orders.id_address_delivery, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_order_invoice.delivery_address\n" +
                            "\n" +
                            "from ps_orders, ps_drivers_orders, ps_order_invoice\n" +
                            "\n" +
                            "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                            "\n" +
                            "and ps_orders.id_order = ps_order_invoice.id_order\n" +
                            "\n" +
                            "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                            "\n" +
                            "and ps_orders.valid = 1\n" +  //valid orders only
                            "\n" +
                            "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                            "\n" +
                            "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                            "\n" +
                            "order by id_order";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_drivers_orders.id_order", "id_order")
                            .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                            .columnMapping("ps_drivers_orders.order_reference", "order_reference")
                            .columnMapping("ps_drivers_orders.order_number", "order_number")
                            .columnMapping("ps_drivers_orders.if_paid", "if_paid")
                            .columnMapping("ps_orders.id_address_delivery", "id_address_delivery")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.total_paid_real", "total_paid_real")  // calculate only the products prices
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_order_invoice.delivery_address", "delivery_address")
                            .create();
            com.avaje.ebean.Query<ReturnDriverOrder> query = Ebean.find(ReturnDriverOrder.class);
            query.setRawSql(rawSql);
            List<ReturnDriverOrder> list = query.findList();
            int size = list.size();
            for (int i = 0; i<size; i++){
                BigDecimal discount_amount = list.get(i).total_discounts;
                BigDecimal shipping_amount = list.get(i).total_shipping;

                if (shipping_amount.doubleValue() == 0.0){
                    marinaNormalOrders +=1;
                }

                if(discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0 ){ //特殊订单
                list.get(i).total_paid_real = list.get(i).total_paid_real;
                list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                }else{
                list.get(i).total_paid_real = list.get(i).total_paid_real.subtract(list.get(i).total_shipping);
                }
            }
            ArrayList<ReturnDriverOrder> totalOrders = new ArrayList<>();
            totalOrders.addAll(list);
            total.driverOrders = totalOrders;
            total.marina_orders = marinaNormalOrders + marinaExceptionOrders;  //总共Marina 订单
            return ok(renderCategories(total));
        } else {
            return notFound("No such driver");
        }
    }

    //单个司机的所有情况， 显示司机个人结算界面
    public static Result driverDetailsPage(int id_driver) {
        Driver this_driver = Driver.findById(id_driver);
        long time_diff; //工作时间
        String working_time = "";
        if(id_driver != 8 ) {
            if (this_driver.app_last_logout == null || this_driver.last_login == null) { //手机没有使用App
                time_diff = this_driver.last_logout.getTime() - this_driver.last_login.getTime();   //上班，从前台pad登陆， 下班，前台app登出
            } else {
                time_diff = this_driver.app_last_logout.getTime() - this_driver.last_login.getTime();   //上班，从前台pad登陆， 下班，司机app登出
            }
        }else { //free driver
            time_diff = this_driver.last_logout.getTime() - this_driver.last_login.getTime();   //上班，从前台pad登陆， 下班，前台app登出
            this_driver.app_last_logout = this_driver.last_logout;  //如果是free delivery, 把下班时间替换，显示在前台
        }
        if(time_diff < 0){
            working_time = "Still Working";
        }else {  //计算工作时间
            long working_hours = time_diff / (60 * 60 * 1000);
            long rest_time = time_diff%(60 * 60 * 1000);
            if((25 * 60 * 1000) < rest_time && rest_time < (45 * 60 * 1000)){  //加班大于25分钟 并且小于45分钟, 补贴半小时工资
                Double workingHours = Double.valueOf(working_hours);
                workingHours +=0.5;
                working_time = String.valueOf(workingHours);
            }else if(rest_time > (45 * 60 * 1000)){  //加班大于45分钟，补贴1小时工资
                working_hours += 1;
                working_time = String.valueOf(working_hours);
            }else {
                working_time = String.valueOf(working_hours);
            }
        }
        Date current_time = new Date();
        SimpleDateFormat matter1 = new SimpleDateFormat("yyyy-MM-dd");
        String date = matter1.format(current_time);
        this_driver.contact_number = working_time;  //把内容封装起来
        this_driver.password = date;
        Form<DriverDailyPayroll> driverDailyPayrollForm = Form.form(DriverDailyPayroll.class);
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders;
        totalReturnDriverOrders = getAllOrdersOfDriver(id_driver);
        /**create two empty object for later use. One will be assigned the value as "Cash payment summary"
         *the other will be assigned the value of "Online payment summary"
         * */
        TotalReturnDriverOrder a; //cash
        TotalReturnDriverOrder b;  //online
        TotalReturnDriverOrder c;  //online
        TotalReturnDriverOrder exception_a = new TotalReturnDriverOrder();  //cash
        TotalReturnDriverOrder exception_b = new TotalReturnDriverOrder();   //online
        TotalReturnDriverOrder exception_c = new TotalReturnDriverOrder();   //online
        TotalReturnDriverOrder total = new TotalReturnDriverOrder();
        TotalReturnDriverOrder summary = getUnpaidOrders(id_driver);
        if (totalReturnDriverOrders != null){
            if (totalReturnDriverOrders.size() == 1 ){
                a = totalReturnDriverOrders.get(0);
                if(a.payment_method.contains("Cash")){
                    total.total_numberof_cash_order = a.total_numberof_online_order;
                    total.total_sale_cash = a.total_sale_online;
                    total.total_shipping_cash = a.total_shipping_online;
                    total.driver_name = a.driver_name;
                }else{
                    total.total_numberof_online_order= a.total_numberof_online_order;
                    total.total_sale_online= a.total_sale_online;
                    total.total_shipping_online = a.total_shipping_online;
                    total.driver_name = a.driver_name;
                }
            }else if (totalReturnDriverOrders.size() == 2){
                if (totalReturnDriverOrders.get(0).payment_method.contains("Ingenico") && totalReturnDriverOrders.get(1).payment_method.contains("Online")){
                    b = totalReturnDriverOrders.get(0); //online
                    c = totalReturnDriverOrders.get(1); //online
                    total.total_numberof_online_order= b.total_numberof_online_order + c.total_numberof_online_order;
                    total.total_sale_online= b.total_sale_online.add(c.total_sale_online);
                    total.total_shipping_online = b.total_shipping_online.add(c.total_shipping_online);
                    total.driver_name = b.driver_name;
                }
                else{
                    a = totalReturnDriverOrders.get(1); //online
                    b = totalReturnDriverOrders.get(0); //cash
                    total.total_numberof_online_order = a.total_numberof_online_order;
                    total.total_sale_online = a.total_sale_online;
                    total.total_shipping_online = a.total_shipping_online;
                    total.total_numberof_cash_order = b.total_numberof_online_order;
                    total.total_sale_cash = b.total_sale_online;
                    total.total_shipping_cash = b.total_shipping_online;
                    total.driver_name = a.driver_name;
                }
            }else if (totalReturnDriverOrders.size() == 3){
                a = totalReturnDriverOrders.get(0);  //代表 cash
                b = totalReturnDriverOrders.get(1);  //代表 online
                c = totalReturnDriverOrders.get(2);  //代表 online

                total.total_numberof_cash_order = a.total_numberof_online_order;
                total.total_sale_cash = a.total_sale_online;
                total.total_shipping_cash = a.total_shipping_online;

                total.total_numberof_online_order = b.total_numberof_online_order + c.total_numberof_online_order;
                total.total_sale_online = b.total_sale_online.add(c.total_sale_online);
                total.total_shipping_online = b.total_shipping_online.add(c.total_shipping_online);
                total.driver_name = a.driver_name;
            }
            if (summary != null){
                total.unpaid_amount = summary.unpaid_amount;
                total.unpaid_number = summary.unpaid_number;
            }
            else {
                total.unpaid_amount = BigDecimal.valueOf(0);
                total.unpaid_number = 0;
            }
            int marinaExceptionOrders = 0;
            int marinaNormalOrders = 0;
            ArrayList<TotalReturnDriverOrder> totalExceptionOrders;
            totalExceptionOrders = getExceptionOrders(id_driver);
            if(totalExceptionOrders.size() > 0 ){
                if (totalExceptionOrders.size() ==1){
                    if(totalExceptionOrders.get(0).payment_method.contains("Cash")){
                        exception_a = totalExceptionOrders.get(0);
                    }else {
                        exception_b = totalExceptionOrders.get(0);
                    }
                }else if (totalExceptionOrders.size() ==2){
                    if(totalExceptionOrders.get(0).payment_method.contains("Cash")){
                        exception_a = totalExceptionOrders.get(0);
                        exception_b = totalExceptionOrders.get(1);
                    }else if(totalExceptionOrders.get(0).payment_method.contains("Ingenico") && totalExceptionOrders.get(1).payment_method.contains("Online")){
                        exception_b = totalExceptionOrders.get(0);
                        exception_c = totalExceptionOrders.get(1);
                    }else{
                        exception_a = totalExceptionOrders.get(1);
                        exception_b = totalExceptionOrders.get(0);
                    }
                }else if (totalExceptionOrders.size() ==3){
                    exception_a = totalExceptionOrders.get(0);
                    exception_b = totalExceptionOrders.get(1);
                    exception_c = totalExceptionOrders.get(2);
                }
                total.total_sale_cash = total.total_sale_cash.add(exception_a.total_shipping_online);
                total.total_sale_online = total.total_sale_online.add(exception_b.total_shipping_online).add(exception_c.total_shipping_online);
                total.total_shipping_cash = total.total_shipping_cash.subtract(exception_a.total_shipping_online);
                total.total_shipping_online = total.total_shipping_online.subtract(exception_b.total_shipping_online).subtract(exception_c.total_shipping_online);
                marinaExceptionOrders = exception_a.marina_orders + exception_b.marina_orders + exception_c.marina_orders;  //网站 使用 marina voucher
            }
            /**
             * Query all the orders delivered by this certain driver, return a list of orders
             * @see DriverOrders
             * */
            String sql =
                    "select ps_drivers_orders.id_order, ps_drivers_orders.driver_name, ps_drivers_orders.order_reference, ps_drivers_orders.order_number, ps_drivers_orders.if_paid, ps_orders.id_address_delivery, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_order_invoice.delivery_address\n" +
                            "\n" +
                            "from ps_orders, ps_drivers_orders, ps_order_invoice\n" +
                            "\n" +
                            "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                            "\n" +
                            "and ps_orders.id_order = ps_order_invoice.id_order\n" +
                            "\n" +
                            "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                            "\n" +
                            "and ps_orders.valid = 1\n" +  //valid orders only
                            "\n" +
                            "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                            "\n" +
                            "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                            "\n" +
                            "order by id_order";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_drivers_orders.id_order", "id_order")
                            .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                            .columnMapping("ps_drivers_orders.order_reference", "order_reference")
                            .columnMapping("ps_drivers_orders.order_number", "order_number")
                            .columnMapping("ps_drivers_orders.if_paid", "if_paid")
                            .columnMapping("ps_orders.id_address_delivery", "id_address_delivery")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.total_paid_real", "total_paid_real")  // calculate only the products prices
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_order_invoice.delivery_address", "delivery_address")
                            .create();
            com.avaje.ebean.Query<ReturnDriverOrder> query = Ebean.find(ReturnDriverOrder.class);
            query.setRawSql(rawSql);
            List<ReturnDriverOrder> list = query.findList();
            int size = list.size();
            for (int i = 0; i<size; i++){
                BigDecimal discount_amount = list.get(i).total_discounts;
                BigDecimal shipping_amount = list.get(i).total_shipping;
                if (shipping_amount.doubleValue() == 0.0){
                    marinaNormalOrders +=1;
                }
                if(discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0 ){ //特殊订单： 运费2， 打折2， 实际运费0
                    list.get(i).total_paid_real = list.get(i).total_paid_real;
                    list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                }else{
                    list.get(i).total_paid_real = list.get(i).total_paid_real.subtract(list.get(i).total_shipping);
                }
            }
            ArrayList<ReturnDriverOrder> totalOrders = new ArrayList<>();
            totalOrders.addAll(list);
            total.driverOrders = totalOrders; //不用 赋值
            total.marina_orders = marinaNormalOrders + marinaExceptionOrders;  //总共Marina 订单
            return ok(driverPersonalPage.render(driverDailyPayrollForm,total,this_driver));
        } else {
            return notFound("No such driver");
        }
    }

    //单个司机的所有情况， 打印司机的个人信息 （司机每天工资单）
    public static Result driverPrintPage(int id_driver) {
        DynamicForm driverForm = Form.form().bindFromRequest();
//        Form<DriverDailyPayroll> driverForm = Form.form(DriverDailyPayroll.class).bindFromRequest();
        if(driverForm != null){  //存在上一个界面传过来的值
        List<DriverOrders> unpaid_orders = DriverOrders.unpaidByDriver(id_driver);
        if(unpaid_orders.size() > 0){  //如果有unpaid 的订单，则不能结算，显示还有多少单没有付款
            return ok(driverUnpaidOrdersPage.render(unpaid_orders));
        }else {
            String till_money;
            String salary; //每小时的 工资
            String extra_cost;
            String specification;
            String working_hours;
            String voucher_quantity;   //Voucher quantity
            String voucher_total_value;   //Voucher total value
            try {
                if (driverForm.get("till_money") != null) {
                    till_money = driverForm.get("till_money");
                } else {
                    till_money = "";
                }
                if (driverForm.get("salary") != null) {
                    salary = driverForm.get("salary");
                } else {
                    salary = "";
                }
                if (driverForm.get("extra_cost") != null) {
                    extra_cost = driverForm.get("extra_cost");
                } else {
                    extra_cost = "0";
                }
                if (driverForm.get("specification") != null) {
                    specification = driverForm.get("specification");
                } else {
                    specification = "";
                }
                if (driverForm.get("working_hours")!= null) {
                    working_hours = driverForm.get("working_hours");
                } else {
                    working_hours = "";
                }
                if (driverForm.get("voucher_quantity") != null) {
                    voucher_quantity = driverForm.get("voucher_quantity");
                } else {
                    voucher_quantity = "";
                }
                if (driverForm.get("voucher_total_value") != null) {
                    voucher_total_value = driverForm.get("voucher_total_value");
                } else {
                    voucher_total_value = "";
                }
            }catch (Exception e){
                return redirect(routes.DriverAPI.driverDetailsPage(id_driver));
            }

            if(till_money.replaceAll(" ","").equals("")|| salary.replaceAll(" ","").equals("")){   //必选条目没有填写 (Till money 和 时薪)
                return redirect(routes.DriverAPI.driverDetailsPage(id_driver));
            }else if(!extra_cost.replaceAll(" ","").equals("") && specification.replaceAll(" ","").equals("")){ //有额外支出却不说原因
                return redirect(routes.DriverAPI.driverDetailsPage(id_driver));
            }else if(!voucher_total_value.replaceAll(" ","").equals("") && voucher_quantity.replaceAll(" ","").equals("")){ //有voucher总价却不说数量
                return redirect(routes.DriverAPI.driverDetailsPage(id_driver));
            }else if(!voucher_quantity.replaceAll(" ","").equals("") && voucher_total_value.replaceAll(" ","").equals("")){ //或者有voucher数量没有说总价
                return redirect(routes.DriverAPI.driverDetailsPage(id_driver));
            }else {
                //当前司机
                Driver this_driver = Driver.findById(id_driver);
                String driver_name = this_driver.name;
                Date current_time = new Date();
                SimpleDateFormat matter1 = new SimpleDateFormat("yyyy-MM-dd");
                String date = matter1.format(current_time);
                this_driver.password = date;
                ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders;
                totalReturnDriverOrders = getAllOrdersOfDriver(id_driver);
                /**create two empty object for later use. One will be assigned the value as "Cash payment summary"
                 *the other will be assigned the value of "Online payment summary"
                 * */
                TotalReturnDriverOrder a; //cash
                TotalReturnDriverOrder b;  //online
                TotalReturnDriverOrder c;  //online
                TotalReturnDriverOrder exception_a = new TotalReturnDriverOrder();  //cash
                TotalReturnDriverOrder exception_b = new TotalReturnDriverOrder();   //online
                TotalReturnDriverOrder exception_c = new TotalReturnDriverOrder();   //online
                TotalReturnDriverOrder total = new TotalReturnDriverOrder();
                    if (totalReturnDriverOrders != null) {
                        if (totalReturnDriverOrders.size() == 1) {
                            a = totalReturnDriverOrders.get(0);
                            if (a.payment_method.contains("Cash")) {
                                total.total_numberof_cash_order = a.total_numberof_online_order;
                                total.total_sale_cash = a.total_sale_online;
                                total.total_shipping_cash = a.total_shipping_online;
                                total.driver_name = a.driver_name;
                            } else {
                                total.total_numberof_online_order = a.total_numberof_online_order;
                                total.total_sale_online = a.total_sale_online;
                                total.total_shipping_online = a.total_shipping_online;
                                total.driver_name = a.driver_name;
                            }
                        } else if (totalReturnDriverOrders.size() == 2) {
                            if (totalReturnDriverOrders.get(0).payment_method.contains("Ingenico") && totalReturnDriverOrders.get(1).payment_method.contains("Online")) {
                                b = totalReturnDriverOrders.get(0); //online
                                c = totalReturnDriverOrders.get(1); //online
                                total.total_numberof_online_order = b.total_numberof_online_order + c.total_numberof_online_order;
                                total.total_sale_online = b.total_sale_online.add(c.total_sale_online);
                                total.total_shipping_online = b.total_shipping_online.add(c.total_shipping_online);
                                total.driver_name = b.driver_name;
                            } else {
                                a = totalReturnDriverOrders.get(1); //online
                                b = totalReturnDriverOrders.get(0); //cash
                                total.total_numberof_online_order = a.total_numberof_online_order;
                                total.total_sale_online = a.total_sale_online;
                                total.total_shipping_online = a.total_shipping_online;
                                total.total_numberof_cash_order = b.total_numberof_online_order;
                                total.total_sale_cash = b.total_sale_online;
                                total.total_shipping_cash = b.total_shipping_online;
                                total.driver_name = a.driver_name;
                            }

                        } else if (totalReturnDriverOrders.size() == 3) {
                            a = totalReturnDriverOrders.get(0);  //代表 cash
                            b = totalReturnDriverOrders.get(1);  //代表 online
                            c = totalReturnDriverOrders.get(2);  //代表 online
                            total.total_numberof_cash_order = a.total_numberof_online_order;
                            total.total_sale_cash = a.total_sale_online;
                            total.total_shipping_cash = a.total_shipping_online;
                            total.total_numberof_online_order = b.total_numberof_online_order + c.total_numberof_online_order;
                            total.total_sale_online = b.total_sale_online.add(c.total_sale_online);
                            total.total_shipping_online = b.total_shipping_online.add(c.total_shipping_online);
                            total.driver_name = a.driver_name;
                        }
                        int marinaExceptionOrders = 0;
                        int marinaNormalOrders = 0;
                        ArrayList<TotalReturnDriverOrder> totalExceptionOrders;
                        totalExceptionOrders = getExceptionOrders(id_driver);
                        if (totalExceptionOrders.size() > 0) {
                            if (totalExceptionOrders.size() == 1) {
                                if (totalExceptionOrders.get(0).payment_method.contains("Cash")) {
                                    exception_a = totalExceptionOrders.get(0);
                                } else {
                                    exception_b = totalExceptionOrders.get(0);
                                }
                            } else if (totalExceptionOrders.size() == 2) {
                                if (totalExceptionOrders.get(0).payment_method.contains("Cash")) {
                                    exception_a = totalExceptionOrders.get(0);
                                    exception_b = totalExceptionOrders.get(1);
                                } else if (totalExceptionOrders.get(0).payment_method.contains("Ingenico") && totalExceptionOrders.get(1).payment_method.contains("Online")) {
                                    exception_b = totalExceptionOrders.get(0);
                                    exception_c = totalExceptionOrders.get(1);
                                } else {
                                    exception_a = totalExceptionOrders.get(1);
                                    exception_b = totalExceptionOrders.get(0);
                                }
                            } else if (totalExceptionOrders.size() == 3) {
                                exception_a = totalExceptionOrders.get(0);
                                exception_b = totalExceptionOrders.get(1);
                                exception_c = totalExceptionOrders.get(2);
                            }
                            total.total_sale_cash = total.total_sale_cash.add(exception_a.total_shipping_online);
                            total.total_sale_online = total.total_sale_online.add(exception_b.total_shipping_online).add(exception_c.total_shipping_online);
                            total.total_shipping_cash = total.total_shipping_cash.subtract(exception_a.total_shipping_online);
                            total.total_shipping_online = total.total_shipping_online.subtract(exception_b.total_shipping_online).subtract(exception_c.total_shipping_online);
                            marinaExceptionOrders = exception_a.marina_orders + exception_b.marina_orders + exception_c.marina_orders;  //网站 使用 marina voucher
                        }

                        /**
                         * Query all the orders delivered by this certain driver, return a list of orders
                         * @see DriverOrders
                         * */
                        String sql =
                                "select ps_drivers_orders.id_order, ps_drivers_orders.driver_name, ps_drivers_orders.order_reference, ps_drivers_orders.order_number, ps_drivers_orders.if_paid, ps_orders.id_address_delivery, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_order_invoice.delivery_address\n" +
                                        "\n" +
                                        "from ps_orders, ps_drivers_orders, ps_order_invoice\n" +
                                        "\n" +
                                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                                        "\n" +
                                        "and ps_orders.id_order = ps_order_invoice.id_order\n" +
                                        "\n" +
                                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                                        "\n" +
                                        "and ps_orders.valid = 1\n" +  //valid orders only
                                        "\n" +
                                        "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                                        "\n" +
                                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                                        "\n" +
                                        "order by id_order";
                        RawSql rawSql =
                                RawSqlBuilder
                                        .parse(sql)
                                        .columnMapping("ps_drivers_orders.id_order", "id_order")
                                        .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                                        .columnMapping("ps_drivers_orders.order_reference", "order_reference")
                                        .columnMapping("ps_drivers_orders.order_number", "order_number")
                                        .columnMapping("ps_drivers_orders.if_paid", "if_paid")
                                        .columnMapping("ps_orders.id_address_delivery", "id_address_delivery")
                                        .columnMapping("ps_orders.payment", "payment")
                                        .columnMapping("ps_orders.total_paid_real", "total_paid_real")  // calculate only the products prices
                                        .columnMapping("ps_orders.total_discounts", "total_discounts")
                                        .columnMapping("ps_orders.total_shipping", "total_shipping")
                                        .columnMapping("ps_order_invoice.delivery_address", "delivery_address")
                                        .create();
                        com.avaje.ebean.Query<ReturnDriverOrder> query = Ebean.find(ReturnDriverOrder.class);
                        query.setRawSql(rawSql);
                        List<ReturnDriverOrder> list = query.findList();
                        int size = list.size();
                        for (int i = 0; i < size; i++) {
                            BigDecimal discount_amount = list.get(i).total_discounts;
                            BigDecimal shipping_amount = list.get(i).total_shipping;
                            if (shipping_amount.doubleValue() == 0.0) {
                                marinaNormalOrders += 1;
                            }
                            if (discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0) { //特殊订单
                                list.get(i).total_paid_real = list.get(i).total_paid_real;
                                list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                            } else {
                                list.get(i).total_paid_real = list.get(i).total_paid_real.subtract(list.get(i).total_shipping);
                            }
                        }
                        ArrayList<ReturnDriverOrder> totalOrders = new ArrayList<>();
                        totalOrders.addAll(list);
                        total.driverOrders = totalOrders; //不用 赋值
                        total.marina_orders = marinaNormalOrders + marinaExceptionOrders;  //总共Marina 订单
                        DriverDailySummary driver_summary;
                        try{
                        if(extra_cost.replaceAll(" ","").equals("")){
                            extra_cost = "0";
                        }
                        if(voucher_total_value.replaceAll(" ","").equals("")){
                            voucher_total_value = "0";
                        }
                        if(voucher_quantity.replaceAll(" ","").equals("")){
                            voucher_quantity = "0";
                        }
                        //进行类型转换和工资计算
                        int voucher_number = Integer.valueOf(voucher_quantity); //voucher 数量
                        Double voucher_value = Double.parseDouble(voucher_total_value);
                        BigDecimal total_voucher_value = BigDecimal.valueOf(voucher_value).setScale(2, BigDecimal.ROUND_DOWN);  // Voucher 代金券总价值
                        Double money_till = Double.parseDouble(till_money);
                        BigDecimal money_from_till = BigDecimal.valueOf(money_till).setScale(2, BigDecimal.ROUND_DOWN);  //从 Till 里面的总钱数
                        Double hour_rate =  Double.parseDouble(salary);  //一个小时的工资
                        BigDecimal hourly_wage = BigDecimal.valueOf(hour_rate).setScale(2, BigDecimal.ROUND_DOWN);  //时薪
                        Double working_hour = Double.parseDouble(working_hours); //工作时间
                        BigDecimal work_time = BigDecimal.valueOf(working_hour).setScale(1);  //当日工作时间
                        Double total_salary = hour_rate*working_hour;  //总收入 = 时薪 * 工作时间
                        BigDecimal totalSalary = BigDecimal.valueOf(total_salary).setScale(2, BigDecimal.ROUND_DOWN);  //当日总工资，取两位小数
                        Double extra_money = Double.parseDouble(extra_cost);
                        BigDecimal extra = BigDecimal.valueOf(extra_money).setScale(2, BigDecimal.ROUND_DOWN); //额外给司机的补贴， 如果有的话
                        BigDecimal marina_delivery = BigDecimal.valueOf(total.marina_orders);//Marina 运费补贴， 算进收入里面去
                        //更新计算钱的方式： 1， 总归还   2. 总收入
                            BigDecimal money_to_return = BigDecimal.valueOf(0);
                            BigDecimal total_income = BigDecimal.valueOf(0);
                            //根据每天的开车情况进行计算
                        //如果是开公司的车， 每小时9.5， 其余运费全部退还给公司（cash 订单运费），
                            //1：自己的车； 2： 公司的车, 0 是 unknown
                        if(this_driver.if_company_car == 1){ //开自己的车
                            //需要归还的钱： 订单总现金 + Till 总现金 - 在线付款的订单运费补贴 - Voucher 代金券总价值
                            money_to_return = total.total_sale_cash.add(money_from_till).subtract(total.total_shipping_online).subtract(total_voucher_value);
                            //司机当日的总收入：总工资 + Marina运费补贴 + 额外补贴（如果有）
                            total_income = totalSalary.add(marina_delivery).add(extra);
                        }else { // 开公司的车
                            //需要归还的钱： 订单总现金 + Till 总现金 + cash订单的所有运费 - Voucher 代金券总价值
                            money_to_return = total.total_sale_cash.add(money_from_till).add(total.total_shipping_cash).subtract(total_voucher_value);
                            //司机当日的总收入：总工资 +  额外补贴（如果有）
                            total_income = totalSalary.add(extra);
                        }
//                        // 司机每天最后手里剩下的钱：订单总现金 + Till现金 - 总工资 - Marina 运费补贴 - 在线付款的运费补贴 - 额外补贴（如果有） - Voucher 代金券总价值
//                        BigDecimal total_money_left = total.total_sale_cash.add(money_from_till).subtract(totalSalary).subtract(marina_delivery).subtract(total.total_shipping_online).subtract(extra).subtract(total_voucher_value);
                        driver_summary = DriverDailySummary.recentRecord(id_driver); //找到是否有结算记录 （可以用新记录替换老记录）
                                if(driver_summary!=null){
                                    driver_summary.update(  //更新
                                            work_time,
                                            hourly_wage,
                                            totalSalary,
                                            money_from_till,
                                            total.total_sale_cash,
                                            extra,
                                            specification,
                                            money_to_return,
                                            current_time,
                                            voucher_number,
                                            total_voucher_value,
                                            marina_delivery,
                                            total.total_numberof_cash_order,
                                            total.total_numberof_online_order,
                                            this_driver.if_company_car,
                                            total.total_shipping_cash,
                                            total.total_shipping_online
                                    );
                                    driver_summary.save();
                                }else {
                                    driver_summary = new DriverDailySummary(
                                            id_driver,
                                            driver_name,
                                            work_time, //工作时间
                                            hourly_wage, //小时工资
                                            totalSalary, //总钱数   = 工作时间 * 每小时工资
                                            money_from_till, //till 拿走的钱
                                            total.total_sale_cash,
                                            extra,
                                            specification,
                                            money_to_return,  //司机手里需要归还的钱
                                            current_time,
                                            voucher_number,   //多少个现金券
                                            total_voucher_value, //现金券总价值
                                            marina_delivery,
                                            total.total_numberof_cash_order,
                                            total.total_numberof_online_order,
                                            this_driver.if_company_car,
                                            total.total_shipping_cash,
                                            total.total_shipping_online
                                    );
                                    driver_summary.save();
                                }
                            //total 可以作为一个载体， 存储司机当天的工资所得
                            total.unpaid_amount = total_income;
                        }catch (Exception e){
                            return redirect(routes.DriverAPI.driverDetailsPage(id_driver));
                        }
                        return ok(driverPrintDailySummary.render(total, this_driver,driver_summary));
                    } else {
                        return notFound("No such driver");
                    }
                }
            }
        }else {
                        return redirect(routes.DriverAPI.driverDetailsPage(id_driver));
        }
    }

    public static Result undeliveredOrders(int id_driver) {   //司机没有送的订单
            /**
             * Query all the orders delivered by this certain driver, return a list of orders
             * @see DriverOrders
             * */
            String sql =
                    "select ps_drivers_orders.id_order, ps_drivers_orders.driver_name, ps_drivers_orders.order_reference, ps_drivers_orders.order_number, ps_drivers_orders.if_paid, ps_drivers_orders.start_time, ps_orders.id_address_delivery, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_order_invoice.delivery_address\n" +
                            "\n" +
                            "from ps_orders, ps_drivers_orders, ps_order_invoice\n" +
                            "\n" +
                            "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                            "\n" +
                            "and ps_orders.id_order = ps_order_invoice.id_order\n" +
                            "\n" +
                            "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                            "\n" +
                            "and ps_orders.valid = 1\n" +  //valid orders only
                            "\n" +
                            "and ps_drivers_orders.id_driver =" + id_driver + "\n" +
                            "\n" +
                            "and ps_drivers_orders.if_delivered =0\n" +
                            "\n" +
                            "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                            "\n" +
                            "order by id_order";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_drivers_orders.id_order", "id_order")
                            .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                            .columnMapping("ps_drivers_orders.order_reference", "order_reference")
                            .columnMapping("ps_drivers_orders.order_number", "order_number")
                            .columnMapping("ps_drivers_orders.if_paid", "if_paid")
                            .columnMapping("ps_drivers_orders.start_time", "start_time")
                            .columnMapping("ps_orders.id_address_delivery", "id_address_delivery")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.total_paid_real", "total_paid_real")  // calculate only the products prices
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_order_invoice.delivery_address", "delivery_address")
                            .create();
            com.avaje.ebean.Query<ReturnDriverOrder> query = Ebean.find(ReturnDriverOrder.class);
            query.setRawSql(rawSql);
            List<ReturnDriverOrder> list = query.findList();
            int size = list.size();
           if(size > 0 ) {
               for (int i = 0; i < size; i++) {
                   BigDecimal discount_amount = list.get(i).total_discounts;
                   BigDecimal shipping_amount = list.get(i).total_shipping;
                   if (discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0) { //特殊订单
                       list.get(i).total_paid_real = list.get(i).total_paid_real;
                       list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                   } else {
                       list.get(i).total_paid_real = list.get(i).total_paid_real.subtract(list.get(i).total_shipping);
                   }
                   int id_address_delivery = list.get(i).id_address_delivery;
                   AddressTemplate delivery_address = new AddressTemplate();                           //Define an object using AddressTemplate for later return
                   if (id_address_delivery != 0) {                                      //If there is an address
                       Address address = Address.findById(id_address_delivery);         //Find the address in the table of Address
                       String alias = address.alias;
                       String company = address.company;
                       String firstname = address.firstname;
                       String lastname = address.lastname;
                       String address1 = address.address1;
                       String address2 = address.address2;
                       String city = address.city;
                       String phone = address.phone;
                       delivery_address = new AddressTemplate(
                               alias,
                               company,
                               firstname,
                               lastname,
                               address1,
                               address2,
                               city,
                               phone
                       );
                   }
                   list.get(i).customer_address = delivery_address;
               }
           }
            ArrayList<ReturnDriverOrder> totalOrders = new ArrayList<>();
            totalOrders.addAll(list);
            return ok(renderCategories(totalOrders));
    }


    public static Result addToDelivered(int id_driver,int id_order) {   //司机发送已经送达请求
        DriverOrders this_order = DriverOrders.findByBoth(id_driver, id_order);
        if (this_order == null){
            return badRequest("Invalid driver-order combination");
        }else{
            this_order.if_delivered = 1;
            this_order.delivered_time = new Date();
            this_order.save();
            changeOrderStatus(id_order,5);
            return ok("Order delivered successfully!");
        }
    }

    public static Result addStartTime(int id_driver,int id_order) {   //司机发送开始配送请求
        DriverOrders this_order = DriverOrders.findByBoth(id_driver, id_order);
        if (this_order == null) {
            return badRequest("Invalid driver-order combination");
        } else {
            this_order.start_time = new Date();
            this_order.save();
            return ok("Order started successfully!");
        }
    }

    /**
     * Cashier controls drivers to log in, log in with Driver_ID
     * Change the value of if_work in the Drivers Table from 0 to 1
     * */
    public static Result AppDriverLogin() throws Exception {
        DynamicForm driverForm = Form.form().bindFromRequest();
        String phone_number = driverForm.get("phone_number");
        if(phone_number == null){
            phone_number = "";
        }
        String password = driverForm.get("password");
        if(password == null){
            password = "";
        }
        Date login_time = new Date();
        Driver driver = Driver.findByPhone(phone_number);
        if (driver != null) {
            if(driver.password.equals(password)){
            driver.app_last_login = login_time;
            driver.save();
            return ok(renderCategory(driver.id_driver));
            }
            else {
                return badRequest("Wrong password");
            }
        }else {
            return notFound("Invalid driver");
        }
    }

    /**
     * Cashier controls drivers to log in, log in with Driver_ID
     * Change the value of if_work in the Drivers Table from 0 to 1
     * */
    public static Result AppDriverLogout(int id_driver) throws Exception {
        Date logout_time = new Date();
        Driver driver = Driver.findById(id_driver);
        if (driver != null) {
            driver.app_last_logout = logout_time;
            driver.save();
        return ok("Log out successfully");
        }else {
        return notFound("Invalid driver");
        }
    }

}
