package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Transactional;
import models.sqlContainer.*;
import models.template.DeliverySummary;
import models.template.ManagerOrdersMarinaNumber;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Discount.discountPage;
import views.html.Discount.discountSummary;
import views.html.delivery.deliveryPage;
import views.html.delivery.deliverySummary;
import views.html.errorMessageTemplate;
import views.html.orderProducts.orderProductsInformation;
import views.html.ordersInfo.*;
import views.html.products.productsPage;
import views.html.products.productsRanking;
import views.html.products.productsSummary;
import views.html.tax.taxPage;
import views.html.tax.taxSummary;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static controllers.ManagerAPI.managerTotalOrdersAPI;
//import static parsers.JsonParser.renderProduct;

public class StatisticsAPI extends Controller
{
    public static class PickDate{
    public String startDate;
    public String endDate;
    }

    public static Result taxPage()
    {   //登陆session 验证
        Form<PickDate> dateForm = Form.form(PickDate.class);
        return ok(taxPage.render(dateForm)); //ignore
    }

    public static Result searchTax()
    {  //登陆session验证
        DynamicForm dateForm = Form.form().bindFromRequest();  //receive
        String startDate = dateForm.get("startDate");
        String endDate = dateForm.get("endDate");
        if (startDate ==null || endDate ==null || startDate.equals("") || endDate.equals("") ){
            flash("error", "please enter Start and End Date");
            return redirect(routes.StatisticsAPI.taxPage());
        }
        else{
        String[]  endString = endDate.split("-");
        String day = endString[2];
        int realDay = Integer.valueOf(day) + 1;
        String real_endDate = endString[0]+"-"+endString[1]+"-"+ String.valueOf(realDay);
        System.out.println(startDate);
        System.out.println(endDate);
        ArrayList<TaxOrders> taxOrders =getTaxOrders(startDate,real_endDate);  // 详细订单
        ArrayList<TaxSummary> taxSummaries= getTaxSummary(startDate,real_endDate); //总价
        DiscountSummary product_vouchers = getProductVouchers(startDate,real_endDate);
            BigDecimal reduced_food_sales = product_vouchers.total_discount;
            BigDecimal total_tax_amount = BigDecimal.valueOf(0);
            for(TaxSummary taxSummary: taxSummaries){
                if(taxSummary.id_tax_rules_group == 3){ //9%
                    taxSummary.total_tax_amount = taxSummary.total_tax_amount.subtract(BigDecimal.valueOf(reduced_food_sales.doubleValue() * (9 * 1.0 / 109))).setScale(2,BigDecimal.ROUND_DOWN);
                }
                total_tax_amount = total_tax_amount.add(taxSummary.total_tax_amount);
            }
        product_vouchers.total_product_price = total_tax_amount;  //借用容器
        HashMap<String, String> submittedDate = new HashMap<String, String>();
        submittedDate.put("startDate",startDate);
        submittedDate.put("endDate",endDate);
        Form<PickDate> newForm = Form.form(PickDate.class).bind(submittedDate);
        return ok(taxSummary.render(newForm,taxOrders,taxSummaries,product_vouchers)); //ignore
        }
    }

    public static ArrayList<TaxOrders> getTaxOrders(String startDate, String endDate){
       String sql =
               "select ps_orders.id_order, ps_orders.total_discounts, ps_orders.total_products_wt, ps_orders.total_shipping, ps_orders.invoice_date, ps_order_detail.id_order_detail, ps_order_detail.product_id, ps_order_detail.product_quantity, ps_order_detail.id_tax_rules_group, ps_order_detail.total_price_tax_incl\n" +
                       "from ps_orders, ps_order_detail \n" +
                       "where ps_orders.id_order = ps_order_detail.id_order\n" +
                       "and ps_orders.valid = 1\n" +
                       "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                       "and ps_orders.invoice_date <" + "'" + endDate +"'" +"\n"+
                       "order by id_order";
       RawSql rawSql =
               RawSqlBuilder
                       .parse(sql)
                       .columnMapping("ps_orders.id_order", "id_order")
                       .columnMapping("ps_orders.total_discounts", "total_discounts")
                       .columnMapping("ps_orders.total_products_wt", "total_paid_real")  //需要减去products voucher
                       .columnMapping("ps_orders.total_shipping", "total_shipping")
                       .columnMapping("ps_orders.invoice_date", "invoice_date")
                       .columnMapping("ps_order_detail.id_order_detail", "id_order_detail")
                       .columnMapping("ps_order_detail.product_id", "product_id")
                       .columnMapping("ps_order_detail.product_quantity", "detail_product_quantity")
                       .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                       .columnMapping("ps_order_detail.total_price_tax_incl", "total_price_tax_incl")
                       .create();
       com.avaje.ebean.Query<TaxOrders> query = Ebean.find(TaxOrders.class);
       query.setRawSql(rawSql);
       List<TaxOrders> list = query.findList();
        ArrayList<TaxOrders> totalTaxOrders = new ArrayList<>();
        int size = list.size();
        if (size == 0){
            return totalTaxOrders;
        }else {
        int current_order_id = list.get(0).id_order;
        int current_order_product_quantity = 0;
        BigDecimal current_order_tax = BigDecimal.valueOf(0);
        for(int i =0; i<size;i++){      //进入每个产品循环
           BigDecimal this_product_tax = BigDecimal.valueOf(0);
           if(list.get(i).id_order != current_order_id){  //跳进下一个订单, 结算到此为止的上一单
               TaxOrders last_one = list.get(i-1);
               BigDecimal delivery_fee;
               BigDecimal product_voucher;
               if(last_one.total_discounts.doubleValue() ==2 && last_one.total_shipping.doubleValue() ==2){
                   delivery_fee = BigDecimal.valueOf(0);
               }else {
                   delivery_fee = last_one.total_shipping;
               }

               if(last_one.total_discounts.doubleValue() ==2){
                   product_voucher = BigDecimal.valueOf(0);   //只是运费
               }else {
                   product_voucher = last_one.total_discounts;
               }

               TaxOrders new_one = new TaxOrders(
                       last_one.id_order,
                       0,
                       0,
                       0,
                       0,
                       current_order_product_quantity,
                       BigDecimal.valueOf(0),
                       current_order_tax.subtract(BigDecimal.valueOf(product_voucher.doubleValue() * (9 * 1.0 / 109))),
                       product_voucher,
                       last_one.total_paid_real.subtract(product_voucher),
                       delivery_fee,
                       last_one.invoice_date
               );
               totalTaxOrders.add(new_one);
               current_order_id = list.get(i).id_order;
               current_order_product_quantity = 0;
               current_order_tax = BigDecimal.valueOf(0);
           }
            //判断税率，计算税
            Integer tax_id = list.get(i).id_tax_rules_group;
            switch (tax_id){
                case 1: this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (23 * 1.0 / 123));  //23%
                    break;
                case 2: this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (135 * 1.0 / 1135));//13.5%
                    break;
                case 3: this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (9 * 1.0 / 109));   //9%
                    break;
                case 4: this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (48 * 1.0 / 1048)); //4.8%
                    break;
                case 6: this_product_tax = BigDecimal.valueOf(0.000000);                                                              //0%
                    break;

            }
            current_order_product_quantity += list.get(i).detail_product_quantity;
            current_order_tax = current_order_tax.add(this_product_tax);
       }
        int last_index = size - 1;
        TaxOrders last_one = list.get(last_index);
        BigDecimal delivery_fee;
        BigDecimal product_voucher;
        if(last_one.total_discounts.doubleValue() ==2 && last_one.total_shipping.doubleValue() ==2){
            delivery_fee = BigDecimal.valueOf(0);
        }else {
            delivery_fee = last_one.total_shipping;
        }
        if(last_one.total_discounts.doubleValue() ==2){
            product_voucher = BigDecimal.valueOf(0);   //只是运费
        }else {
            product_voucher = last_one.total_discounts;
        }
        TaxOrders new_one = new TaxOrders(
                last_one.id_order,
                0,
                0,
                0,
                0,
                current_order_product_quantity,
                BigDecimal.valueOf(0),
                current_order_tax.subtract(BigDecimal.valueOf(product_voucher.doubleValue() * (9 * 1.0 / 109))),
                product_voucher,
                last_one.total_paid_real.subtract(product_voucher),
                delivery_fee,
                last_one.invoice_date
        );
          totalTaxOrders.add(new_one);
         return totalTaxOrders;
        }
    }

    public static ArrayList<TaxSummary> getTaxSummary(String startDate, String endDate){
        String sql =
                "select ps_order_detail.id_tax_rules_group, sum(ps_order_detail.total_price_tax_incl)\n" +
                        "from ps_orders, ps_order_detail \n" +
                        "where ps_orders.id_order = ps_order_detail.id_order\n" +
                        "and ps_orders.valid = 1\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" +"\n"+
                        "group by ps_order_detail.id_tax_rules_group";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("sum(ps_order_detail.total_price_tax_incl)", "total_sales")
                        .create();
        com.avaje.ebean.Query<TaxSummary> query = Ebean.find(TaxSummary.class);
        query.setRawSql(rawSql);
        List<TaxSummary> list = query.findList();
        int size = list.size();
        for(int i =0; i<size;i++){
            if (list.get(i).id_tax_rules_group ==1){
                list.get(i).tax_type = "23%";
                list.get(i).total_tax_amount = BigDecimal.valueOf(list.get(i).total_sales.doubleValue() * (23*1.0/123));
            }
            else if(list.get(i).id_tax_rules_group ==2){
                list.get(i).tax_type = "13.5%";
                list.get(i).total_tax_amount = BigDecimal.valueOf(list.get(i).total_sales.doubleValue() * (135*1.0/1135));
            }
            else if(list.get(i).id_tax_rules_group ==3){
                list.get(i).tax_type = "9%";
                list.get(i).total_tax_amount = BigDecimal.valueOf(list.get(i).total_sales.doubleValue() * (9*1.0/109));
            }
            else if(list.get(i).id_tax_rules_group ==4){
                list.get(i).tax_type = "4.8%";
                list.get(i).total_tax_amount = BigDecimal.valueOf(list.get(i).total_sales.doubleValue() * (48*1.0/1048));
            }
            else if(list.get(i).id_tax_rules_group ==6){
                list.get(i).tax_type = "0%";
                list.get(i).total_tax_amount = BigDecimal.valueOf(0.000000);
            }
            list.get(i).total_tax_amount = list.get(i).total_tax_amount.setScale(2, BigDecimal.ROUND_DOWN);  //四舍五入
        }
        ArrayList<TaxSummary> totalTaxSummary = new ArrayList<>();
        totalTaxSummary.addAll(list);
        return totalTaxSummary;
    }

    public static Result deliveryPage()
    {   //登陆session 验证
        Form<PickDate> dateForm = Form.form(PickDate.class);
        return ok(deliveryPage.render(dateForm)); //ignore
    }

    public static Result searchDelivery() throws Exception {  //登陆session验证
        DynamicForm dateForm = Form.form().bindFromRequest();  //receive
        String startDate = dateForm.get("startDate");
        String endDate = dateForm.get("endDate");
        if (startDate ==null || endDate ==null || startDate.equals("") || endDate.equals("") ){
            flash("error", "please enter Start and End Date");
            return redirect(routes.StatisticsAPI.deliveryPage());
        }
        else{
            String[]  endString = endDate.split("-");
            String day = endString[2];
            int realDay = Integer.valueOf(day) + 1;
            String real_endDate = endString[0]+"-"+endString[1]+"-"+ String.valueOf(realDay);
            System.out.println(startDate);
            System.out.println(endDate);
            DeliverySummary freeDelivery = new DeliverySummary(0,BigDecimal.valueOf(0),"Free delivery");
            DeliverySummary chargedDelivery = new DeliverySummary(0,BigDecimal.valueOf(0),"Charged delivery");
            ArrayList<DeliveryOrders> all_orders = getDeliveryOrders(startDate,real_endDate);
            int size = all_orders.size();
            for (DeliveryOrders each_one: all_orders){
//                BigDecimal discount_amount = each_one.total_discounts;
//                BigDecimal shipping_amount = each_one.total_shipping;
                if (each_one.total_shipping.doubleValue() == Double.valueOf(0)){
                    each_one.delivery_type = "Free delivery";
                    freeDelivery.quantity += 1;
                    freeDelivery.total_shipping = freeDelivery.total_shipping.add(BigDecimal.valueOf(2));
                }else if(each_one.total_shipping.doubleValue() == Double.valueOf(2) && each_one.total_discounts.doubleValue() == Double.valueOf(2)){
                    each_one.delivery_type = "Free delivery";
                    each_one.total_shipping = BigDecimal.valueOf(0);
                    freeDelivery.quantity += 1;
                    freeDelivery.total_shipping = freeDelivery.total_shipping.add(BigDecimal.valueOf(2));
                }else if(each_one.total_shipping.doubleValue() == Double.valueOf(2) && each_one.total_discounts.doubleValue() == Double.valueOf(7)){  //网站点单折扣7欧，运费2欧的情况也是免运费
                    each_one.delivery_type = "Free delivery";
                    each_one.total_shipping = BigDecimal.valueOf(0);
                    freeDelivery.quantity += 1;
                    freeDelivery.total_shipping = freeDelivery.total_shipping.add(BigDecimal.valueOf(2));
                }else{
                    each_one.delivery_type = "Charged delivery";
                    chargedDelivery.quantity += 1;
                    chargedDelivery.total_shipping = chargedDelivery.total_shipping.add(BigDecimal.valueOf(2));
                }
            }
            freeDelivery.total_shipping = freeDelivery.total_shipping.setScale(2, BigDecimal.ROUND_DOWN);
            chargedDelivery.total_shipping = chargedDelivery.total_shipping.setScale(2, BigDecimal.ROUND_DOWN);
            ManagerOrdersMarinaNumber managerOrdersMarinaNumber = managerTotalOrdersAPI(startDate,real_endDate);
//            int marinaOrders = managerOrdersMarinaNumber.marinaOrders;
//            int marinaChargedOrders = managerOrdersMarinaNumber.marinaChargedOrders;
            HashMap<String, String> submittedDate = new HashMap<String, String>();
            submittedDate.put("startDate",startDate);
            submittedDate.put("endDate",endDate);
            Form<PickDate> newForm = Form.form(PickDate.class).bind(submittedDate);
            return ok(deliverySummary.render(newForm,all_orders,size,freeDelivery, chargedDelivery,managerOrdersMarinaNumber)); //ignore
        }
    }

    public static ArrayList<DeliveryOrders> getDeliveryOrders(String startDate, String endDate){
        //搜索delivery 记录
        String sql =
                "select ps_orders.id_order, ps_orders.total_shipping, ps_orders.total_discounts, ps_orders.payment, ps_orders.invoice_date, ps_drivers_orders.id_driver, ps_drivers_orders.driver_name, ps_drivers_orders.id_cashier\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "and ps_orders.valid = 1\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" +"\n"+
                        "order by id_order";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_orders.id_order", "id_order")
                        .columnMapping("ps_orders.total_shipping", "total_shipping")
                        .columnMapping("ps_orders.total_discounts", "total_discounts")
                        .columnMapping("ps_orders.payment", "payment_method")
                        .columnMapping("ps_orders.invoice_date", "invoice_date")
                        .columnMapping("ps_drivers_orders.id_driver", "id_driver")
                        .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                        .columnMapping("ps_drivers_orders.id_cashier", "id_cashier")
                        .create();
        com.avaje.ebean.Query<DeliveryOrders> query = Ebean.find(DeliveryOrders.class);
        query.setRawSql(rawSql);
        List<DeliveryOrders> list = query.findList();
        ArrayList<DeliveryOrders> totalDeliveryOrders = new ArrayList<>();
        totalDeliveryOrders.addAll(list);
        //结束搜索
       return totalDeliveryOrders;
    }


    public static Result discountPage()
    {   //登陆session 验证
        Form<PickDate> dateForm = Form.form(PickDate.class);
        return ok(discountPage.render(dateForm)); //ignore
    }

    public static Result searchDiscount()
    {  //登陆session验证
        DynamicForm dateForm = Form.form().bindFromRequest();  //receive
        String startDate = dateForm.get("startDate");
        String endDate = dateForm.get("endDate");
        if (startDate ==null || endDate ==null || startDate.equals("") || endDate.equals("") ){
            flash("error", "please enter Start and End Date");
            return redirect(routes.StatisticsAPI.discountPage());
        }
        else{
            String[]  endString = endDate.split("-");
            String day = endString[2];
            int realDay = Integer.valueOf(day) + 1;
            String real_endDate = endString[0]+"-"+endString[1]+"-"+ String.valueOf(realDay);
            System.out.println(startDate);
            System.out.println(endDate);
//得到详细 orders
            int  tax_1_products = 0;
            int  tax_2_products = 0;
            int  tax_3_products = 0;
            int  tax_6_products = 0;
            BigDecimal tax_1_discounts = BigDecimal.valueOf(0);
            BigDecimal tax_2_discounts = BigDecimal.valueOf(0);
            BigDecimal tax_3_discounts = BigDecimal.valueOf(0);
            BigDecimal tax_6_discounts = BigDecimal.valueOf(0);
            BigDecimal total_discount = BigDecimal.valueOf(0);
            //搜索order记录
            String sql =
                    "select ps_orders.id_order, ps_orders.invoice_date, ps_order_detail.id_order_detail, ps_order_detail.product_id, ps_order_detail.product_name, ps_order_detail.total_price_tax_incl, ps_order_detail.id_tax_rules_group, ps_order_detail.product_quantity, ps_order_detail.original_product_price\n" +
                            "from ps_orders, ps_order_detail\n" +
                            "where ps_orders.id_order = ps_order_detail.id_order\n" +
                            "and ps_orders.valid = 1\n" +
                            "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                            "and ps_orders.invoice_date <" + "'" + real_endDate +"'" +"\n"+
                            "order by id_order";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "id_order")
                            .columnMapping("ps_orders.invoice_date", "invoice_date")
                            .columnMapping("ps_order_detail.id_order_detail", "id_order_detail")
                            .columnMapping("ps_order_detail.product_id", "product_id")
                            .columnMapping("ps_order_detail.product_name", "product_name")
                            .columnMapping("ps_order_detail.total_price_tax_incl", "total_price_tax_incl")
                            .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_order_detail.product_quantity", "product_quantity")
                            .columnMapping("ps_order_detail.original_product_price", "original_product_price")
                            .create();
            com.avaje.ebean.Query<DiscountOrders> query = Ebean.find(DiscountOrders.class);
            query.setRawSql(rawSql);
            List<DiscountOrders> list = query.findList();
            if(list !=null && list.size() >0){
            ArrayList<DiscountOrders> totalDiscountOrders = new ArrayList<>();
            totalDiscountOrders.addAll(list);
            int current_order_id = list.get(0).id_order;
            int index = 1;
            int id_order_detail_index = 0;
            list = null;
            for(DiscountOrders this_order_discount: totalDiscountOrders){
                if(this_order_discount.id_order != current_order_id){
                    current_order_id = this_order_discount.id_order;
                    this_order_discount.id_order = index + 1;
                    index +=1;
                }else {
                    this_order_discount.id_order = index;
                }
                switch (this_order_discount.id_tax_rules_group){
                    case 1:
                        this_order_discount.tax_type = "23%";
                        this_order_discount.total_product_discount = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.23) ).subtract(this_order_discount.total_price_tax_incl); //总折扣
                        this_order_discount.original_unit_price = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.23) ); // 总原价
                        if (this_order_discount.total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() != 0){
                            tax_1_products +=1;
                            tax_1_discounts = tax_1_discounts.add(this_order_discount.total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP));
                        }
                        break;


                    case 2:
                        this_order_discount.tax_type = "13.5%";
                        this_order_discount.total_product_discount = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.135) ).subtract(this_order_discount.total_price_tax_incl);
                        this_order_discount.original_unit_price = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.135) );
                        if (this_order_discount.total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() != 0){
                            tax_2_products +=1;
                            tax_2_discounts = tax_2_discounts.add(this_order_discount.total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP));
                        }
                        break;

                    case 3:
                        this_order_discount.tax_type = "9%";
                        this_order_discount.total_product_discount = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.09) ).subtract(this_order_discount.total_price_tax_incl);
                        this_order_discount.original_unit_price = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.09) );
                        if (this_order_discount.total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() != 0){
                            tax_3_products +=1;
                            tax_3_discounts = tax_3_discounts.add(this_order_discount.total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP));
                        }
                        break;

                    case 4:
                        this_order_discount.tax_type = "4.8%";
                        this_order_discount.total_product_discount = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.048) ).subtract(this_order_discount.total_price_tax_incl);
                        this_order_discount.original_unit_price = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.048) );
                        break;

                    case 6:
                        this_order_discount.tax_type = "0%";
                        this_order_discount.total_product_discount = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.0) ).subtract(this_order_discount.total_price_tax_incl);
                        this_order_discount.original_unit_price = BigDecimal.valueOf(this_order_discount.original_product_price.doubleValue() * (this_order_discount.product_quantity * 1.0) );
                        if (this_order_discount.total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() != 0){
                            tax_6_products +=1;
                            tax_6_discounts = tax_6_discounts.add(this_order_discount.total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP));
                        }
                        break;
                }
                this_order_discount.total_product_discount = this_order_discount.total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
                this_order_discount.total_price_tax_incl = this_order_discount.total_price_tax_incl.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
                this_order_discount.original_unit_price = this_order_discount.original_unit_price.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
                this_order_discount.product_name= this_order_discount.product_name
                        .replace("<b>","")
                        .replace("</b>","")
                        .replace("<br>"," ")
                        .replace("<br/>","")
                        .replace("<br />","")
                        .replace(":","")
                        .replace("class=awp_mark_38>","")
                        .replace("<span ","")
                        .replace("</span ","");
                //todo replaceinArrray
                if(this_order_discount.product_name.length()>50){
                    this_order_discount.product_name = this_order_discount.product_name.substring(0,50);
                }
                totalDiscountOrders.set(id_order_detail_index,this_order_discount);
                total_discount = total_discount.add(this_order_discount.total_product_discount);
                id_order_detail_index ++;
            }
            DiscountSummary summary_1 = new DiscountSummary(1,"23%", tax_1_discounts, BigDecimal.valueOf(0), tax_1_products);
            DiscountSummary summary_2 = new DiscountSummary(2,"13.5%", tax_2_discounts, BigDecimal.valueOf(0), tax_2_products);
            DiscountSummary summary_3 = new DiscountSummary(3,"9%", tax_3_discounts, BigDecimal.valueOf(0), tax_3_products);
            DiscountSummary summary_6 = new DiscountSummary(6,"0%", tax_6_discounts, BigDecimal.valueOf(0), tax_6_products);
            DiscountSummary total_summay = new DiscountSummary(0,"0%", total_discount, BigDecimal.valueOf(0), index);
            int voucher_numbers = getFreeShippingOrders(startDate,real_endDate);
            DiscountSummary free_delivery = new DiscountSummary(0,"Free Delivery", BigDecimal.valueOf(2.0 * voucher_numbers), BigDecimal.valueOf(0), voucher_numbers);
            DiscountSummary product_vouchers = getProductVouchers(startDate,real_endDate);
            HashMap<String, String> submittedDate = new HashMap<String, String>();
            submittedDate.put("startDate",startDate);
            submittedDate.put("endDate",endDate);
            Form<PickDate> newForm = Form.form(PickDate.class).bind(submittedDate);
            return ok(discountSummary.render(newForm,totalDiscountOrders,total_summay, summary_1,summary_2,summary_3,summary_6, free_delivery,product_vouchers));
            }else {
                return notFound("Please choose a valid time period");
            }
        }
    }

    //搜索免运费订单
    public static Integer getFreeShippingOrders(String startDate, String endDate){
        //搜索delivery 记录
        String sql =
                "select ps_orders.id_order\n" +
                        "from ps_orders\n" +
                        "where valid =1\n" +
                        "and total_shipping = 2\n" +
                        "and total_discounts = 2\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" +"\n"+
                        "union \n" +
                        "select ps_orders.id_order\n" +
                        "from ps_orders\n" +
                        "where valid =1\n" +
                        "and total_shipping = 0\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" +"\n"+
                        "union \n" +
                        "select ps_orders.id_order\n" +
                        "from ps_orders\n" +
                        "where valid =1\n" +
                        "and total_shipping = 2\n" +
                        "and total_discounts = 7\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_orders.id_order", "quantity")
                        .create();
        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        List<ProductQuantity> list = query.findList();
        int number_of_orders = list.size();
        return number_of_orders;
    }

    //Products
    public static Result productsPage() {
        //登陆session 验证
        Form<PickDate> dateForm = Form.form(PickDate.class);
        return ok(productsPage.render(dateForm)); //ignore
    }

    //产品销售情况详细界面
    public static Result searchProducts(){
         //登陆session验证
        DynamicForm dateForm = Form.form().bindFromRequest();  //receive
        String startDate = dateForm.get("startDate");
        String endDate = dateForm.get("endDate");
        if (startDate ==null || endDate ==null || startDate.equals("") || endDate.equals("") ){
            flash("error", "please enter Start and End Date");
            return redirect(routes.StatisticsAPI.productsPage());
        }
        else{
            String[]  endString = endDate.split("-");
            String day = endString[2];
            int realDay = Integer.valueOf(day) + 1;
            String real_endDate = endString[0]+"-"+endString[1]+"-"+ String.valueOf(realDay);
            System.out.println(startDate);
            System.out.println(endDate);
            //code here
            ArrayList<BrandSummary> totalBrands = productBrands(startDate,real_endDate);
            //得到详细 orders
            int  brand_1_orders = 0; //shop
            int  brand_2_orders = 0; //pizza
            int  brand_3_orders = 0; //asian
            int  brand_4_orders = 0; //chinese
            int  brand_5_orders = 0; //starter
            int  brand_6_orders = 0; //tobacco
            int  brand_7_orders = 0; //lunch
            BigDecimal brand_1_discounts = BigDecimal.valueOf(0);
            BigDecimal brand_2_discounts = BigDecimal.valueOf(0);
            BigDecimal brand_3_discounts = BigDecimal.valueOf(0);
            BigDecimal brand_4_discounts = BigDecimal.valueOf(0);
            BigDecimal brand_5_discounts = BigDecimal.valueOf(0);
            BigDecimal brand_6_discounts = BigDecimal.valueOf(0);
            BigDecimal brand_7_discounts = BigDecimal.valueOf(0);
            int current_order_index = 0;
            int  current_if_has_1 = 0;
            int  current_if_has_2 = 0;
            int  current_if_has_3 = 0;
            int  current_if_has_4 = 0;
            int  current_if_has_5 = 0;
            int  current_if_has_6 = 0;
            int  current_if_has_7 = 0;
            int order_number = 0;
            //搜索orders brands
            String sql =
                    "select ps_orders.id_order, ps_order_detail.id_order_detail, ps_order_detail.product_id, ps_order_detail.total_price_tax_incl, ps_order_detail.id_tax_rules_group, ps_order_detail.product_quantity, ps_order_detail.original_product_price, ps_order_detail.product_reference\n" +
                            "from ps_orders, ps_order_detail\n" +
                            "where ps_orders.id_order = ps_order_detail.id_order\n" +
                            "and ps_orders.valid = 1\n" +
                            "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                            "and ps_orders.invoice_date <" + "'" + real_endDate +"'" +"\n"+
                            "order by id_order";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "id_order")
                            .columnMapping("ps_order_detail.id_order_detail", "id_order_detail")
                            .columnMapping("ps_order_detail.product_id", "product_id")
                            .columnMapping("ps_order_detail.total_price_tax_incl", "total_price_tax_incl")  //每一件产品单独的实际价格
                            .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_order_detail.product_quantity", "product_quantity")
                            .columnMapping("ps_order_detail.original_product_price", "original_product_price")
                            .columnMapping("ps_order_detail.product_reference", "product_reference")
                            .create();
            com.avaje.ebean.Query<DiscountOrders> query = Ebean.find(DiscountOrders.class);
            query.setRawSql(rawSql);
            List<DiscountOrders> list = query.findList();
            if(list !=null && list.size() >0){
            int size = list.size();
            for (int i = 0; i< size; i++){
                if (list.get(i).id_order != current_order_index){
                    current_order_index = list.get(i).id_order;
                    current_if_has_1 = 0;
                    current_if_has_2 = 0;
                    current_if_has_3 = 0;
                    current_if_has_4 = 0;
                    current_if_has_5 = 0;
                    current_if_has_6 = 0;
                    current_if_has_7 = 0;
                    order_number +=1;
                }
                //判断税率，计算折扣
                if (list.get(i).id_tax_rules_group ==1){
                    list.get(i).tax_type = "23%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23) ).subtract(list.get(i).total_price_tax_incl); //总折扣
                }
                else if(list.get(i).id_tax_rules_group ==2){
                    list.get(i).tax_type = "13.5%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135) ).subtract(list.get(i).total_price_tax_incl);
                }
                else if(list.get(i).id_tax_rules_group ==3){
                    list.get(i).tax_type = "9%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09) ).subtract(list.get(i).total_price_tax_incl);
                }
                else if(list.get(i).id_tax_rules_group ==4){  //Special tax
                    list.get(i).tax_type = "4.8%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048) ).subtract(list.get(i).total_price_tax_incl);
                }
                else if(list.get(i).id_tax_rules_group ==6){
                    list.get(i).tax_type = "0%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0) ).subtract(list.get(i).total_price_tax_incl);
                }
                list.get(i).total_product_discount = list.get(i).total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
                if(list.get(i).product_reference.equals("")){
                    if(current_if_has_1 == 0){
                        brand_1_orders +=1;
                        current_if_has_1 = 1;
                    }
                    brand_1_discounts = brand_1_discounts.add(list.get(i).total_product_discount);
                }else if(list.get(i).product_reference.equals("pizza")){
                    if(current_if_has_2 == 0){
                        brand_2_orders +=1;
                        current_if_has_2 = 1;
                    }
                    brand_2_discounts = brand_2_discounts.add(list.get(i).total_product_discount);
                }else if(list.get(i).product_reference.equals("asian")){
                    if(current_if_has_3 == 0){
                        brand_3_orders +=1;
                        current_if_has_3 = 1;
                    }
                    brand_3_discounts = brand_3_discounts.add(list.get(i).total_product_discount);
                }else if(list.get(i).product_reference.equals("chinese")){
                    if(current_if_has_4 == 0){
                        brand_4_orders +=1;
                        current_if_has_4 = 1;
                    }
                    brand_4_discounts = brand_4_discounts.add(list.get(i).total_product_discount);
                }else if(list.get(i).product_reference.equals("starter")){
                    if(current_if_has_5 == 0){
                        brand_5_orders +=1;
                        current_if_has_5 = 1;
                    }
                    brand_5_discounts = brand_5_discounts.add(list.get(i).total_product_discount);
                }else if(list.get(i).product_reference.equals("tobacco")){
                    if(current_if_has_6 == 0){
                        brand_6_orders +=1;
                        current_if_has_6 = 1;
                    }
                    brand_6_discounts = brand_6_discounts.add(list.get(i).total_product_discount);
                }else if(list.get(i).product_reference.equals("lunch")){
                    if(current_if_has_7 == 0){
                        brand_7_orders +=1;
                        current_if_has_7 = 1;
                    }
                    brand_7_discounts = brand_7_discounts.add(list.get(i).total_product_discount);
                }
            }
            //结束循环
//结束得到详细orders
            DiscountSummary brand_1 = new DiscountSummary(1,"Shop", brand_1_discounts, BigDecimal.valueOf(0), brand_1_orders);
            DiscountSummary brand_2 = new DiscountSummary(1,"Pizza", brand_2_discounts, BigDecimal.valueOf(0), brand_2_orders);
            DiscountSummary brand_3 = new DiscountSummary(1,"Asian", brand_3_discounts, BigDecimal.valueOf(0), brand_3_orders);
            DiscountSummary brand_4 = new DiscountSummary(1,"Chinese", brand_4_discounts, BigDecimal.valueOf(0), brand_4_orders);
            DiscountSummary brand_5 = new DiscountSummary(1,"Starter", brand_5_discounts, BigDecimal.valueOf(0), brand_5_orders);
            DiscountSummary brand_6 = new DiscountSummary(1,"Tobacco", brand_6_discounts, BigDecimal.valueOf(0), brand_6_orders);
            DiscountSummary brand_7 = new DiscountSummary(1,"Lunch", brand_7_discounts, BigDecimal.valueOf(0), brand_7_orders);

            ArrayList<DiscountSummary> total_brand_summary = new ArrayList<DiscountSummary>();
            total_brand_summary.add(brand_1);
            total_brand_summary.add(brand_2);
            total_brand_summary.add(brand_3);
            total_brand_summary.add(brand_4);
            total_brand_summary.add(brand_5);
            total_brand_summary.add(brand_6);
            total_brand_summary.add(brand_7);
            //产品vouvher 统计
            DiscountSummary productVoucher = getProductVouchers(startDate,real_endDate);
            //code finish here
            HashMap<String, String> submittedDate = new HashMap<String, String>();
            submittedDate.put("startDate",startDate);
            submittedDate.put("endDate",endDate);
            Form<PickDate> newForm = Form.form(PickDate.class).bind(submittedDate);
            return ok(productsSummary.render(newForm,totalBrands,total_brand_summary, order_number,productVoucher));
            }else {
                return notFound("Please choose a valid time period");
            }
        }
    }
    /**
     * Get a list of items, showing the sales conditions of a day
     * @see BrandSummary
     * */
    public static ArrayList<BrandSummary> productBrands(String startDate, String endDate) {
        BrandSummary shop = new BrandSummary();
        BrandSummary pizza = new BrandSummary();
        BrandSummary asian = new BrandSummary();
        BrandSummary chinese = new BrandSummary();
        BrandSummary starter = new BrandSummary();
        BrandSummary tobacco = new BrandSummary();
        BrandSummary lunch = new BrandSummary(); //new
        String sql =
                "SELECT distinct ps_order_detail.product_reference, sum(ps_order_detail.product_quantity), sum(ps_order_detail.total_price_tax_incl)\n" +
                        "\n" +
                        "from  ps_orders, ps_order_detail\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_order_detail.id_order\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" +"\n"+
                        "group by ps_order_detail.product_reference";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_order_detail.product_reference", "brand_name")
                        .columnMapping("sum(ps_order_detail.product_quantity)", "number_of_piece")
                        .columnMapping("sum(ps_order_detail.total_price_tax_incl)", "total_sales")
                        .create();
        com.avaje.ebean.Query<BrandSummary> query = Ebean.find(BrandSummary.class);
        query.setRawSql(rawSql);
        List<BrandSummary> list = query.findList();
        int number = list.size();
        for (int i = 0; i < number;i++){
            if(list.get(i).brand_name.equals("")){
                shop = list.get(i);
            }else if(list.get(i).brand_name.equals("pizza")){
                pizza = list.get(i);
            }else if(list.get(i).brand_name.equals("asian")){
                asian = list.get(i);
            }else if(list.get(i).brand_name.equals("chinese")){
                chinese = list.get(i);
            }else if(list.get(i).brand_name.equals("starter")){
                starter = list.get(i);
            }else if(list.get(i).brand_name.equals("tobacco")){
                tobacco = list.get(i);
            }else if(list.get(i).brand_name.equals("lunch")){
                lunch = list.get(i);
            }
        }
        ArrayList<BrandSummary> totalSummary = new ArrayList<>();
        totalSummary.add(shop);
        totalSummary.add(pizza);
        totalSummary.add(asian);
        totalSummary.add(chinese);
        totalSummary.add(starter);
        totalSummary.add(tobacco);
        totalSummary.add(lunch);
        return totalSummary;
    }

   //GET 链接，点击进入产品销量排名
    public static Result getShopProductRanking(String startTime, String endTime, int product){
        //登陆session验证
        DynamicForm dateForm = Form.form().bindFromRequest();  //receive
        String form_startDate = dateForm.get("startDate");
        String form_endDate = dateForm.get("endDate");
        String startDate = "";
        String endDate = "";
        String product_type = "";
        switch (product){
            case 1: product_type = "";
                break;
            case 2: product_type = "pizza";
                break;
            case 3: product_type = "asian";
                break;
            case 4: product_type = "chinese";
                break;
            case 5: product_type = "starter";
                break;
            case 6: product_type = "tobacco";
                break;
            case 7: product_type = "lunch";
                break;
        }
        if (form_startDate ==null || form_endDate ==null || form_startDate.equals("") || form_endDate.equals("") ){   //如果不是表格重新传入数据
            startDate = startTime;
            endDate = endTime;
        }
        else {
            startDate = form_startDate;
            endDate = form_endDate;
        }
            int sizeOfStart = startDate.length();
            int sizeOfEnd  = endDate.length();
        if(sizeOfEnd < 10 || sizeOfStart < 10){
            return badRequest("Please enter a valid Start or End Date");
        }else{
            String[]  endString = endDate.split("-");
            if(endString.length < 3){
                return badRequest("Please enter a valid Start or End Date");
            }else{
            String day = endString[2];
            int realDay = Integer.valueOf(day) + 1;
            String real_endDate = endString[0]+"-"+endString[1]+"-"+ String.valueOf(realDay);
        //搜索orders brands
        String sql =
                "select ps_order_detail.product_name, ps_order_detail.product_id, sum(ps_order_detail.product_quantity), sum(ps_order_detail.total_price_tax_incl), ps_order_detail.product_reference, ps_order_detail.original_product_price, ps_order_detail.id_tax_rules_group\n" +
                        "from ps_order_detail, ps_orders\n" +
                        "where ps_order_detail.id_order = ps_orders.id_order\n" +
                        "and ps_order_detail.product_reference =" + "'"+ product_type + "'"+"\n" +
                        "and ps_orders.valid = 1\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate + "'" + "\n" +
                        "and ps_orders.invoice_date <" + "'" + real_endDate + "'" + "\n" +
                        "group by ps_order_detail.product_id\n" +
                        "order by sum(ps_order_detail.product_quantity) desc";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_order_detail.product_name", "product_name")
                        .columnMapping("ps_order_detail.product_id", "product_id")
                        .columnMapping("sum(ps_order_detail.product_quantity)", "product_quantity")
                        .columnMapping("sum(ps_order_detail.total_price_tax_incl)", "total_price_tax_incl")
                        .columnMapping("ps_order_detail.product_reference", "product_reference")
                        .columnMapping("ps_order_detail.original_product_price", "original_product_price")
                        .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                        .create();
        com.avaje.ebean.Query<DiscountOrders> query = Ebean.find(DiscountOrders.class);
        query.setRawSql(rawSql);
        List<DiscountOrders> list = query.findList();
        int size = list.size();
        BigDecimal total_sales = BigDecimal.valueOf(0);
        int total_number = 0;
        for(int i =0; i<size;i++) {
            total_sales = total_sales.add(list.get(i).total_price_tax_incl);
            total_number += list.get(i).product_quantity;
            if (list.get(i).id_tax_rules_group == 1) {
                list.get(i).tax_type = "23%";
                list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23)).subtract(list.get(i).total_price_tax_incl); //总折扣
                list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23) ); // 总原价（含数量)
                list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.23); //原价单价
            } else if (list.get(i).id_tax_rules_group == 2) {
                list.get(i).tax_type = "13.5%";
                list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135)).subtract(list.get(i).total_price_tax_incl);
                list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135) ); // 总原价（含数量)
                list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.135); //原价单价
            } else if (list.get(i).id_tax_rules_group == 3) {
                list.get(i).tax_type = "9%";
                list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09)).subtract(list.get(i).total_price_tax_incl);
                list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09) ); // 总原价（含数量)
                list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.09); //原价单价
            } else if (list.get(i).id_tax_rules_group == 4) {  //Special tax
                list.get(i).tax_type = "4.8%";
                list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048)).subtract(list.get(i).total_price_tax_incl);
                list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048) ); // 总原价（含数量)
                list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.048); //原价单价
            } else if (list.get(i).id_tax_rules_group == 6) {
                list.get(i).tax_type = "0%";
                list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0)).subtract(list.get(i).total_price_tax_incl);
                list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0) ); // 总原价（含数量)
                list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.0); //原价单价
            }
            list.get(i).total_product_discount = list.get(i).total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
            list.get(i).original_unit_price = list.get(i).original_unit_price.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
            list.get(i).original_product_price = list.get(i).original_product_price.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
            list.get(i).product_name= list.get(i).product_name.replace("<b>","").replace("</b>","").replace("<br>"," ").replace("<br/>","").replace("<br />","").replace(":","")
                    .replace("class=awp_mark_38>","")
                    .replace("<span ","")
                    .replace("</span ","")
                    .replace("</span ","")
                    .replace("</span ","");
            if(list.get(i).product_name.length()>100){
                list.get(i).product_name = list.get(i).product_name.substring(0,100);
            }
         }
        total_sales = total_sales.setScale(2, BigDecimal.ROUND_HALF_UP);
        BrandSummary summary = new BrandSummary(product_type,total_number, total_sales);
        ArrayList<DiscountOrders> shop_products = new ArrayList<DiscountOrders>();
        shop_products.addAll(list);
        //code finish here
        HashMap<String, String> submittedDate = new HashMap<String, String>();
        submittedDate.put("startDate",startDate);
        submittedDate.put("endDate",endDate);
        submittedDate.put("productType",product_type);
        Form<PickDate> newForm = Form.form(PickDate.class).bind(submittedDate);
        return ok(productsRanking.render(newForm,shop_products,summary));
            }
        }
     }

    // GET 链接 第二次更换日期查询产品排名
    public static Result postShopProductRanking() {
//        /登陆session验证
        DynamicForm dateForm = Form.form().bindFromRequest();  //receive
        String startDate = dateForm.get("startDate");
        String endDate = dateForm.get("endDate");
        String product_type = dateForm.get("product_type");
        if (startDate == null || endDate == null || startDate.equals("") || endDate.equals("") || product_type ==null) {
            flash("error", "please enter Start and End Date");
            return redirect(routes.StatisticsAPI.productsPage());
        } else {
            String[] endString = endDate.split("-");
            String day = endString[2];
            int realDay = Integer.valueOf(day) + 1;
            String real_endDate = endString[0] + "-" + endString[1] + "-" + String.valueOf(realDay);
            //搜索orders brands
            String sql =
                    "select ps_order_detail.product_name, ps_order_detail.product_id, sum(ps_order_detail.product_quantity), sum(ps_order_detail.total_price_tax_incl), ps_order_detail.product_reference, ps_order_detail.original_product_price, ps_order_detail.id_tax_rules_group\n" +
                            "from ps_order_detail, ps_orders\n" +
                            "where ps_order_detail.id_order = ps_orders.id_order\n" +
                            "and ps_order_detail.product_reference =" + "'"+ product_type + "'"+"\n" +
                            "and ps_orders.valid = 1\n" +  //有效订单
                            "and ps_orders.invoice_date >" + "'" + startDate + "'" + "\n" +
                            "and ps_orders.invoice_date <" + "'" + real_endDate + "'" + "\n" +
                            "group by ps_order_detail.product_id\n" +
                            "order by sum(ps_order_detail.product_quantity) desc";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_order_detail.product_name", "product_name")
                            .columnMapping("ps_order_detail.product_id", "product_id")
                            .columnMapping("sum(ps_order_detail.product_quantity)", "product_quantity")
                            .columnMapping("sum(ps_order_detail.total_price_tax_incl)", "total_price_tax_incl")
                            .columnMapping("ps_order_detail.product_reference", "product_reference")
                            .columnMapping("ps_order_detail.original_product_price", "original_product_price")
                            .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                            .create();
            com.avaje.ebean.Query<DiscountOrders> query = Ebean.find(DiscountOrders.class);
            query.setRawSql(rawSql);
            List<DiscountOrders> list = query.findList();
            if(list !=null && list.size() >0){
            int size = list.size();
            BigDecimal total_sales = BigDecimal.valueOf(0);
            int total_number = 0;

            for (int i = 0; i < size; i++) {
                total_sales = total_sales.add(list.get(i).total_price_tax_incl);
                total_number += list.get(i).product_quantity;
                if (list.get(i).id_tax_rules_group == 1) {
                    list.get(i).tax_type = "23%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23)).subtract(list.get(i).total_price_tax_incl); //总折扣
                    list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23)); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * 1.23); //原价单价
                } else if (list.get(i).id_tax_rules_group == 2) {
                    list.get(i).tax_type = "13.5%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135)).subtract(list.get(i).total_price_tax_incl);
                    list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135)); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * 1.135); //原价单价
                } else if (list.get(i).id_tax_rules_group == 3) {
                    list.get(i).tax_type = "9%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09)).subtract(list.get(i).total_price_tax_incl);
                    list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09)); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * 1.09); //原价单价
                } else if (list.get(i).id_tax_rules_group == 4) {  //Special tax
                    list.get(i).tax_type = "4.8%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048)).subtract(list.get(i).total_price_tax_incl);
                    list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048)); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * 1.048); //原价单价
                } else if (list.get(i).id_tax_rules_group == 6) {
                    list.get(i).tax_type = "0%";
                    list.get(i).total_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0)).subtract(list.get(i).total_price_tax_incl);
                    list.get(i).original_unit_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0)); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * 1.0); //原价单价
                }
                list.get(i).total_product_discount = list.get(i).total_product_discount.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
                list.get(i).original_unit_price = list.get(i).original_unit_price.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
                list.get(i).original_product_price = list.get(i).original_product_price.setScale(2, BigDecimal.ROUND_HALF_UP);  //四舍五入
                list.get(i).product_name = list.get(i).product_name
                        .replace("<b>", "")
                        .replace("</b>", "")
                        .replace("<br>", " ")
                        .replace("<br/>", "")
                        .replace("<br />", "")
                        .replace(":", "")
                        .replace("class=awp_mark_38>", "")
                        .replace("<span ", "")
                        .replace("</span ", "");
                if (list.get(i).product_name.length() > 100) {
                    list.get(i).product_name = list.get(i).product_name.substring(0, 100);
                }
            }
            total_sales = total_sales.setScale(2, BigDecimal.ROUND_HALF_UP);
            BrandSummary summary = new BrandSummary(product_type,total_number, total_sales);
            ArrayList<DiscountOrders> shop_products = new ArrayList<DiscountOrders>();
            shop_products.addAll(list);
            //code finish here
            HashMap<String, String> submittedDate = new HashMap<String, String>();
            submittedDate.put("startDate", startDate);
            submittedDate.put("endDate", endDate);
            submittedDate.put("productType",product_type);
            Form<PickDate> newForm = Form.form(PickDate.class).bind(submittedDate);
            return ok(productsRanking.render(newForm,shop_products,summary));
            }else{
                return notFound("Please choose a valid time period");
            }
        }
    }

    public static Result showSearchOrdersPage(){
        Form<PickDate> dateForm = Form.form(PickDate.class);
        return ok(ordersSearchPage.render(dateForm)); //ignore
    }

    /**
     * 拿到当天的所有订单记录
     * */
    @Transactional
    public static Result getOrdersDetails(){
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDate = sdf.format(today);
        String endDate = startDate;

        String[]  endString = endDate.split("-");
        String day = endString[2];
        int realDay = Integer.valueOf(day) + 1;
        String real_endDate = endString[0]+"-"+endString[1]+"-"+ String.valueOf(realDay);
        System.out.println(startDate);
        System.out.println(endDate);
            //code here
        try{
            String sql =
                    "select ps_orders.id_order, ps_orders.payment, ps_orders.id_customer, ps_orders.valid, ps_orders.total_discounts, ps_orders.total_shipping, ps_orders.invoice_date, ps_order_detail.product_id, ps_order_detail.total_price_tax_incl, ps_order_detail.original_product_price, ps_order_detail.id_tax_rules_group, ps_order_detail.product_quantity ,ps_drivers_orders.id_cashier, ps_drivers_orders.id_driver, ps_customer.firstname, ps_customer.lastname\n" +
                            "from ps_orders\n" +
                            "left join ps_order_detail\n" +
                            "on ps_orders.id_order = ps_order_detail.id_order\n" +
                            "left join ps_drivers_orders\n" +
                            "on ps_orders.id_order = ps_drivers_orders.id_order\n" +
                            "left join ps_customer\n" +
                            "on ps_orders.id_customer = ps_customer.id_customer\n" +
                            "where ps_orders.invoice_date >" +"'" + startDate +"'" + "\n" +
                            "and ps_orders.invoice_date <" + "'"+ real_endDate + "'" + "\n" +
                            "order by ps_orders.id_order";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "id_order")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.id_customer", "id_customer")
                            .columnMapping("ps_orders.valid", "if_valid")
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_orders.invoice_date", "invoice_date")
                            .columnMapping("ps_order_detail.product_id", "product_id")
                            .columnMapping("ps_order_detail.total_price_tax_incl", "total_price_tax_incl")
                            .columnMapping("ps_order_detail.original_product_price", "original_product_price")
                            .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_order_detail.product_quantity", "product_quantity")
                            .columnMapping("ps_drivers_orders.id_cashier", "id_cashier")
                            .columnMapping("ps_drivers_orders.id_driver", "id_driver")
                            .columnMapping("ps_customer.firstname", "first_name")
                            .columnMapping("ps_customer.lastname", "last_name")
                            .create();
            com.avaje.ebean.Query<SummaryOrderDetails> query = Ebean.find(SummaryOrderDetails.class);
            query.setRawSql(rawSql);
            List<SummaryOrderDetails> list = query.findList();
            int size = list.size();
            ArrayList<SummaryOrdersTotal> final_orders = new ArrayList<SummaryOrdersTotal>();
            if( list.size() >0){
                int  current_order_index = list.get(0).id_order;
                int  current_order_product_quantity = 0;
                BigDecimal current_order_sales = BigDecimal.valueOf(0);
                BigDecimal current_order_tax  = BigDecimal.valueOf(0);
                BigDecimal current_order_discount = BigDecimal.valueOf(0);
                for (int i = 0; i< size; i++){
                    BigDecimal this_product_discount = BigDecimal.valueOf(0);
                    BigDecimal this_product_tax = BigDecimal.valueOf(0);
                    //跳进下一个订单循环
                    if (list.get(i).id_order != current_order_index){
                        SummaryOrderDetails last_one = list.get(i-1);
                        String payment_method = "";
                        BigDecimal delivery_fee;
                        BigDecimal product_voucher;
                        if(last_one.payment.contains("Cash")){
                            payment_method = "Cash";
                        }else{
                            payment_method = "Online Payment";
                        }

                        if(last_one.total_discounts.doubleValue() ==2 && last_one.total_shipping.doubleValue() ==2){
                            delivery_fee = BigDecimal.valueOf(0);
                        }else if(last_one.total_discounts.doubleValue() ==7 && last_one.total_shipping.doubleValue() ==2){
                            delivery_fee = BigDecimal.valueOf(0);
                        }else {
                            delivery_fee = last_one.total_shipping;
                        }

                        if(last_one.total_discounts.doubleValue() ==2){
                            product_voucher = BigDecimal.valueOf(0);   //只是运费
                        }else if(last_one.total_discounts.doubleValue() ==7){
                            product_voucher = BigDecimal.valueOf(5);   // Marina Park 2 + 5 的情况下只算是打折 5 元
                        }else {
                            product_voucher = last_one.total_discounts;
                        }
                        SummaryOrdersTotal new_one = new SummaryOrdersTotal(
                                last_one.id_order,
                                last_one.id_cashier,
                                last_one.id_driver,
                                payment_method,
                                last_one.first_name,
                                last_one.last_name,
                                last_one.if_valid,
                                current_order_product_quantity,
                                current_order_sales,
                                current_order_tax,
                                current_order_discount,
                                product_voucher,
                                delivery_fee,
                                last_one.invoice_date
                        );
                        final_orders.add(new_one);
                        current_order_index = list.get(i).id_order;
                        current_order_product_quantity = 0;
                        current_order_sales = BigDecimal.valueOf(0);
                        current_order_tax  = BigDecimal.valueOf(0);
                        current_order_discount = BigDecimal.valueOf(0);
                    }
                    // 计算
                    //判断税率，计算折扣, 计算税
                    Integer tax_id = list.get(i).id_tax_rules_group;
                    switch (tax_id){
                        case 1: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (23 * 1.0 / 123));
                            break;
                        case 2: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (135 * 1.0 / 1135));
                            break;
                        case 3: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (9 * 1.0 / 109));
                            break;
                        case 4: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (48 * 1.0 / 1048));
                            break;
                        case 6: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(0.000000);
                            break;

                    }
                    current_order_product_quantity += list.get(i).product_quantity;
                    current_order_sales = current_order_sales.add(list.get(i).total_price_tax_incl);
                    current_order_tax = current_order_tax.add(this_product_tax);
                    current_order_discount = current_order_discount.add(this_product_discount);
                }
                int last_index = size - 1;
                    SummaryOrderDetails last_one = list.get(last_index);
                    String payment_method = "";
                    BigDecimal delivery_fee;
                    BigDecimal product_voucher;
                    if(last_one.payment.contains("Cash")){
                        payment_method = "Cash";
                    }else {
                        payment_method = "Online Payment";
                    }

                    if(last_one.total_discounts.doubleValue() ==2 && last_one.total_shipping.doubleValue() ==2){
                        delivery_fee = BigDecimal.valueOf(0);
                    }else if(last_one.total_discounts.doubleValue() ==7 && last_one.total_shipping.doubleValue() ==2){
                        delivery_fee = BigDecimal.valueOf(0);
                    }else {
                        delivery_fee = last_one.total_shipping;
                    }

                    if(last_one.total_discounts.doubleValue() ==2){
                        product_voucher = BigDecimal.valueOf(0);   //只是运费
                    }else if(last_one.total_discounts.doubleValue() ==7){
                        product_voucher = BigDecimal.valueOf(5);   //Marina 特殊情况
                    }else {
                        product_voucher = last_one.total_discounts;
                    }

                    SummaryOrdersTotal new_one = new SummaryOrdersTotal(
                            current_order_index,
                            last_one.id_cashier,
                            last_one.id_driver,
                            payment_method,
                            last_one.first_name,
                            last_one.last_name,
                            last_one.if_valid,
                            current_order_product_quantity,
                            current_order_sales,
                            current_order_tax,
                            current_order_discount,
                            product_voucher,
                            delivery_fee,
                            last_one.invoice_date
                    );
                    final_orders.add(new_one);
                // 生成一个容器
                ProductQuantity app_orders = getAppOrders(startDate,real_endDate);
                ProductQuantity phone_orders = getTelephoneOrders(startDate,real_endDate);
                ProductQuantity cashPayOrders = getCashPayOrders(startDate,real_endDate);
                ProductQuantity onlinePayOrders = getOnlinePayOrders(startDate,real_endDate);
                Integer total_orders = app_orders.quantity + phone_orders.quantity;
                //code finish here
                HashMap<String, String> submittedDate = new HashMap<String, String>();
                submittedDate.put("startDate",startDate);
                submittedDate.put("endDate",endDate);
                Form<PickDate> newForm = Form.form(PickDate.class).bind(submittedDate);
                return ok(ordersDetailsListSearch.render(newForm,final_orders,app_orders ,phone_orders ,cashPayOrders ,onlinePayOrders,total_orders));
            }else{
                return notFound(errorMessageTemplate.render("No Orders so far today!"));
            }
        }catch (Exception e){
            String sql =
                    "select ps_orders.id_order, ps_orders.payment, ps_orders.id_customer, ps_orders.valid, ps_orders.total_discounts, ps_orders.total_shipping, ps_orders.invoice_date, ps_order_detail.product_id, ps_order_detail.total_price_tax_incl, ps_order_detail.original_product_price, ps_order_detail.id_tax_rules_group, ps_order_detail.product_quantity, ps_customer.firstname, ps_customer.lastname\n" +
                            "from ps_orders\n" +
                            "left join ps_order_detail\n" +
                            "on ps_orders.id_order = ps_order_detail.id_order\n" +
                            "left join ps_drivers_orders\n" +
                            "on ps_orders.id_order = ps_drivers_orders.id_order\n" +
                            "left join ps_customer\n" +
                            "on ps_orders.id_customer = ps_customer.id_customer\n" +
                            "where ps_orders.invoice_date >" +"'" + startDate +"'" + "\n" +
                            "and ps_orders.invoice_date <" + "'"+ real_endDate + "'" + "\n" +
                            "order by ps_orders.id_order\n";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "id_order")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.id_customer", "id_customer")
                            .columnMapping("ps_orders.valid", "if_valid")
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_orders.invoice_date", "invoice_date")
                            .columnMapping("ps_order_detail.product_id", "product_id")
                            .columnMapping("ps_order_detail.total_price_tax_incl", "total_price_tax_incl")
                            .columnMapping("ps_order_detail.original_product_price", "original_product_price")
                            .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_order_detail.product_quantity", "product_quantity")
                            .columnMapping("ps_customer.firstname", "first_name")
                            .columnMapping("ps_customer.lastname", "last_name")
                            .create();
            com.avaje.ebean.Query<SummaryOrderDetails> query = Ebean.find(SummaryOrderDetails.class);
            query.setRawSql(rawSql);
            List<SummaryOrderDetails> list = query.findList();
            int size = list.size();
            ArrayList<SummaryOrdersTotal> final_orders = new ArrayList<SummaryOrdersTotal>();
            if(list != null && list.size() >0){
                int  current_order_index = list.get(0).id_order;
                int  current_order_product_quantity = 0;
                BigDecimal current_order_sales = BigDecimal.valueOf(0);
                BigDecimal current_order_tax  = BigDecimal.valueOf(0);
                BigDecimal current_order_discount = BigDecimal.valueOf(0);
                for (int i = 0; i< size; i++){
                    BigDecimal this_product_discount = BigDecimal.valueOf(0);
                    BigDecimal this_product_tax = BigDecimal.valueOf(0);
                    //跳进下一个订单循环
                    if (list.get(i).id_order != current_order_index){
                        SummaryOrderDetails last_one = list.get(i-1);
                        String payment_method = "";
                        BigDecimal delivery_fee;
                        BigDecimal product_voucher;
                        if(last_one.payment.contains("Cash")){
                            payment_method = "Cash";
                        }else{
                            payment_method = "Online Payment";
                        }

                        if(last_one.total_discounts.doubleValue() ==2 && last_one.total_shipping.doubleValue() ==2){
                            delivery_fee = BigDecimal.valueOf(0);
                        }else if(last_one.total_discounts.doubleValue() ==7 && last_one.total_shipping.doubleValue() ==2){
                            delivery_fee = BigDecimal.valueOf(0);
                        }else {
                            delivery_fee = last_one.total_shipping;
                        }

                        if(last_one.total_discounts.doubleValue() ==2){
                            product_voucher = BigDecimal.valueOf(0);   //只是运费
                        }else if(last_one.total_discounts.doubleValue() ==7){
                            product_voucher = BigDecimal.valueOf(5);   //只是运费
                        }else {
                            product_voucher = last_one.total_discounts;
                        }
                        SummaryOrdersTotal new_one = new SummaryOrdersTotal(
                                last_one.id_order,
                                last_one.id_cashier,
                                last_one.id_driver,
                                payment_method,
                                last_one.first_name,
                                last_one.last_name,
                                last_one.if_valid,
                                current_order_product_quantity,
                                current_order_sales,
                                current_order_tax,
                                current_order_discount,
                                product_voucher,
                                delivery_fee,
                                last_one.invoice_date
                        );
                        final_orders.add(new_one);
                        current_order_index = list.get(i).id_order;
                        current_order_product_quantity = 0;
                        current_order_sales = BigDecimal.valueOf(0);
                        current_order_tax  = BigDecimal.valueOf(0);
                        current_order_discount = BigDecimal.valueOf(0);
                    }
                    // 计算
                    //判断税率，计算折扣, 计算税
                    Integer tax_id = list.get(i).id_tax_rules_group;
                    switch (tax_id){
                        case 1: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (23 * 1.0 / 123));
                            break;
                        case 2: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (135 * 1.0 / 1135));
                            break;
                        case 3: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (9 * 1.0 / 109));
                            break;
                        case 4: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (48 * 1.0 / 1048));
                            break;
                        case 6: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(0.000000);
                            break;

                    }
                    current_order_product_quantity += list.get(i).product_quantity;
                    current_order_sales = current_order_sales.add(list.get(i).total_price_tax_incl);
                    current_order_tax = current_order_tax.add(this_product_tax);
                    current_order_discount = current_order_discount.add(this_product_discount);

                }
                    int last_index = size -1;
                    SummaryOrderDetails last_one = list.get(last_index);
                    String payment_method = "";
                    BigDecimal delivery_fee;
                    BigDecimal product_voucher;
                    if(last_one.payment.contains("Cash")){
                        payment_method = "Cash";
                    }else {
                        payment_method = "Online Payment";
                    }

                    if(last_one.total_discounts.doubleValue() ==2 && last_one.total_shipping.doubleValue() ==2){
                        delivery_fee = BigDecimal.valueOf(0);
                    }else if(last_one.total_discounts.doubleValue() ==7 && last_one.total_shipping.doubleValue() ==2){
                    delivery_fee = BigDecimal.valueOf(0);
                    }else {
                        delivery_fee = last_one.total_shipping;
                    }

                    if(last_one.total_discounts.doubleValue() ==2){
                        product_voucher = BigDecimal.valueOf(0);   //只是运费
                    }else if(last_one.total_discounts.doubleValue() ==7){
                        product_voucher = BigDecimal.valueOf(5);   //只是运费
                    }else {
                        product_voucher = last_one.total_discounts;
                    }

                    SummaryOrdersTotal new_one = new SummaryOrdersTotal(
                            current_order_index,
                            last_one.id_cashier,
                            last_one.id_driver,
                            payment_method,
                            last_one.first_name,
                            last_one.last_name,
                            last_one.if_valid,
                            current_order_product_quantity,
                            current_order_sales,
                            current_order_tax,
                            current_order_discount,
                            product_voucher,
                            delivery_fee,
                            last_one.invoice_date
                    );
                    final_orders.add(new_one);

                // 生成一个容器
                ProductQuantity app_orders = getAppOrders(startDate,real_endDate);
                ProductQuantity phone_orders = getTelephoneOrders(startDate,real_endDate);
                ProductQuantity cashPayOrders = getCashPayOrders(startDate,real_endDate);
                ProductQuantity onlinePayOrders = getOnlinePayOrders(startDate,real_endDate);
                Integer total_orders = app_orders.quantity + phone_orders.quantity;
                //code finish here
                HashMap<String, String> submittedDate = new HashMap<String, String>();
                submittedDate.put("startDate",startDate);
                submittedDate.put("endDate",endDate);
                Form<PickDate> newForm = Form.form(PickDate.class).bind(submittedDate);
                return ok(ordersDetailsListSearch.render(newForm,final_orders,app_orders ,phone_orders ,cashPayOrders ,onlinePayOrders,total_orders));
            }else{
                return notFound(errorMessageTemplate.render("No Orders so far today!"));
            }
        }
    }

    @Transactional
    public static Result postOrdersDetails(){
        DynamicForm dateForm = Form.form().bindFromRequest();  //receive
        String startDateForm = dateForm.get("startDate");
        String endDateForm = dateForm.get("endDate");
        String startDate = "";
        String endDate = "";
        if (startDateForm ==null || endDateForm ==null || startDateForm.equals("") || endDateForm.equals("")){
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            startDate = sdf.format(today);
            endDate = startDate;
        }else{
            startDate = startDateForm;
            endDate = endDateForm;
        }
        String[]  endString = endDate.split("-");
        String day = endString[2];
        int realDay = Integer.valueOf(day) + 1;
        String real_endDate = endString[0]+"-"+endString[1]+"-"+ String.valueOf(realDay);
        System.out.println(startDate);
        System.out.println(endDate);
        //code here
        try{
            String sql =
                    "select ps_orders.id_order, ps_orders.payment, ps_orders.id_customer, ps_orders.valid, ps_orders.total_discounts, ps_orders.total_shipping, ps_orders.invoice_date, ps_order_detail.product_id, ps_order_detail.total_price_tax_incl, ps_order_detail.original_product_price, ps_order_detail.id_tax_rules_group, ps_order_detail.product_quantity, ps_customer.firstname, ps_customer.lastname\n" +
                            "from ps_orders,ps_order_detail,ps_customer\n" +
                            "where ps_orders.id_order = ps_order_detail.id_order\n" +
                            "and ps_orders.id_customer = ps_customer.id_customer\n" +
                            "and ps_orders.invoice_date >" +"'" + startDate +"'" + "\n" +
                            "and ps_orders.invoice_date <" + "'"+ real_endDate + "'" + "\n" +
                            "order by ps_orders.id_order\n";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_orders.id_order", "id_order")
                            .columnMapping("ps_orders.payment", "payment")
                            .columnMapping("ps_orders.id_customer", "id_customer")
                            .columnMapping("ps_orders.valid", "if_valid")
                            .columnMapping("ps_orders.total_discounts", "total_discounts")
                            .columnMapping("ps_orders.total_shipping", "total_shipping")
                            .columnMapping("ps_orders.invoice_date", "invoice_date")
                            .columnMapping("ps_order_detail.product_id", "product_id")
                            .columnMapping("ps_order_detail.total_price_tax_incl", "total_price_tax_incl")
                            .columnMapping("ps_order_detail.original_product_price", "original_product_price")
                            .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_order_detail.product_quantity", "product_quantity")
                            .columnMapping("ps_customer.firstname", "first_name")
                            .columnMapping("ps_customer.lastname", "last_name")
                            .create();
            com.avaje.ebean.Query<SummaryOrderDetails> query = Ebean.find(SummaryOrderDetails.class);
            query.setRawSql(rawSql);
            List<SummaryOrderDetails> list = query.findList();
            int size = list.size();
            ArrayList<SummaryOrdersTotal> final_orders = new ArrayList<SummaryOrdersTotal>();
            if(list != null && list.size() >0){
                int  current_order_index = list.get(0).id_order;
                int  current_order_product_quantity = 0;
                BigDecimal current_order_sales = BigDecimal.valueOf(0);
                BigDecimal current_order_tax  = BigDecimal.valueOf(0);
                BigDecimal current_order_discount = BigDecimal.valueOf(0);
                for (int i = 0; i< size; i++){
                    BigDecimal this_product_discount = BigDecimal.valueOf(0);
                    BigDecimal this_product_tax = BigDecimal.valueOf(0);
                    //跳进下一个订单循环
                    if (list.get(i).id_order != current_order_index){
                        SummaryOrderDetails last_one = list.get(i-1);
                        String payment_method = "";
                        BigDecimal delivery_fee;
                        BigDecimal product_voucher;
                        if(last_one.payment.contains("Cash")){
                            payment_method = "Cash";
                        }else{
                            payment_method = "Online Payment";
                        }

                        if(last_one.total_discounts.doubleValue() ==2 && last_one.total_shipping.doubleValue() ==2){
                            delivery_fee = BigDecimal.valueOf(0);
                        }else if(last_one.total_discounts.doubleValue() ==7 && last_one.total_shipping.doubleValue() ==2){
                            delivery_fee = BigDecimal.valueOf(0);
                        }else {
                            delivery_fee = last_one.total_shipping;
                        }
                        if(last_one.total_discounts.doubleValue() ==2){
                            product_voucher = BigDecimal.valueOf(0);   //只是运费
                        }else if(last_one.total_discounts.doubleValue() ==7){
                            product_voucher = BigDecimal.valueOf(5);   //只是运费
                        }else {
                            product_voucher = last_one.total_discounts;
                        }
                        SummaryOrdersTotal new_one = new SummaryOrdersTotal(
                                last_one.id_order,
                                last_one.id_cashier,
                                last_one.id_driver,
                                payment_method,
                                last_one.first_name,
                                last_one.last_name,
                                last_one.if_valid,
                                current_order_product_quantity,
                                current_order_sales,
                                current_order_tax,
                                current_order_discount,
                                product_voucher,
                                delivery_fee,
                                last_one.invoice_date
                        );
                        final_orders.add(new_one);
                        current_order_index = list.get(i).id_order;
                        current_order_product_quantity = 0;
                        current_order_sales = BigDecimal.valueOf(0);
                        current_order_tax  = BigDecimal.valueOf(0);
                        current_order_discount = BigDecimal.valueOf(0);
                    }
                    // 计算
                    //判断税率，计算折扣, 计算税
                    Integer tax_id = list.get(i).id_tax_rules_group;
                    switch (tax_id){
                        case 1: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (23 * 1.0 / 123));
                            break;
                        case 2: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (135 * 1.0 / 1135));
                            break;
                        case 3: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (9 * 1.0 / 109));
                            break;
                        case 4: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (48 * 1.0 / 1048));
                            break;
                        case 6: this_product_discount = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0)).subtract(list.get(i).total_price_tax_incl);
                            this_product_tax = BigDecimal.valueOf(0.000000);
                            break;
                    }
                    current_order_product_quantity += list.get(i).product_quantity;
                    current_order_sales = current_order_sales.add(list.get(i).total_price_tax_incl);
                    current_order_tax = current_order_tax.add(this_product_tax);
                    current_order_discount = current_order_discount.add(this_product_discount);
                }
                int last_index = size - 1;
                SummaryOrderDetails last_one = list.get(last_index);
                String payment_method = "";
                BigDecimal delivery_fee;
                BigDecimal product_voucher;
                if(last_one.payment.contains("Cash")){
                    payment_method = "Cash";
                }else {
                    payment_method = "Online Payment";
                }

                if(last_one.total_discounts.doubleValue() ==2 && last_one.total_shipping.doubleValue() ==2){
                    delivery_fee = BigDecimal.valueOf(0);
                }else if(last_one.total_discounts.doubleValue() ==7 && last_one.total_shipping.doubleValue() ==2){
                    delivery_fee = BigDecimal.valueOf(0);
                }else {
                    delivery_fee = last_one.total_shipping;
                }
                if(last_one.total_discounts.doubleValue() ==2){
                    product_voucher = BigDecimal.valueOf(0);   //只是运费
                }else if(last_one.total_discounts.doubleValue() ==7){
                    product_voucher = BigDecimal.valueOf(5);   //只是运费
                }else {
                    product_voucher = last_one.total_discounts;
                }
                SummaryOrdersTotal new_one = new SummaryOrdersTotal(
                        current_order_index,
                        last_one.id_cashier,
                        last_one.id_driver,
                        payment_method,
                        last_one.first_name,
                        last_one.last_name,
                        last_one.if_valid,
                        current_order_product_quantity,
                        current_order_sales,
                        current_order_tax,
                        current_order_discount,
                        product_voucher,
                        delivery_fee,
                        last_one.invoice_date
                );
                final_orders.add(new_one);

                // 生成一个容器
                ProductQuantity app_orders = getAppOrders(startDate,real_endDate);
                ProductQuantity phone_orders = getTelephoneOrders(startDate,real_endDate);
                ProductQuantity cashPayOrders = getCashPayOrders(startDate,real_endDate);
                ProductQuantity onlinePayOrders = getOnlinePayOrders(startDate,real_endDate);
                Integer total_orders = app_orders.quantity + phone_orders.quantity;
                //code finish here
                HashMap<String, String> submittedDate = new HashMap<String, String>();
                submittedDate.put("startDate",startDate);
                submittedDate.put("endDate",endDate);
                Form<PickDate> newForm = Form.form(PickDate.class).bind(submittedDate);
                return ok(ordersDetailsListSearch.render(newForm,final_orders,app_orders ,phone_orders ,cashPayOrders ,onlinePayOrders,total_orders));
            }else{
                return notFound(errorMessageTemplate.render("please choose a valid time period"));
            }
        }catch (Exception e){
               return badRequest(errorMessageTemplate.render("Some records were damaged, please contact IT staff to fix."));
        }
    }

    public static ProductQuantity getAppOrders(String startDate, String endDate){
        String sql =
                "select count(id_customer)\n" +
                        "from ps_orders\n" +
                        "where ps_orders.id_customer !=149\n" +
                        "and ps_orders.id_customer != 102\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" ;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(id_customer)", "quantity")
                        .create();
        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        ProductQuantity appOrders = query.findUnique();
        return  appOrders;
    }

    public static ProductQuantity getTelephoneOrders(String startDate, String endDate){
        String sql =
                "select count(id_customer)\n" +
                        "from ps_orders\n" +
                        "where (ps_orders.id_customer = 102 OR ps_orders.id_customer = 149)\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" ;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(id_customer)", "quantity")
                        .create();
        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        ProductQuantity phoneOrders = query.findUnique();
        return  phoneOrders;
    }

    public static ProductQuantity getCashPayOrders(String startDate, String endDate){
        String sql =
                "select count(ps_orders.id_order)\n" +
                        "from ps_orders\n" +
                        "where ps_orders.payment like \"Cash%\"\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" ;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(ps_orders.id_order)", "quantity")
                        .create();
        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        ProductQuantity cashPayOrders = query.findUnique();
        return  cashPayOrders;
    }

    public static ProductQuantity getOnlinePayOrders(String startDate, String endDate){
        String sql =
                "select count(ps_orders.id_order)\n" +
                        "from ps_orders\n" +
                        "where ps_orders.payment not like \"Cash%\"\n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" ;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("count(ps_orders.id_order)", "quantity")
                        .create();
        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        ProductQuantity onlinePayOrders = query.findUnique();
        return  onlinePayOrders;
    }

    public static DiscountSummary getProductVouchers(String startDate, String endDate){
        String sql =
                "select sum(ps_orders.total_discounts), count(ps_orders.id_order)\n" +
                        "from ps_orders\n" +
                        "where ps_orders.valid = 1\n" +
                        "and ps_orders.total_discounts != 2\n" +  //默认免运费 不算是产品打折
                        "and ps_orders.total_discounts > 0 \n" +
                        "and ps_orders.invoice_date >" + "'" + startDate +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + endDate +"'" ;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(ps_orders.total_discounts)", "total_discount")
                        .columnMapping("count(ps_orders.id_order)", "product_quantity")
                        .create();
        com.avaje.ebean.Query<DiscountSummary> query = Ebean.find(DiscountSummary.class);
        query.setRawSql(rawSql);
        DiscountSummary productVoucherSummary = query.findUnique();
        if (productVoucherSummary.total_discount ==null){
            productVoucherSummary.total_discount = BigDecimal.valueOf(0);
        }
        return  productVoucherSummary;
    }

    public static ArrayList<OrderDrillProducts> getOrderProductsDetail(int order_id) {

        String sql =
                "select ps_orders.total_discounts, ps_orders.total_shipping, ps_orders.invoice_date, ps_order_detail.product_name, ps_order_detail.product_reference, ps_order_detail.total_price_tax_incl, ps_order_detail.product_quantity, ps_order_detail.id_tax_rules_group, ps_order_detail.original_product_price\n" +
                        "from ps_orders, ps_order_detail\n" +
                        "where ps_orders.id_order = ps_order_detail.id_order\n" +
                        "and ps_orders.id_order =" + order_id;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_orders.total_discounts", "total_discounts")
                        .columnMapping("ps_orders.total_shipping", "total_shipping")
                        .columnMapping("ps_orders.invoice_date", "invoice_date")
                        .columnMapping("ps_order_detail.product_name", "product_name")
                        .columnMapping("ps_order_detail.product_reference", "product_reference")
                        .columnMapping("ps_order_detail.total_price_tax_incl", "total_price_tax_incl")
                        .columnMapping("ps_order_detail.product_quantity", "product_quantity")
                        .columnMapping("ps_order_detail.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_order_detail.original_product_price", "original_product_price")
                        .create();
        com.avaje.ebean.Query<OrderDrillProducts> query = Ebean.find(OrderDrillProducts.class);
        query.setRawSql(rawSql);
        List<OrderDrillProducts> list = query.findList();
        int number = list.size();
        for (int i = 0; i < number;i++){
            if(list.get(i).product_reference.equals("")){
                list.get(i).product_reference = "Shop";
            }

            if(list.get(i).total_discounts.doubleValue() ==2 && list.get(i).total_shipping.doubleValue() ==2){
                list.get(i).total_shipping = BigDecimal.valueOf(0);
            }


            if(list.get(i).total_discounts.doubleValue() ==2){
                list.get(i).total_discounts = BigDecimal.valueOf(0);   //只是运费
            }

            int tax_id = list.get(i).id_tax_rules_group;
            switch (tax_id){
                case 1:
                    list.get(i).product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (23 * 1.0 / 123));
                    list.get(i).tax_type = "23%";
                    list.get(i).total_reduction = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23)).subtract(list.get(i).total_price_tax_incl); //总折扣
                    list.get(i).real_original_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.23) ); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.23); //原价单价
                    break;
                case 2:
                    list.get(i).product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (135 * 1.0 / 1135));
                    list.get(i).tax_type = "13.5%";
                    list.get(i).total_reduction = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135)).subtract(list.get(i).total_price_tax_incl); //总折扣
                    list.get(i).real_original_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.135) ); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.135); //原价单价
                    break;
                case 3:
                    list.get(i).product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (9 * 1.0 / 109));
                    list.get(i).tax_type = "9%";
                    list.get(i).total_reduction = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09)).subtract(list.get(i).total_price_tax_incl); //总折扣
                    list.get(i).real_original_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.09) ); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.09); //原价单价
                    break;
                case 4:
                    list.get(i).product_tax = BigDecimal.valueOf(list.get(i).total_price_tax_incl.doubleValue() * (48 * 1.0 / 1048));
                    list.get(i).tax_type = "4.8%";
                    list.get(i).total_reduction = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048)).subtract(list.get(i).total_price_tax_incl); //总折扣
                    list.get(i).real_original_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.048) ); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.048); //原价单价
                    break;
                case 6:
                    list.get(i).product_tax = BigDecimal.valueOf(0);
                    list.get(i).tax_type = "0%";
                    list.get(i).total_reduction = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0)).subtract(list.get(i).total_price_tax_incl); //总折扣
                    list.get(i).real_original_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() * (list.get(i).product_quantity * 1.0) ); // 总原价（含数量)
                    list.get(i).original_product_price = BigDecimal.valueOf(list.get(i).original_product_price.doubleValue() *  1.0); //原价单价
                    break;
            }

            list.get(i).product_name= list.get(i).product_name.replace("<b>","").replace("</b>","").replace("<br>"," ").replace("<br/>","").replace("<br />","").replace(":","")
                    .replace("class=awp_mark_38>","")
                    .replace("<span ","")
                    .replace("</span ","")
                    .replace("</span ","")
                    .replace("</span ","");
            if(list.get(i).product_name.length()>100){
                list.get(i).product_name = list.get(i).product_name.substring(0,100);
            }
        }
        ArrayList<OrderDrillProducts> orderDrilledInfo = new ArrayList<>();
        orderDrilledInfo.addAll(list);
        return orderDrilledInfo;
    }

    public static Result getOrderProductInformation(int id_order)
    {
        ArrayList<OrderDrillProducts> orderDrilledInfo = getOrderProductsDetail(id_order);
        if(orderDrilledInfo.size() ==0){
            return badRequest(errorMessageTemplate.render("This is not a valid Order"));
        }else {
            return ok(orderProductsInformation.render(orderDrilledInfo)); //ignore
        }
    }

     //Products
    public static Result ordersInformationPage()
    {
        return ok(ordersInfoPage.render()); //ignore
    }
}