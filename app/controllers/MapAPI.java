/*
 * Copyright (c) 2017. Zhi Chen.
 * AddressAPI is used for actions related to user's addresses
 * CRUD to the database
 */

package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.GoogleMap;
public class MapAPI extends Controller{

    public static Result showMap() {
        return ok(GoogleMap.render("Find Your Area","hi"));
    }

//    public static WebSocket<String> index() {
//        return new WebSocket<String>() {
//            // Called when the Websocket Handshake is done.
//            public void onReady(WebSocket.In<String> in, WebSocket.Out<String> out) {
//                // For each event received on the socket,
//                in.onMessage(new F.Callback<String>() {
//                    public void invoke(String event) {
//                        // Log events to the console
//                        System.out.println(event);
//                    }
//                });
//                // When the socket is closed.
//                in.onClose(new F.Callback0() {
//                    public void invoke() {
//                        System.out.println("Disconnected");
//                    }
//                });
//                // Send a single 'Hello!' message
//                out.write("Hello!");
//            }
//        };
//    }


}