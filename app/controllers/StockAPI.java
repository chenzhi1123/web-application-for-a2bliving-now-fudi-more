package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.database.*;
import models.sqlContainer.BuyOneGetOneProduct;
import models.sqlContainer.Category;
import models.sqlContainer.ProductQuantity;
import models.sqlContainer.StockManagementProduct;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.bundleSale.*;
import views.html.errorMessageTemplate;
import views.html.shopStock.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static controllers.ProductAPI.getQuantity;
import static controllers.ProductAPI.get_image_id;
import static controllers.routes.WebAPI;
import static models.database.DatabaseProduct.updateOneRecord;
import static parsers.JsonParser.renderCategories;

//import static parsers.JsonParser.renderProduct;

public class StockAPI extends Controller
{
    public static class SearchBox{
        public String productID = "";
        public String productName ="";
        public String ean = "";
        public String category ="";
        public String if_valid = "";
        public String expiryDate ="";
    }

    public static class BundleProductRecord{
        @Constraints.Required
        public int product_id;
        @Constraints.Required
        public int basic_unit;
        @Constraints.Required
        public int bundle_number;
        @Constraints.Required
        public Date start_date;
        @Constraints.Required
        public Date end_date;
        @Constraints.Required
        public int target_product_id;
        @Constraints.Required
        public String admin_name;

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }

        public int getBasic_unit() {
            return basic_unit;
        }

        public void setBasic_unit(int basic_unit) {
            this.basic_unit = basic_unit;
        }

        public int getBundle_number() {
            return bundle_number;
        }

        public void setBundle_number(int bundle_number) {
            this.bundle_number = bundle_number;
        }

        public Date getStart_date() {
            return start_date;
        }

        public void setStart_date(Date start_date) {
            this.start_date = start_date;
        }

        public Date getEnd_date() {
            return end_date;
        }

        public void setEnd_date(Date end_date) {
            this.end_date = end_date;
        }

        public int getTarget_product_id() {
            return target_product_id;
        }

        public void setTarget_product_id(int target_product_id) {
            this.target_product_id = target_product_id;
        }

        public String getAdmin_name() {
            return admin_name;
        }

        public void setAdmin_name(String admin_name) {
            this.admin_name = admin_name;
        }

        public BundleProductRecord(){

        }

        public BundleProductRecord(int product_id, int basic_unit, int bundle_number, Date start_date, Date end_date, int target_product_id, String admin_name) {
            this.product_id = product_id;
            this.basic_unit = basic_unit;
            this.bundle_number = bundle_number;
            this.start_date = start_date;
            this.end_date = end_date;
            this.target_product_id = target_product_id;
            this.admin_name = admin_name;
        }
    }


    public static class OnSalePerson{
        @Constraints.Required
        public String staff_name;

    }

    public static class AddStockForm{
        @Constraints.Required
        public int quantity;       //position_number
        @Constraints.Required
        public String expiry_date;  //zone

        public String if_on_sale;   //layer
        @Constraints.Required
        public String person_add;   //part
    }

    //Products Page, according to category
    public static Result productsStockPage(int id_category){
        String email = session("shopAssistantEmail");
        // if in session
        if (email != null && email.equals("chenzhi1111@gmail.com")) {
            Form<SearchBox> dateForm = Form.form(SearchBox.class);
            String sql =
                    "SELECT distinct ps_category_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_category_lang.name, ps_product_expiry.expiry_date\n" +
                            "FROM ps_category_product\n" +
                            "LEFT JOIN ps_product\n" +
                            "ON ps_category_product.id_product = ps_product.id_product\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            "AND NOW() > ps_specific_price.from\n" +
                            "AND NOW() < ps_specific_price.to\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_category_product.id_product\n" +
                            "AND ps_image.cover = 1\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_stock_available.id_product = ps_category_product.id_product\n" +
                            "AND ps_stock_available.id_product_attribute = 0 \n" +
                            "LEFT JOIN ps_category_lang\n" +
                            "ON ps_category_product.id_category = ps_category_lang.id_category\n" +
                            "LEFT JOIN ps_product_expiry\n" +
                            "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                            "WHERE ps_category_product.id_category =" + id_category + "\n" +
                            "AND ps_product.upc != 1 \n" +
                            "AND ps_product.id_product not in (select distinct id_onsale_product from ps_product_buyone_getone)\n" +  //不显示买一送一产品
                            "Order BY ps_product.active desc, ps_category_product.position";

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_category_product.id_product", "id_product")
                            .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_product.active", "if_valid")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                            .columnMapping("ps_product.ean13", "ean")
                            .columnMapping("ps_product.price", "price")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_image.id_image", "id_image")
                            .columnMapping("ps_stock_available.quantity", "quantity")
                            .columnMapping("ps_category_lang.name", "category_name")
                            .columnMapping("ps_product_expiry.expiry_date", "expiry_date")    //expiry date
                            .create();
            com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
            query.setRawSql(rawSql);
            List<StockManagementProduct> list = query.findList();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
            for (StockManagementProduct this_one : list) {  //循环每件产品
                if (this_one.expiry_date == null) {
                    this_one.expiry_date = new Date(0);
                }
                this_one.expiryDate = sdf.format(this_one.expiry_date);

                if (this_one.expiry_date.before(future_date)) {
                    this_one.if_expiring = 1;   //加入标记。即将过期
                }
            }
            ArrayList<StockManagementProduct> all_products = new ArrayList<StockManagementProduct>();
            all_products.addAll(list);
            ArrayList<Category> all_category = CategoryAPI.getAllCategories();
            return ok(shopStockPage.render(all_products, dateForm,all_category));
//            return ok(renderCategories(all_category));
        }else {
            flash("error","please log in first");
            return redirect(WebAPI.shopAssistantLogin());
        }
    }

    //所有打折产品查询
    public static Result discountProducts()
    {
        Form<SearchBox> dateForm = Form.form(SearchBox.class);
        String sql =
                "SELECT distinct ps_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_product_expiry.expiry_date\n" +
                        "FROM ps_product\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        "AND NOW() > ps_specific_price.from\n" +
                        "AND NOW() < ps_specific_price.to\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_product.id_product\n" +
                        "AND ps_image.cover = 1\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_product.id_product\n" +
                        "AND ps_stock_available.id_product_attribute = 0 \n" +
                        "LEFT JOIN ps_product_expiry\n" +
                        "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                        "WHERE ps_product.upc != 1\n" +
                        "AND ps_specific_price.reduction IS NOT NULL\n" +
                        "AND ps_specific_price.reduction_type IS NOT NULL\n" +
                        "AND ps_product.id_product not in (select distinct id_onsale_product from ps_product_buyone_getone)\n" +   //不显示买一送一产品
                        "Order BY ps_product.active desc, ps_product_lang.name";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product.id_product", "id_product")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_product.active", "if_valid")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                        .columnMapping("ps_product.ean13", "ean")
                        .columnMapping("ps_product.price", "price")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .columnMapping("ps_product_expiry.expiry_date", "expiry_date")    //expiry date
                        .create();
        com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
        query.setRawSql(rawSql);
        List<StockManagementProduct> list = query.findList();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
        for(StockManagementProduct this_one: list){  //循环每件产品
            if (this_one.expiry_date ==null){
                this_one.expiry_date = new Date(0);
            }
            this_one.expiryDate = sdf.format(this_one.expiry_date);

            if(this_one.expiry_date.before(future_date)){
                this_one.if_expiring = 1;   //加入标记。即将过期
            }
        }
        ArrayList<StockManagementProduct> all_products = new ArrayList<StockManagementProduct>();
        all_products.addAll(list);
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(shopStockPage.render(all_products,dateForm,all_category)); //ignore
    }

    //低数量产品报警（包含过期日期）
    public static Result lowQuantityAlert()
    {

        Form<SearchBox> dateForm = Form.form(SearchBox.class);
        String sql =
                "SELECT distinct ps_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_product_expiry.expiry_date\n" +
                        "FROM ps_product\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        "AND NOW() > ps_specific_price.from\n" +
                        "AND NOW() < ps_specific_price.to\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_product.id_product\n" +
                        "AND ps_image.cover = 1\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_product.id_product\n" +
                        "AND ps_stock_available.id_product_attribute = 0 \n" +
                        "LEFT JOIN ps_product_expiry\n" +
                        "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                        "WHERE ps_product.upc != 1\n" +
                        "AND ps_product.active = 1 \n" +    //status = abled
                        "AND ps_product.id_product not in (select distinct id_onsale_product from ps_product_buyone_getone)\n" +
                        "AND ps_stock_available.quantity < 10\n" +
                        "Order BY ps_product.active desc, ps_stock_available.quantity" ;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product.id_product", "id_product")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_product.active", "if_valid")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                        .columnMapping("ps_product.ean13", "ean")
                        .columnMapping("ps_product.price", "price")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .columnMapping("ps_product_expiry.expiry_date", "expiry_date")    //expiry date
                        .create();
        com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
        query.setRawSql(rawSql);
        List<StockManagementProduct> list = query.findList();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
        for(StockManagementProduct this_one: list){  //循环每件产品
            if (this_one.expiry_date ==null){
                this_one.expiry_date = new Date(0);
            }
            this_one.expiryDate = sdf.format(this_one.expiry_date);

            if(this_one.expiry_date.before(future_date)){
                this_one.if_expiring = 1;   //加入标记。即将过期
            }
        }
        ArrayList<StockManagementProduct> all_products = new ArrayList<StockManagementProduct>();
        all_products.addAll(list);
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(shopStockPage.render(all_products,dateForm,all_category)); //ignore
    }

    //即将过期产品报警
    public static Result expiringAlert(){
        Form<SearchBox> dateForm = Form.form(SearchBox.class);
        String sql =
                "SELECT distinct ps_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_product_expiry.expiry_date\n" +
                        "FROM ps_product\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        "AND NOW() > ps_specific_price.from\n" +
                        "AND NOW() < ps_specific_price.to\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_product.id_product\n" +
                        "AND ps_image.cover = 1\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_product.id_product\n" +
                        "AND ps_stock_available.id_product_attribute = 0 \n" +
                        "LEFT JOIN ps_product_expiry\n" +
                        "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                        "WHERE ps_product.upc != 1\n" +
                        "AND ps_product.active = 1 \n" +    //status = abled
                        "AND ps_product.id_product not in (select distinct id_onsale_product from ps_product_buyone_getone)\n" +    //status = abled
                        "AND (ps_product_expiry.expiry_date < DATE_ADD(NOW(), INTERVAL 1 MONTH) OR ps_product_expiry.expiry_date IS NULL)\n" +
                        "Order BY ps_product.active desc, ps_product_expiry.expiry_date " ;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product.id_product", "id_product")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_product.active", "if_valid")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                        .columnMapping("ps_product.ean13", "ean")
                        .columnMapping("ps_product.price", "price")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .columnMapping("ps_product_expiry.expiry_date", "expiry_date")    //expiry date
                        .create();
        com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
        query.setRawSql(rawSql);
        List<StockManagementProduct> list = query.findList();
        ArrayList<StockManagementProduct> all_products = new ArrayList<StockManagementProduct>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (list.size()>0) {
            for (StockManagementProduct this_one : list) {  //循环每件产品
                if(this_one.quantity ==null){
                    this_one.quantity = 0;
                }
                if (this_one.quantity != 0) {//数量为0就不要用提醒
                    this_one.if_expiring = 1;   //加入标记。即将过期
                    if (this_one.expiry_date == null) {
                        this_one.expiry_date = new Date(0);
                    }
                    this_one.expiryDate = sdf.format(this_one.expiry_date);

                    all_products.add(this_one);
                }

            }
        }
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(shopStockPage.render(all_products,dateForm,all_category)); //ignore
    }

    //有库存的产品查询
    public static Result productWithStorage()
    {
        Form<SearchBox> dateForm = Form.form(SearchBox.class);
        String sql =
                "SELECT distinct ps_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_product_expiry.expiry_date\n" +
                        "from ps_product\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        "AND NOW() > ps_specific_price.from\n" +
                        "AND NOW() < ps_specific_price.to\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_product.id_product\n" +
                        "AND ps_image.cover = 1\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_product.id_product\n" +
                        "AND ps_stock_available.id_product_attribute = 0 \n" +
                        "LEFT JOIN ps_product_expiry\n" +
                        "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                        "LEFT JOIN ps_stock_product\n" +
                        "ON ps_stock_product.id_product = ps_product.id_product\n" +
                        "WHERE ps_product.upc != 1\n" +
                        "AND ps_stock_product.if_active = 0\n" +
                        "AND ps_stock_product.if_backup = 1\n" +
                        "Order BY ps_product.active desc, ps_product_lang.name" ;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product.id_product", "id_product")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_product.active", "if_valid")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                        .columnMapping("ps_product.ean13", "ean")
                        .columnMapping("ps_product.price", "price")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .columnMapping("ps_product_expiry.expiry_date", "expiry_date")    //expiry date
                        .create();
        com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
        query.setRawSql(rawSql);
        List<StockManagementProduct> list = query.findList();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
        for(StockManagementProduct this_one: list){  //循环每件产品
            if(this_one.quantity ==null){
                this_one.quantity =0;
            }
            if (this_one.expiry_date ==null){
                this_one.expiry_date = new Date(0);
            }
            this_one.expiryDate = sdf.format(this_one.expiry_date);

            if(this_one.expiry_date.before(future_date)){
                this_one.if_expiring = 1;   //加入标记。即将过期
            }

        }
        ArrayList<StockManagementProduct> all_products = new ArrayList<StockManagementProduct>();
        all_products.addAll(list);
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(shopStockPage.render(all_products,dateForm,all_category)); //ignore
    }

    public static Result conditionalSearch()
    {
        //登陆session验证
        DynamicForm conditionsForm = Form.form().bindFromRequest();  //receive
        String productID = conditionsForm.get("productID");
        if (productID ==null){
            productID = "";
        }
        String productName = conditionsForm.get("productName");
        if (productName==null){
            productName = "";
        }
        String ean = conditionsForm.get("ean");
        if (ean==null){
            ean = "";
        }
        String category = conditionsForm.get("category");
        if (category==null){
            category = "";
        }
        String if_valid = conditionsForm.get("if_valid");
        if (if_valid==null){
            if_valid = "unSelected";
        }
        String expiryDate = conditionsForm.get("expiryDate");
        if (expiryDate==null){
            expiryDate = "";
        }
        String conditionsString = "";
        if(!productID.equals("")){
            if(!category.equals("")){
                conditionsString += "AND ps_category_product.id_product =" + productID + "\n";
            }else{
                conditionsString += "AND ps_product.id_product =" + productID + "\n";
            }
        }
        if(!productName.equals("")){  //shop 产品名称
            if(productName.contains("&")){ //如果产品名字里面含有 &， 比如 MM 豆
                productName = productName.replaceAll("'s", "");
            }else{
            productName = productName.replaceAll("'s", "");
            productName = productName.replaceAll("[^A-Za-z0-9 ]", "");
            productName = productName.replaceAll(" ", "%");
            }
            conditionsString +=  "AND ps_product_lang.name like" + "'%" + productName + "%'\n";
        }
        if(!ean.equals("")){
            conditionsString +="AND ps_product.ean13 =" + ean + "\n";
        }
//        if(!category.equals("")){
//        category = category.replaceAll("'s", "");
//        category = category.replaceAll("[^A-Za-z0-9 ]", "");
//        category = category.replaceAll(" ", "%");
//        conditionsString += "AND ps_category_lang.name like" + "'%" + category + "%'\n";
//    }
        if(!if_valid.equals("unSelected")){
            if(if_valid.contains("onsale")){
                conditionsString +=   "AND ps_product.active = 1 \n";
            }else if(if_valid.contains("disabled")){
                conditionsString +=   "AND ps_product.active = 0 \n";
            }else {
                conditionsString +=   "AND ps_product.active = 1 \n";
            }
          }
        if(!expiryDate.equals("")){
            conditionsString +=   "AND (ps_product_expiry.expiry_date <" + "'" + expiryDate +"'" + "OR ps_product_expiry.expiry_date IS NULL)\n";
        }
        HashMap<String, String> submittedData = new HashMap<String, String>();
        submittedData.put("productID",productID);
        submittedData.put("productName",productName);
        submittedData.put("ean",ean);
//        submittedData.put("category",category);
        submittedData.put("if_valid",if_valid);
        submittedData.put("expiryDate",expiryDate);
        Form<SearchBox> newForm = Form.form(SearchBox.class).bind(submittedData);

        if(conditionsString.length() > 0) {
            if(!category.equals("")){
            String sql =
                    "SELECT distinct ps_category_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_product_expiry.expiry_date\n" +
                            "FROM ps_category_product\n" +
                            "LEFT JOIN ps_product\n" +
                            "ON ps_category_product.id_product = ps_product.id_product\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            "AND NOW() > ps_specific_price.from\n" +
                            "AND NOW() < ps_specific_price.to\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_category_product.id_product\n" +
                            "AND ps_image.cover = 1\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_stock_available.id_product = ps_category_product.id_product\n" +
                            "AND ps_stock_available.id_product_attribute = 0 \n" +
//                            "LEFT JOIN ps_category_lang\n" +
//                            "ON ps_category_product.id_category = ps_category_lang.id_category\n" +
                            "LEFT JOIN ps_product_expiry\n" +
                            "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                            "WHERE ps_product.upc != 1 \n" +
                            "AND ps_product.id_product not in (select distinct id_onsale_product from ps_product_buyone_getone)\n" +
                             conditionsString +
                            "Order BY ps_product.active desc, ps_category_product.position";

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_category_product.id_product", "id_product")
                            .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_product.active", "if_valid")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                            .columnMapping("ps_product.ean13", "ean")
                            .columnMapping("ps_product.price", "price")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_image.id_image", "id_image")
                            .columnMapping("ps_stock_available.quantity", "quantity")
                            .columnMapping("ps_product_expiry.expiry_date", "expiry_date")
                            .create();
            com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
            query.setRawSql(rawSql);
            List<StockManagementProduct> list = query.findList();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
            for(StockManagementProduct this_one: list){
                if (this_one.expiry_date ==null){
                    this_one.expiry_date = new Date(0);
                }
                this_one.expiryDate = sdf.format(this_one.expiry_date);

                if(this_one.expiry_date.before(future_date)){
                    this_one.if_expiring = 1;   //加入标记。即将过期
                }
            }
            ArrayList<StockManagementProduct> all_products = new ArrayList<StockManagementProduct>();
            all_products.addAll(list);
            ArrayList<Category> all_category = CategoryAPI.getAllCategories();
            return ok(shopStockPage.render(all_products,newForm,all_category));
            }
            else{
                String sql =
                        "SELECT distinct ps_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_product_expiry.expiry_date\n" +
                                "FROM ps_product\n" +
                                "LEFT JOIN  ps_product_lang\n" +
                                "ON ps_product_lang.id_product = ps_product.id_product\n" +
                                "AND ps_product_lang.id_shop = 1\n" +
                                "LEFT JOIN ps_tax_rule\n" +
                                "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                                "LEFT JOIN ps_tax\n" +
                                "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                                "LEFT JOIN ps_specific_price\n" +
                                "ON ps_product.id_product = ps_specific_price.id_product\n" +
                                "AND NOW() > ps_specific_price.from\n" +
                                "AND NOW() < ps_specific_price.to\n" +
                                "LEFT JOIN ps_image\n" +
                                "ON ps_image.id_product = ps_product.id_product\n" +
                                "AND ps_image.cover = 1\n" +
                                "LEFT JOIN ps_stock_available\n" +
                                "ON ps_stock_available.id_product = ps_product.id_product\n" +
                                "AND ps_stock_available.id_product_attribute = 0 \n" +
                                "LEFT JOIN ps_product_expiry\n" +
                                "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                                "WHERE ps_product.upc != 1 \n" +
                                "AND ps_product.id_product not in (select distinct id_onsale_product from ps_product_buyone_getone)\n" +
                                conditionsString +
                                "Order BY ps_product.active desc, ps_product_lang.name";

                RawSql rawSql =
                        RawSqlBuilder
                                .parse(sql)
                                .columnMapping("ps_product.id_product", "id_product")
                                .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                                .columnMapping("ps_product.active", "if_valid")
                                .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                                .columnMapping("ps_product.ean13", "ean")
                                .columnMapping("ps_product.price", "price")
                                .columnMapping("ps_product_lang.name", "name")
                                .columnMapping("ps_specific_price.reduction", "reduction")
                                .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                                .columnMapping("ps_image.id_image", "id_image")
                                .columnMapping("ps_stock_available.quantity", "quantity")
                                .columnMapping("ps_product_expiry.expiry_date", "expiry_date")
                                .create();
                com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
                query.setRawSql(rawSql);
                List<StockManagementProduct> list = query.findList();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
                for(StockManagementProduct this_one: list){
                    if (this_one.expiry_date ==null){
                        this_one.expiry_date = new Date(0);
                    }
                    this_one.expiryDate = sdf.format(this_one.expiry_date);

                    if(this_one.expiry_date.before(future_date)){
                        this_one.if_expiring = 1;   //加入标记。即将过期
                    }
                }
                ArrayList<StockManagementProduct> all_products = new ArrayList<StockManagementProduct>();
                all_products.addAll(list);
                ArrayList<Category> all_category = CategoryAPI.getAllCategories();
                return ok(shopStockPage.render(all_products,newForm,all_category));

            }
        }else{
            ArrayList<StockManagementProduct> all_products = new ArrayList<StockManagementProduct>();
            ArrayList<Category> all_category = CategoryAPI.getAllCategories();
            return ok(shopStockPage.render(all_products,newForm,all_category)); //ignore
        }
    }

    public static Result allStockProducts() {
//        List<StockProduct> all_products = StockProduct.findAll();
        List<StockProduct> all_products = StockProduct.findValidStock(627);
        return ok(renderCategories(all_products));
    }

    public static Result allStockAbandon() {
        List<StockAbandon> all_abandons = StockAbandon.findAll();
        return ok(renderCategories(all_abandons));
    }

    public static Result allProductExpiry() {
        List<ProductExpiry> all_expiry = ProductExpiry.findAll();
        return ok(renderCategories(all_expiry));
    }

    public static Result allProductLocation() {
        List<ProductLocation> all_location = ProductLocation.findAll();
        return ok(renderCategories(all_location));
    }

    //查询某个产品的详细情况
    public static Result checkProductStock(int id_product) {
        String sql =
                "SELECT distinct ps_category_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_category_lang.name, ps_product_expiry.expiry_date\n" +
                        "FROM ps_category_product\n" +
                        "LEFT JOIN ps_product\n" +
                        "ON ps_category_product.id_product = ps_product.id_product\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        "AND NOW() > ps_specific_price.from\n" +
                        "AND NOW() < ps_specific_price.to\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_category_product.id_product\n" +
                        "AND ps_image.cover = 1\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_category_product.id_product\n" +
                        "AND ps_stock_available.id_product_attribute = 0 \n" +
                        "LEFT JOIN ps_category_lang\n" +
                        "ON ps_category_product.id_category = ps_category_lang.id_category\n" +
                        "LEFT JOIN ps_product_expiry\n" +
                        "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                        "WHERE ps_product.upc != 1 \n" +
                        "AND ps_product.id_product =" + id_product +"\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category_product.id_product", "id_product")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_product.active", "if_valid")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                        .columnMapping("ps_product.ean13", "ean")
                        .columnMapping("ps_product.price", "price")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .columnMapping("ps_category_lang.name", "category_name")
                        .columnMapping("ps_product_expiry.expiry_date", "expiry_date")
                        .create();
        com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
        query.setRawSql(rawSql);
        List<StockManagementProduct> list = query.findList();
        int size = list.size();
        String category_name = "";
        if(size > 1){
            for(int i = 0; i<size; i++){
                category_name = category_name + list.get(i).category_name + "||";
            }
        }
        StockManagementProduct product_info;
        if(size>0){
            product_info = list.get(0);
        }else {
            product_info = new StockManagementProduct();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (product_info.expiry_date == null){
            product_info.expiry_date = new Date(0);
        }
        Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
        if(product_info.expiry_date.before(future_date)){
            product_info.if_expiring = 1;   //加入标记。即将过期
        }
        product_info.expiryDate = sdf.format(product_info.expiry_date);
        product_info.category_name = category_name;
        //当前数量组成， 找到当前这个，和之前一个
        ArrayList<StockProduct> now_and_former = new ArrayList<StockProduct>();
        StockProduct current_one = StockProduct.findCurrentStock(id_product);
        int current_quantity = StockAvailable.findByAttribute(id_product,0).quantity;
        if(current_one !=null){ //存在补货记录
            int sold_quantity = current_one.quantity - current_quantity;
            if(current_one.last_product_group ==0){ //目前只有一批货
                if(sold_quantity >= 0){ //余货卖完了
                    current_one.quantity = current_quantity;
                    now_and_former.add(current_one);
                }else{
                    Date temporary_time = new Date(0);
                    StockProduct former_quantity = new StockProduct(  //进货之前的剩余库存
                            id_product,
                            temporary_time,
                                0,
                                0,
                            Math.abs(sold_quantity),
                            temporary_time,
                                0,
                                0,
                                "",
                            temporary_time,
                                "",
                                0
                    );
                    now_and_former.add(former_quantity);
                    now_and_former.add(current_one);
                }
            }else if(current_one.last_product_group > 0 ){  //之前有存货
                if(sold_quantity >=0){ //余货卖完了
                    current_one.quantity = current_quantity;
                    now_and_former.add(current_one);
                }else{
                    StockProduct former_group = StockProduct.findById(current_one.last_product_group);
                    if(former_group !=null){ //能找到有效补货记录，没有被扔掉
                    former_group.quantity = Math.abs(sold_quantity);
                        now_and_former.add(former_group);
                        now_and_former.add(current_one);
                    }
                }
            }
        }
        if(now_and_former.size()>0){  //转日期格式
            for(StockProduct this_one: now_and_former){
                this_one.person_in_charge = sdf.format(this_one.expiry_date);
            }
        }
        //剩余库存
        List<StockProduct> all_backups = StockProduct.findValidStock(id_product);
        ArrayList<String> expiry_dates = new ArrayList<>();
        if (all_backups.size()>0){
            for(StockProduct this_one: all_backups){
                if(this_one.expiry_date !=null){
                    String expiry_date = sdf.format(this_one.expiry_date);
                    expiry_dates.add(expiry_date);

                }else {
                    this_one.expiry_date = new Date(0);
                    String expiry_date = sdf.format(this_one.expiry_date);
                    expiry_dates.add(expiry_date);
                }
             }
        }
        //Abandon History
        List<StockAbandon> all_abandons = StockAbandon.findByProduct(id_product);
        ArrayList<String> abandon_expires = new ArrayList<>();
        if (all_abandons.size()>0){
            for(StockAbandon abandon: all_abandons){
                if(abandon.expiry_date !=null){
                    String expiry_date = sdf.format(abandon.expiry_date);
                    abandon_expires.add(expiry_date);
                }else {
                    abandon.expiry_date = new Date(0);
                    String expiry_date = sdf.format(abandon.expiry_date);
                    abandon_expires.add(expiry_date);
                }
            }
        }
        ProductLocation location = ProductLocation.findByProduct(id_product);
        String product_location;
        if(location == null){
            product_location = "";
        }else {
//            product_location = location.zone + "-" + location.layer + "-" + location.part + location.position_number;
            product_location = location.zone + "-" + location.part + location.position_number; //布局更改，去掉层数
        }
        Form<OnSalePerson> personForm = Form.form(OnSalePerson.class);
        Form<SearchBox> dateForm = Form.form(SearchBox.class);
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(productStockPage.render(personForm,product_info,all_backups,dateForm,expiry_dates,all_abandons,abandon_expires,now_and_former,product_location,all_category));
    }

    public static Result showAddStockPage(int id_product) {
        //不涉及产品过期日期
        String sql =
                "SELECT distinct ps_category_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_category_lang.name\n" +
                        "FROM ps_category_product\n" +
                        "LEFT JOIN ps_product\n" +
                        "ON ps_category_product.id_product = ps_product.id_product\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        "AND NOW() > ps_specific_price.from\n" +
                        "AND NOW() < ps_specific_price.to\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_category_product.id_product\n" +
                        "AND ps_image.cover = 1\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_category_product.id_product\n" +
                        "AND ps_stock_available.id_product_attribute = 0 \n" +
                        "LEFT JOIN ps_category_lang\n" +
                        "ON ps_category_product.id_category = ps_category_lang.id_category\n" +
                        "WHERE ps_product.upc != 1 \n" +
                        "AND ps_product.id_product =" + id_product +"\n";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category_product.id_product", "id_product")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_product.active", "if_valid")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                        .columnMapping("ps_product.ean13", "ean")
                        .columnMapping("ps_product.price", "price")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .columnMapping("ps_category_lang.name", "category_name")
                        .create();
        com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
        query.setRawSql(rawSql);
        List<StockManagementProduct> list = query.findList();
        StockManagementProduct product_info;
        if (list.size()==0){
            product_info = new StockManagementProduct();
        }else {
            product_info = list.get(0);
        }
        Form<AddStockForm> addStockForm = Form.form(AddStockForm.class);
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(addInventoryPage.render(addStockForm,product_info,all_category));
    }

    public static Result addStockToProduct(int id_product) throws ParseException {

        DynamicForm newStockForm = Form.form().bindFromRequest();  //receive
        String product_quantity = newStockForm.get("quantity");
        String expiryDate = newStockForm.get("expiry_date");
        String if_on_sale = newStockForm.get("if_on_sale");
        String person_in_charge = newStockForm.get("person_add");
        if(product_quantity ==null ||expiryDate ==null || expiryDate.equals("")||if_on_sale ==null||person_in_charge ==null||person_in_charge.replaceAll(" ","").equals("")){
            return badRequest(errorMessageTemplate.render("Important information missing, please try again!"));
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date expiry_date;
            try{
                expiry_date = sdf.parse(expiryDate);
            }catch (ParseException e){
                return badRequest(errorMessageTemplate.render("Bad Date Format, Please Try Again!"));
            }
        int quantity = Integer.valueOf(product_quantity);
        Date current_time = new Date();
        if(if_on_sale.contains("yes")){  //添加库存并且上架
            int current_product_quantity = getQuantity(id_product);
            if(current_product_quantity > 0){
                return badRequest(errorMessageTemplate.render("You cannot add new Stock before the last stock is sold out!"));
            }
        StockProduct current_one = StockProduct.findCurrentStock(id_product);
            int last_product_group = 0;
            //ps_stock_product
            if(current_one != null) {  //如果存在正在卖的那批货
                current_one.if_active = 0;
                current_one.last_update = current_time;
                current_one.save();
                last_product_group = current_one.id_stock_product;
            }
            //ps_product  turn to active = 1
            updateOneRecord(id_product,1,1);
            //ps_stock_available
            StockAvailable original_stock = StockAvailable.findByAttribute(id_product,0);
            int original_quantity = original_stock.quantity;
            original_stock.quantity = quantity + original_quantity; //添加进当前产品总数
            original_stock.save();
            //ps_stock_product
                StockProduct new_one = new StockProduct(
                id_product,
                current_time,
                1,
                0,
                quantity,
                expiry_date,
                original_quantity,
                1,
                person_in_charge,
                current_time,
                person_in_charge,
                last_product_group  //记录上一批货
                );
                new_one.save();
            //ps_product_expiry  更新最新库存时间
            ProductExpiry former_expiry = ProductExpiry.findByProduct(id_product);
            if (former_expiry != null){
                former_expiry.expiry_date = expiry_date;
                former_expiry.save();
            }else {
                ProductExpiry new_expiry = new ProductExpiry(
                        id_product,
                        expiry_date
                );
                new_expiry.save();
            }
        }else if(if_on_sale.contains("no")){ //只是添加进库存，先不上架
            //ps_stock_available
            StockAvailable original_stock = StockAvailable.findByAttribute(id_product,0);
            int original_quantity = original_stock.quantity; //原来的库存数量
            //ps_stock_product
            StockProduct new_one = new StockProduct(
                    id_product,
                    current_time,
                    0,
                    1,
                    quantity,
                    expiry_date,
                    original_quantity,
                    1,
                    person_in_charge,
                    current_time,
                    "",
                    0
            );
            new_one.save();
        }
        return redirect(routes.StockAPI.checkProductStock(id_product));
    }

    public static Result addStockToSale(int id_stock_product, int id_product){
        DynamicForm newStockForm = Form.form().bindFromRequest();  //receive
        String staff_name = newStockForm.get("staff_name");
        if(staff_name ==null || staff_name.replaceAll(" ","").equals("")){
            return badRequest(errorMessageTemplate.render("You have to put your name correctly"));
        }else{    //添加新货到正在卖
            //如果当前产品数量不是0， 就不能上架
            int current_product_quantity = getQuantity(id_product);
            if(current_product_quantity > 0){
                return badRequest(errorMessageTemplate.render("You cannot add new Stock before the last stock is sold out!"));
            }
            StockProduct this_group_product = StockProduct.findById(id_stock_product);  //找到那批货
            Date current_time = new Date();
           //先找到当前正在卖的那批货
            StockProduct current_one = StockProduct.findCurrentStock(id_product);
            int last_product_group = 0;
            //ps_stock_product
            if(current_one != null) {  //如果存在正在卖的那批货
                current_one.if_active = 0;
                current_one.last_update = current_time;
                current_one.save();
                last_product_group = current_one.id_stock_product;
            }
            //ps_stock_available
            StockAvailable original_stock = StockAvailable.findByAttribute(id_product,0);
            int original_quantity = original_stock.quantity;
            original_stock.quantity = this_group_product.quantity + original_quantity; //添加进当前产品总数
            original_stock.save();
            //ps_product  turn to active = 1
            updateOneRecord(id_product,1,1);
            //ps_stock_product
            this_group_product.on_sale_person = staff_name;
            this_group_product.last_product_group = last_product_group;
            this_group_product.if_backup = 0;
            this_group_product.if_active = 1;
            this_group_product.last_update = current_time;
            this_group_product.original_quantity = original_quantity;   //Quantity - current_quantity (stock available)= how many of this products sold
            this_group_product.save();
            //ps_product_expiry  更新最新库存时间
            ProductExpiry former_expiry = ProductExpiry.findByProduct(id_product);
            if (former_expiry !=null){
            former_expiry.expiry_date = this_group_product.expiry_date;
            former_expiry.save();
            }else {
                ProductExpiry new_record = new ProductExpiry(id_product,this_group_product.expiry_date);
                new_record.save();
            }
            return redirect(routes.StockAPI.checkProductStock(id_product));
        }
    }
    public static Result showAbandonPage(int id_product) {
        //不涉及产品过期日期
        String sql =
                "SELECT distinct ps_category_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_category_lang.name\n" +
                        "FROM ps_category_product\n" +
                        "LEFT JOIN ps_product\n" +
                        "ON ps_category_product.id_product = ps_product.id_product\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        "AND NOW() > ps_specific_price.from\n" +
                        "AND NOW() < ps_specific_price.to\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_category_product.id_product\n" +
                        "AND ps_image.cover = 1\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_category_product.id_product\n" +
                        "AND ps_stock_available.id_product_attribute = 0 \n" +
                        "LEFT JOIN ps_category_lang\n" +
                        "ON ps_category_product.id_category = ps_category_lang.id_category\n" +
                        "WHERE ps_product.upc != 1 \n" +
                        "AND ps_product.id_product =" + id_product +"\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category_product.id_product", "id_product")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_product.active", "if_valid")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                        .columnMapping("ps_product.ean13", "ean")
                        .columnMapping("ps_product.price", "price")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .columnMapping("ps_category_lang.name", "category_name")
                        .create();
        com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
        query.setRawSql(rawSql);
        List<StockManagementProduct> list = query.findList();
        StockManagementProduct product_info;
        if (list.size()==0){
        product_info = new StockManagementProduct();
        }else {
        product_info = list.get(0);
        }
        Form<AddStockForm> addStockForm = Form.form(AddStockForm.class);
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(abandonPage.render(addStockForm,product_info,all_category));
    }

    public static Result abandonProduct(int id_product) throws ParseException {
        DynamicForm newStockForm = Form.form().bindFromRequest();  //receive
        String product_quantity = newStockForm.get("quantity");
        String expiryDate = newStockForm.get("expiry_date");
        String person_in_charge = newStockForm.get("person_add");
        if (expiryDate == null || expiryDate.equals("") || product_quantity == null || person_in_charge == null) {
            return badRequest(errorMessageTemplate.render("You have to put expiry Date & product quantity"));
        }
        if(person_in_charge ==null || person_in_charge.replaceAll(" ","").equals("")){
            return  badRequest(errorMessageTemplate.render("You have to put your name there!"));
        }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date expiry_date;
            try{
                expiry_date = sdf.parse(expiryDate);
            }catch (ParseException e){
                return badRequest(errorMessageTemplate.render("Invalid Date format! Please try to input correct data."));
            }
            int quantity = Integer.valueOf(product_quantity);
            Date current_time = new Date();
            //ps_stock_product下架
            List<StockProduct> that_group = StockProduct.findThatGroup(id_product, expiry_date);  //找到对应的那批进货产品
            int id_stock_product = 0;
            if (that_group.size() != 0) {
                for (StockProduct this_one : that_group) {
                    id_stock_product = this_one.id_stock_product;
                    this_one.last_update = current_time;
                    this_one.if_active = 0;
                    this_one.valid = 0;
                    this_one.save();
                }
            }
            //ps_stock_available  更新数量， 减去扔掉的产品数量
            StockAvailable current_quantity = StockAvailable.findByAttribute(id_product, 0);
            int new_quantity = current_quantity.quantity - quantity;
            if(new_quantity <=0){   //这件产品的数量小于0
                new_quantity = 0;
                //ps_product  turn to active = 0
                updateOneRecord(id_product,0,0);
            }
            current_quantity.quantity = new_quantity;
            current_quantity.save();
            if(new_quantity <=0){
                //进行判断是否参与买一送一活动
                List<BuyOneGetOne> if_exist_original_product = BuyOneGetOne.findByOriginalProduct(id_product);
                List<BuyOneGetOne> if_exist_sales_product = BuyOneGetOne.findBySaleProductId(id_product);
                if (if_exist_original_product.size() > 0) { //这件产品参与买一送一，数量已经为0
                    // 把买一送一那件产品正在做活动的产品数量变成0
                    for(BuyOneGetOne this_sales_product: if_exist_original_product){
                        int sales_product_id = this_sales_product.id_onsale_product;
                        StockAvailable sales_product_stock = StockAvailable.findByAttribute(sales_product_id,0);
                        int sales_product_quantity = sales_product_stock.quantity;
                        if(sales_product_quantity !=0){
                            sales_product_stock.quantity = 0;
                            sales_product_stock.save();
                            updateOneRecord(sales_product_id,0,0); //把买一送一的产品下架
                        }
                    }
                }else if (if_exist_sales_product.size() > 0) { //这件产品是买一送一产品，数量已经卖完了
                    // 查看每件原产品数量是不是已经被改成0
                    for(BuyOneGetOne this_original_product: if_exist_sales_product){
                        int original_product_id = this_original_product.id_real_product;
                        StockAvailable original_product_stock = StockAvailable.findByAttribute(original_product_id,0);
                        int original_product_quantity = original_product_stock.quantity;
                        if(original_product_quantity !=0){
                            original_product_stock.quantity = 0;
                            original_product_stock.save();
                            updateOneRecord(original_product_id,0,0); //把原产品下架
                        }
                    }
                }
            }
            //ps_stock_abandon
            StockAbandon new_record = new StockAbandon(
                    id_product,
                    expiry_date,
                    quantity,
                    current_time,
                    person_in_charge,
                    id_stock_product
            );
            new_record.save();
            return redirect(routes.StockAPI.checkProductStock(id_product));

    }

    public static Result showAddExpiryPage(int id_product) {
        //不涉及产品过期日期
        String sql =
                "SELECT distinct ps_category_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_category_lang.name\n" +
                        "FROM ps_category_product\n" +
                        "LEFT JOIN ps_product\n" +
                        "ON ps_category_product.id_product = ps_product.id_product\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        "AND NOW() > ps_specific_price.from\n" +
                        "AND NOW() < ps_specific_price.to\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_category_product.id_product\n" +
                        "AND ps_image.cover = 1\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_category_product.id_product\n" +
                        "AND ps_stock_available.id_product_attribute = 0 \n" +
                        "LEFT JOIN ps_category_lang\n" +
                        "ON ps_category_product.id_category = ps_category_lang.id_category\n" +
                        "WHERE ps_product.upc != 1 \n" +
                        "AND ps_product.id_product =" + id_product +"\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category_product.id_product", "id_product")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_product.active", "if_valid")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                        .columnMapping("ps_product.ean13", "ean")
                        .columnMapping("ps_product.price", "price")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .columnMapping("ps_category_lang.name", "category_name")
                        .create();
        com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
        query.setRawSql(rawSql);
        List<StockManagementProduct> list = query.findList();
        StockManagementProduct product_info;
        if (list.size()==0){
            product_info = new StockManagementProduct();
        }else {
            product_info = list.get(0);
        }
        Form<AddStockForm> addStockForm = Form.form(AddStockForm.class);
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(addExpiryPage.render(addStockForm,product_info,all_category));
    }

    public static Result addExpiryToProduct(int id_product) throws ParseException {
        DynamicForm newStockForm = Form.form().bindFromRequest();  //receive
        String expiryDate = newStockForm.get("expiry_date");
        if(expiryDate ==null){
            expiryDate = "0000-00-00";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date expiry_date;
        try{
            expiry_date= sdf.parse(expiryDate);
        }catch (ParseException e){
            return redirect(routes.StockAPI.checkProductStock(id_product));
        }
        ProductExpiry product_expiry = ProductExpiry.findByProduct(id_product);
        if(product_expiry == null){
            ProductExpiry new_record = new ProductExpiry(id_product,expiry_date);
            new_record.save();
        }else{
            product_expiry.expiry_date = expiry_date;
            product_expiry.save();
        }
        return redirect(routes.StockAPI.checkProductStock(id_product));
    }

    public static Result showAddLocationPage(int id_product) {
        //不涉及产品过期日期
        String sql =
                "SELECT distinct ps_category_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_category_lang.name\n" +
                        "FROM ps_category_product\n" +
                        "LEFT JOIN ps_product\n" +
                        "ON ps_category_product.id_product = ps_product.id_product\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        "AND NOW() > ps_specific_price.from\n" +
                        "AND NOW() < ps_specific_price.to\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_category_product.id_product\n" +
                        "AND ps_image.cover = 1\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_category_product.id_product\n" +
                        "AND ps_stock_available.id_product_attribute = 0 \n" +
                        "LEFT JOIN ps_category_lang\n" +
                        "ON ps_category_product.id_category = ps_category_lang.id_category\n" +
                        "WHERE ps_product.upc != 1 \n" +
                        "AND ps_product.id_product =" + id_product +"\n";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category_product.id_product", "id_product")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_product.active", "if_valid")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                        .columnMapping("ps_product.ean13", "ean")
                        .columnMapping("ps_product.price", "price")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .columnMapping("ps_category_lang.name", "category_name")
                        .create();
        com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
        query.setRawSql(rawSql);
        List<StockManagementProduct> list = query.findList();
        StockManagementProduct product_info;
        if (list.size()==0){
            product_info = new StockManagementProduct();
        }else {
            product_info = list.get(0);
        }
        Form<AddStockForm> addStockForm = Form.form(AddStockForm.class);
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(addLocationPage.render(addStockForm,product_info,all_category));
    }

    public static Result addLocationToProduct(int id_product) throws ParseException {
        DynamicForm newStockForm = Form.form().bindFromRequest();  //receive
        String zone = newStockForm.get("expiry_date");
        String layer = newStockForm.get("if_on_sale");
        String part = newStockForm.get("person_add");
        String position_number = newStockForm.get("quantity");
        if(zone ==null || layer ==null || position_number == null || part == null|| zone.equals("")|| position_number.equals("") || part.equals("")){
            return badRequest(errorMessageTemplate.render("Important location information missing"));
        }else{
            int number_position;
            try{
               number_position = Integer.valueOf(position_number);
            }catch (Exception e){
               number_position = 0;
            }

            if (layer.equals("N/A")){
                layer = "";
            }

            if (part.equals("N/A")){
                part = "";
            }
        ProductLocation this_one = ProductLocation.findByProduct(id_product);
            if(this_one == null){
                ProductLocation new_one = new ProductLocation(
                        id_product,
                        zone,
                        layer,
                        part,
                        number_position
                );
                new_one.save();
            }else {
                this_one.zone = zone;
                this_one.layer = layer;
                this_one.part = part;
                this_one.position_number = number_position;
                this_one.save();
            }
        return redirect(routes.StockAPI.checkProductStock(id_product));
        }
    }

    //search product through location
    public static Result showSearchLocationPage() {
        Form<AddStockForm> addStockForm = Form.form(AddStockForm.class);
        ArrayList<Category> all_category = CategoryAPI.getAllCategories();
        return ok(searchLocationPage.render(addStockForm,all_category));
    }

    public static Result showProductsByLocation() throws ParseException {
        DynamicForm newStockForm = Form.form().bindFromRequest();  //receive
        String zone = newStockForm.get("expiry_date");
        String layer = newStockForm.get("if_on_sale");
        String part = newStockForm.get("person_add");
        String position_number = newStockForm.get("quantity");
        if(zone ==null || layer ==null || position_number == null || part == null|| zone.equals("")|| position_number.equals("") || part.equals("")){
            return badRequest(errorMessageTemplate.render("Invalid product location format.Please try again."));
        }else{
            int number_position;
            try{
                number_position = Integer.valueOf(position_number);
            }catch (Exception e){
                number_position = 0;
            }

            if (layer.equals("N/A")){
                layer = "";
            }

            if (part.equals("N/A")){
                part = "";
            }
            String product_id_string = "";

            //start
            String sql_1 =
                    "SELECT ps_product_location.id_product\n" +
                            "from ps_product_location\n" +
                            "where zone = '" + zone + "'\n" +
                            "and layer = '" + layer + "'\n" +
                            "and part = '" + part + "'\n" +
                            "and position_number = " + number_position ;
            RawSql rawSql_1 =
                    RawSqlBuilder
                            .parse(sql_1)
                            .columnMapping("ps_product_location.id_product", "quantity")
                            .create();
            com.avaje.ebean.Query<ProductQuantity> query_1 = Ebean.find(ProductQuantity.class);
            query_1.setRawSql(rawSql_1);
            List<ProductQuantity> list_1 = query_1.findList();
            int number_of_product = list_1.size();
            if(number_of_product>0){

            if(number_of_product==1){
                product_id_string += "AND ps_product.id_product =" + list_1.get(0).quantity +"\n";

            } else if(number_of_product > 1){
                product_id_string += "AND ps_product.id_product =" + list_1.get(0).quantity +"\n";
              for(int x = 1; x<number_of_product; x++){
                  product_id_string += "OR ps_product.id_product =" + list_1.get(x).quantity +"\n";
              }
            }
            //end
            Form<SearchBox> dateForm = Form.form(SearchBox.class);
            String sql =
                    "SELECT distinct ps_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_product_expiry.expiry_date\n" +
                            "FROM ps_product\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            "AND NOW() > ps_specific_price.from\n" +
                            "AND NOW() < ps_specific_price.to\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_product.id_product\n" +
                            "AND ps_image.cover = 1\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_stock_available.id_product = ps_product.id_product\n" +
                            "AND ps_stock_available.id_product_attribute = 0 \n" +
                            "LEFT JOIN ps_product_expiry\n" +
                            "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                            "WHERE ps_product.upc != 1\n" +
                             product_id_string +
                            "Order BY ps_product.active desc, ps_product_lang.name" ;

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_product.id_product", "id_product")
                            .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_product.active", "if_valid")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                            .columnMapping("ps_product.ean13", "ean")
                            .columnMapping("ps_product.price", "price")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_image.id_image", "id_image")
                            .columnMapping("ps_stock_available.quantity", "quantity")
                            .columnMapping("ps_product_expiry.expiry_date", "expiry_date")    //expiry date
                            .create();
            com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
            query.setRawSql(rawSql);
            List<StockManagementProduct> list = query.findList();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
            for(StockManagementProduct this_one: list){  //循环每件产品
                if (this_one.expiry_date ==null){
                    this_one.expiry_date = new Date(0);
                }
                this_one.expiryDate = sdf.format(this_one.expiry_date);

                if(this_one.expiry_date.before(future_date)){
                    this_one.if_expiring = 1;   //加入标记。即将过期
                }
            }
            ArrayList<StockManagementProduct> all_products = new ArrayList<StockManagementProduct>();
            all_products.addAll(list);
            ArrayList<Category> all_category = CategoryAPI.getAllCategories();
            return ok(shopStockPage.render(all_products,dateForm,all_category));
            }else {
             return redirect(routes.StockAPI.showSearchLocationPage());
            }
        }
    }

    /**
     * 主页上一个按钮，用来打开显示买一送一的产品组合页面
     * */
    public static Result bundleSalesPage(){
        String email = session("shopAssistantEmail");
        // if in session
        if (email != null && email.equals("chenzhi1111@gmail.com")) {
//            Form<SearchBox> dateForm = Form.form(SearchBox.class);
            String sql =
                    "SELECT distinct ps_product.id_product, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image,ps_stock_available.quantity, ps_product_expiry.expiry_date,ps_product_buyone_getone.id_onsale_product, ps_product_buyone_getone.id_bogo, ps_product_buyone_getone.basic_unit, ps_product_buyone_getone.bundle_number, ps_product_buyone_getone.start_date, ps_product_buyone_getone.end_date, ps_product_buyone_getone.admin_name\n" +
                            "FROM ps_product\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            "AND NOW() > ps_specific_price.from\n" +
                            "AND NOW() < ps_specific_price.to\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_product.id_product\n" +
                            "AND ps_image.cover = 1\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_stock_available.id_product = ps_product.id_product\n" +
                            "AND ps_stock_available.id_product_attribute = 0\n" +
                            "LEFT JOIN ps_product_expiry\n" +
                            "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                            "LEFT JOIN ps_product_buyone_getone\n" +
                            "ON ps_product_buyone_getone.id_real_product = ps_product.id_product\n" +
                            "WHERE ps_product.upc != 1 \n" +
                            "and ps_product_buyone_getone.if_valid = 1\n" +
                            "and ps_product_buyone_getone.start_date < now()\n" +
                            "and ps_product_buyone_getone.end_date > now()\n" +
                            "Order BY ps_product_buyone_getone.id_onsale_product, ps_product.active desc";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_product.id_product", "id_product")
                            .columnMapping("ps_product.active", "if_active")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                            .columnMapping("ps_product.price", "price")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_image.id_image", "id_image")
                            .columnMapping("ps_stock_available.quantity", "quantity")
                            .columnMapping("ps_product_expiry.expiry_date", "expiry_date")
                            .columnMapping("ps_product_buyone_getone.id_onsale_product", "id_onsale_product")
                            .columnMapping("ps_product_buyone_getone.id_bogo", "id_bogo")
                            .columnMapping("ps_product_buyone_getone.basic_unit", "basic_unit")
                            .columnMapping("ps_product_buyone_getone.bundle_number", "bundle_number")
                            .columnMapping("ps_product_buyone_getone.start_date", "start_date")
                            .columnMapping("ps_product_buyone_getone.end_date", "end_date")
                            .columnMapping("ps_product_buyone_getone.admin_name", "admin_name")
                            .create();
            com.avaje.ebean.Query<BuyOneGetOneProduct> query = Ebean.find(BuyOneGetOneProduct.class);
            query.setRawSql(rawSql);
            List<BuyOneGetOneProduct> list = query.findList();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
            for (BuyOneGetOneProduct this_one : list) {  //循环每件产品
                if (this_one.expiry_date == null) {
                    this_one.expiry_date = new Date(0);
                }
                this_one.expiryDate = sdf.format(this_one.expiry_date);
                this_one.startDate = sdf.format(this_one.start_date);
                this_one.endDate = sdf.format(this_one.end_date);
                if (this_one.expiry_date.before(future_date)) {
                    this_one.if_expiring = 1;   //加入标记。即将过期
                }
                if(this_one.reduction ==null){
                    this_one.reduction = BigDecimal.valueOf(0.00);
                }
                this_one.id_target_img = get_image_id(this_one.id_onsale_product).quantity;
            }
            ArrayList<BuyOneGetOneProduct> all_products = new ArrayList<BuyOneGetOneProduct>();
            all_products.addAll(list);
//            ArrayList<Category> all_category = CategoryAPI.getAllCategories();
            Form<OnSalePerson> personForm = Form.form(OnSalePerson.class);
            Form<BundleProductRecord> bundleRecordForm = Form.form(BundleProductRecord.class);
            return ok(bundleProductsPage.render(all_products,personForm,bundleRecordForm));
//            return ok(renderCategories(all_category));
        }else {
            flash("error","please log in first");
            return redirect(WebAPI.shopAssistantLogin());
        }
    }

    /**
     * 添加一个新的买一送一规则
     * */
    public static Result addOneBundleRule(){
        Form<BundleProductRecord> bundleProductRecordForm = Form.form(BundleProductRecord.class).bindFromRequest(); //get a new record form from HTTP
//        DynamicForm newStockForm = Form.form().bindFromRequest();  //receive
        if(bundleProductRecordForm.hasErrors()){
            return badRequest(errorMessageTemplate.render("Data format is not correct"));
        } else{   //添加新纪录， 先判断是否存在
            //得到一堆参数
            BundleProductRecord new_record = bundleProductRecordForm.get(); //得到对象
            int original_product_id = new_record.product_id;
            int basic_unit = new_record.basic_unit;
            int bundle_number = new_record.bundle_number;
            int id_sales_product = new_record.target_product_id;
            Date start_date = new_record.start_date;
            Date end_date = new_record.end_date;
            Date current_time = new Date();
            if(start_date.after(end_date)){
                return badRequest(errorMessageTemplate.render("Start Time cannot be later than End time!"));
            }
            if(end_date.before(current_time)){
                return badRequest(errorMessageTemplate.render("End time cannot be earlier than NOW!"));
            }
            if(DatabaseProduct.findById(id_sales_product) == null){
                return badRequest(errorMessageTemplate.render("You have to create the new Product for Bundle Sale!"));
            }
            if(DatabaseProduct.findById(original_product_id) == null){
                return badRequest(errorMessageTemplate.render("The original product doesn't exist!"));
            }
            BuyOneGetOne if_exist = BuyOneGetOne.findByCombination(original_product_id,id_sales_product,basic_unit,bundle_number);
            if(if_exist ==null){  //如果不存在重复的bundle rule， 那就可以继续添加新的rule
                   BuyOneGetOne new_bogo = new BuyOneGetOne(
                   id_sales_product,
                   original_product_id,
                   start_date,
                   end_date,
                   current_time,
                   current_time,
                   basic_unit,
                   bundle_number,
                   new_record.admin_name,
                   1
                   );
                   new_bogo.save();
                return redirect(routes.StockAPI.bundleSalesPage());
            }else {
                return badRequest(errorMessageTemplate.render("Similar bundle rule already exists"));
            }
        }
    }
    /**
     * 打开修改原有记录的界面
     * */
    public static Result openEditBundlePage(int id_bogo){
        BuyOneGetOne existing_record = BuyOneGetOne.findById(id_bogo);
        if(existing_record == null){
            return badRequest(errorMessageTemplate.render("This record doesn't exist."));
        }else{
            String sql =
                    "SELECT distinct ps_product.id_product, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image,ps_stock_available.quantity, ps_product_expiry.expiry_date, ps_product_buyone_getone.id_bogo\n" +
                            "FROM ps_product\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            "AND NOW() > ps_specific_price.from\n" +
                            "AND NOW() < ps_specific_price.to\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_product.id_product\n" +
                            "AND ps_image.cover = 1\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_stock_available.id_product = ps_product.id_product\n" +
                            "AND ps_stock_available.id_product_attribute = 0\n" +
                            "LEFT JOIN ps_product_expiry\n" +
                            "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                            "LEFT JOIN ps_product_buyone_getone\n" +
                            "ON ps_product_buyone_getone.id_onsale_product = ps_product.id_product\n" +
                            "WHERE ps_product.upc != 1 \n" +
                            "and ps_product_buyone_getone.id_bogo = " + id_bogo;

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_product.id_product", "id_onsale_product")
                            .columnMapping("ps_product.active", "if_active")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                            .columnMapping("ps_product.price", "price")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_image.id_image", "id_image")
                            .columnMapping("ps_stock_available.quantity", "quantity")
                            .columnMapping("ps_product_expiry.expiry_date", "expiry_date")
                            .columnMapping("ps_product_buyone_getone.id_bogo", "id_bogo")
                            .create();
            com.avaje.ebean.Query<BuyOneGetOneProduct> query = Ebean.find(BuyOneGetOneProduct.class);
            query.setRawSql(rawSql);
            BuyOneGetOneProduct that_record = query.findUnique();
            that_record.id_target_img = get_image_id(existing_record.id_real_product).quantity; //找到原产品的id_image
            Date startDate = existing_record.start_date;
            Date endDate = existing_record.end_date;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String start_date;
            String end_date;
            String expiryDate;
            try{
                 start_date = sdf.format(startDate);
                 end_date = sdf.format(endDate);
                expiryDate = sdf.format(that_record.expiry_date);
            }catch (Exception e){
                start_date = "1970-01-01";
                end_date = "1970-01-01";
                expiryDate = "1970-01-01";
            }
            that_record.expiryDate = expiryDate;
            HashMap<String, String> dataContainer = new HashMap<String, String>();
            dataContainer.put("product_id",String.valueOf(existing_record.id_real_product));
            dataContainer.put("basic_unit",String.valueOf(existing_record.basic_unit));
            dataContainer.put("bundle_number",String.valueOf(existing_record.bundle_number));
            dataContainer.put("start_date",start_date);
            dataContainer.put("end_date",end_date);
            dataContainer.put("target_product_id",String.valueOf(existing_record.id_onsale_product));
            dataContainer.put("admin_name","");
            Form<BundleProductRecord> bundleRecordForm = Form.form(BundleProductRecord.class).bind(dataContainer); //收到上一个页面传过来的信息，节省数据库资源
//            ArrayList<Category> all_category = CategoryAPI.getAllCategories();
            return ok(editBundleRulePage.render(bundleRecordForm,that_record));
        }
    }

    /**
     * 修改原有的买一送一规则, HTTP: POST 请求
     * Update 原有的记录
     * */
    public static Result editOneBundleRule(int id_bogo){
        Form<BundleProductRecord> bundleProductRecordForm = Form.form(BundleProductRecord.class).bindFromRequest(); //get a new record form from HTTP
        if(bundleProductRecordForm.hasErrors()){
            return badRequest(errorMessageTemplate.render("Data format is not correct"));
        } else{
            //得到一堆参数
            BundleProductRecord new_record = bundleProductRecordForm.get(); //得到对象
            int original_product_id = new_record.product_id;  //原产品（单件）
            int basic_unit = new_record.basic_unit;
            int bundle_number = new_record.bundle_number;
            int id_sales_product = new_record.target_product_id;  //bundle 单品
            Date end_date = new_record.end_date;
            Date current_time = new Date();
            if(end_date.before(current_time)){
                return badRequest(errorMessageTemplate.render("End time cannot be earlier than NOW!"));
            }
            if(DatabaseProduct.findById(id_sales_product) == null){
                return badRequest(errorMessageTemplate.render("You have to create the new Product for Bundle Sale!"));
            }
            if(DatabaseProduct.findById(original_product_id) == null){
                return badRequest(errorMessageTemplate.render("The original product doesn't exist!"));
            }
            BuyOneGetOne if_exist = BuyOneGetOne.findById(id_bogo); //原来的那条记录
            if(if_exist !=null){  //存在已经有的记录，就可以修改
                if_exist.update(
                        id_sales_product,
                        original_product_id,
                        new_record.start_date,
                        end_date,
                        current_time,
                        basic_unit,
                        bundle_number,
                        new_record.admin_name,
                        1
                );
                return redirect(routes.StockAPI.bundleSalesPage());
            }else {
                return notFound(errorMessageTemplate.render("No such record before"));
            }
        }
    }

    public static Result deleteOneBundleRule(int id_bogo){
        DynamicForm newStockForm = Form.form().bindFromRequest();  //receive
        String staff_name = newStockForm.get("staff_name");
        if(staff_name ==null || staff_name.equals("")){
            return badRequest(errorMessageTemplate.render("You need to put your name in the blank"));
        }else{    //根据 bogo id 来找到相应的捆绑销售规则
            BuyOneGetOne this_bundle_rule = BuyOneGetOne.findById(id_bogo);
            Date current_time = new Date();
            int sale_product_id = this_bundle_rule.id_onsale_product; //这个产品准备下架，买一送一停止
            int if_valid = this_bundle_rule.if_valid;
            if(if_valid ==1){
             this_bundle_rule.admin_name = staff_name;
             this_bundle_rule.date_upd = current_time;
             this_bundle_rule.if_valid = 0;
             this_bundle_rule.save();
            }
            //准备下架那件买一送一的产品，因为并不是真正的产品
            DatabaseProduct that_product = DatabaseProduct.findById(sale_product_id);
            if(that_product!=null && that_product.active==1){ //如果存在那件产品，并且还在卖
                updateOneRecord(sale_product_id,0,0);
            }
            return redirect(routes.StockAPI.bundleSalesPage());
        }
    }
}