package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.database.DealTimeStamp;
import models.sqlContainer.MealDealProduct;
import models.database.StockAvailable;
import play.data.validation.Constraints;
import play.db.ebean.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//import static parsers.JsonParser.renderProduct;

public class MealDealAPI extends Controller
{
    public static class SearchBox{
        public String deal_product_id = "";
        public String id_product_attribute ="";
        public String id_product_shop = "";
        public String key_word ="";
    }

    public static class OnSalePerson{
        @Constraints.Required
        public String staff_name;
    }

    public static class AddStockForm{
        @Constraints.Required
        public int quantity;       //position_number
        @Constraints.Required
        public String expiry_date;  //zone

        public String if_on_sale;   //layer
        @Constraints.Required
        public String person_add;   //part
    }

    @Transactional
    public static Result upDateTimestamp() {  //更新库存数量
        Date current_time = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp_1 = sdf.format(current_time);
        DealTimeStamp last_time = DealTimeStamp.findById(1);
        Date last_access_time = last_time.last_access_time;
        String timestamp_2 = sdf.format(last_access_time);
        last_time.last_access_time = current_time;
        last_time.save();

            String sql =
                    "select ps_deal_sale_table.id_product_shop, ps_deal_sale_table.key_word,count(ps_deal_sale_table.id_product_shop)\n" +
                            "from ps_orders, ps_order_detail, ps_deal_sale_table\n" +
                            "where ps_orders.id_order = ps_order_detail.id_order\n" +
                            "and ps_orders.invoice_date >= " + "'" + timestamp_2+"'" + "\n" +
                            "and ps_orders.invoice_date < " + "'" + timestamp_1 + "'" + "\n" +
                            "and ps_order_detail.product_id = ps_deal_sale_table.deal_product_id\n" +
                            "and  ps_order_detail.product_name like CONCAT('%', ps_deal_sale_table.key_word, '%')\n" +
                            "group by ps_deal_sale_table.id_product_shop\n";

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_deal_sale_table.id_product_shop", "id_product_shop")
                            .columnMapping("ps_deal_sale_table.key_word", "key_word")
                            .columnMapping("count(ps_deal_sale_table.id_product_shop)", "purchased_quantity")
                            .create();

            com.avaje.ebean.Query<MealDealProduct> query = Ebean.find(MealDealProduct.class);
            query.setRawSql(rawSql);
            List<MealDealProduct> list = query.findList();
            if(list.size() >0){
                for(MealDealProduct this_one: list){
                   int bought_quantity = this_one.purchased_quantity;
                    StockAvailable current_quantity = StockAvailable.findByAttribute(this_one.id_product_shop,0);
                    current_quantity.quantity = current_quantity.quantity - bought_quantity;
                    current_quantity.save();
                }
            }
            return ok("Quantity updated");
        }
    }