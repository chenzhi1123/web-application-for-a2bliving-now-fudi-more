package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.socketControl.ChatRoom;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import views.html.chatRoom;
//import flexjson.JSONDeserializer;

public class ChatRoomAPI extends Controller {

	public static Result chatRoomJs(String username) {
		return ok(views.js.chatRoom.render(username));
	}

	/**
	 * Display the chat room.
	 */
	public static Result chatRoom(String username) {
		if(username == null || username.trim().equals("")) {
			flash("error", "Please choose a valid username.");
			return redirect(routes.Application.index());
		}
		return ok(chatRoom.render(username));
	}

	/**
	 * Handle the chat websocket.
	 */
	public static WebSocket<JsonNode> chat(final String username) {
		return new WebSocket<JsonNode>() {

			// Called when the Websocket Handshake is done.
			public void onReady(In<JsonNode> in, Out<JsonNode> out){
				// Join the chat room.
				try {
					ChatRoom.join(username, in, out);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};
	}
}