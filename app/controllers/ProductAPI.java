/*
 * Copyright (c) 2017. Zhi Chen.
 * ProductAPI is for SQL querying all the records related to products.
 * Price, quantity, attribute, image id or button index, etc.
 * Cache must be used on the server or data I/O will be overloading.
 * When adding something into cache, firstly and safely should initiate
 * an object, later that will be easier to retrieve it.
 */

package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.SqlUpdate;
import com.google.gson.Gson;
import flexjson.JSONSerializer;
import models.database.*;
import models.sqlContainer.*;
import models.template.ReturnProduct;
import play.cache.Cache;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.productSalesCheck.*;
import views.html.sellOrNot;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static controllers.AttributeAPI.getProductAttributes;
import static controllers.AttributeAPI.setNewCache;
import static parsers.JsonParser.renderCategories;
import static util.StringResource.EMAIL_2;

public class ProductAPI extends Controller {
    /**
     * Get all products of one category, SQL query from database, using Join among multiple tables
     * @param categoryID
     * @see Product
     * @return List<Product>
     * */
    public static Result getProduct(long categoryID) {
        System.out.println("Start-------" + categoryID);
        //check if target data is in the cache
        if (Cache.get("allProductsOfCategory"+ categoryID) == null){
        String sql =
                "SELECT distinct ps_category_product.id_product, ps_product.id_supplier, ps_product.reference, ps_product.upc, ps_product.id_tax_rules_group, ps_tax.id_tax, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.description_short, ps_product_lang.link_rewrite, ps_product_lang.name, ps_product_lang.available_now, ps_product_lang.available_later, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_specific_price.reduction_tax, ps_image.id_image, ps_stock_available.quantity\n" +
                        "\n" +
                        "FROM ps_category_product\n" +
                        "\n" +
                        "LEFT JOIN ps_product\n" +
                        "ON ps_category_product.id_product = ps_product.id_product\n" +
                        "\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_product.id_product\n" +
                        "\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        " AND NOW() > ps_specific_price.from\n" +
                        " AND NOW() < ps_specific_price.to\n" +
                        "\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_category_product.id_product\n" +
                        " AND ps_image.cover = 1\n" +
                        "\n" +
                        "WHERE ps_category_product.id_category =" + categoryID +
                        " AND ps_product.active = 1\n" +
                        " AND ps_stock_available.quantity > 0\n" +
                        " AND ps_stock_available.id_product_attribute = 0\n" +
                        "\n" +
                        "Order BY ps_category_product.position\n";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category_product.id_product", "id_product")
                        .columnMapping("ps_product.id_supplier", "id_supplier")
                        .columnMapping("ps_product.reference", "reference")
                        .columnMapping("ps_product.upc", "upc")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_tax.id_tax", "tax_id")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "rate")     //transfer the tax rate from integers to float numbers
                        .columnMapping("ps_product.price", "price")
//                        .columnMapping("ps_product_lang.id_shop", "id_shop")
//                        .columnMapping("ps_product_lang.description", "description")     Description data is too big for SQL query along with this query
                        .columnMapping("ps_product_lang.description_short", "description_short")
                        .columnMapping("ps_product_lang.link_rewrite", "link_rewrite")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_product_lang.available_now", "available_now")
                        .columnMapping("ps_product_lang.available_later", "available_later")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_specific_price.reduction_tax", "reduction_tax")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity") //// TODO: 2017/6/8  
                        .create();

        com.avaje.ebean.Query<Product> query = Ebean.find(Product.class);
        query.setRawSql(rawSql);
        List<Product> list = query.findList();
        ArrayList<Product> total_products = new ArrayList<Product>();
        total_products.addAll(list);
        // Iterate in the List and remove those products of which the quantity is 0
        for (Iterator<Product> products = total_products.iterator();products.hasNext();) {
            Product p =products.next();
            String reduction_type = p.reduction_type;
            BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
            if( reduction_type !=null && reduction_type.contains("percentage")){
                reduction_amount = p.reduction;
            }
            int product_id = p.id_product;
            ArrayList<ProductAttribute> new_attributes = getProductAttributes(product_id,reduction_amount);   //use thread
            p.attributes = new_attributes;
            if(p.rate == null){
                p.rate = BigDecimal.valueOf(1.000);
            }

        }
        Products p = new Products(total_products);                                  //Use an object to contain everything then put it into cache
//      Add the list of products into cache, expiry time is 1 day
//      Cache.set("allProductsOfCategory"+ categoryID, total_products, 60 * 60);
        Cache.set("allProductsOfCategory"+ categoryID, p, 60 * 60 * 24);
        System.out.println("this is not cache-------------" + categoryID);
        return ok(renderCategories(total_products));
        }else {
//        ArrayList<Product> all_products = (ArrayList<Product>) Cache.get("allProductsOfCategory"+categoryID);
          Products cache_List = (Products)Cache.get("allProductsOfCategory"+categoryID);  //new line to retrieve records from cache
          ArrayList<Product> all_products = cache_List.products;
            for (Iterator<Product> products = all_products.iterator();products.hasNext();) {
                Product p =products.next();
                String reduction_type = p.reduction_type;
                BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
                if( reduction_type !=null && reduction_type.contains("percentage")){
                    reduction_amount = p.reduction;
                }
                int product_id = p.id_product;
                ArrayList<ProductAttribute> new_attributes = getProductAttributes(product_id, reduction_amount);
                int product_quantity = getQuantity(product_id); //获取当前最新数量
                if (product_quantity >0){               //remove the product of which the quantity is 0
                    p.attributes = new_attributes;
                    p.quantity = product_quantity;
                    if(!(p.rate != null)){
                        p.rate = BigDecimal.valueOf(1.000);
                    }
                }
                else {
                    products.remove();
                }
            }
        System.out.println("Cache used");
        return ok(renderCategories(all_products));
        }
    }

    /**
     * Get descriptions of one product. The result will be "heavy" string data.
     * @param productID
     * @see Description
     * */
    public static Result getDescription(long productID) {

        if (Cache.get("DescriptionOfTheProduct"+ productID) == null){
            String sql =
                    "select ps_product_lang.description\n" +
                            "\n" +
                            "from ps_product_lang\n" +
                            "\n" +
                            "where id_product =" + productID;

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_product_lang.description", "product_description")
                            .create();

            com.avaje.ebean.Query<Description> query = Ebean.find(Description.class);
            query.setRawSql(rawSql);
//        query.setParameter
            List<Description> list = query.findList();
            ArrayList<Description> descriptions = new ArrayList<Description>();
            descriptions.addAll(list);
            if (descriptions.size() >0){
                Description description_product = descriptions.get(0);

                Cache.set("DescriptionOfTheProduct"+ productID, description_product, 60 * 60 * 24);

                return ok(renderCategories(description_product));

            }else {
                return notFound("no such product");
            }
        }else {
//            ArrayList<Description> descriptions = (ArrayList<Description>) Cache.get("DescriptionOfTheProduct"+ productID);
            Description description = (Description)Cache.get("DescriptionOfTheProduct"+ productID);

            return ok(renderCategories(description));
        }
    }

   /**Get quantity of the product
    * @param productID
    * @return an integer
    * @see ProductQuantity
    * */
    public static int getQuantity(long productID) {
         String sql =
                "select ps_stock_available.quantity\n" +
                        "\n" +
                        "from ps_stock_available\n" +
                        "\n" +
                        " where ps_stock_available.id_product = \n" + productID +
                        " and ps_stock_available.id_product_attribute = 0" ;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .create();

        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        List<ProductQuantity> list = query.findList();
        ArrayList<ProductQuantity> quantities = new ArrayList<ProductQuantity>();
        quantities.addAll(list);
        int quantity;
        if(quantities.size() > 0){
            quantity = quantities.get(0).quantity;
        }else {
            quantity = 0;
        }
        return quantity;
    }

    /**Get the wholesale price of a product
     * @param productID
     * @see ProductWholesale
     * */
    public static Result getWholePrice(int productID) {
        String sql =
                "select ps_product.wholesale_price\n" +
                        "\n" +
                        "from ps_product\n" +
                        "\n" +
                        " where ps_product.id_product = \n" + productID;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product.wholesale_price", "whole_sale")
                        .create();

        com.avaje.ebean.Query<ProductWholesale> query = Ebean.find(ProductWholesale.class);
        query.setRawSql(rawSql);
        List<ProductWholesale> list = query.findList();
        ArrayList<ProductWholesale> whole_prices = new ArrayList<ProductWholesale>();
        whole_prices.addAll(list);                  //list to Array
        BigDecimal wholesale_price = whole_prices.get(0).whole_sale;
        return ok(renderCategories(wholesale_price));
    }


    /**
     * Shop products search with key words, the minimum word length is 3 letters
     * @param name
     * @return ArrayList<Product>
     * @see Product
     * */
    public static Result searchProduct(String name) {
        if (name.length() < 3) {
            return status(403,"Not enough letters");
        }else {
            name = name.replaceAll("'s", "");
            name = name.replaceAll("[^A-Za-z0-9 ]", ""); //保留字符，去除符号
            name = name.replaceAll(" ", "%");
            String sql =
                    "SELECT distinct ps_category_product.id_product, ps_product.id_supplier, ps_product.reference, ps_product.upc, ps_product.id_tax_rules_group, ps_tax.id_tax, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.description_short,\n" +
                            "ps_product_lang.link_rewrite, ps_product_lang.name, ps_product_lang.available_now, ps_product_lang.available_later, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_specific_price.reduction_tax, ps_image.id_image\n" +
                            "\n" +
                            "FROM ps_category_product\n" +
                            "\n" +
                            "LEFT JOIN ps_product\n" +
                            "ON ps_category_product.id_product = ps_product.id_product\n" +
                            "\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_product.id_product = ps_stock_available.id_product\n" +
                            "\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            " AND NOW() > ps_specific_price.from\n" +
                            " AND NOW() < ps_specific_price.to\n" +
                            "\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_category_product.id_product\n" +
                            "AND ps_image.cover = 1\n" +
                            "\n" +
                            "WHERE ps_product_lang.name like" + "'%" + name + "%'\n" +
                            "AND ps_product.active = 1\n" +
                            "AND ps_product.upc != 1\n" +
                            "AND ps_stock_available.id_product_attribute = 0\n" +
                            "AND ps_stock_available.quantity > 0\n" +
                            "Order BY ps_product.id_product";

            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_category_product.id_product", "id_product")
                            .columnMapping("ps_product.id_supplier", "id_supplier")
                            .columnMapping("ps_product.reference", "reference")
                            .columnMapping("ps_product.upc", "upc")
                            .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_tax.id_tax", "tax_id")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "rate")
                            .columnMapping("ps_product.price", "price")
//                        .columnMapping("ps_product_lang.id_shop", "id_shop")
//                    .    columnMapping("ps_product_lang.description", "description")   // must delete it because of the heavy data
                            .columnMapping("ps_product_lang.description_short", "description_short")
                            .columnMapping("ps_product_lang.link_rewrite", "link_rewrite")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_product_lang.available_now", "available_now")
                            .columnMapping("ps_product_lang.available_later", "available_later")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_specific_price.reduction_tax", "reduction_tax")
                            .columnMapping("ps_image.id_image", "id_image")
                            .create();

            com.avaje.ebean.Query<Product> query = Ebean.find(Product.class);
            query.setRawSql(rawSql);
            List<Product> list = query.findList();
            Product[] all_products = new Product[list.size()];
            int number = list.size();
            list.toArray(all_products);
            for (int i = 0; i < number; i++) {
                if (all_products[i].rate == null) {
                    all_products[i].rate = BigDecimal.valueOf(1.000);
                }
                int product_id = all_products[i].id_product;
                String reduction_type = all_products[i].reduction_type;
                BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
                if( reduction_type !=null && reduction_type.contains("percentage")){
                    reduction_amount = all_products[i].reduction;
                }
                all_products[i].attributes = getProductAttributes(product_id, reduction_amount);
                all_products[i].quantity = getQuantity(product_id);
            }
            return ok(renderCategories(all_products));
        }
    }

    public static Result updateMealDeals(int deal_shop_product_id, int if_available) {
          int available_number = 0;
          if(if_available !=0){
              available_number =999;
          }
          ArrayList<Integer> id_product_attributes = DealProductsTable.findByShopProduct(deal_shop_product_id);
         if(id_product_attributes.size() !=0){  //循环更新数量
             try {
                 for (Integer this_one : id_product_attributes) {
                     String dml = "UPDATE ps_stock_available\n" +
                             "SET quantity = :quantity\n" +
                             "WHERE id_product_attribute = :id_product_attribute";
                     SqlUpdate update = Ebean.createSqlUpdate(dml)
                             .setParameter("quantity", available_number)
                             .setParameter("id_product_attribute", this_one);
                     update.execute();
                 }
             }catch (Exception e){
                 return badRequest("Something is wrong in the Stock Available Table");
             }
        //check if target data is in the cache
        String sql =
                    "SELECT distinct ps_category_product.id_product, ps_product.id_supplier, ps_product.reference, ps_product.upc, ps_product.id_tax_rules_group, ps_tax.id_tax, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.description_short, ps_product_lang.link_rewrite, ps_product_lang.name, ps_product_lang.available_now, ps_product_lang.available_later, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_specific_price.reduction_tax, ps_image.id_image, ps_stock_available.quantity\n" +
                            "\n" +
                            "FROM ps_category_product\n" +
                            "\n" +
                            "LEFT JOIN ps_product\n" +
                            "ON ps_category_product.id_product = ps_product.id_product\n" +
                            "\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_stock_available.id_product = ps_product.id_product\n" +
                            "\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            " AND NOW() > ps_specific_price.from\n" +
                            " AND NOW() < ps_specific_price.to\n" +
                            "\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_category_product.id_product\n" +
                            " AND ps_image.cover = 1\n" +
                            "\n" +
                            "WHERE ps_category_product.id_category = 522\n"+
                            " AND ps_product.active = 1\n" +
                            " AND ps_stock_available.quantity > 0\n" +
                            " AND ps_stock_available.id_product_attribute = 0\n" +
                            "\n" +
                            "Order BY ps_category_product.position\n";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_category_product.id_product", "id_product")
                            .columnMapping("ps_product.id_supplier", "id_supplier")
                            .columnMapping("ps_product.reference", "reference")
                            .columnMapping("ps_product.upc", "upc")
                            .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_tax.id_tax", "tax_id")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "rate")     //transfer the tax rate from integers to float numbers
                            .columnMapping("ps_product.price", "price")
//                        .columnMapping("ps_product_lang.id_shop", "id_shop")
//                        .columnMapping("ps_product_lang.description", "description")     Description data is too big for SQL query along with this query
                            .columnMapping("ps_product_lang.description_short", "description_short")
                            .columnMapping("ps_product_lang.link_rewrite", "link_rewrite")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_product_lang.available_now", "available_now")
                            .columnMapping("ps_product_lang.available_later", "available_later")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_specific_price.reduction_tax", "reduction_tax")
                            .columnMapping("ps_image.id_image", "id_image")
                            .columnMapping("ps_stock_available.quantity", "quantity")
                            .create();
            com.avaje.ebean.Query<Product> query = Ebean.find(Product.class);
            query.setRawSql(rawSql);
            List<Product> list = query.findList();
            ArrayList<Product> total_products = new ArrayList<Product>();
            total_products.addAll(list);
            // Iterate in the List and remove those products of which the quantity is 0
            for (Iterator<Product> products = total_products.iterator();products.hasNext();) {
                Product p =products.next();
                String reduction_type = p.reduction_type;
                BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
                if( reduction_type !=null && reduction_type.contains("percentage")){
                    reduction_amount = p.reduction;
                }
                int product_id = p.id_product;
                ArrayList<ProductAttribute> new_attributes = setNewCache(product_id,reduction_amount);   //use thread
                p.attributes = new_attributes;
                if(p.rate == null){
                    p.rate = BigDecimal.valueOf(1.000);
                }
            }
            Products p = new Products(total_products);                                  //Use an object to contain everything then put it into cache
//      Add the list of products into cache, expiry time is 1 day
//      Cache.set("allProductsOfCategory"+ categoryID, total_products, 60 * 60);
            Cache.set("allProductsOfCategory"+ 522, p, 60 * 60 * 24);
            System.out.println("generate new cache");
            return ok("successfully cleared");
         }else {
             return badRequest("This shop product is not mealDeal product");
         }
    }

    public static void reGenerateCache(long categoryID) {
        String sql =
                "SELECT distinct ps_category_product.id_product, ps_product.id_supplier, ps_product.reference, ps_product.upc, ps_product.id_tax_rules_group, ps_tax.id_tax, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.price, ps_product_lang.description_short, ps_product_lang.link_rewrite, ps_product_lang.name, ps_product_lang.available_now, ps_product_lang.available_later, ps_specific_price.reduction, ps_specific_price.reduction_type, ps_specific_price.reduction_tax, ps_image.id_image, ps_stock_available.quantity\n" +
                        "\n" +
                        "FROM ps_category_product\n" +
                        "\n" +
                        "LEFT JOIN ps_product\n" +
                        "ON ps_category_product.id_product = ps_product.id_product\n" +
                        "\n" +
                        "LEFT JOIN ps_stock_available\n" +
                        "ON ps_stock_available.id_product = ps_product.id_product\n" +
                        "\n" +
                        "LEFT JOIN  ps_product_lang\n" +
                        "ON ps_product_lang.id_product = ps_product.id_product\n" +
                        "AND ps_product_lang.id_shop = 1\n" +
                        "\n" +
                        "LEFT JOIN ps_tax_rule\n" +
                        "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                        "\n" +
                        "LEFT JOIN ps_tax\n" +
                        "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                        "\n" +
                        "LEFT JOIN ps_specific_price\n" +
                        "ON ps_product.id_product = ps_specific_price.id_product\n" +
                        " AND NOW() > ps_specific_price.from\n" +
                        " AND NOW() < ps_specific_price.to\n" +
                        "\n" +
                        "LEFT JOIN ps_image\n" +
                        "ON ps_image.id_product = ps_category_product.id_product\n" +
                        " AND ps_image.cover = 1\n" +
                        "\n" +
                        "WHERE ps_category_product.id_category = 522\n"+
                        " AND ps_product.active = 1\n" +
                        " AND ps_stock_available.quantity > 0\n" +
                        " AND ps_stock_available.id_product_attribute = 0\n" +
                        "\n" +
                        "Order BY ps_category_product.position\n";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_category_product.id_product", "id_product")
                        .columnMapping("ps_product.id_supplier", "id_supplier")
                        .columnMapping("ps_product.reference", "reference")
                        .columnMapping("ps_product.upc", "upc")
                        .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                        .columnMapping("ps_tax.id_tax", "tax_id")
                        .columnMapping("((ps_tax.rate+100)*0.01)", "rate")     //transfer the tax rate from integers to float numbers
                        .columnMapping("ps_product.price", "price")
//                        .columnMapping("ps_product_lang.id_shop", "id_shop")
//                        .columnMapping("ps_product_lang.description", "description")     Description data is too big for SQL query along with this query
                        .columnMapping("ps_product_lang.description_short", "description_short")
                        .columnMapping("ps_product_lang.link_rewrite", "link_rewrite")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_product_lang.available_now", "available_now")
                        .columnMapping("ps_product_lang.available_later", "available_later")
                        .columnMapping("ps_specific_price.reduction", "reduction")
                        .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                        .columnMapping("ps_specific_price.reduction_tax", "reduction_tax")
                        .columnMapping("ps_image.id_image", "id_image")
                        .columnMapping("ps_stock_available.quantity", "quantity")
                        .create();

        com.avaje.ebean.Query<Product> query = Ebean.find(Product.class);
        query.setRawSql(rawSql);
        List<Product> list = query.findList();
        ArrayList<Product> total_products = new ArrayList<Product>();
        total_products.addAll(list);
        // Iterate in the List and remove those products of which the quantity is 0
        for (Iterator<Product> products = total_products.iterator(); products.hasNext(); ) {
            Product p = products.next();
            String reduction_type = p.reduction_type;
            BigDecimal reduction_amount = BigDecimal.valueOf(0.0);
            if (reduction_type != null && reduction_type.contains("percentage")) {
                reduction_amount = p.reduction;
            }
            int product_id = p.id_product;
            ArrayList<ProductAttribute> new_attributes = setNewCache(product_id, reduction_amount);   //use thread
                p.attributes = new_attributes;
                if (p.rate == null) {
                    p.rate = BigDecimal.valueOf(1.000);
                }

        }
        Products p = new Products(total_products);                                  //Use an object to contain everything then put it into cache
//      Add the list of products into cache, expiry time is 1 day
//      Cache.set("allProductsOfCategory"+ categoryID, total_products, 60 * 60);
        Cache.set("allProductsOfCategory" + categoryID, p, 60 * 60 * 24);
    }

    public static ProductQuantity get_image_id(int target_product_id){
        String sql =
                "select id_image \n" +
                        "from ps_image\n" +
                        "where cover = 1\n" +
                        "and id_product = " + target_product_id;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("id_image", "quantity")
                        .create();

        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
        List<ProductQuantity> list = query.findList();
        ProductQuantity image_number = new ProductQuantity(0);
        if(list.size()>0){
          image_number = list.get(0);
        }
        return image_number;
    }

    public static class ProductSalesDate{
        public int id_product;
        public Date startDate;
        public Date endDate;

        public int getId_product() {
            return id_product;
        }

        public void setId_product(int id_product) {
            this.id_product = id_product;
        }

        public Date getStartDate() {
            return startDate;
        }

        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        public ProductSalesDate() {
        }

        public ProductSalesDate(int id_product, Date startDate, Date endDate) {
            this.id_product = id_product;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }

    /**
     * 统计在某个时间段内，一件产品的总销售情况，打开该页面
     * @param id_product
     * */
    public static Result checkSalesPage(int id_product){
        HashMap<String, String> dataContainer = new HashMap<String, String>();
        dataContainer.put("id_product",String.valueOf(id_product));
        Form<ProductSalesDate> dateForm = Form.form(ProductSalesDate.class).bind(dataContainer);
        if(id_product!=0) {
            //筛选出这件产品信息
            String sql =
                    "SELECT distinct ps_category_product.id_product, ps_product.id_tax_rules_group, ps_product.active, ((ps_tax.rate+100)*0.01) AS tax_rate, ps_product.ean13, ps_product.price, ps_product_lang.name,ps_specific_price.reduction, ps_specific_price.reduction_type, ps_image.id_image, ps_stock_available.quantity, ps_category_lang.name, ps_product_expiry.expiry_date\n" +
                            "FROM ps_category_product\n" +
                            "LEFT JOIN ps_product\n" +
                            "ON ps_category_product.id_product = ps_product.id_product\n" +
                            "LEFT JOIN  ps_product_lang\n" +
                            "ON ps_product_lang.id_product = ps_product.id_product\n" +
                            "AND ps_product_lang.id_shop = 1\n" +
                            "LEFT JOIN ps_tax_rule\n" +
                            "ON ps_product.id_tax_rules_group = ps_tax_rule.id_tax_rules_group\n" +
                            "LEFT JOIN ps_tax\n" +
                            "ON ps_tax_rule.id_tax = ps_tax.id_tax\n" +
                            "LEFT JOIN ps_specific_price\n" +
                            "ON ps_product.id_product = ps_specific_price.id_product\n" +
                            "AND NOW() > ps_specific_price.from\n" +
                            "AND NOW() < ps_specific_price.to\n" +
                            "LEFT JOIN ps_image\n" +
                            "ON ps_image.id_product = ps_category_product.id_product\n" +
                            "AND ps_image.cover = 1\n" +
                            "LEFT JOIN ps_stock_available\n" +
                            "ON ps_stock_available.id_product = ps_category_product.id_product\n" +
                            "AND ps_stock_available.id_product_attribute = 0 \n" +
                            "LEFT JOIN ps_category_lang\n" +
                            "ON ps_category_product.id_category = ps_category_lang.id_category\n" +
                            "LEFT JOIN ps_product_expiry\n" +
                            "ON ps_product.id_product = ps_product_expiry.id_product\n" +
                            "WHERE ps_product.upc != 1 \n" +
                            "AND ps_product.id_product =" + id_product + "\n";
            RawSql rawSql =
                    RawSqlBuilder
                            .parse(sql)
                            .columnMapping("ps_category_product.id_product", "id_product")
                            .columnMapping("ps_product.id_tax_rules_group", "id_tax_rules_group")
                            .columnMapping("ps_product.active", "if_valid")
                            .columnMapping("((ps_tax.rate+100)*0.01)", "tax_rate")
                            .columnMapping("ps_product.ean13", "ean")
                            .columnMapping("ps_product.price", "price")
                            .columnMapping("ps_product_lang.name", "name")
                            .columnMapping("ps_specific_price.reduction", "reduction")
                            .columnMapping("ps_specific_price.reduction_type", "reduction_type")
                            .columnMapping("ps_image.id_image", "id_image")
                            .columnMapping("ps_stock_available.quantity", "quantity")
                            .columnMapping("ps_category_lang.name", "category_name")
                            .columnMapping("ps_product_expiry.expiry_date", "expiry_date")
                            .create();
            com.avaje.ebean.Query<StockManagementProduct> query = Ebean.find(StockManagementProduct.class);
            query.setRawSql(rawSql);
            List<StockManagementProduct> list = query.findList();
            int size = list.size();
            String category_name = "";
            if (size > 1) {
                for (int i = 0; i < size; i++) {
                    category_name = category_name + list.get(i).category_name + "||";
                }
            }
            StockManagementProduct product_info;
            if (size > 0) {
                product_info = list.get(0);
            } else {
                product_info = new StockManagementProduct();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if (product_info.expiry_date == null) {
                product_info.expiry_date = new Date(0);
            }
            Date future_date = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS)); //当前时间的一个月以后
            if (product_info.expiry_date.before(future_date)) {
                product_info.if_expiring = 1;   //加入标记。即将过期
            }
            product_info.expiryDate = sdf.format(product_info.expiry_date);
            product_info.category_name = category_name;
            ProductLocation location = ProductLocation.findByProduct(id_product);
            String product_location;
            if (location == null) {
                product_location = "";
            } else {
                product_location = location.zone + "-" + location.part + location.position_number;
            }
            return ok(searchSalesPage.render(dateForm, product_info, product_location)); //ignore
        }else {
            return ok(searchSalesPage.render(dateForm, new StockManagementProduct(), ""));
        }
    }

    /**
    * 显示一件产品在某个特殊时间段之内的销售情况 （多少件，总销售额）
    */
    public static Result totalSoldSales(){
        //解析网络请求里面传来的时间间隔
        Form<ProductSalesDate> productSalesDateForm = Form.form(ProductSalesDate.class).bindFromRequest(); //get a new record form from HTTP
//        DynamicForm newStockForm = Form.form().bindFromRequest();  //receive
        if(productSalesDateForm.hasErrors()){
            return badRequest("Data format is not correct");
        } else{
            //得到一堆参数
            ProductSalesDate new_record = productSalesDateForm.get(); //得到对象
            int product_id = new_record.id_product;
            Date start_date = new_record.startDate;
            Date end_date = new_record.endDate;
            if (start_date == null || end_date ==null){
                return badRequest("You have to put both start time and end time!");
            }
            if(start_date.after(end_date)){
                return badRequest("Start Time cannot be later than End time!");
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String search_start = sdf.format(start_date);
            String search_end = sdf.format(end_date);
            //开始查看售出数量
            //1. 作为普通产品的数量 （orders, order_details, order）
           ProductQuantity sold_as_common = getSalesQuantity(product_id,search_start,search_end);
           int common_sales_quantity = 0;
           if(sold_as_common !=null){
               common_sales_quantity += sold_as_common.quantity; //计算出作为普通产品，在搜索时间范围之内的总销量
           }
            //2. MealDeal 产品数量（如果这个在meal_deal 的附属产品里面）
            ProductQuantity sold_as_mealProduct = getMealDealSales(product_id,search_start,search_end);
           int meal_product_sales = 0;
           if(sold_as_common !=null){
               meal_product_sales +=sold_as_mealProduct.quantity;
           }
            //3. Bundle Sale 产品数量（如果这个是bundle sale 中的原产品）
            //3.1 先找到组合销售规则，获取实际产品ID，获取组合销售规则的结束时间（end_date）
            int bundle_sale_quantity = 0;
            List<BuyOneGetOne> if_exist_rules = BuyOneGetOne.findByDateProduct(product_id,end_date);
           if(if_exist_rules.size()>0){
               //3.2 遍历目标数组
               for(BuyOneGetOne each_one: if_exist_rules ){
                        int id_sales_product = each_one.id_onsale_product; //根据原产品，找到被卖出的买一送一产品
                        int basic_unit = each_one.basic_unit;
                        int bundle_number = each_one.bundle_number;
                        Date bundle_end_date = each_one.end_date;
                        Date bundle_start_date = each_one.start_date;
                        Date final_end_date;
                        Date final_start_date;
                   //3.3 每个循环体内部，组合销售规则结束时间 && 搜索结束时间做比较，哪个小取那个值作为搜索结束时间
                   if(bundle_start_date.after(start_date)){ //开始时间，哪个大 取哪个值
                        final_start_date = bundle_start_date;
                   }else {
                        final_start_date = start_date;
                   }
                   if(bundle_end_date.after(end_date)){  //结束时间，哪个小取哪个
                        final_end_date = end_date;
                   }else {
                        final_end_date = bundle_end_date;
                   }
                        String final_start = sdf.format(final_start_date);
                        String final_end = sdf.format(final_end_date);
                   //3.4 查找实际销售产品的销量，用来累加到总销量里面
                   ProductQuantity sold_as_bundle = getSalesQuantity(id_sales_product,final_start,final_end);
                   if(sold_as_bundle !=null){
                        int sold_bundle_qty = sold_as_bundle.quantity;
                        int real_sold_qty = (sold_bundle_qty*bundle_number)/basic_unit;
                       bundle_sale_quantity += real_sold_qty;
                   }
               }
           }
           int total_sold_quantity = common_sales_quantity + meal_product_sales + bundle_sale_quantity;
            //4. 进货多少个（（上货记录）包括之前剩下的产品）
            List<StockProduct> add_history = StockProduct.findAddRecord(product_id,start_date,end_date);
            //5. 被扔掉多少个 （丢货记录）
            List<StockAbandon> abandon_history = StockAbandon.findByProductDate(product_id,start_date,end_date);
            int id_product_img = get_image_id(product_id).quantity;
            HashMap<String, String> dataContainer = new HashMap<String, String>();
            dataContainer.put("id_product",String.valueOf(product_id));
            dataContainer.put("startDate",search_start);
            dataContainer.put("endDate",search_end);
            Form<ProductSalesDate> seachDataForm = Form.form(ProductSalesDate.class).bind(dataContainer);
            //找一个容器来装数据
            ReturnProduct data_container = new ReturnProduct(
                    id_product_img,
                    String.valueOf(common_sales_quantity),
                    String.valueOf(meal_product_sales),
                    String.valueOf(bundle_sale_quantity),
                    total_sold_quantity,
                    BigDecimal.valueOf(0.000000)
            );
            String product_name = getProductName(product_id);
            return ok(productHistorySalesPage.render(seachDataForm,data_container,add_history,abandon_history,product_name));
        }
    }

    //根据时间查询一件产品的销量
    private static ProductQuantity getSalesQuantity(int product_id, String search_start, String search_end) {
        ProductQuantity sold_quantity;
        try{
        String sql =
                "select sum(ps_order_detail.product_quantity)\n" +
                        "from ps_orders, ps_order_detail\n" +
                        "where ps_orders.id_order = ps_order_detail.id_order\n" +
                        "and ps_orders.valid = 1\n" +
                        "and ps_orders.invoice_date >" + "'" + search_start +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + search_end +"'" +"\n"+
                        "and ps_order_detail.product_id = " + product_id;
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(ps_order_detail.product_quantity)", "quantity")
                        .create();
        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
            sold_quantity = query.findUnique();
            if(sold_quantity ==null || sold_quantity.quantity ==null){
                sold_quantity = new ProductQuantity(0);
            }
        }catch (Exception e){
            sold_quantity = new ProductQuantity(0);
        }
        return sold_quantity;
    }

    //根据时间查询Meal Deal 里面一件产品的销量
    private static ProductQuantity getMealDealSales(int product_id, String search_start, String search_end) {
        ProductQuantity sold_quantity;
        try{
        String sql =
                "select sum(ps_order_detail.product_quantity)\n" +
                        "from ps_orders, ps_order_detail, ps_deal_sale_table\n" +
                        "where ps_orders.id_order = ps_order_detail.id_order\n" +
                        "and ps_orders.invoice_date >" + "'" + search_start +"'" +"\n"+
                        "and ps_orders.invoice_date <" + "'" + search_end +"'" +"\n"+
                        "and ps_orders.valid = 1\n" +
                        "and ps_order_detail.product_id = ps_deal_sale_table.deal_product_id\n" +
                        "and  ps_order_detail.product_name like CONCAT('%', ps_deal_sale_table.key_word, '%')\n" +
                        "and ps_deal_sale_table.id_product_shop = " + product_id;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(ps_order_detail.product_quantity)", "quantity")
                        .create();
        com.avaje.ebean.Query<ProductQuantity> query = Ebean.find(ProductQuantity.class);
        query.setRawSql(rawSql);
            sold_quantity = query.findUnique();
            if(sold_quantity ==null || sold_quantity.quantity ==null){
                sold_quantity = new ProductQuantity(0);
            }
        }catch (Exception e){
            sold_quantity = new ProductQuantity(0);
        }
        return sold_quantity;
    }

    private static String getProductName(int product_id){
        String sql =
                "select name\n" +
                        "from ps_product_lang\n" +
                        "where id_product = " + product_id;

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("name", "product_description")
                        .create();
        com.avaje.ebean.Query<Description> query = Ebean.find(Description.class);
        query.setRawSql(rawSql);
        Description product_name = query.findUnique();
        String productName = "";
        if(product_name !=null){
            productName = product_name.product_description;
        }
        return productName;
    }


    public static Result sellOrNot(){
        String email = session("email");
        // if in session
        if (email != null && email.equals(EMAIL_2)) {
            SellingData sellingData=getNextProduct();
            return ok(sellOrNot.render(sellingData));
        }else {
            flash("error","please log in first");
            return redirect(routes.WebAPI.shopAssistantLogin());
        }

    }

    public static Result sellOrNotFeedback(){
        String email = session("email");
        // if in session
        if (email != null && email.equals(EMAIL_2)) {
            DynamicForm cashierForm = Form.form().bindFromRequest();  //receive
            boolean ifSell = Boolean.parseBoolean(cashierForm.get("ifSell"));
            int id_product = Integer.parseInt(cashierForm.get("id_product"));
            SellInformation sellingInformation=SellInformation.findById(id_product);
            if (sellingInformation==null){
                sellingInformation=new SellInformation(id_product,ifSell?1:0);
                sellingInformation.save();
                Gson gson =new Gson();
               // String gsonSting=gson.toJson(getNextProduct());
                SellingData sellingData=getNextProduct();
                return status(201,new JSONSerializer().serialize(sellingData));
            }else {
                sellingInformation.setSelling(ifSell?1:0);
                sellingInformation.save();
                Gson gson =new Gson();
                SellingData sellingData=getNextProduct();
                //String gsonSting=gson.toJson(sellingData);
                return status(201,new JSONSerializer().serialize(sellingData));
            }
        }else {
            return status(404,"Please Log in");
        }

    }

    private static SellingData getNextProduct(){

        String sql ="SELECT ps_product.id_product,ps_product_lang.name,ps_product_lang.link_rewrite,ps_product.ean13,ps_product_location.zone,ps_product_location.part,ps_product_location.position_number,ps_image.id_image \n" +
                "FROM `ps_product`\n" +
                "INNER JOIN  `ps_product_lang` ON `ps_product`.`id_product`=`ps_product_lang`.`id_product` \n" +
                "LEFT JOIN `ps_product_location` ON `ps_product`.`id_product`=`ps_product_location`.`id_product` \n" +
                "INNER JOIN `ps_image` ON `ps_product`.`id_product`=`ps_image`.`id_product` \n" +
                "where `ps_product`.`upc`!=1 \n" +
                "AND  `ps_product`. `active`=0 \n" +
                "AND `ps_product`.`id_product` NOT IN (SELECT `id_product` FROM `ps_sell_information`)  \n" +
                "LIMIT 1";

        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("ps_product.id_product", "id_product")
                        .columnMapping("ps_product_lang.name", "name")
                        .columnMapping("ps_product_lang.link_rewrite", "link_rewrite")
                        .columnMapping("ps_product.ean13", "ean13")
                        .columnMapping("ps_product_location.zone", "zone")
                        .columnMapping("ps_product_location.part", "part")
                        .columnMapping("ps_product_location.position_number", "position_number")
                        .columnMapping("ps_image.id_image", "id_image")
                        .create();
        com.avaje.ebean.Query<SellingData> query = Ebean.find(SellingData.class);
        query.setRawSql(rawSql);
        SellingData sellingInformation=query.findUnique();
       return sellingInformation;
    }

}
