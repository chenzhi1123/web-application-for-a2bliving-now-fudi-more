/*
 * Copyright (c) 2017. Zhi Chen.
 * CashierAPI is used for cashier to hae a summary of daily sales.
 * Also Cashier uses apis to add record of a driver's cash payment issues
 */

package controllers;


import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Transactional;
import models.database.*;
import models.sqlContainer.ReturnDriverOrder;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.cashierSummary;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static controllers.routes.WebAPI;
import static parsers.JsonParser.renderCategories;

public class CashierAPI extends Controller {
    /**
     * Calculate the total sales amount of a cashier in the last 14 hours
     * @param id_cashier
     * @return Total cash(product prices only), total order numbers
     * */
    public static TotalReturnDriverOrder getOrdersOfCashier(int id_cashier) {  //总价 cash, 只有cash 订单会存在 if_paid = 1, paid_cashier !=0
        String sql =
                "select sum(total_paid_real) as total_amount_sale, sum(total_shipping) as total_shipping_fee, count(*) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_drivers_orders.if_paid = 1\n" +
                        "\n" +
                        "and ps_drivers_orders.paid_cashier =" + id_cashier+ "\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(total_paid_real)", "total_sale_cash")
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(*)", "total_numberof_cash_order")
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
        List<TotalReturnDriverOrder> list = query.findList();
//        int size = list.size();
//        for (int i =0; i<size; i++){
//            list.get(i).total_sale_cash = list.get(i).total_sale_cash.subtract(list.get(i).total_shipping_online);
//        }
        ArrayList<TotalReturnDriverOrder> totalReturnDriverOrders = new ArrayList<TotalReturnDriverOrder>(); //空数组
        totalReturnDriverOrders.addAll(list);
        TotalReturnDriverOrder final_record = totalReturnDriverOrders.get(0);
        return final_record;
    }


    /**
     * Calculate the total sales amount of a cashier in the last 14 hours
     * @param id_cashier
     * @return Total cash(product prices only), total order numbers
     * */
    public static TotalReturnDriverOrder getExceptionOrders(int id_cashier) {  //网站使用 Marina 订单， cash_on_delivery
        String sql =
                "select sum(total_shipping) as total_shipping_fee, count(*) as number_of_order\n" +
                        "\n" +
                        "from ps_orders, ps_drivers_orders\n" +
                        "\n" +
                        "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                        "\n" +
                        "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                        "\n" +
                        "and ps_orders.valid = 1\n" +  //valid orders only
                        "\n" +
                        "and ps_orders.total_discounts = 2.000000\n" +  //Marina Park 如果使用两个voucher, 就会少算Marina 单
                        "\n" +
                        "and ps_orders.total_shipping = 2.000000\n" +
                        "\n" +
                        "and ps_drivers_orders.if_paid = 1\n" +
                        "\n" +
                        "and ps_drivers_orders.paid_cashier =" + id_cashier+ "\n" +
                        "\n" +
                        "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)";
        RawSql rawSql =
                RawSqlBuilder
                        .parse(sql)
                        .columnMapping("sum(total_shipping)", "total_shipping_online")
                        .columnMapping("count(*)", "marina_orders")    //特殊的Marina订单
                        .create();

        com.avaje.ebean.Query<TotalReturnDriverOrder> query = Ebean.find(TotalReturnDriverOrder.class);
        query.setRawSql(rawSql);
        TotalReturnDriverOrder exceptionOrders = query.findUnique();
        if (exceptionOrders==null || exceptionOrders.total_shipping_online ==null){
            exceptionOrders = new TotalReturnDriverOrder();
        }
        return exceptionOrders;
    }

    /**
     *Daily summary of a cashier (Cash received from the driver + delivery fee paid back to the driver)
     *@return CashierSummary receipt view page
     * */
    @Transactional
    public static Result totalOrdersOfCashier() {    //参数为 cashier_id, 动作为POST
        DynamicForm cashierForm = Form.form().bindFromRequest();  //receive
        String cashier_id = cashierForm.get("cashier_id");
        if(cashier_id ==null || cashier_id.equals("")){
            cashier_id = "0";
        }
        int id_cashier;

        try{
            id_cashier = Integer.valueOf(cashier_id);
        }catch (Exception e){
            id_cashier = 0;
        }
        if(id_cashier ==0){
            return redirect(WebAPI.counterLogin());
        }else{
            TotalReturnDriverOrder a = getOrdersOfCashier(id_cashier);
            TotalReturnDriverOrder b = getExceptionOrders(id_cashier); //多扣的运费
            Date current_time = new Date();
            SimpleDateFormat matter1 = new SimpleDateFormat("yyyy-MM-dd");
            String date = matter1.format(current_time);
            double total_delivery = 0;
            //get all the paid drivers from Cashier_driver relational table
            List<CashierDriver> paid_to_driver = CashierDriver.findByCashier(id_cashier);
            int size = paid_to_driver.size();
            ArrayList<DriverDailySummary> driver_summary = new ArrayList<>();
            BigDecimal total_driver_salary = BigDecimal.valueOf(0.00);
            BigDecimal total_till_money = BigDecimal.valueOf(0.00);
            BigDecimal total_voucher_value = BigDecimal.valueOf(0.00);
//            TaxSummary driver_salary_till = new TaxSummary();
            TotalReturnManagerOrder driver_salary_till = new TotalReturnManagerOrder();

            //get all the delivery fee paid to the driver
            if(size >0){
                for (int i = 0; i < size; i++) {
                    DriverDailySummary this_driver = DriverDailySummary.recentRecord(paid_to_driver.get(i).id_driver);
                    if(this_driver != null){ //如果这个司机是个已经结算下班的司机
                        driver_summary.add(this_driver);
                        total_driver_salary = total_driver_salary.add(this_driver.extra_cost).add(this_driver.total_wage);
                        total_till_money = total_till_money.add(this_driver.money_from_till);
                        total_voucher_value = total_voucher_value.add(this_driver.total_voucher_value);
                    }
                    total_delivery += paid_to_driver.get(i).delivery_fee_paid;
                }
                driver_salary_till = new TotalReturnManagerOrder(0,0,total_driver_salary,total_till_money,total_voucher_value,BigDecimal.valueOf(0), "");
            }
            //Paid-by-cash orders of a cashier
            if (a.total_sale_cash != null) {   // 如果有记录
                a.total_sale_cash = a.total_sale_cash.subtract(a.total_shipping_online).add(b.total_shipping_online);
                String sql =
                        "select ps_drivers_orders.order_number, ps_drivers_orders.id_order, ps_drivers_orders.driver_name, ps_drivers_orders.order_reference, ps_orders.id_address_delivery, ps_orders.payment, ps_orders.total_paid_real, ps_orders.total_discounts, ps_orders.total_shipping, ps_order_invoice.delivery_address\n" +
                                "\n" +
                                "from ps_orders, ps_drivers_orders, ps_order_invoice\n" +
                                "\n" +
                                "where ps_orders.id_order = ps_drivers_orders.id_order\n" +
                                "\n" +
                                "and ps_orders.id_order = ps_order_invoice.id_order\n" +
                                "\n" +
                                "and ps_orders.reference = ps_drivers_orders.order_reference\n" +
                                "\n" +
                                "and ps_orders.valid = 1\n" +  //valid orders only
                                "\n" +
                                "and ps_drivers_orders.if_paid = 1\n" +
                                "\n" +
                                "and ps_drivers_orders.paid_cashier =" + id_cashier + "\n" +
                                "\n" +
                                "and ps_drivers_orders.date_add > DATE_SUB(NOW(), INTERVAL 14 HOUR)\n" +
                                "\n" +
                                "order by id_order";
                RawSql rawSql =
                        RawSqlBuilder
                                .parse(sql)
                                .columnMapping("ps_drivers_orders.order_number", "order_number")
                                .columnMapping("ps_drivers_orders.id_order", "id_order")
                                .columnMapping("ps_drivers_orders.driver_name", "driver_name")
                                .columnMapping("ps_drivers_orders.order_reference", "order_reference")
                                .columnMapping("ps_orders.id_address_delivery", "id_address_delivery")
                                .columnMapping("ps_orders.payment", "payment")
                                .columnMapping("ps_orders.total_paid_real", "total_paid_real")
                                .columnMapping("ps_orders.total_discounts", "total_discounts")
                                .columnMapping("ps_orders.total_shipping", "total_shipping")
                                .columnMapping("ps_order_invoice.delivery_address", "delivery_address")
                                .create();
                com.avaje.ebean.Query<ReturnDriverOrder> query = Ebean.find(ReturnDriverOrder.class);
                query.setRawSql(rawSql);
                List<ReturnDriverOrder> list = query.findList();    //orders 一个数组
                int number = list.size();
                int charged_marina_orders = 0;
                for (int i = 0; i < number; i++) {
                    BigDecimal discount_amount = list.get(i).total_discounts;
                    BigDecimal shipping_amount = list.get(i).total_shipping;
                    if (shipping_amount.doubleValue() == 0.0 && !list.get(i).driver_name.contains("free")) {  //free driver 不需要算marina
                        charged_marina_orders += 1;
                    }

                    if (discount_amount.equals(shipping_amount) && discount_amount.doubleValue() == 2.0) {
                        list.get(i).total_paid_real = list.get(i).total_paid_real;
                        list.get(i).total_shipping = BigDecimal.valueOf(0.0);
                        if (!list.get(i).driver_name.contains("free")) {
                            charged_marina_orders += 1;
                        }
                    } else {
                        list.get(i).total_paid_real = list.get(i).total_paid_real.subtract(list.get(i).total_shipping);
                    }
                }
                ArrayList<ReturnDriverOrder> totalOrders = new ArrayList<ReturnDriverOrder>();
                totalOrders.addAll(list);
                a.driverOrders = totalOrders;
                //Cashier final cash in hand = all cash from drivers - delivery fee paid to drivers - Marina_charged_orders - Driver_salary - Total Voucher Value (代金券)+ Till(from driver)
                //前台最后的现金= 司机还回来的所有现金 - 付给司机的运费补贴 - Marina补贴 - 司机工资 - 总(代金券)+ Till(from driver)
                double final_cash = a.total_sale_cash.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() - total_delivery - charged_marina_orders - total_driver_salary.setScale(2, BigDecimal.ROUND_DOWN).doubleValue() - total_voucher_value.setScale(2, BigDecimal.ROUND_DOWN).doubleValue() + total_till_money.setScale(2, BigDecimal.ROUND_DOWN).doubleValue();// round the number to two digits
                DecimalFormat df = new DecimalFormat("#.##");
                final_cash = Double.valueOf(df.format(final_cash));
                //Add one record into the Daily Cashier cash summary table, one record for one cashier per day
                if(CashierCash.findByCashier(id_cashier) == null) {
                    CashierCash new_record = new CashierCash(id_cashier, final_cash, current_time);
                    new_record.save();
                }else {
                    CashierCash former_record = CashierCash.findByCashier(id_cashier);
                    former_record.final_cash = final_cash;
                    former_record.date_add = new Date();
                    former_record.save();
                }
                return ok(cashierSummary.render(a, id_cashier, date, paid_to_driver, total_delivery, final_cash, charged_marina_orders, driver_summary, driver_salary_till));
            }else{
                return notFound("Not a valid working cashier");
            }
        }

    }

   /**
    * Return all the delivery from online-payment orders back to the driver
    * Add one record into the Cashier_driver relational table. One driver can
    * only have one record per day for the final cash paid summary.
    * @param id_driver
    * */
    @Transactional
    public static Result payBackToDriver(int id_driver) throws Exception {
        DynamicForm cashier_driver = Form.form().bindFromRequest();
        int id_cashier = Integer.parseInt(cashier_driver.get("id_cashier"));
        String delivery_fee_paid = cashier_driver.get("total_shipping_online");
        if (delivery_fee_paid == null){
            delivery_fee_paid = "";
        }
        String name = cashier_driver.get("name");
        if (name == null){
            name = "";
        }
        double delivery_fee_online =  Double.parseDouble(delivery_fee_paid);
        int number_of_onlinepayment = Integer.parseInt(cashier_driver.get("number_of_onlinepayment"));
        Date date_add = new Date();
        CashierDriver a = CashierDriver.findByDriverID(id_driver);
        if (a != null){
            return status(409,"This driver was already paid");
        }else{
            //Add one record into the Cashier Driver Table
            CashierDriver cashierDriver = new CashierDriver(
                    id_cashier,
                    id_driver,
                    name,
                    delivery_fee_online,
                    number_of_onlinepayment,
                    date_add
            );
            cashierDriver.save();
         return ok("successfully paid to the driver fees");
        }
    }

    /**
     * Query one record of a driver, about how much delivery fee paid to that driver in total
     * @param id (driver_id)
     * @return record from database
     * */
    @Transactional
    public static Result getRecord(int id) throws Exception {
     CashierDriver one = CashierDriver.findByID(id);
            return ok(renderCategories(one));
    }

    // test CashierCash table
    public static Result seeCash(int id) throws Exception {
        CashierCash one = CashierCash.findById(id);
        return ok(renderCategories(one));
    }

   /**
    * Check if the driver is already paid by the cashier
    * For Pad Cashier App
    * @param id_driver
    * @return 0 or 1(status 200)
    * */
    public static Result checkIfPaid(int id_driver) throws Exception {
       CashierDriver recent_one = CashierDriver.findByDriverID(id_driver);
      if (recent_one != null){
          return ok("false");
      }
        else{
          return ok("true");
      }
    }

}
